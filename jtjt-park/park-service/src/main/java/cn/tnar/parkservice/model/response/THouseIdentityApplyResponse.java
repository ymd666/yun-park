package cn.tnar.parkservice.model.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zzb
 * @since 2018-12-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "THouseIdentityApply对象", description = "")
public class THouseIdentityApplyResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private int Id;

    @ApiModelProperty(value = "账号ID")
    private Long custId;

    @ApiModelProperty(value = "小区ID")
    private Long houseId;

    @ApiModelProperty(value = "小区名")
    private String houseName;

    @ApiModelProperty(value = "门牌号")
    private String houseNum;

    @ApiModelProperty(value = "账号姓名")
    private String custName;

    @ApiModelProperty(value = "账号手机号")
    private String custPhone;

    @ApiModelProperty(value = "车牌号")
    private String carId;

    @ApiModelProperty(value = "预留照片路径")
    private String photoKey;

    @ApiModelProperty(value = "审核结果 1:发起申请 2:审核不通过 3:打回修改  4:撤销认证 5:审核通过")
    private Integer applyResult;

    @ApiModelProperty(value = "原因")
    private String applyReason;

    @ApiModelProperty(value = "申请日期")
    private Long applyTime;

    @ApiModelProperty(value = "审核日期")
    private Long resultTime;

    @ApiModelProperty(value = "删除日期")
    private Long delTime;

    @ApiModelProperty(value = "删除状态 1为删除 0 为正常")
    private Integer delflag;

    @ApiModelProperty(value = "修改时间")
    private Long modifyTime;

    @ApiModelProperty(value = "审核人Id")
    private Long lookId;

    @ApiModelProperty(value = "对应优惠区域")
    private String regionCode;

    @ApiModelProperty(value = "对应优惠Id")
    private int cheapId;

    @ApiModelProperty(value = "缴费方式： 1 计时缴费 2趸交方式")
    private int payMode;

    private long activityStart;

    private long activityEnd;

    private String activityTime;

    private String regionName;

    private long outTime;

}
