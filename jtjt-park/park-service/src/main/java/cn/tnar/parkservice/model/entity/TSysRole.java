package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/10/9
 * @Description:
 */
@Data
@TableName("t_sys_role")
public class TSysRole {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Integer auditFlag;

    private Integer maxAssignMembers;

    private Long modifyOper;

    private Long modifyTime;

    private Long parentRoleId;

    private String remarks;

    private Integer roleAttr;

    private String roleCode;

    private Long roleId;

    private String roleName;

    private Long roleOwner;

    private Integer roleType;

    private Integer stat;

    private Integer roleLevel;
}
