package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2019-02-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TParkGate对象", description="")
public class TParkGate implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Integer nodeId;

    @ApiModelProperty(value = "NOT NULL")
    private String parkCode;

    private String regionCode;

    private String gateCode;

    @ApiModelProperty(value = " 数据字典：1入口 2出口")
    private Integer gateType;

    private String name;

    private Long lng;

    private Long lat;

    private String desciption;

    private Integer orderNum;

    @ApiModelProperty(value = "出入口简单描述用于语音播放（可能与出入口描述有）")
    private String descVoice;

    @ApiModelProperty(value = " yyyyMMddHHmmss")
    private Long addtime;

    private String photourl;

    private Integer ledType;

    @ApiModelProperty(value = "启用日期")
    private Integer enableDate;

    @ApiModelProperty(value = "删除日期")
    private Integer deleteDate;

    @ApiModelProperty(value = "设备状态，1: 启用 , 2:报废  ,  3: 故障")
    private Integer status;

    private Long updatetime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "进场区域")
    private String inRegionNo;

    @ApiModelProperty(value = "出场区域")
    private String outRegionNo;


}
