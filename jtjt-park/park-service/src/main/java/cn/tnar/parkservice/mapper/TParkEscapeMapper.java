package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.BadDebtExportDto;
import cn.tnar.parkservice.model.dto.OverduePaymentExportDto;
import cn.tnar.parkservice.model.dto.TParkEscapeDto;
import cn.tnar.parkservice.model.entity.TParkEscape;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author gl
 * @date 2018-10-22
 */
@Mapper
@QuerySupport
public interface TParkEscapeMapper extends BaseMapper<TParkEscape> {


//    @QuerySupport
//    @Select("SELECT\n" +
//            "\te.car_id,\n" +
//            "\tt.`name`,\n" +
//            "\te.parkname,\n" +
//            "\tpr.name as regionname,\n" +
//            "\te.intime,\n" +
//            "\te.outtime,\n" +
//            "\te.parktime,\n" +
//            "\te.parkamt,\n" +
//            "\te.chargeamt,\n" +
//            "\te.paystate \n" +
//            "FROM\n" +
//            "\tt_park_escape e\n" +
//            "\tLEFT JOIN t_park_toll t ON e.chargemanno = t.`no`\n" +
//          //  "\tLEFT JOIN t_acct_enterprise_info i ON e.agent_id = i.cust_id \n"+
//            "\tLEFT JOIN t_park_region  pr on pr.region_code = e.region_code \n")
//    List<TParkEscapeDto> pageTParkEscapeList( @Param(Constants.WRAPPER) Wrapper<TParkEscapeDto> wrapper);


    List<TParkEscapeDto> pageTParkEscapeList(@Param("startTime") Long startTime, @Param("endTime") Long endTime, @Param("carId") String carId, @Param("chargemanno") String chargemanno, @Param("parkCode") String parkCode);

    List<String> selectCustIds(@Param("custId") String custId);

    /**
     *  欠款补交记录查询【导出使用】
     * @param carId 车牌号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param chargemanno 收费员编码
     * @param parkCode 停车场编号
     * @return
     */
    List<OverduePaymentExportDto> getOverduePaymentList(@Param("carId") String carId, @Param("startTime") Long startTime, @Param("endTime") Long endTime, @Param("chargemanno") String chargemanno, @Param("parkCode") String parkCode);

    /**
     * 坏账记录查询【导出使用】
     * @param carId 车牌号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param chargemanno 收费员编码
     * @param parkCode 停车场编号
     * @return
     */
    List<BadDebtExportDto> getBadDebtList(@Param("carId") String carId, @Param("startTime") Long startTime, @Param("endTime") Long endTime, @Param("chargemanno") String chargemanno, @Param("parkCode") String parkCode);
}
