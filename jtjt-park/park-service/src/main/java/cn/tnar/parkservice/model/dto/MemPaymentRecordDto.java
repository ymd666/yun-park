package cn.tnar.parkservice.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: xinZhang
 * @Date: 2019/8/15
 * @Description:
 */
@Data
public class MemPaymentRecordDto {

    /**
     *录入时间查询-开始时间
     */
    private String beginTime;

    /**
     *录入时间查询-结束时间
     */
    private String endTime;

    /**
     *停车场id
     */
    private String parkCode;

    /**
     *车牌号
     */
    private String carId;

    /**
     *卡组类型
     */
    private Integer classify;

    /**
     * 卡证类型
     */
    private Integer type;

    @NotNull
    private Integer pageNo;

    @NotNull
    private Integer pageSize;
}
