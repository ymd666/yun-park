package cn.tnar.parkservice.model.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author kanwen
 * @description 欠费补交记录导出 DTO
 * @data 2019-03-19 23:10
 */
@Data
public class OverduePaymentExportDto {

    /**
     * 车牌号
     */
    private String carId;

    /**
     * 收费员
     */
    private String tollName;

    /**
     * 区域
     */
    private String regionName;

    /**
     * 入场时间
     */
    private Long intime;

    /**
     * 出场时间
     */
    private Long outtime;

    /**
     * 停车时长
     */
    private Integer parktime;

    /**
     * 应收金额
     */
    private BigDecimal parkamt;

    /**
     * 实收金额
     */
    private BigDecimal chargeamt;

    /**
     * 补交时间
     */
    private Long uploadtime;

    /**
     * 是否缴费
     */
    private Integer paystate;
}
