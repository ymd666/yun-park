package cn.tnar.parkservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 11:08:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class CalcBaseInfoResponse {

    private Long id; //计费规则表id

    private String calcName;  //计费名称

    private String carType; //车辆类型

    private String workType; //工作日历类型

    private Integer freeTime; //免费时段

    private BigDecimal limit;//收费上限

    private String regionCodes; //区域编码

    private String regionNames; //区域名称
}
