package cn.tnar.yunpark.xunlang;

/**
 * author : CaineZhu
 * date   : 2017/12/22 ${time}
 * desc   : class desc
 */
public class BCCTool {

    public static byte bccCalc(byte[] data, int offSet, int len) {
        byte temp = (byte)0x00;
        for (int i = offSet; i < offSet+len; i++) {
            temp ^= data[i];
        }
        return temp;
    }
}
