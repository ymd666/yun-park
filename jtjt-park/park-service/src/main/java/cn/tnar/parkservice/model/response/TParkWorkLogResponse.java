package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * @Author: 陈亮
 * @Date: 2018/10/18 18:45
 */
@Data
public class TParkWorkLogResponse {


    private int id;

    /**
     * 停车编号
     */
    private Long parkCode;

    /**
     * 账号
     */
    private String tollNo;

    /**
     * 上班时间
     */
    private String onDuty;

    /**
     * 下班时间
     */
    private String offDuty;

    /**
     * 状态
     */
    private int isvalid;

    /**
     * 上班打卡照片
     */
    private String onPhoto;

    /**
     * 下班打卡照片
     */
    private String offPhoto;

    /**
     *员工姓名
     */
    private String name;

    /**
     * 角色类别
     */
    private String tollType;

    /**
     * 停车场名
     */
    private String parkName;


    private String item;

    /**
     * 签到时间
     */
    private String beginTime;

    /**
     * 签出时间
     */
    private String endTime;

    /**
     * 代班人姓名
     */
    private String insteadMan;

    /**
     * 账号
     */
    private String loginname;

    private String insteadNo;

    private String tel;

    private String mobile;

    /**
     * 允许早退时间，分钟
     * */
    private String earlyTime;

    /**
     * 允许迟到时间，分钟
     * */
    private String lateTime;

    private String statusName;

    private String workDate;

    /**
     * 上班经纬度
     */
    private String onLatLng;

    /**
     * 下班经纬度
     */
    private String offLatLng;

}
