package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.dto.TParkVisitDto;
import cn.tnar.parkservice.model.entity.TParkVisitApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author clarck
 * @since 2018-10-05
 */
@Repository
@Mapper
public interface TParkVisitApplyMapper extends BaseMapper<TParkVisitApply> {

    @Select("select cust_id,apply_status FROM t_park_visit_apply WHERE cust_id = #{custId}")
    public TParkVisitDto queryInfo(@Param("custId")String custId);
}
