package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description: 停车流水卡组关联表
 * @date 2019年10月10日 14:06:47
 */
@TableName("t_park_record_group_relation")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class TParkRecordGroupRelation extends Model<TParkRecordGroupRelation> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("parking_record_serialno")
    private String parkingRecordSerialno;

    @TableField("group_id")
    private Long groupId;

    @TableField("create_time")
    private Long createTime;


}
