package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.AccessRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * @Description:
 * @author: yl
 * @date 2018/9/13 15:45
 */
@Repository
@Mapper
public interface AccessRuleMapper extends BaseMapper<AccessRule> {

    @Update("UPDATE t_park_access_rule set is_using = 0 WHERE park_code = #{rule.parkCode} and is_using=1 and access_gate Like #{rule.accessGate} and id != #{rule.id}")
    boolean closeOtherSameGateRule(@Param("rule") AccessRule rule);

    @Select("SELECT r.rule_name,r.park_code,r.is_using,i.name as park_name,r.access_rule_id,r.temporary_color\n" +
            "FROM\n" +
            "\t`t_park_access_rule` r \n" +
            "\tleft join t_park_info i on i.park_code=r.park_code\n" +
            "WHERE\n" +
            "\taccess_rule_id IS NOT NULL and r.park_code=#{parkCode} \n" +
            "GROUP BY\n" +
            "\taccess_rule_id\n" +
            "\tORDER BY r.id desc ")
    List<Map<String,Object>> queryByParkCode(Page<Map<String, Object>> page, @Param("parkCode") String parkCode);

    @Update("UPDATE t_park_access_rule set is_using = #{IsUsing} where access_rule_id=#{accessRuleId}")
    boolean updateIsUsing(@Param("IsUsing") Integer IsUsing, @Param("accessRuleId") Long accessRuleId);


    @Select("select r.id ,r.access_gate accessGate,LPAD(r.start_time,6,0) startTime,LPAD(r.end_time,6,0) endTime,g.name gateName,g.id as gateId,g.gate_type gateType from t_park_access_rule r left join t_park_gate g on g.gate_code=r.access_gate where access_rule_id=#{ruleId} and r.region_code=#{regionCode}" )
    List<Map<String,Object>> getGateList(@Param("ruleId") String ruleId,@Param("regionCode")String regionCode);

    @Update("UPDATE t_park_member_group set access_rule_id =null where id=${id}")
    boolean updateRule(@Param("id") Long id);

}
