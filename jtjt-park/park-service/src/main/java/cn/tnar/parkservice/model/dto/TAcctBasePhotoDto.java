package cn.tnar.parkservice.model.dto;


import com.baomidou.mybatisplus.annotation.TableField;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public class TAcctBasePhotoDto {

    @TableField("id")
    private Long id;
    @TableField("node_id")
    private Integer nodeId;
    @TableField("object_id")
    private Long objectId;
    private Integer filetype;
    private String filekey;
    @TableField("file_name")
    private String fileName;
    @TableField("yun_name")
    private String yunName;
    @TableField("file_url")
    private String fileUrl;
    @TableField("file_desc")
    private String fileDesc;
    private Long addtime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Integer getFiletype() {
        return filetype;
    }

    public void setFiletype(Integer filetype) {
        this.filetype = filetype;
    }

    public String getFilekey() {
        return filekey;
    }

    public void setFilekey(String filekey) {
        this.filekey = filekey;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getYunName() {
        return yunName;
    }

    public void setYunName(String yunName) {
        this.yunName = yunName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public Long getAddtime() {
        return addtime;
    }

    public void setAddtime(Long addtime) {
        this.addtime = addtime;
    }

}
