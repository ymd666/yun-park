package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.mapper.TAcctBasePhotoMapper;
import cn.tnar.parkservice.model.entity.TAcctBasePhoto;
import cn.tnar.parkservice.service.ITAcctBasePhotoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Service
public class TAcctBasePhotoServiceImpl extends ServiceImpl<TAcctBasePhotoMapper, TAcctBasePhoto> implements ITAcctBasePhotoService {

}
