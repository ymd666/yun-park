# Spring Cloud Alibaba **0.9.0 RELEASE** _2019.4.12_

## Nacos **1.0.1** _2019.6.12_

Nacos 独立的 Jar 包。不依赖于 Spring Cloud Alibaba Dependency

### Bug

该版本下 bug 均已修复。
还存在:

- It can't input text in Nacos Web Console with Firefox(Windows 10) sometimes
- Nacos V1.0.0 console Close health check failed `https://github.com/alibaba/nacos/issues/1357`
- Private network can't load fonts, favicon

## Sentinel 1.5.2

### Bug

- Start sentinel-dashboard error in JDK11
- grpc adapter-Tracer.trace() can't success
- Threshold of rate limiting mode does not support 1000

``` java
rule1.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_RATE_LIMITER);
rule1.setCount(2000)
```

- SentinelResourceAspect can not get @SentinelResource annotation in Proxy Interface

``` java
public interface EchoService {
    @SentinelResource(value = "testResource")
    String echo(String str);
}
```

## spring-cloud-starter-alibaba-nacos-discovery **0.9.0.RELEASE** _2019.4.19_
 
- Nacos-discovery-starter does not support Spring Cloud Config 'dicovery way' to fetch remote configuration
```
 A Spring Cloud Config Server, named configserver, has registered in nacos already.
```

# Spring Cloud **Greenwich.SR1** _2019.3.7_

## spring-cloud-openfeign **2.1.1.RELEASE** _2019-3.6_

无