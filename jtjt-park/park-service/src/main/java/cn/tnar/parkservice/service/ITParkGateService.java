package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TParkGate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zzb
 * @since 2019-02-28
 */
public interface ITParkGateService extends IService<TParkGate> {

}
