package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @author xzhang
 * @date 2019/9/7
 */
@Data
public class ParkingInfo {
    private String parkname;
    private String park_code;
    private String region_code;
    private String inphoto;
    private String insmallphoto;
    private String intime;
    private String parktime;
    private String car_id;
    private String cartype;
    private String parkamt;
    private String paidamt;
}
