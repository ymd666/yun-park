package cn.tnar.yunpark.careyes.nbiot.model;

import cn.tnar.yunpark.careyes.BCCTool;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * package: cn.tnar.yunpark.careyes.nbiot.model
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/9 10:07
 * desc   : AckMessage
 */
public class NBAckMessage {

    private int len;
    private int type;
    private int deviceType = 0x00;

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public ByteBuf toArray(byte[] sensorIdBytes, byte[] parkIdBytes, int city, int area, int sequence) {
        ByteBuf buf = Unpooled.buffer(this.len);
        //HEADER
        buf.writeByte(0XFF);
        buf.writeByte(0XFE);
        //type
        buf.writeByte(this.type);
        buf.writeByte(this.len);
        buf.writeByte(this.deviceType);
        buf.writeBytes(sensorIdBytes);
        buf.writeByte(0x88);
        buf.writeByte(city);
        buf.writeByte(area);
        buf.writeBytes(parkIdBytes);
        buf.writeByte(0x88);
        buf.writeByte(0x00);
        buf.writeByte(sequence);
        buf.writeByte(BCCTool.bccCalc(buf));
        buf.writeByte(0xAB);
        buf.writeByte(0xAA);
        return buf;
    }
}
