package cn.tnar.parkservice.controller.customer;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.model.entity.TParkRecordGroupRelation;
import cn.tnar.parkservice.service.*;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月10日 14:14:58
 */
@RestController
@RequestMapping("/tParkRecordGroupRelation")
@Slf4j
@Api(tags = "卡组流水")
public class TParkRecordGroupRelationController {

    @Autowired
    private ITParkRecordGroupRelationService itParkRecordGroupRelationService;

    @Autowired
    private ITParkVisitHistoryService tParkVisitHistoryService;

    @Autowired
    private ITParkGroupMemberinfoService itParkGroupMemberinfoService;

    @Autowired
    private ITParkMemberGroupService itParkMemberGroupService;

    private static final ResultJson RESULT_JSON = new ResultJson();

    @PostMapping("/insert")
    public ResultJson insertParkRecordGroupRelation(@RequestBody String json) {
        try {
            TParkRecordGroupRelation tParkRecordGroupRelation = JSON.parseObject(json, TParkRecordGroupRelation.class);
            Long groupId = tParkRecordGroupRelation.getGroupId();
            String parkingRecordSerialno = tParkRecordGroupRelation.getParkingRecordSerialno();
            QueryWrapper<TParkRecordGroupRelation> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parking_record_serialno", parkingRecordSerialno)
                    .last("limit 1");
            TParkRecordGroupRelation one = itParkRecordGroupRelationService.getOne(queryWrapper);
            if (one == null) {
                boolean save = itParkRecordGroupRelationService.save(tParkRecordGroupRelation);
                if (save) {
                    return RESULT_JSON.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(true);
                } else {
                    return RESULT_JSON.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG).setData(false);
                }
            } else {
                return RESULT_JSON.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG).setData("当前卡组流水关联数据已存在");
            }
        } catch (Exception e) {
            log.error("新增流水卡组关联数据异常", e);
            return RESULT_JSON.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG).setData(false);
        }
    }


    @ApiOperation("修改访客车辆来访状态")
    @PostMapping("/updataStatus")
    public ResultJson updateStatus(@RequestBody Map<String, Object> param) {
        return tParkVisitHistoryService.updateStatus(param);
    }


    @ApiOperation("更新会员当月累计额度（最大不超过会员所属卡组设置的满免金额）")
    @PostMapping("/updateMemAccAmt")
    public ResultJson updateMemAccAmt(@RequestBody Map<String, String> param) {
        return itParkGroupMemberinfoService.updateMemAccAmt(param);
    }

    @ApiOperation("更新卡组当月累计满免金额")
    @PostMapping("/updateCardGroupTotalAmt")
    public ResultJson updateCardGroupTotalAmt(@RequestBody Map<String, String> param) {
        return itParkMemberGroupService.updateCardGroupTotalAmt(param);
    }


    @Autowired
    private  MqService mqService;
    @ApiOperation("测试")
    @PostMapping("/sendTest")
    public  ResultJson sendTest(){
        Map<String,String> map = new HashMap<>();
        map.put("parkParamValue", "5");
        mqService.convertAndSend(Constant.EXCHANGE_NAME,
                "parkParam.110101200034", JSON.toJSONString(map));
        return  new ResultJson().setData(true);
    }


}
