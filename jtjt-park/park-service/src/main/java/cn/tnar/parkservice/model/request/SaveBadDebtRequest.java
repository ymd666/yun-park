package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * @author Bao
 * @date 2018-10-22
 */
@ApiModel("逃逸 -> 坏账 请求")
@Data
public class SaveBadDebtRequest {

    @ApiModelProperty("备注")
    @NotBlank(message = "备注不能为空")
    private String remark;

    @ApiModelProperty("逃逸流水 id")
    @Min(value = 1, message = "逃逸流水不能为空")
    private long id;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SaveBadDebtRequest{" +
                "remark='" + remark + '\'' +
                ", id=" + id +
                '}';
    }
}
