package cn.tnar.parkservice.exception;

/**
 * @author xzhang
 * @date 2019/9/7
 */
public class CouponsException extends Exception {
    private final String code;
    private final String message;

    public CouponsException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public CouponsException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
