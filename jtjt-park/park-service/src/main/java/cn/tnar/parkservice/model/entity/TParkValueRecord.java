package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 18:47:28
 */

@TableName("t_park_value_record")
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
@Data
public class TParkValueRecord extends Model<TParkValueRecord> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("park_code")
    private String parkCode;

    @TableField("park_value")
    private BigDecimal parkValue; //c试算接口获取到的值

    @TableField("cur_time")
    private Long curTime;  //日期，每个卡证，每天有一条数据

    @TableField("group_id")
    private Long groupId; //子卡id

    @TableField("reduce_money")
    private BigDecimal reduceMoney; //每个卡证当天的 停车流水 parkamt

    @TableField("fee_money")
    private BigDecimal feeMoney;  //子卡的套餐策略的缴费标准

    @TableField("count_time")
    private Long countTime; //每个卡证，当天的停车流水的 停车时长之和

    @TableField("count_number")
    private Long countNumber; //每个卡证，当天的停车流水的出场记录数

    @TableField("count_member")
    private Long countMember; //每个卡证，当天的停车流水的会员数


}
