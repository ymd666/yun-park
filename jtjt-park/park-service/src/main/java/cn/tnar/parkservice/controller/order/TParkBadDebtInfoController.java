package cn.tnar.parkservice.controller.order;

import cn.tnar.parkservice.model.dto.BadDebtDetailDto;
import cn.tnar.parkservice.model.entity.TParkBadDebtInfo;
import cn.tnar.parkservice.model.entity.TParkEscape;
import cn.tnar.parkservice.model.request.BadDebtRequest;
import cn.tnar.parkservice.model.request.SaveBadDebtRequest;
import cn.tnar.parkservice.service.ITParkBadDebtService;
import cn.tnar.parkservice.service.ITParkEscapeService;
import cn.tnar.parkservice.util.ErrorUtils;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author Bao
 * @date 2018-10-18
 */
@Api(tags = "坏账名单- Controller")
@RestController
@RequestMapping("service/baddebt")
public class TParkBadDebtInfoController {

    @Autowired
    @Qualifier("tParkBadDebtService")
    private ITParkBadDebtService tParkBadDebtService;

    @Autowired
    private ITParkEscapeService escapeService;


    @ApiOperation("分页查询坏账详情:t_park_bad_debt_info")
    @PostMapping("query")
    public ResultJson query(@RequestBody BadDebtRequest request) {
        Page<BadDebtDetailDto> page = new Page<>((request.getPageNo() / request.getPageSize()) + 1, request.getPageSize());
        QueryWrapper<BadDebtDetailDto> wrapper = new QueryWrapper<BadDebtDetailDto>()
                .eq(StringUtils.isNotEmpty(request.getCarId()), "bd.car_id", request.getCarId())
                .between("bd.baddebt_time", request.getStartTime(), request.getEndTime());
        Page<BadDebtDetailDto> ret = tParkBadDebtService.pageBadDebtInfoDetail(page, wrapper);

        QueryWrapper<BigDecimal> sumWrapper = new QueryWrapper<BigDecimal>()
                .eq(StringUtils.isNotEmpty(request.getCarId()), "bd.car_id", request.getCarId())
                .between("bd.baddebt_time", request.getStartTime(), request.getEndTime());
        BigDecimal sumAmount = Optional.ofNullable(tParkBadDebtService.sumBadDebtInfoAmount(sumWrapper)).orElse(BigDecimal.ZERO);

        Optional.of(ret.getRecords()).filter(records -> !records.isEmpty()).ifPresent(records -> records.get(0).setSumamt(sumAmount.toPlainString()));

        return ResultJson.success().data(ret).build();
    }

    @ApiOperation("逃逸转坏账")
    @PostMapping("escapeToBadDebt")
    public ResultJson escapeToBadDebt(@Valid @RequestBody SaveBadDebtRequest request, BindingResult result) {
        if (result.hasErrors()) {
            return ErrorUtils.error(result);
        }

        TParkEscape escape = escapeService.getById(request.getId());

        if (escape == null) {
            return ResultJson.clientError().msg("未找到逃逸流水").build();
        }

        TParkBadDebtInfo badDebtInfo = tParkBadDebtService.getById(request.getId());

        if (badDebtInfo != null) {
            return ResultJson.clientError().msg("坏账流水已存在").build();
        }

        tParkBadDebtService.escapeToBadDebt(escape, request.getRemark());
        return ResultJson.success().build();
    }
}
