package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TAcctRefundRecordMapper;
import cn.tnar.parkservice.model.entity.TAcctRefundRecord;
import cn.tnar.parkservice.service.ITAcctRefundRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 11:28:34
 */
@Service("tAcctRefundRecordServiceImpl")
public class TAcctRefundRecordServiceImpl extends ServiceImpl<TAcctRefundRecordMapper, TAcctRefundRecord> implements ITAcctRefundRecordService {
}
