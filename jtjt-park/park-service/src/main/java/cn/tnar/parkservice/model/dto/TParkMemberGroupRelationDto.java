package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月02日 15:04:04
 */
@TableName("t_park_member_group_relation")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class TParkMemberGroupRelationDto extends Model<TParkMemberGroupRelationDto> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("group_id")
    private Long groupId;

    @TableField("member_id")
    private Long memberId;

    @TableField("create_time")
    private Long createTime;

    @TableField("update_time")
    private Long updateTime;

    // 1新增 2 删除 3 修改
    private Integer type;


}
