package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:被访者申请审核实体
 * @author: Tiyer.Tao
 * @date 2019/8/23 15:43
 */
@TableName("t_park_visit_apply")
@Data
public class TParkVisitApply extends Model<TParkVisitApply> implements Serializable {

    private static final long serialVersionUID = 1L;

    //主键
    @TableId(value = "id", type = IdType.AUTO)
    private long id;

    //停车场编码
    private String parkCode;

    //申请人姓名
    private String applyName;

    //申请人手机号
    private String applyMobile;

    //申请人住址
    private String applyAdds;

    //申请时间
    private long applyTime;

    //备注
    private String remarks;

    //审核状态
    private int applyStatus;

    //通过时间
    private long accessTime;

    //申请用户编号
    private String custId;

    //操作人
    private String operId;

/*
    //对应会员卡组id
    private String memGroupId;

*/

}