package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.service.TParkStallService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/22
 * @Description:
 */
@Api(tags = "车位管理（张鑫）")
@RestController
@RequestMapping("service/parkStall")
public class TParkStallController {

    private Logger log = LoggerFactory.getLogger(TParkStallController.class);

    @Autowired
    @Qualifier("kesbAPIService")
    private KesbApiService kesbApiService;

    @Autowired
    private TParkStallService tParkStallService;

    @ApiOperation("新增停车场车位信息")
    @PostMapping("/add")
    public ResultJson addParkStall(@RequestBody Map<String, Object> param){
        try {
            log.info("新增停车场车位信息：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_STALL_ADD, param);
            log.info("新增停车场车位信息：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("新增停车场车位信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("删除停车场车位信息")
    @PostMapping("/delete")
    public ResultJson deleteParkStall(@RequestBody Map<String, Object> param){
        try {
            log.info("删除停车场车位信息：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_STALL_DELETE, param);
            log.info("删除停车场车位信息：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("删除停车场车位信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场车位信息")
    @PostMapping("/update")
    public ResultJson updateParkStall(@RequestBody Map<String, Object> param){
        try {
            log.info("修改停车场车位信息：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_STALL_UPDATE, param);
            log.info("修改停车场车位信息：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("修改停车场车位信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

/*    @ApiOperation("查询停车场车位信息（C接口）")
    @PostMapping("/query")
    public ResultJson queryParkStall(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_STALL_QUERY, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        }catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }*/

    @ApiOperation("批量新增停车场车位")
    @PostMapping("/batchAdd")
    public ResultJson batchAllParkStall(@RequestBody Map<String, Object> param){
        try {
            log.info("批量新增停车场车位：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_STALL_BATCHADD, param);
            log.info("批量新增停车场车位：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("批量新增停车场车位:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场车位信息（JAVA接口）")
    @PostMapping("/query")
    public ResultJson queryParkStall(@RequestBody Map<String,Object> param){
        return tParkStallService.query(param);
    }
}
