package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.model.entity.TDictionary;
import cn.tnar.parkservice.mapper.TDictionaryMapper;
import cn.tnar.parkservice.service.ITDictionaryService;
import cn.tnar.parkservice.service.MqService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Service("tDictionaryServiceImpl")
public class TDictionaryServiceImpl extends ServiceImpl<TDictionaryMapper, TDictionary> implements ITDictionaryService {

    @Autowired
    private TDictionaryMapper tDictionaryMapper;
    @Autowired
    private MqService mqService;

    @Override
    public ResultJson batchQuery(List<String> categoryEns) {
        List<Map<String, Object>> list = tDictionaryMapper.batchQuery(categoryEns);
        List<Map<String, Object>> tem = null;
        Map rep = null;
        List<Map<String, Object>> response = new ArrayList<>();
        for (String en : categoryEns) {
            rep = new HashMap();
            tem = new ArrayList();
            for (Map<String, Object> map : list) {
                if (map.get("category_en").equals(en)) {
                    tem.add(map);
                }
            }
            rep.put(en, tem);
            response.add(rep);
        }
        return new ResultJson().setCode(ResultCode.SUCCESS).setData(response);
    }

}
