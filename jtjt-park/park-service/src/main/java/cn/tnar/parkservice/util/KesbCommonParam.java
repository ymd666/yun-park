package cn.tnar.parkservice.util;

import java.util.Map;

/**
 * @Author: xzhang
 * @Description:
 * @Date: Created on 2018/3/30
 */
public class KesbCommonParam {
    /**
     * 添加kesb公共参数
     *
     * @param params
     * @param serviceId
     * @param memberId
     * @param sessionId
     * @return
     */
    public static Map<String, String> commonRequstParam(Map<String, String> params, String serviceId, String memberId, String sessionId) {
        //系统号(固定送8)
        params.put("g_sysid", "8");
        //菜单编号(固定送0)
        params.put("g_menuid", "0");
        //服务ID
        params.put("g_funcid", serviceId);
        //原节点编号(如果是操作员，送操作员的原节点编号)
        params.put("g_srcnodeid", "0");
        //目节点编号(如果是操作员，送操作员的需要操作客户的节点编号)
        params.put("g_dstnodeid", "0");
        //客户编号(登录时可送0)
        String custId = memberId;
        //String custId="0";
        params.put("g_custid", custId);
        //客户密码(对于非登录业务：使用用户编号作为密钥加密的密文；对于登录业务：使用用户代码登录时，以用户代码作为密钥加密；使用用户编号登录时，以用户编号作为密钥加密)
        params.put("g_custpwd", "0");
        //操作方式(必须送，不可以为空，后台系统送：9)
        params.put("g_userway", "9");
        //操作站点(填写格式：CPU[CPU序列号]IDE[硬盘序列号]IP[IP地址]MAC[MAC地址])
        params.put("g_stationaddr", "0");
        //会话编号(登录时送空，登录业务返回此编号，以后的业务操作必须送入此编号)
        String custsession = sessionId;
        params.put("g_custsession", custsession);

        return params;
    }
}
