package cn.tnar.yunpark.careyes.tcp.model;

import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * package: cn.tnar.yunpark.careyes.tcp.model
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/10 16:25
 * desc   : TimeCalibration
 */
public class TimeCalibration {

    private int routerId;
    private int sequence;

    public int getRouterId() {
        return routerId;
    }

    public void setRouterId(int routerId) {
        this.routerId = routerId;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public static TimeCalibration parse(ByteBuf byteBuf){
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        TimeCalibration tc = new TimeCalibration();
        buf.skipBytes(2);
        buf.skipBytes(3);
        tc.setRouterId(buf.readByte());
        buf.skipBytes(10);
        tc.setSequence(buf.readByte());
        return tc;
    }
}
