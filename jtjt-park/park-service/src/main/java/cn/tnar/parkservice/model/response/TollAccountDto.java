package cn.tnar.parkservice.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author: xinZhang
 * @Date: 2019/8/21
 * @Description:
 */
@Data
public class TollAccountDto {

    @ApiModelProperty(value = "停车场id",example = "110101200001")
    private String park_code;

    @ApiModelProperty(value = "自定义工号",example = "domain")
    private String nick;

    @ApiModelProperty(value = "员工姓名",example = "张三")
    private String name;

    @ApiModelProperty(value = "性别",example = "1")
    private Integer sex;

    @ApiModelProperty(value = "出生日期",example = "1997-12-7")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @ApiModelProperty(value = "状态标识",example = "1")
    private Integer tollstatus;

    @ApiModelProperty(value = "登录用户名",example = "zhangsan")
    private String loginname;

    @ApiModelProperty(value = "登录密码",example = "123")
    private String password;

    @ApiModelProperty(value = "证件类型",example = "1")
    private Integer idtype;

    @ApiModelProperty(value = "证件号",example = "420204199712066515")
    private String card_id;

    @ApiModelProperty(value = "手机",example = "18655667788")
    private String mobile;

    @ApiModelProperty(value = "电话",example = "010-1234567")
    private String tel;

    @ApiModelProperty(value = "启用日期",example = "2019-04-29")
    private Date beginday;

    @ApiModelProperty(value = "到期日期",example = "2019-05-29")
    private Date expday;

    @ApiModelProperty(value = "类型",example = "1")
    private Integer toll_type;

    @ApiModelProperty(value = "银行联行号",example = "12346")
    private String bankid;

    @ApiModelProperty(value = "银行号",example = "")
    private String bank_code;

    @ApiModelProperty(value = "银行名称",example = "")
    private String bankname;

    @ApiModelProperty(value = "银行账号0",example = "")
    private String acctno;

    @ApiModelProperty(value = "银行户名",example = "")
    private String acctname;

    @ApiModelProperty(value = "居住地址",example = "")
    private String saddress;

    @ApiModelProperty(value = "居住地址邮编",example = "")
    private String szip;

    @ApiModelProperty(value = "户口地址邮编",example = "")
    private String hzip;

    @ApiModelProperty(value = "备注",example = "")
    private String remark;

    @ApiModelProperty(value = "风险级别",example = "")
    private Integer risk;

    @ApiModelProperty(value = "运营商id",example = "")
    private Long agent_id;

    private Long g_custid;
}
