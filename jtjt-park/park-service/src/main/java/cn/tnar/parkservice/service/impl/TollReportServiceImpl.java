package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkOrderMapper;
import cn.tnar.parkservice.mapper.TollReportMapper;
import cn.tnar.parkservice.model.response.TollReportResponse;
import cn.tnar.parkservice.service.TollReportService;
import cn.tnar.parkservice.util.DateUtils;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/9/26
 * @Description:
 */
@Service
public class TollReportServiceImpl implements TollReportService {

    @Autowired
    private TollReportMapper tollReportMapper;

    @Autowired
    private TParkOrderMapper tParkOrderMapper;

    /**
     * 收费员排班报表记录
     *
     * @param map
     * @return
     */
    @Override
    @Transactional
    public ResultJson tollAutomated(Map<String, Object> map) {
        Integer pageNo = (Integer) map.get("pageNo");
        Integer pageSize = (Integer) map.get("pageSize");
        Boolean flag = (Boolean) (map.get("queryFlag") == null ? true : map.get("queryFlag")); //当日/历史流水标识
        Page<TollReportResponse> page = new Page<>(pageNo, pageSize);
        //获取上下班工作日志信息
        List<TollReportResponse> tollReport = tollReportMapper.getTollReport(page, map);
        if (!CollectionUtils.isEmpty(tollReport)) {
            tollReport.forEach(tollReportResponse -> {
                String batchno = tollReportResponse.getBatchno();
                //获取收费总额，优惠券总额 ，长租车总额
                Map<String, Object> amount = null;
                Map<String, Object> couponAmt = null;
                Map<String, Object> memCarAmt = null;
                if (!flag) {
                    //查询历史收费统计
                    amount = tParkOrderMapper.getAmount(batchno);
                    couponAmt = tParkOrderMapper.getCouponAmt(batchno);
                    memCarAmt = tParkOrderMapper.getMemCarAmt(batchno);
                } else {
                    //查询当日收费统计
                    amount = tParkOrderMapper.getDailyAmount(batchno);
                    couponAmt = tParkOrderMapper.getDailyCouponAmt(batchno);
                    memCarAmt = tParkOrderMapper.getDailyMemCarAmt(batchno);
                }
                tollReportResponse.setChargeAmount((BigDecimal) amount.get("chargeAmt"));
                tollReportResponse.setCouponAmount((BigDecimal) couponAmt.get("couponAmt"));
                tollReportResponse.setMemCardAmount((BigDecimal) memCarAmt.get("memCarAmt"));
                //计算合计金额
                tollReportResponse.setTotal(tollReportResponse.getChargeAmount().add(tollReportResponse.getCouponAmount()).add(tollReportResponse.getMemCardAmount()));
                //计算工作时长
                String timeDiff = DateUtils.getTimeDiff(tollReportResponse.getOnDuty(), tollReportResponse.getOffDuty());
                tollReportResponse.setWorkTime(timeDiff);
            });
        }
        page.setRecords(tollReport);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(page);
    }
}
