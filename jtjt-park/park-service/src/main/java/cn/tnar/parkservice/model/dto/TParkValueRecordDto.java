package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 18:47:28
 */

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
@Data
@TableName("t_park_value_record")
public class TParkValueRecordDto extends Model<TParkValueRecordDto> {

    @TableId("id")
    private Long id;

    @TableField("park_code")
    private String parkCode;

    @TableField("park_value")
    private BigDecimal parkValue; //c试算接口获取到的值

    @TableField("cur_time")
    private Long curTime;  //日期，每个卡证，每天有一条数据

    @TableField("group_id")
    private Long groupId; //子卡id

    @TableField("reduce_money")
    private BigDecimal reduceMoney; //每个卡证当天的 停车流水 parkamt

    @TableField("fee_money")
    private BigDecimal feeMoney;  //子卡的套餐策略的缴费标准

    @TableField("count_time")
    private Long countTime; //每个卡证，当天的停车流水的 停车时长之和

    @TableField("count_number")
    private Long countNumber; //每个卡证，当天的停车流水的出场记录数

    @TableField("group_name")
    private String groupName; // 卡组名称

    @TableField("month_value")
    private BigDecimal monthValue; //套餐金额

    @TableField("count_member")
    private BigDecimal countMember;//会员数量

}
