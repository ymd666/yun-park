package cn.tnar.parkservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 15:53:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class FeesRoleInfoResponse {

    private Long id; //套餐策略表id

    private String roleName; //套餐策略名称

    private String carType; //车辆类型

    private String cardType; //日卡，月卡，

    private String startTime; //开始时段

    private String endTime; //结束时段

    private BigDecimal amount; //收费金额
}
