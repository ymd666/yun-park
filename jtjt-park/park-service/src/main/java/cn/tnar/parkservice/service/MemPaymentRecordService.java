package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.dto.MemPaymentRecordDto;
import cn.tnar.parkservice.model.entity.TCarMemberamountinfo;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;


public interface MemPaymentRecordService extends IService<TCarMemberamountinfo> {

    ResultJson queryRecords(MemPaymentRecordDto memPaymentRecordDto);

//    ResultJson count(MemPaymentRecordDto memPaymentRecordDto);

    ResultJson baseRecords(MemPaymentRecordDto memPaymentRecordDto);

    ResultJson detailRecords(Long id);
}
