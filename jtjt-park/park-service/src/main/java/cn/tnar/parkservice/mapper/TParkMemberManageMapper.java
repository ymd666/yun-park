package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.response.TMeberCardManageResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TParkMemberManageMapper {

    List<TMeberCardManageResponse> queryOldMember(Page<TMeberCardManageResponse> page ,@Param("param") Map<String,Object> param);

    List<TMeberCardManageResponse> queryNewMember(Page<TMeberCardManageResponse> page ,@Param("param") Map<String,Object> param);
}
