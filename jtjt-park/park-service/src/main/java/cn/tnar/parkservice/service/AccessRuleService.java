package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.AccessRule;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @Description:
 * @author: yl
 * @date 2018/9/13 15:19
 */
public interface AccessRuleService extends IService<AccessRule> {

    boolean closeOtherSameGateRule(AccessRule entity);

    Page<Map<String, Object>> queryByParkCode(String parkCode, Integer pageNo, Integer pageSize);

    boolean updateIsUsing(Integer IsUsing, Long accessRuleId);

    ResultJson insert(String json);

    ResultJson update(String json);

    ResultJson updateYes(String json);

    ResultJson delete(String json);

    ResultJson query(String json);

    ResultJson queryOne(String json);

    ResultJson configRule(String json);

    ResultJson getRegionList(String json);

    ResultJson cancelRule(String json);

}
