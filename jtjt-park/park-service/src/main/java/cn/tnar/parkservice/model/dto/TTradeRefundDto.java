package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Data
public class TTradeRefundDto {

    /**
     * 列表ID
     */
    @ApiModelProperty(value = "列表ID")
    private Long id;
    @TableField("node_id")
    private Integer nodeId;
    @TableField("object_id")
    private Long objectId;
    /**
     * 退款用户ID
     */
    @ApiModelProperty(value = "退款用户ID")
    @TableField("cust_id")
    private Long custId;
    @TableField("order_number")
    private String orderNumber;
    /**
     * 退款金额
     */
    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundamount;
    /**
     * 退款发起时间
     */
    @ApiModelProperty(value = "退款发起时间")
    private Long refundtime;
    /**
     * 退款原因
     */
    @ApiModelProperty(value = "退款原因")
    private String refundreason;
    /**
     * 数据字典：1 初始 2 处理中 3 成功 4 失败 5 取消
     */
    @ApiModelProperty(value = "数据字典：1 初始 2 处理中 3 成功 4 失败 5 取消")
    private Integer refundstate;
    /**
     * 退款完成时间
     */
    @ApiModelProperty(value = "退款完成时间")
    private Long endtime;
    /**
     * 退款操作人
     */
    @ApiModelProperty(value = "退款操作人")
    @TableField("oper_id")
    private Long operId;
    /**
     * 失败原因描述
     */
    @ApiModelProperty(value = "失败原因描述")
    private String description;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     * 审核人1
     */
    @ApiModelProperty(value = "审核人1")
    @TableField("first_check_id")
    private Long firstCheckId;
    /**
     * 审核时间
     */
    @ApiModelProperty(value = "审核时间")
    @TableField("first_check_time")
    private Long firstCheckTime;
    /**
     * 第二次审核人id
     */
    @ApiModelProperty(value = "第二次审核人id")
    @TableField("sec_check_id")
    private Long secCheckId;
    /**
     * 第二次校验时间
     */
    @ApiModelProperty(value = "第二次校验时间")
    @TableField("sec_check_time")
    private Long secCheckTime;
    /**
     * 第三个审核人ID
     */
    @ApiModelProperty(value = "第三个审核人ID")
    @TableField("third_check_id")
    private Long thirdCheckId;
    /**
     * 第三次审核时间
     */
    @ApiModelProperty(value = "第三次审核时间")
    @TableField("third_check_time")
    private Long thirdCheckTime;

}
