package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class BWInvoiceConfigDto {

    /**
     * 百旺url
     */
    private String bwUrl;

    /**
     *开票账户appId
     */
    private String bwKey;

    /**
     *开票账户appSecret
     */
    private String bwSecret;

    /**
     *登录账户
     */
    private String bwUsername;

    /**
     *登录密码
     */
    private String bwPassword;

    /**
     *盐值
     */
    private String bwSalt;

    /**
     *公司税号
     */
    private String bwTaxNo;

}
