package cn.tnar.parkservice.controller.customer;


import cn.tnar.parkservice.service.ITParkGroupMemberinfoService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Api(tags = "月卡分组-白名单/黑名单(张志彪)")
@RestController
@RequestMapping("service/groupMemberinfo")
public class TParkGroupMemberinfoController {

    @Autowired
    @Qualifier("tParkGroupMemberinfoServiceImpl")
    private ITParkGroupMemberinfoService itParkGroupMemberinfoService;

    @ApiOperation(value = "新增白名单/黑名单")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody String json) {
        return itParkGroupMemberinfoService.insert(json);
    }

    @ApiOperation(value = "白名单审核")
    @PostMapping(value = "check")
    public ResultJson check(@RequestBody String json) {
        return itParkGroupMemberinfoService.check(json);
    }

    @ApiOperation(value = "分页查询白名单/黑名单")
    @PostMapping(value = "select")
    public ResultJson select(@RequestBody String json) {
        return itParkGroupMemberinfoService.select(json);
    }

    @ApiOperation(value = "详情白名单")
    @PostMapping(value = "selectOne")
    public ResultJson selectOne(@RequestBody String json) {
        return itParkGroupMemberinfoService.selectOne(json);
    }


    @ApiOperation(value = "暂停白名单")
    @PostMapping(value = "stop")
    public ResultJson stop(@RequestBody String json) {
        return itParkGroupMemberinfoService.stop(json);
    }

    @ApiOperation(value = "延期白名单")
    @PostMapping(value = "defer")
    public ResultJson defer(@RequestBody String json) {
        return itParkGroupMemberinfoService.defer(json);
    }

    @ApiOperation(value = "解除白名单")
    @PostMapping(value = "delete")
    public ResultJson delete(@RequestBody String json) {
        return itParkGroupMemberinfoService.delete(json);
    }

    @ApiOperation(value = "启用白名单")
    @PostMapping(value = "start")
    public ResultJson start(@RequestBody String json) {
        return itParkGroupMemberinfoService.start(json);
    }

    @ApiOperation(value = "修改白名单/黑名单")
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody String json) {
        return itParkGroupMemberinfoService.update(json);
    }

    @ApiOperation(value = "查询卡组显示所有白名单/黑名单")
    @PostMapping(value = "queryAll")
    public ResultJson queryAll(@RequestBody String json) {
        return itParkGroupMemberinfoService.queryAll(json);
    }

    @ApiOperation(value = "新版会员卡组显示所有会员")
    @PostMapping(value = "queryOtherAll")
    public ResultJson queryOtherAll(@RequestBody String json) {
        return itParkGroupMemberinfoService.queryOtherAll(json);
    }


    @ApiOperation(value = "新版会员新增")
    @PostMapping(value = "insertMember")
    public ResultJson insertMember(@RequestBody String json) {
        return itParkGroupMemberinfoService.insertMember(json);
    }

    @ApiOperation(value = "新版会员修改")
    @PostMapping(value = "updateMember")
    public ResultJson updateMember(@RequestBody String json) {
        return itParkGroupMemberinfoService.updateMember(json);
    }

    @ApiOperation(value = "新版会员车位删除")
    @PostMapping(value = "deleteMemberSeat")
    public ResultJson deleteMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.deleteMemberSeat(json);
    }

    @ApiOperation(value = "新版会员查询车牌")
    @PostMapping(value = "queryMemberCarId")
    public ResultJson queryMemberCarId(@RequestBody String json) {
        return itParkGroupMemberinfoService.queryMemberCarId(json);
    }

    @ApiOperation(value = "新版会员校验车牌")
    @PostMapping(value = "checkMemberCarId")
    public ResultJson checkMemberCarId(@RequestBody String json) {
        return itParkGroupMemberinfoService.checkMemberCarId(json);
    }

    @ApiOperation(value = "新版会员删除车牌")
    @PostMapping(value = "deleteMemberCarId")
    public ResultJson deleteMemberCarId(@RequestBody String json) {
        return itParkGroupMemberinfoService.deleteMemberCarId(json);
    }

    @ApiOperation(value = "新版会员新增车牌")
    @PostMapping(value = "insertMemberCarId")
    public ResultJson insertMemberCarId(@RequestBody String json) {
        return itParkGroupMemberinfoService.insertMemberCarId(json);
    }

    @ApiOperation(value = "新版会员修改车牌")
    @PostMapping(value = "updateMemberCarId")
    public ResultJson updateMemberCarId(@RequestBody String json) {

        return itParkGroupMemberinfoService.updateMemberCarId(json);
    }

    @ApiOperation(value = "新版会员查询车位")
    @PostMapping(value = "queryMemberSeat")
    public ResultJson queryMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.queryMemberSeat(json);
    }


    @ApiOperation(value = "新版会员新增车位")
    @PostMapping(value = "insertMemberSeat")
    public ResultJson insertMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.insertMemberSeat(json);
    }

    @ApiOperation(value = "新版会员暂停车位")
    @PostMapping(value = "stopMemberSeat")
    public ResultJson stopMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.stopMemberSeat(json);
    }

    @ApiOperation(value = "新版会员启用车位")
    @PostMapping(value = "startMemberSeat")
    public ResultJson startMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.startMemberSeat(json);
    }

    /*@ApiOperation(value = "新版会员延期车位-计算时间")
    @PostMapping(value = "deferMemberSeat")
    public ResultJson deferMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.deferMemberSeat(json);
    }*/

    @ApiOperation(value = "新版会员确定延期车位")
    @PostMapping(value = "deferConfirmMemberSeat")
    public ResultJson deferConfirmMemberSeat(@RequestBody String json) {
        return itParkGroupMemberinfoService.deferConfirmMemberSeat(json);
    }

    @ApiOperation(value = "新版会员批量转移")
    @PostMapping(value = "changeMemberGroup")
    public ResultJson changeMemberGroup(@RequestBody String json) {
        return itParkGroupMemberinfoService.changeMemberGroup(json);
    }

    @ApiOperation(value = "新版会员批量转移-查询所有自定义子卡")
    @PostMapping(value = "getMemberGroup")
    public ResultJson getMemberGroup(@RequestBody String json) {
        return itParkGroupMemberinfoService.getMemberGroup(json);
    }

    @ApiOperation(value = "新版会员查询记录")
    @PostMapping(value = "queryMemberRecord")
    public ResultJson queryMemberRecord(@RequestBody String json) {
        return itParkGroupMemberinfoService.queryMemberRecord(json);
    }


}
