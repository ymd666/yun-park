package cn.tnar.parkservice.model.dto;

import lombok.Data;



/**
 * Created by fly on 2018/11/28.
 */
@Data
public class TParkVisitDto {

    private int id=0;

    private String  custId;// 用户ID

    private int  applyStatus=0;// 申请权限

    private String operId;// 操作人custId

    private String parkCode;// 停车场编号

    private String groupId;//卡组ID

    private String applyId;//被访人ID

    private String applyName;//卡组姓名

    private String applyMobile;//被访人手机号

    private int size;//每页条数

    private int page;//页码

}
