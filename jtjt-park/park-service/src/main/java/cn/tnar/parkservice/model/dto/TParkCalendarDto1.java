package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月04日 19:43:12
 */
@Data
@TableName("t_park_calendar")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class TParkCalendarDto1 {
    private Long id;

    private Integer nodeId;

    private Integer holiday;

    private String regionCode;

    private Long agentId;

    private String parkCode;

    private Integer status; //0 新增 1删除


}