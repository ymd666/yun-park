#!/usr/bin/env bash
name=bay-service

# export JAVA_HOME=/fly/jdk1.8.0_121

die ( ) {
    echo
    echo "$*"
    echo
    exit 1
}

# Determine the Java command to use to start the JVM.
if [ -n "$JAVA_HOME" ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="$JAVA_HOME/bin/java"
    fi
    if [ ! -x "$JAVACMD" ] ; then
        die "ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

# Determine the Java version
JAVA_VER=$($JAVACMD -version 2>&1 | awk -F '"' '/version/ {print $2}')
echo "Java version is $JAVA_VER"
if [[ "$JAVA_VER" < "1.8" ]]; then
    die "Java version must be 1.8"
fi

echo Starting $name...
nohup $JAVACMD -jar $name.jar > /dev/null 2>&1 &
