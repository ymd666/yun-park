package cn.tnar.parkservice.model.entity;


import java.math.BigDecimal;

public class TParkBadDebtInfo {

    private Long id;
    private Long nodeId;
    private Long agentId;
    private String parkCode;
    private String regionCode;
    private String parkname;
    private String inbatch;
    private String serialno;
    private String inoperno;
    private String outbatch;
    private String outoperno;
    private String traceIndex;
    private Long tracetype;
    private Long clienttype;
    private Long seatnum;
    private String seatCode1;
    private String seatno1;
    private String seatCode2;
    private String seatno2;
    private Long ownerCustId;
    private Long cartype;
    private String carId;
    private String carnocolor;
    private Long intime;
    private String ingatenoId;
    private String inphoto;
    private String insmallphoto;
    private Long inconfidence;
    private Long outtime;
    private String outgatenoId;
    private String outphoto;
    private String outsmallphoto;
    private Long outconfidence;
    private Long parktime;
    private Long paydatetime;
    private BigDecimal preamt;
    private BigDecimal parkamt;
    private BigDecimal chargeamt;
    private Long paystate;
    private Long outtype;
    private String chargemanno;
    private Long whitetype;
    private Long result;
    private String deviceCode;
    private String posCode;
    private String reserve;
    private Long settledate;
    private Long inoperate;
    private Long outoperate;
    private Long uploadtime;
    private Long payflag;
    private Long tradeorderid;
    private Long hisflag;
    private Long memberdiscount;
    private String feedesc;
    private Long settle;
    private Long baddebtTime;
    private String remark;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }


    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }


    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }


    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }


    public String getParkname() {
        return parkname;
    }

    public void setParkname(String parkname) {
        this.parkname = parkname;
    }


    public String getInbatch() {
        return inbatch;
    }

    public void setInbatch(String inbatch) {
        this.inbatch = inbatch;
    }


    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }


    public String getInoperno() {
        return inoperno;
    }

    public void setInoperno(String inoperno) {
        this.inoperno = inoperno;
    }


    public String getOutbatch() {
        return outbatch;
    }

    public void setOutbatch(String outbatch) {
        this.outbatch = outbatch;
    }


    public String getOutoperno() {
        return outoperno;
    }

    public void setOutoperno(String outoperno) {
        this.outoperno = outoperno;
    }


    public String getTraceIndex() {
        return traceIndex;
    }

    public void setTraceIndex(String traceIndex) {
        this.traceIndex = traceIndex;
    }


    public Long getTracetype() {
        return tracetype;
    }

    public void setTracetype(Long tracetype) {
        this.tracetype = tracetype;
    }


    public Long getClienttype() {
        return clienttype;
    }

    public void setClienttype(Long clienttype) {
        this.clienttype = clienttype;
    }


    public Long getSeatnum() {
        return seatnum;
    }

    public void setSeatnum(Long seatnum) {
        this.seatnum = seatnum;
    }


    public String getSeatCode1() {
        return seatCode1;
    }

    public void setSeatCode1(String seatCode1) {
        this.seatCode1 = seatCode1;
    }


    public String getSeatno1() {
        return seatno1;
    }

    public void setSeatno1(String seatno1) {
        this.seatno1 = seatno1;
    }


    public String getSeatCode2() {
        return seatCode2;
    }

    public void setSeatCode2(String seatCode2) {
        this.seatCode2 = seatCode2;
    }


    public String getSeatno2() {
        return seatno2;
    }

    public void setSeatno2(String seatno2) {
        this.seatno2 = seatno2;
    }


    public Long getOwnerCustId() {
        return ownerCustId;
    }

    public void setOwnerCustId(Long ownerCustId) {
        this.ownerCustId = ownerCustId;
    }


    public Long getCartype() {
        return cartype;
    }

    public void setCartype(Long cartype) {
        this.cartype = cartype;
    }


    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }


    public String getCarnocolor() {
        return carnocolor;
    }

    public void setCarnocolor(String carnocolor) {
        this.carnocolor = carnocolor;
    }


    public Long getIntime() {
        return intime;
    }

    public void setIntime(Long intime) {
        this.intime = intime;
    }


    public String getIngatenoId() {
        return ingatenoId;
    }

    public void setIngatenoId(String ingatenoId) {
        this.ingatenoId = ingatenoId;
    }


    public String getInphoto() {
        return inphoto;
    }

    public void setInphoto(String inphoto) {
        this.inphoto = inphoto;
    }


    public String getInsmallphoto() {
        return insmallphoto;
    }

    public void setInsmallphoto(String insmallphoto) {
        this.insmallphoto = insmallphoto;
    }


    public Long getInconfidence() {
        return inconfidence;
    }

    public void setInconfidence(Long inconfidence) {
        this.inconfidence = inconfidence;
    }


    public Long getOuttime() {
        return outtime;
    }

    public void setOuttime(Long outtime) {
        this.outtime = outtime;
    }


    public String getOutgatenoId() {
        return outgatenoId;
    }

    public void setOutgatenoId(String outgatenoId) {
        this.outgatenoId = outgatenoId;
    }


    public String getOutphoto() {
        return outphoto;
    }

    public void setOutphoto(String outphoto) {
        this.outphoto = outphoto;
    }


    public String getOutsmallphoto() {
        return outsmallphoto;
    }

    public void setOutsmallphoto(String outsmallphoto) {
        this.outsmallphoto = outsmallphoto;
    }


    public Long getOutconfidence() {
        return outconfidence;
    }

    public void setOutconfidence(Long outconfidence) {
        this.outconfidence = outconfidence;
    }


    public Long getParktime() {
        return parktime;
    }

    public void setParktime(Long parktime) {
        this.parktime = parktime;
    }


    public Long getPaydatetime() {
        return paydatetime;
    }

    public void setPaydatetime(Long paydatetime) {
        this.paydatetime = paydatetime;
    }


    public BigDecimal getPreamt() {
        return preamt;
    }

    public void setPreamt(BigDecimal preamt) {
        this.preamt = preamt;
    }


    public BigDecimal getParkamt() {
        return parkamt;
    }

    public void setParkamt(BigDecimal parkamt) {
        this.parkamt = parkamt;
    }


    public BigDecimal getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(BigDecimal chargeamt) {
        this.chargeamt = chargeamt;
    }


    public Long getPaystate() {
        return paystate;
    }

    public void setPaystate(Long paystate) {
        this.paystate = paystate;
    }


    public Long getOuttype() {
        return outtype;
    }

    public void setOuttype(Long outtype) {
        this.outtype = outtype;
    }


    public String getChargemanno() {
        return chargemanno;
    }

    public void setChargemanno(String chargemanno) {
        this.chargemanno = chargemanno;
    }


    public Long getWhitetype() {
        return whitetype;
    }

    public void setWhitetype(Long whitetype) {
        this.whitetype = whitetype;
    }


    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }


    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }


    public String getPosCode() {
        return posCode;
    }

    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }


    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }


    public Long getSettledate() {
        return settledate;
    }

    public void setSettledate(Long settledate) {
        this.settledate = settledate;
    }


    public Long getInoperate() {
        return inoperate;
    }

    public void setInoperate(Long inoperate) {
        this.inoperate = inoperate;
    }


    public Long getOutoperate() {
        return outoperate;
    }

    public void setOutoperate(Long outoperate) {
        this.outoperate = outoperate;
    }


    public Long getUploadtime() {
        return uploadtime;
    }

    public void setUploadtime(Long uploadtime) {
        this.uploadtime = uploadtime;
    }


    public Long getPayflag() {
        return payflag;
    }

    public void setPayflag(Long payflag) {
        this.payflag = payflag;
    }


    public Long getTradeorderid() {
        return tradeorderid;
    }

    public void setTradeorderid(Long tradeorderid) {
        this.tradeorderid = tradeorderid;
    }


    public Long getHisflag() {
        return hisflag;
    }

    public void setHisflag(Long hisflag) {
        this.hisflag = hisflag;
    }


    public Long getMemberdiscount() {
        return memberdiscount;
    }

    public void setMemberdiscount(Long memberdiscount) {
        this.memberdiscount = memberdiscount;
    }


    public String getFeedesc() {
        return feedesc;
    }

    public void setFeedesc(String feedesc) {
        this.feedesc = feedesc;
    }


    public Long getSettle() {
        return settle;
    }

    public void setSettle(Long settle) {
        this.settle = settle;
    }

    public Long getBaddebtTime() {
        return baddebtTime;
    }

    public void setBaddebtTime(Long baddebtTime) {
        this.baddebtTime = baddebtTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
