package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.TParkValueRecordDto;
import cn.tnar.parkservice.model.entity.TParkValueRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 19:38:16
 */
@Repository
@Mapper
@QuerySupport
public interface TParkValueRecordMapper extends BaseMapper<TParkValueRecord> {

    @QuerySupport
    @Select("SELECT a.id,a.park_code,a.cur_time,a.group_id,\n" +
            "(SELECT name from t_park_member_group where id = a.group_id) as group_name,\n" +
            "(SELECT month_value from t_car_feesroleinfo \n" +
            "where id = (SELECT feesroleinfo_id  from t_park_member_group where id = a.group_id )) as month_value,\n" +
            "sum(a.count_member) as count_member,\n" +
            "sum(a.reduce_money) as reduce_money,\n" +
            "sum(a.fee_money) as fee_money,\n" +
            "sum(a.park_value) as park_value,\n" +
            "sum(a.count_time) as count_time,\n" +
            "sum(a.count_number) as count_number\n" +
            "from  t_park_value_record a where group_id in (${group_id})  and cur_time BETWEEN #{start_date} AND #{end_date} group by group_id order by  cur_time asc\n")
    List<TParkValueRecordDto> getRecordByGroupId(Page<TParkValueRecordDto> page, @Param("group_id") String group_id,
                                                 @Param("start_date") Long start_date, @Param("end_date") Long end_date);
}
