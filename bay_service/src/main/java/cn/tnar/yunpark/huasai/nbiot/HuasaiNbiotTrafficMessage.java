package cn.tnar.yunpark.huasai.nbiot;

import cn.tnar.yunpark.huasai.HuasaiUtil;
import cn.tnar.yunpark.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * 华赛NBIOT地磁车位检测器车位状态变化消息
 * Created by tieh on 2017/6/21.
 */
public class HuasaiNbiotTrafficMessage extends HuasaiNbiotMessage {
    private static final Logger log = LoggerFactory.getLogger(HuasaiNbiotTrafficMessage.class);

    /**
     * 车位状态，0 - 无车， 1 - 有车
     * offset: 19, length: 4
     */
    private int status;

    /**
     * 停车批次号
     * offet: 23, length: 2
     */
    private int batchNo;

    /**
     * 电量
     * offset: 25, length:1
     */
    private byte power;

    /**
     * 信号强度
     * offset: 30, length: 1
     */
    private byte rssi;

    /**
     * 保留位
     * offset: 31, length: 2
     */
    private short reserved;

    /**
     * CRC16
     * offset: 33, length: 2
     */
    @Override
    protected int getCrcOffset() {
        return 33;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public int getRssi() {
        return rssi;
    }

    public int getBatchNo() {
        return batchNo;
    }


    @Override
    protected void checkValid(byte[] data, int len) throws HuasaiException {
        super.checkValid(data, len);
        if (len != 35) {
            String error = "报文长度不等于15: " + len;
            log.warn(error);
            throw new HuasaiException(error);
        }
    }

    @Override
    protected void parse(byte[] data, int len) throws HuasaiException {
        super.parse(data, len);
        status = buf.getInt(19);
        batchNo = buf.getUnsignedShort(23);
        power = buf.getByte(25);
        magId = buf.getInt(26);
        rssi = buf.getByte(30);
    }

    @Override
    public String toString() {
        return "车位状态变化消息{" +
                "magCode=" + Util.toHex(magCode) +
                ", timestamp=" + timestamp +
                ", status=" + status +
                ", power=" + power +
                ", rssi=" + rssi +
                ", magId=" + magId +
                '}';
    }

    public String getPowerShow() {
        int vPower = power * 100;
        if(vPower < 0) {
            vPower = 0;
        } else if(vPower > 700) {
            vPower = 700;
        }
        return Integer.toString(vPower / 7);
    }

    /**
     * 0 -113dBm or less
     1 -111dBm
     2...30 -109... -53dBm
     31 -51dBm or greater
     * @return 0% ~ 100%
     */
    public String getRssiShow() {
        int scopeRssi = rssi;
        if (rssi < 0) {
            scopeRssi = 0;
        } else if (rssi > 31) {
            scopeRssi = 31;
        }
        return Integer.toString((31 - scopeRssi) * 100 / 31);
    }
}
