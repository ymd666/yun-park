package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.model.dto.BadDebtDetailDto;
import cn.tnar.parkservice.mapper.TParkBadDebtInfoMapper;
import cn.tnar.parkservice.model.entity.TParkBadDebtInfo;
import cn.tnar.parkservice.model.entity.TParkEscape;
import cn.tnar.parkservice.service.ITParkBadDebtService;
import cn.tnar.parkservice.service.ITParkEscapeService;
import cn.tnar.parkservice.util.DateTimeUtils;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Bao
 * @date 2018-10-18
 */
@Service("tParkBadDebtService")
public class TParkBadDebtServiceImpl extends ServiceImpl<TParkBadDebtInfoMapper, TParkBadDebtInfo> implements ITParkBadDebtService {


    private final ITParkEscapeService escapeService;

    public TParkBadDebtServiceImpl(ITParkEscapeService escapeService) {
        this.escapeService = escapeService;
    }

    @Override
    public Page<BadDebtDetailDto> pageBadDebtInfoDetail(Page<BadDebtDetailDto> page, Wrapper<BadDebtDetailDto> wrapper) {
        return baseMapper.pageBadDebtInfoDetail(page, wrapper);
    }

    @Override
    public BigDecimal sumBadDebtInfoAmount(Wrapper<BigDecimal> wrapper) {
        return baseMapper.sumBadDebtInfoAmount(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void escapeToBadDebt(TParkEscape escape, String remark) {
        TParkBadDebtInfo badDebtInfo = new TParkBadDebtInfo();
        BeanUtils.copyProperties(escape, badDebtInfo);
        badDebtInfo.setBaddebtTime(DateTimeUtils.toLBM(LocalDateTime.now()));
        badDebtInfo.setRemark(remark);
        escapeService.removeById(escape.getId());
        save(badDebtInfo);
    }
}
