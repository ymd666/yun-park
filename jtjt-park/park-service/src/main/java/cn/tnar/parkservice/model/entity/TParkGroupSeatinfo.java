package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TParkGroupSeatinfo对象", description="")
public class TParkGroupSeatinfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Integer nodeId;

    private Long agentId;

    @ApiModelProperty(value = "停车场编号")
    private String parkCode;

    @ApiModelProperty(value = "区域编号")
    private String regionCode;

    @ApiModelProperty(value = "会员id")
    private Long memberId;

    @ApiModelProperty(value = "会员状态 1 正常、2 过期、3 暂停 4审核中， 5审核失败")
    private Integer carState;

    @ApiModelProperty(value = "组id")
    private Long groupId;

    @ApiModelProperty(value = "缴费标准")
    private Long feesRoleid;

    @ApiModelProperty(value = "起始日期")
    private Integer startDate;

    @ApiModelProperty(value = "结束日期")
    private Integer endDate;

    @ApiModelProperty(value = "月卡在审核中是否有效 0-无效 1-有效")
    private Integer memberAuditingValid;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    @ApiModelProperty(value = "创建日期")
    private Long createTime;

    @ApiModelProperty(value = "购买份数")
    private Integer buyNum;


}
