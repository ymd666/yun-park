package cn.tnar.parkservice.controller.common;

import cn.tnar.parkservice.model.dto.ExportDto;
import cn.tnar.parkservice.service.ExportService;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Api(tags = "导出(张志彪)")
@RestController
@Slf4j
public class ExportController {

    @Autowired
    ExportService exportService;

    @PostMapping(value = "export")
    public void export(@RequestParam String json, HttpServletResponse response) throws Exception {
        try {
            log.info("导出excel请求参数：" + json);
            ExportDto exportDto = JSON.parseObject(json, ExportDto.class);
            switch (exportDto.getService()) {
                //卡组管理-访客证
                case "visit_card":
                    log.info("开始导出访客证excel.....");
                    exportService.getVisit(exportDto, response);
                    log.info("结束导出访客证excel.....");
                    break;
                //卡组管理-白名单证、黑名单证
                case "white_black_card":
                    log.info("开始导出白名单证、黑名单证excel.....");
                    exportService.getWhiteBlack(exportDto, response);
                    log.info("结束导出白名单证、黑名单证excel.....");
                    break;
                //卡组管理-自定义卡
                case "other_card":
                    log.info("开始导出自定义卡excel.....");
                    exportService.getOther(exportDto, response);
                    log.info("结束导出自定义卡excel.....");
                    break;
                // 新版会员导入模版下载
                case "member_demo_down":
                    log.info("开始下载新版会员导入模版.....");
                    exportService.down(exportDto.getFilename(), response);
                    log.info("结束下载新版会员导入模版.....");
                    break;
                //客户管理-访客审批导出
                case "visit_apply":
                    log.info("开始导出访客审批excel.....");
                    exportService.getVisitApply(exportDto, response);
                    log.info("结束导出访客审批excel.....");
                    break;
                //客户管理-访客权限导出
                case "visit_juris":
                    log.info("开始导出访客权限excel.....");
                    exportService.getVisitJurisdiction(exportDto, response);
                    log.info("结束导出访客权限excel.....");
                    break;
                //订单管理-订单查询  总数不能大于10000
                case "query_order":
                    log.info("开始导出订单查询excel.....");
                    exportService.queryOrder(exportDto, response);
                    log.info("结束导出订单查询excel.....");
                    break;
                //订单管理-欠费记录(type=1,2,3对应tab页)
                case "query_arrearage":
                    log.info("开始导出欠费记录excel.....");
                    exportService.queryArrearage(exportDto, response);
                    log.info("结束导出欠费记录excel.....");
                    break;
                //订单管理-异常订单
                case "query_abNormal":
                    log.info("开始导出异常订单excel.....");
                    exportService.queryAbNormal(exportDto, response);
                    log.info("结束导出异常订单excel.....");
                    break;
                //订单管理-停车流水
                case "query_record":
                    log.info("开始导出停车流水excel.....");
                    exportService.queryRecord(exportDto, response);
                    log.info("结束导出停车流水excel.....");
                    break;
                //客户管理-交费查询
                case "payment_query":
                    log.info("开始导出交费查询excel.....");
                    exportService.getPaymentQuery(exportDto, response);
                    log.info("结束导出交费查询excel.....");
                    break;
                //人员管理-收费统计-收费员排班报表
                case "toll_shift_title":
                    log.info("开始导出收费员排班报表excel.....");
                    exportService.getTollShift(exportDto, response);
                    log.info("结束导出收费员排班报表excel.....");
                    break;
                //人员管理-收费统计-日统计报表
                case "day_statistics":
                    log.info("开始导出日统计报表excel.....");
                    exportService.getDayStatistics(exportDto, response);
                    log.info("结束导出日统计报表excel.....");
                    break;
                //人员管理-考勤管理-出勤统计
                case "attendance_statistics":
                    log.info("开始导出出勤统计excel.....");
                    exportService.getAttendanceStatistics(exportDto, response);
                    log.info("结束导出出勤统计excel.....");
                    break;
                // 人员管理-考勤管理-收费员账号
                case "toll_account_title":
                    log.info("开始导出收费员账号excel.....");
                    exportService.getTollAccount(exportDto, response);
                    log.info("结束导出收费员账号excel.....");
                    break;
                //财务管理-电子发票统计
                case "invoice_statistics":
                    log.info("开始导出电子发票统计excel.....");
                    exportService.queryInvoiceStatistics(exportDto, response);
                    log.info("结束导出电子发票统计excel.....");
                    break;
                //财务管理-财务报表
                case "financial_statements":
                    log.info("开始导出财务报表excel.....");
                    exportService.queryFinancialStatements(exportDto, response);
                    log.info("结束导出财务报表excel.....");
                    break;
            }
        } catch (Exception e) {
            log.error("导出报表异常", e);
        }
    }

    @PostMapping(value = "importMember")
    public ResultJson importMember(@RequestParam("file") MultipartFile file, @RequestParam String json) {
        return exportService.importMember(file, json);

    }

    @ApiOperation(value = "导入延期车位-只针对一位一车")
    @PostMapping(value = "importDeferMember")
    public ResultJson importDeferMember(@RequestParam("file") MultipartFile file, @RequestParam String json) {
        return exportService.importDeferMember(file, json);

    }

    @ApiOperation(value = "批量导入白名单")
    @PostMapping(value = "importWhiteMember")
    @Transactional
    public ResultJson importWhiteMember(@RequestParam("file") MultipartFile file, @RequestParam String json) {
        return exportService.importWhiteMember(file, json);
    }


    @ApiOperation(value = "批量延期白名单")
    @PostMapping(value = "importDeferWhiteMember")
    @Transactional
    public ResultJson importDeferWhiteMember(@RequestParam("file") MultipartFile file, @RequestParam String json) {
        return exportService.importDeferWhiteMember(file, json);

    }
}
