package cn.tnar.parkservice.config;


/**
 * 常量
 */
public class Constant {


    /**
     * redis-OK
     */
    public final static String OK = "OK";

    /**
     * redis过期时间，以秒为单位，一分钟
     */
    public final static int EXRP_MINUTE = 60;

    /**
     * redis过期时间，以秒为单位，一小时
     */
    public final static int EXRP_HOUR = 60 * 60;

    /**
     * redis过期时间，以秒为单位，一天
     */
    public final static int EXRP_DAY = 60 * 60 * 24;

    /**
     * JWT-account:
     */
    public final static String ACCOUNT = "account";

    /**
     * JWT-currentTimeMillis:
     */
    public final static String CURRENT_TIME_MILLIS = "currentTimeMillis";

    /**
     * PASSWORD_MAX_LEN
     */
    public static final Integer PASSWORD_MAX_LEN = 8;


    /**==============================  C业务接口定义   ====================================**/

    /**
     * 新增会员接口
     */
    public static final String C_ADD_MEMBER = "769548";

    /**
     * 删除会员接口
     */
    public static final String C_DELETE_MEMBER = "769577";

    //字典查询
    public static final String C_QUERY_DICTIONARY = "81102001";
    /**
     * 收费统计查询接口
     * 收费员排班报表统计（85403013）
     * 日统计报表（85403012）
     */
    public static final String C_TOLL_STATISTICS = "85403013";
    public static final String C_Daily_STATISTICS = "85403012";

    /**
     * 收费员账号相关接口
     * 增（85105001）
     * 删（85105002）
     * 改（85105003）
     * 查（85105004）
     */
    public static final String C_TOLL_ACCOUNT_ADD = "85105001";
    public static final String C_TOLL_ACCOUNT_DELETE = "85105002";
    public static final String C_TOLL_ACCOUNT_UPDATE = "85105003";
    public static final String C_TOLL_ACCOUNT_QUERY = "85105004";

    /**
     * 角色权限管理相关接口
     * 增加角色（81404001）
     * 删除角色（81404002）
     * 修改角色（81404003）
     * 查询角色（81404004）
     * 树形结构-查询所有权限（81103114）
     * 树形结构-查询已选中权限（81303124）
     * 角色添加权限（81303125）
     */
    public static final String C_ROLE_ADD = "81404001";
    public static final String C_ROLE_DELETE = "81404002";
    public static final String C_ROLE_UPDATE = "81404003";
    public static final String C_ROLE_QUERY = "81404004";
    public static final String C_PERMISSION_QUERYCHECKED = "81303124";
    public static final String C_PERMISSION_QUERYALL = "81103114";
    public static final String C_ROLE_ADDPERMISSION = "81303125";

    /**
     * 系统账号相关接口
     * 新增平台操作员账号（82101810）
     * 修改平台操作员账号（82101815）
     * 查询平台操作员账号（82101811）
     * 修改密码（82101803）
     * 查询机构角色（81202004）
     * 查询机构角色人员（81303004）
     * 系统账号-修改机构角色人员(81303003)
     * 为机构分配角色(81202001)
     */
    public static final String C_SYSTEM_ACCOUNT_ADD = "82101810";
    public static final String C_SYSTEM_ACCOUNT_UPDATE = "82101815";
    public static final String C_SYSTEM_ACCOUNT_QUERY = "82101811";
    public static final String C_SYSTEM_ACCOUNT_UPDATEPASSWORD = "82101803";
    public static final String C_QUERY_ORG_ROLE = "81202004";
    public static final String C_QUERY_ROLE_USER = "81303004";
    public static final String C_UPDATE_ORT_ROLE = "81303003";
    public static final String C_DISTRIBUTE_ROLE = "81202001";

    /**
     * 停车场信息接口
     * 85101004  根据运营商查询停车场基本信息
     * 85101003  修改停车场基础信息
     * 85101002  删除停车场基础信息
     * 85101001  新增停车场基础信息
     * 85101014  查询停车场扩展信息
     * 85101013  修改停车场扩展信息
     * 85101011  新增停车场扩展信息
     */
    public static final String C_PARK_INFO_QUERY = "85101004";
    public static final String C_PARK_INFO_UPDATE = "85101003";
    public static final String C_PARK_INFO_DELETE = "85101002";
    public static final String C_PARK_INFO_ADD = "85101001";
    public static final String C_PARK_EXTEND_INFO_QERUY = "85101014";
    public static final String C_PARK_EXTEND_INFO_UPDATE = "85101013";
    public static final String C_PARK_EXTEND_INFO_ADD = "85101011";

    /**
     * 停车场区域信息
     * 85103001  新增停车场区域信息
     * 85103002  删除停车场区域信息
     * 85103003  修改停车场区域信息
     * 85103004  查询停车场区域信息
     * 85103005  查询停车场区域信息及其收费索引信息
     * <p>
     * 85103008  设置区域类型
     */
    public static final String C_PARK_REGION_ADD = "85103001";
    public static final String C_PARK_REGION_DELETE = "85103002";
    public static final String C_PARK_REGION_UPDATE = "85103003";
    public static final String C_PARK_REGION_QUERY = "85103004";
    public static final String C_PARK_REGION_FEE_INDEX = "85103005";
    public static final String C_PARK_SET_REGION_TYPE = "85103008";

    /**
     * 停车场计费索引
     * 85102001   添加停车场计费索引
     * 85102002   删除停车场计费索引
     * 85102003   修改停车场计费索引
     * 85102004   查询停车场计费索引
     * 81101002   全国区域查询
     */
    public static final String C_PARK_FEE_INDEX_ADD = "85102001";
    public static final String C_PARK_FEE_INDEX_DELETE = "85102002";
    public static final String C_PARK_FEE_INDEX_UPDATE = "85102003";
    public static final String C_PARK_FEE_INDEX_QUERY = "85102004";
    public static final String C_COUNTRY_REGION_QUERY = "81101002";

    /**
     * 停车场计费信息
     * 85102011   添加停车场计费信息
     * 85102012   删除停车场计费信息
     * 85102013   修改停车场计费信息
     * 85102014   查询停车场计费信息
     * 85102015   停车场计费规则试算
     */
    public static final String C_PARK_FEE_INFO_ADD = "85102011";
    public static final String C_PARK_FEE_INFO_DELETE = "85102012";
    public static final String C_PARK_FEE_INFO_UPDATE = "85102013";
    public static final String C_PARK_FEE_INFO_QUERY = "85102014";
    public static final String C_PARK_FEE_RULE_TEST = "85102015";

    /**
     * 停车场车位管理
     * 85103026  查询停车场所有车位信息及其附加信息
     * 85103021  新增停车场车位信息
     * 85103023  修改停车场车位信息
     * 85103022  删除停车场车位信息
     * 85103027  批量新增车位
     */
    public static final String C_PARK_STALL_ADD = "85103021";
    public static final String C_PARK_STALL_DELETE = "85103022";
    public static final String C_PARK_STALL_UPDATE = "85103023";
    public static final String C_PARK_STALL_QUERY = "85103026";
    public static final String C_PARK_STALL_BATCHADD = "85103027";

    /**
     * 停车场出入口管理
     * 85103014  查询停车场出入口信息
     * 85103011  新增停车场出入口信息
     * 85103013  修改停车场出入口信息
     * 85103012  删除停车场出入口信息
     */
    public static final String C_PARK_ACCESS_ADD = "85103011";
    public static final String C_PARK_ACCESS_DELETE = "85103012";
    public static final String C_PARK_ACCESS_UPDATE = "85103013";
    public static final String C_PARK_ACCESS_QUERY = "85103014";

    /**
     * 月卡车收费标准
     * 82202070   新增月卡车收费标准记录
     * 82202071   修改月卡车收费标准记录
     * 82202072   删除月卡车收费标准记录   删除多卡多车
     * 82202073   查询月卡车收费标准记录
     */
    public static final String C_MEMBER_CAR_FEE_SCALE_ADD = "82202070";
    public static final String C_MEMBER_CAR_FEE_SCALE_DELETE = "82202072";
    public static final String C_MEMBER_CAR_FEE_SCALE_UPDATE = "82202071";
    public static final String C_MEMBER_CAR_FEE_SCALE_QUERY = "82202073";

    /**
     * 订单管理
     * 83201005  通过客户号查询商品交易订单信息
     * 85404003  逃逸流水-交易流水
     * 85404004   逃逸补交流水-交易流水
     * 85450011  查询异常订单信息
     * 85404021  坏账查询
     * 85404101  当日停车流水-交易流水
     * 85404102  历史停车流水-交易流水
     */
    public static final String C_ORDER_ListInfo = "83201005";
    public static final String C_ORDER_ARREARAGE = "85404003";
    public static final String C_ORDER_ARREARAGE_PAY = "85404004";
    public static final String C_ORDER_ABNORMAL = "85450011";
    public static final String C_ORDER_BADDEBT = "85404021";
    public static final String C_DAILY_PARK_RECORD = "85404101";
    public static final String C_HISTORY_PARK_RECORD = "85404102";


    /**===============================分组会员常量============================**/
    /**
     * 月卡分组 初始化目录 常量
     */
    public static final String PARKING_CAR = "临停车";
    public static final String VISITOR_CAR = "访客车";
    public static final String WHITE_LIST = "白名单";
    public static final String BLACK_LIST = "黑名单";

    /**
     * 月卡分组 初始化子卡 常量
     */
    public static final String PARKING_CAR_CARD = "临停车证";
    public static final String VISITOR_CAR_CARD = "访客车证";
    public static final String WHITE_LIST_CARD = "白名单证";
    public static final String BLACK_LIST_CARD = "黑名单证";

    /**
     * 月卡类别
     */
    public static final Integer ROOT_CLASSIFY = 0;   //根节点 类别 0
    public static final Integer PARKING_CAR_CLASSIFY = 1; //临停车 类别 1
    public static final Integer VISITOR_CAR_CLASSIFY = 2; //访客车 类别 2
    public static final Integer WHITE_LIST_CLASSIFY = 3;  //白名单 类别 3
    public static final Integer BLACK_LIST_CLASSIFY = 4;  //黑名单 类别 4
    public static final Integer CUSTOM_CAR_CLASSIFY = 5;//用户自定义 类别 5


    /**
     * 卡组下发相关常量
     */
    public static final String EXCHANGE_NAME = "amq.topic";
    public static final String SPOT = ".";
    public static final String MEMBERGROUP_QUEUENAME_PREFIX = "memberGroup" + SPOT;
    public static final String PARKCALENDAR_QUEUENAME_PREFIX = "parkCalendar" + SPOT;
    public static final String FEEINDEX_QUEUENAME_PREFIX = "parkFeeIndex" + SPOT;
    public static final String TPARKFEE_QUEUENAME_PREFIX = "parkBilling" + SPOT;
    public static final String FEESROLEINFO_QUEUENAME_PREFIX = "feesroleinfo" + SPOT;


    /**
     * 财务管理相关接口
     * 83800034  可提现金额
     * 85403014  停车流水
     * 82202121  查询月卡会员变更记录
     * 83800033  提现记录
     * 83800101  查询清算账户资金变动记录
     */
    public static final String CAN_WIHTDRAW = "83800034";
    public static final String PARK_BILL = "85403014";
    public static final String PARK_CAR_MEMBERMOUUT = "82202121";
    public static final String WITHDRAW_RECORD = "83800033";
    public static final String CLEAR_ACCOUNT_RECORD = "83800101";

    /**
     * 新版会员相关接口
     * 87500001  新增会员基本信息
     * 87500021  新增会员车牌
     * 87500011  新增会员车位
     * 87500013  删除车位
     * 87500003  会员修改
     * 87500023  查询车牌信息
     * 87500104  修改校验车牌
     * 87500022  删除车牌
     * 87500012  车位信息
     * 87500015  暂停车位
     * 87500014  启用车位
     * 87500017  延期车位
     * 87500016  确定延期车位
     */
    public static final String MEMBER_INFO = "87500001";
    public static final String MEMBER_CARID = "87500021";
    public static final String MEMBER_SEAT = "87500011";
    public static final String MEMBER_DELETE_SEAT = "87500013";
    public static final String MEMBER_UPDATE = "87500003";
    public static final String MEMBER_SELECT_CARID = "87500023";
    public static final String MEMBER_CHECK_CARID = "87500104";
    public static final String MEMBER_DELETE_CARID = "87500022";
    public static final String MEMBER_SELECT_SEAT = "87500012";
    public static final String MEMBER_STOP_SEAT = "87500015";
    public static final String MEMBER_START_SEAT = "87500014";
    public static final String MEMBER_DEFER_SEAT = "87500017";
    public static final String MEMBER_DEFER_SEAT_CONFIRM = "87500016";


    /**
     * ResultJson_MSG常量
     */

    public static final String MSG_CAR_EXISTS = "该车牌已存在！";
    public static final String MSG_RULE_FAIL = "解除绑定卡组失败";
    public static final String MSG_REGION_NULL = "停车场无区域";
    public static final String MSG_CONFIG_YES = "该规则已绑定子卡";
    public static final String MSG_CONFIG_NO = "该规则未绑定子卡";
    public static final String MSG_RULE_ERROR = "无该准入规则";
    public static final String MSG_MEMBER_ERROR = "无该白名单";
    public static final String MSG_DAYS_ERROR = "暂停天数输入错误";
    public static final String MSG_YEAR_ERROR = "请输入年份";
    public static final String MSG_EXCEL_NULL = "数据不能为空";
    public static final String MSG_EXCEL_ONLY = "导入失败,只能输入同一年数据";
    public static final String MSG_EXCEL_EXISTS = "已存在该年份日历";
    public static final String MSG_EXCEL_NOEXISTS = "不存在该年份日历";
    public static final String MSG_EXCEL_TYPE = "文件不是excel类型";
    public static final String MSG_EXCEL_SPACE = "导入失败,请删除空白行";
    public static final String MSG_PARKCODE_NULL = "停车场编号不能为空";
    public static final String MSG_FEEROLEID_NULL = "卡组计费规则不能为空";
    public static final String MSG_FEEROLEID_MONEY = "卡组计费规则金额不一致不能转移";
    public static final String MSG_MEMBERINFOID_NULL = "请选择转移会员";
    public static final String MSG_MEMBERGROUP_NULL = "请选择转移子卡";
    public static final String MSG_CURRENTMEMBERGROUP_NULL = "当前子卡不能为空";
    public static final String MSG_GATECONFIG_NULL = "区域配置不能为空";
    public static final String MSG_PAY_MONTH= "预支付购买停车场月卡套餐";
    public static final String MSG_CAR_MAX= "该子卡下已存在该车牌";
    public static final String MSG_RULE_CANCEL= "子卡与配置规则id不匹配";

    /**
     * LOG_MSG常量
     */
    public static final String ERROR_MSG_SNO = "获取主键异常";
    public static final String ERROR_MSG_DATE = "日期格式化异常";
    public static final String ERROR_MSG_AMOUNT = "存储会员变更表异常";
    public static final String ERROR_MSG_GROUPLOG = "存储会员操作记录表异常";
    public static final String ERROR_MSG_CARINFO = "根据会员id获取车牌异常";
    public static final String ERROR_MSG_INSERT_MEMBER = "新增新版会员信息异常";
    public static final String ERROR_MSG_INSERT_CARID = "新增新版会员车牌异常";
    public static final String ERROR_MSG_INSERT_SEAT = "新增新版会员车位异常";
    public static final String ERROR_MSG_UPDATENEW = "新版会员修改异常";
    public static final String ERROR_MSG_DELETE_SEAT = "新版会员删除车位异常";
    public static final String ERROR_MSG_SELECT_CARID = "新版会员查询车牌异常";
    public static final String ERROR_MSG_CHECK_CARID = "新版会员校验车牌异常";
    public static final String ERROR_MSG_DELETE_CARID = "新版会员删除车牌异常";
    public static final String ERROR_MSG_SELECT_SEAT = "新版会员查询车位异常";
    public static final String ERROR_MSG_STOP_SEAT = "新版会员暂停车位异常";
    public static final String ERROR_MSG_START_SEAT = "新版会员启用车位异常";
    public static final String ERROR_MSG_DEFER_SEAT = "新版会员延期车位异常";
    public static final String ERROR_MSG_Prepare_Order = "月卡缴费预下单失败";
    public static final String ERROR_MSG_FEEROLEID_AMOUNT = "会员变更表转移时更新计费规则失败";
    public static final String ERROR_MSG_FEEROLEID_SEAT = "车位表转移时更新计费规则失败";
    public static final String ERROR_MSG_MQ = "MQ推送失败{}";

    /**
     * 会员下发oper_type
     * <p>
     * 新增车牌0
     * 新增会员车位信息 1
     * 删除会员车位/车牌 2（传车牌）
     * 启用车位 3
     * 暂停车位 4
     * 车位延期 5
     * 修改会员信息 6
     */
    public static final String OPER_CAR = "0";
    public static final String OPER_INSERT = "1";
    public static final String OPER_DELETE = "2";
    public static final String OPER_START = "3";
    public static final String OPER_STOP = "4";
    public static final String OPER_DEFER = "5";
    public static final String OPER_UPDATE = "6";
    /**
     * 会员下发member_type
     * <p>
     * 白名单 1
     * 黑名单 7改为1
     * 新版会员 3
     */
    public static final String MEMBER_TYPE_WHITE = "3";
    public static final String MEMBER_TYPE_BLACK = "3";
    public static final String MEMBER_TYPE_NEW = "3";
    public static final String MEMBER_TYPE_VISIT = "3";

    /**
     * 登录接口
     */

    public static final String C_LOGIN_INFO = "82101003";
    public static final String C_OPERATOR_INFO = "82101813";
    public static final String C_MENU_INFO = "81103110";


    /**
     * 资产编号
     */
    public static final String ASSETS_THIRD_CASH_PUSH = "01020101";//三方现金充值


    /**
     * 线下优惠劵
     * 82205041 创建线下优惠券
     * 82205042 删除线下优惠券
     * 82205043 更新线下优惠券
     * 82205051 使用统计量
     * 82205044 线下券列表
     */
    public static final String CREATE_OFFLINE_COUPON = "82205041";
    public static final String DELETE_OFFLINE_COUPON = "82205042";
    public static final String UPDATE_OFFLINE_COUPON = "82205043";
    public static final String USER_STATISTICS = "82205051";
    public static final String OFFLINE_COUPON_LIST = "82205044";


    /**
     * 线下优惠券实时下发队列名称
     */
    public static final String OFFLINE_COUPON_PREFIX = "offlineCoupon" + SPOT;


    public static final String API_BUY_MONTH_ORDER = "87401027";

}
