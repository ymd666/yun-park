package cn.tnar.parkservice.controller.customer;


import cn.tnar.parkservice.service.AccessRuleService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = "准入规则(张志彪)")
@RestController
@RequestMapping("service/accessRule")
@Slf4j
public class AccessRuleController {

    @Autowired
//    @Qualifier("accessRuleServiceImpl")
    private AccessRuleService accessRuleService;

    /**
     * 新增,修改,删除,分页查询,查询单个
     *
     * @param json
     * @return
     **/

    @ApiOperation(value = "新增准入规则 - t_park_access_rule")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody String json) {
        return accessRuleService.insert(json);
    }


    @ApiOperation(value = "修改单个入场规则 - t_park_access_rule")
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody String json) {
        return accessRuleService.update(json);
    }

    @ApiOperation(value = "删除单个入场规则 - t_park_access_rule")
    @PostMapping(value = "delete")
    public ResultJson delete(@RequestBody String json) {
        return accessRuleService.delete(json);
    }

    @ApiOperation(value = "分页查询入场规则 - t_park_access_rule")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody String json) {
        return accessRuleService.query(json);
    }

    @ApiOperation(value = "查询单个入场规则详情 - t_park_access_rule")
    @PostMapping(value = "queryOne")
    public ResultJson queryOne(@RequestBody String json) {
        return accessRuleService.queryOne(json);
    }

    @ApiOperation(value = "子卡绑定准入规则 - t_park_access_rule")
    @PostMapping(value = "configRule")
    public ResultJson configRule(@RequestBody String json) {
        return accessRuleService.configRule(json);

    }

    @ApiOperation(value = "修改准入规则前判断是否已绑定子卡- t_park_access_rule")
    @PostMapping(value = "updateYes")
    public ResultJson updateYes(@RequestBody String json) {
        return accessRuleService.updateYes(json);

    }

    @ApiOperation(value = "准入规则新增时区域入口显示 - t_park_access_rule")
    @PostMapping(value = "getRegionList")
    public ResultJson getRegionList(@RequestBody String json) {
        return accessRuleService.getRegionList(json);
    }

    @ApiOperation(value = "取消已绑定的准入规则 - t_park_access_rule")
    @PostMapping(value = "cancelRule")
    public ResultJson cancelRule(@RequestBody String json) {
        return accessRuleService.cancelRule(json);
    }

}
