package cn.tnar.parkservice.model.dto;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年11月18日 15:27:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SyncMemberDto {

    private List<MemberinfoMqDto> members;

    private List<TParkMemberGroupRelationDto> memberGroupRelations;

}
