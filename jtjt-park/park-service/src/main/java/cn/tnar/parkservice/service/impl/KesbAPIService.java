package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.pms.kesb.KesbClient;
import cn.tnar.pms.kesb.KesbParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class KesbAPIService implements KesbApiService {

    @Autowired
    private KesbClient kesbClient;

    @Override
    public Map<String,String> queryKesbApi(String serviceId, Map<String,Object> req){
        Map<String,String> resp = new HashMap<>();
        KesbParam params = new KesbParam();
        req.forEach((k,v)->params.kput(k,v.toString()));
        this.buildAPIParams(params);
        try {
            resp = kesbClient.request(serviceId,params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  resp;
    }

    @Override
    public List<Map<String, String>> queryKesbApiList(String serviceId, Map<String,Object> req){
        List<Map<String, String>> resp = new ArrayList<>();
        KesbParam params = new KesbParam();
        req.forEach((k,v)->params.kput(k,v.toString()));
        this.buildAPIParams(params);
        try {
            resp = kesbClient.requestList(serviceId,params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  resp;
    }

    @Override
    public Map<String, String> queryKesbApiByNestedMap(String serviceId, Map<String, Object> req) {
        Map<String,String> resp = new HashMap<>();
        KesbParam params = new KesbParam();
        Map<String,String> region_code = (Map<String,String>)req.get("region_code");
        req.forEach((k,v)->params.kput(k,v.toString()));
        params.put("region_code",region_code);
        this.buildAPIParams(params);
        this.buildAPIParams(params);
        try {
            resp = kesbClient.request(serviceId,params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  resp;
    }


    public void buildAPIParams(KesbParam params){
        params.kput("g_sysid", "8");
        params.kput("g_menuid", "0");
        params.kput("g_srcnodeid", "01");
        params.kput("g_dstnodeid", "01");
        params.kput("g_custpwd", "1");
        params.kput("g_userway", "1");
        params.kput("g_stationaddr", "1");
    }
}
