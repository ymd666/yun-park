package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @author xzhang
 * @date 2019/9/7
 */
@Data
public class CouponRequest {
    private String carNo;
    private String carType;
    private String parkNo;
}
