package cn.tnar.parkservice.model.dto;

import lombok.Data;

/**
 * 车牌颜色构成
 *
 * @author Tiyer.Tao
 * @since 2018-10-15
 */
@Data
public class TimeDto {

    private int num = 0;

    private int time;

}
