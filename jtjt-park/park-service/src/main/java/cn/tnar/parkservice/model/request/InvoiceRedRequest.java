package cn.tnar.parkservice.model.request;

/**
 * Created by Administrator on 2017/6/14.
 */
public class InvoiceRedRequest {
    private String taxpayer_num;
    private String b_trade_no;
    private String g_trade_no	;

    private String serialNo	;

    public InvoiceRedRequest(){

    }

    public InvoiceRedRequest(String taxpayer_num, String b_trade_no, String g_trade_no, String serialNo) {
        this.taxpayer_num = taxpayer_num;
        this.b_trade_no = b_trade_no;
        this.g_trade_no = g_trade_no;
        this.serialNo = serialNo;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getTaxpayer_num() {
        return taxpayer_num;
    }

    public void setTaxpayer_num(String taxpayer_num) {
        this.taxpayer_num = taxpayer_num;
    }

    public String getB_trade_no() {
        return b_trade_no;
    }

    public void setB_trade_no(String b_trade_no) {
        this.b_trade_no = b_trade_no;
    }

    public String getG_trade_no() {
        return g_trade_no;
    }

    public void setG_trade_no(String g_trade_no) {
        this.g_trade_no = g_trade_no;
    }
}
