package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.TParkGroupCarinfo;
import cn.tnar.parkservice.model.request.TParkGroupMemberinfoRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface TParkGroupCarinfoMapper extends BaseMapper<TParkGroupCarinfo> {
    @Select("SELECT  c.id from  t_park_group_carinfo c  WHERE  c.delflag=1   and car_id=#{carId} and car_type=#{carType} and c.group_id in (${memberinfoIds})")
    List<Map<String,Object>> checkCarId(@Param("carId") String carId,@Param("carType") Integer carType, @Param("memberinfoIds") String memberinfoIds);

    @Select("SELECT  c.id from  t_park_group_carinfo c  WHERE  c.delflag=1  and c.group_id in (${memberinfoIds})")
    List<Map<String,Object>> getAllCarIdByMemberIds( @Param("memberinfoIds") String memberinfoIds);


    List<Map<String,Object>> selectBaiList(@Param("map") TParkGroupMemberinfoRequest map);

    List<Map<String,Object>> selectMemberList(@Param("map") TParkGroupMemberinfoRequest map);

}
