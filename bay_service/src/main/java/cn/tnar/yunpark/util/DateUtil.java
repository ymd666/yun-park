package cn.tnar.yunpark.util;

import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 时间转化类
 * 
 * @author z
 * 
 */
public class DateUtil {

	public static Date strUTCToDate(String str) {

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}


	/**
	 * 字符串转日期(yyyyMMddHHmmss)
	 */
	public static Date stringToDate14(String str) throws RuntimeException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = null;
		try {
			if (StringUtils.isNotBlank(str)) {
				date = sdf.parse(str);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	/**
	 * 字符串转日期(yyyyMMdd)
	 */
	public static Date stringToDate8(String str) throws RuntimeException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = null;
		try {
			if (StringUtils.isNotBlank(str)) {
				date = sdf.parse(str);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	/**
	 * 字符串转日期
	 */
	public static Date stringToDateByPattern(String str, String pattern) throws RuntimeException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			if (StringUtils.isNotBlank(str)) {
				date = sdf.parse(str);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 日期转字符串(yyyyMMddHHmmss)
	 */
	public static String dateToString14(Date date) throws RuntimeException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String dateStr = "";
		if (date != null) {
			dateStr = sdf.format(date);
		}
		return dateStr;
	}
	
	/**
	 * 日期转字符串(yyyyMMdd)
	 */
	public static String dateToString8(Date date) throws RuntimeException {
		return getStringFromDate(date, "yyyyMMdd");
	}

	/**
	 * 日期转字符串(yyyyMMdd)
	 */
	public static String dateToString6(Date date) throws RuntimeException {
		return getStringFromDate(date, "HHmmss");
	}

	/**
	 * 日期转字符串
	 */
	public static String dateToStringPattern(Date date, String pattern) throws RuntimeException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String dateStr = "";
		if (date != null) {
			dateStr = sdf.format(date);
		}
		return dateStr;
	}

	/**
	 * 格式化时间(yyyy-MM-dd HH:mm)
	 */
	public static String getTime(Date date) throws RuntimeException {
		return getStringFromDate(date, "yyyy-MM-dd HH:mm");
	}
	
	/**
	 * 格式化时间(yyyy-MM-dd HH:mm:ss)
	 */
	public static String getTimeToSS(Date date) throws RuntimeException {
		return getStringFromDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 格式化时间(MM/dd HH:mm)
	 */
	public static String getTime2(Date date) throws RuntimeException {
		return getStringFromDate(date, "MM/dd HH:mm");
	}

	/**
	 * 格式化时间(yyyyMMddHHmm)
	 */
	public static String getTime3(Date date) throws RuntimeException {
		return getStringFromDate(date, "yyyyMMddHHmm");
	}

	/**
	 * 格式化日期(yyyy-MM-dd)
	 */
	public static String getDate(Date date) throws RuntimeException {
		return getStringFromDate(date, "yyyy-MM-dd");
	}

	/**
	 * 日期转字符串
	 */
	public static String getStringFromDate(Date date, String pattern) throws RuntimeException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		String result = "";
		if (date != null) {
			result = dateFormat.format(date);
		}
		return result;
	}

	/**
	 * 获取距离当前时间
	 */
	public static String getPassTime(Date date) throws RuntimeException {
		String str = "";

		long now = new Date().getTime();
		long difference = now - date.getTime();
		long second = 1000l;
		long minute = 60000l;
		long hour = 3600000l;
		long day = 86400000l;
		long month = 2592000000l;
		long year = 31536000000l;

		if (difference < minute) {
			str = difference / second + "秒前";
		} else if (difference < hour) {
			str = difference / minute + "分" + difference % minute / second + "秒前";
		} else if (difference < day) {
			str = difference / hour + "小时" + difference % hour / minute + "分前";
		} else if (difference < month) {
			str = difference / day + "天" + difference % day / hour + "小时前";
		} else if (difference < year) {
			str = difference / month + "月" + difference % month / day + "天前";
		} else {
			str = difference / year + "年" + difference % year / month + "月前";
		}
		return str;
	}

	/**
	 * 分钟转时间字符串
	 */
	public static String minute2Time(long num) {
		String str = "";
		long difference = num;
		long hour = 60l;
		long day = 1440l;
		long month = 43200l;
		long year = 525600l;

		if (difference < hour) {
			str = num + "分钟";
		} else if (difference < day) {

			String min_val = difference % hour != 0 ? (difference % hour + "分钟") : "";
			str = (difference / hour + "小时") + min_val;

		} else if (difference < month) {

			String min_val = difference % hour != 0 ? (difference % hour + "分钟") : "";
			String hour_val = (difference % day / hour) != 0 ? (difference % day / hour) + "小时" : "";
			str = (difference / day + "天") + hour_val + min_val;

		} else if (difference < year) {

			String hour_val = (difference % day / hour) != 0 ? (difference % day / hour) + "小时" : "";
			String day_val = (difference % month / day) != 0 ? (difference % month / day) + "天" : "";
			str = (difference / month + "月") + day_val + hour_val;

		} else {

			String day_val = (difference % month / day) != 0 ? (difference % month / day) + "天" : "";
			String month_val = (difference % year / month) != 0 ? (difference % year / month) + "月" : "";
			str = difference / year + "年" + month_val + day_val;

		}
		return str;
	}

	/**
	 * 收费时段
	 */
	public static String getFeeShortTime(String time) throws RuntimeException {
		String result = "";
		if (StringUtils.isNotBlank(time) && time.length() == 6) {
			result = time.substring(0, 2) + ":" + time.substring(2, 4);
		}
		return result;
	}

	/**
	 * 字符串转Calendar(yyyyMMddHHmmss)
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static Calendar getCalendar(String str) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(DateUtil.stringToDate14(str));
		return cal;
	}

	/**
	 * 计算两个日期字符串相隔天数(yyyyMMdd)
	 */
	public static int daysBetween(String sDate, String eDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(sDate));
		long time1 = cal.getTimeInMillis();

		cal.setTime(sdf.parse(eDate));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);
		return Integer.parseInt(String.valueOf(between_days));
	}
	
	/**
	 * 计算两个日期字符串相隔天数(yyyyMMddHHmm)
	 */
	public static int daysBetween2(String sDate, String eDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(sDate));
		long time1 = cal.getTimeInMillis();

		cal.setTime(sdf.parse(eDate));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);
		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 获取去年年份
	 */
	public static List<Integer> getLatestYear(int num) {
		List<Integer> years = new ArrayList<Integer>();
		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i < num; i++) {
			years.add(calendar.get(Calendar.YEAR));
			calendar.add(Calendar.YEAR, -1);
		}
		return years;
	}

	/**
	 * 获取年份列表
	 */
	public static List<Integer> getYearList() {
		List<Integer> years = new ArrayList<Integer>();
		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i < 50; i++) {
			years.add(calendar.get(Calendar.YEAR));
			calendar.add(Calendar.YEAR, -1);
		}
		return years;
	}

	/**
	 * 获取月份列表
	 */
	public static List<DateUtilHelper> getMonthListClass() {
		List<DateUtilHelper> helpers = new ArrayList<DateUtilHelper>();
		for (int i = 1; i <= 12; i++) {
			DateUtilHelper helper = new DateUtilHelper();
			helper.setKey(i / 10 == 0 ? ("0" + i) : (i + ""));
			helper.setValue(i + "月");
			int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
			if (month == i) {
				helper.setSelected(true);
			}
			helpers.add(helper);
		}
		return helpers;
	}

	/**
	 * 获取月份列表
	 */
	public static List<String> getMonthList() {
		List<String> months = new ArrayList<String>();
		for (int i = 1; i <= 12; i++) {
			months.add(i / 10 == 0 ? ("0" + i) : String.valueOf(i));
		}
		return months;
	}

	/**
	 * 获取日期列表
	 */
	public static List<String> getDayList() {
		List<String> days = new ArrayList<String>();
		for (int i = 1; i <= 31; i++) {
			days.add(i / 10 == 0 ? ("0" + i) : String.valueOf(i));
		}
		return days;
	}

	/**
	 * 获取日期列表
	 */
	public static List<String> getDayList(int day) {
		List<String> days = new ArrayList<String>();
		for (int i = 1; i <= day; i++) {
			days.add(i / 10 == 0 ? ("0" + i) : String.valueOf(i));
		}
		return days;
	}

	
	/**
	 * 获取昨天日期
	 */
	public static String getYesterday() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -1);

		return sdf.format(cal.getTime());
	}
	
	/**
	 * 获取一月中的第几天
	 */
	public static int getDayByCalenday(Calendar cal){
		int month = -1;
		if(cal != null){
			month = cal.get(Calendar.DAY_OF_MONTH);
		}
		return month;
	}
	
	/**
	 * 获取明天的日期
	 * @param
	 * @return
	 */
	public static String getTomorrowDate(){
		String tomorrow = "";
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		Date date = calendar.getTime();
		tomorrow = dateToString8(date);
		return tomorrow;
	}
	
	/**
	 * 当前时间加减分钟
	 * @param min
	 * @return
	 */
	public static String getLastMinDate(int min){
		String lastMinDate = "";
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, min);
		Date date = calendar.getTime();
		lastMinDate = dateToString14(date);
		return lastMinDate;
	}
	
	/**
	 * 时间错：1442304019000
	*	转化成时间：2015-09-15 16:00:15
	 * @param stamp
	 * @return
	 */
	public static String stamp2Date(Long stamp){
			 Date nowTime = new Date(stamp);
	          SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
	          String retStrFormatNowDate = sdFormatter.format(nowTime);
	         return retStrFormatNowDate;
	}

	public static class DateUtilHelper {
		private Boolean selected = false;
		private String key;
		private String value;

		public Boolean getSelected() {
			return selected;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}
