package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TCarFeesroleinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:36:07
 */
@Repository
@Mapper
public interface TCarFeesroleinfoMapper extends BaseMapper<TCarFeesroleinfo> {

}
