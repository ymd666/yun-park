package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;


@TableName("t_park_day_parking_record")
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
@Data
public class TParkDayParkingRecord extends Model<TParkDayParkingRecord> {

    @TableId("id")
    private long id;

    @TableField("node_id")
    private long nodeId;

    @TableField("agent_id")
    private long agentId;

    @TableField("park_code")
    private String parkCode;

    @TableField("region_code")
    private String regionCode;

    @TableField("parkname")
    private String parkname;

    @TableField("inbatch")
    private String inbatch;

    @TableField("serialno")
    private String serialno;

    @TableField("inoperno")
    private String inoperno;

    @TableField("outbatch")
    private String outbatch;

    @TableField("outoperno")
    private String outoperno;

    @TableField("trace_index")
    private String traceIndex;

    @TableField("tracetype")
    private long tracetype;

    @TableField("clienttype")
    private long clienttype;

    @TableField("seatnum")
    private long seatnum;

    @TableField("seat_code1")
    private String seatCode1;

    @TableField("seatno1")
    private String seatno1;

    @TableField("seat_code2")
    private String seatCode2;

    @TableField("seatno2")
    private String seatno2;

    @TableField("owner_cust_id")
    private long ownerCustId;

    @TableField("cartype")
    private long cartype;

    @TableField("car_id")
    private String carId;
    @TableField("carnocolor")
    private String carnocolor;

    @TableField("intime")
    private long intime;

    @TableField("ingateno_id")
    private String ingatenoId;

    @TableField("inphoto")
    private String inphoto;

    @TableField("insmallphoto")
    private String insmallphoto;

    @TableField("inconfidence")
    private long inconfidence;

    @TableField("outtime")
    private long outtime;

    @TableField("outgateno_id")
    private String outgatenoId;

    @TableField("outphoto")
    private String outphoto;

    @TableField("outsmallphoto")
    private String outsmallphoto;

    @TableField("outconfidence")
    private long outconfidence;

    @TableField("parktime")
    private long parktime;

    @TableField("paydatetime")
    private long paydatetime;

    @TableField("preamt")
    private double preamt;

    @TableField("parkamt")
    private double parkamt;
    @TableField("chargeamt")
    private double chargeamt;
    @TableField("paystate")
    private long paystate;
    @TableField("outtype")
    private long outtype;
    @TableField("chargemanno")
    private String chargemanno;
    @TableField("whitetype")
    private long whitetype;
    @TableField("result")
    private long result;
    @TableField("device_code")
    private String deviceCode;

    @TableField("pos_code")
    private String posCode;

    @TableField("reserve")
    private String reserve;

    @TableField("settledate")
    private long settledate;

    @TableField("inoperate")
    private long inoperate;

    @TableField("outoperate")
    private long outoperate;

    @TableField("uploadtime")
    private long uploadtime;

    @TableField("payflag")
    private long payflag;

    @TableField("tradeorderid")
    private long tradeorderid;

    @TableField("hisflag")
    private long hisflag;

    @TableField("memberdiscount")
    private long memberdiscount;

    @TableField("feedesc")
    private String feedesc;

    @TableField("settle")
    private long settle;

    @TableField("problem_flag")
    private long problemFlag;
    @TableField("submit_time")
    private long submitTime;

    @TableField("coupon_type")
    private long couponType;

    @TableField("coupon_amt")
    private double couponAmt;

    @TableField("offline_coupon_type")
    private long offlineCouponType;

    @TableField("offline_coupon_amt")
    private double offlineCouponAmt;

    @TableField("elec_coupon_type")
    private long elecCouponType;

    @TableField("elec_coupon_amt")
    private double elecCouponAmt;
    @TableField("pay_for_cloud")
    private long payForCloud;
    @TableField("in_car_id")
    private String inCarId;


//    private java.sql.Timestamp highIntime;

//    private java.sql.Timestamp highOuttime;
}
