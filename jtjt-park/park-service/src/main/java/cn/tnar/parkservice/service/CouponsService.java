package cn.tnar.parkservice.service;


import cn.tnar.parkservice.exception.CouponsException;
import cn.tnar.parkservice.model.request.ParkingInfo;
import cn.tnar.parkservice.model.request.RegRequest;

import java.util.List;
import java.util.Map;

/**
 * @author xzhang
 * @date 2019/9/6
 */
public interface CouponsService {

    public Map<String, String> registerTnarMember(RegRequest regRequest) throws CouponsException;

    public Map<String, String> getMobileCode(String mobile) throws CouponsException;

    public void bindCarPlateNo(String custId, String carPlateNo, String carType, String session) throws CouponsException;

    public List<ParkingInfo> queryParkingRecord(String custId, String carPlateNo, String carType) throws CouponsException;

    public void pushCouponsInfo(String parkCode, String carPlateNo, String carType) throws CouponsException;

    ParkingInfo calcFee(ParkingInfo parkingInfo) throws CouponsException;
}
