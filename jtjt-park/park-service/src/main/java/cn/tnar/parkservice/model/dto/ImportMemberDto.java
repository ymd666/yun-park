package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class ImportMemberDto {
    private String owner_name;
    private String tel_no;
    private String seat_num;
    private String buy_num;
    private String expire_time;
    private String car_type;
    private String car_id;
    private String satrtTime;
    private String endTime;
    private String amount;
    private String yn;
    private String memberinfoId;
    private String remark;
    private String memberType;
}
