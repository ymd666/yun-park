package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkRegion;
import cn.tnar.parkservice.model.response.RegionInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2018-12-08
 */
public interface TParkRegionMapper extends BaseMapper<TParkRegion> {

    List<RegionInfo> queryCardAndRegion(List<String> regions);

    @Select("select name FROM t_park_region where region_code in (${regionCodes})")
    List<String> getName(@Param("regionCodes") String regionCodes);

}
