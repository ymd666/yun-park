package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.model.dto.BadDebtExportDto;
import cn.tnar.parkservice.model.dto.OverduePaymentExportDto;
import cn.tnar.parkservice.model.dto.TParkEscapeDto;
import cn.tnar.parkservice.mapper.TParkEscapeMapper;
import cn.tnar.parkservice.model.entity.TParkEscape;
import cn.tnar.parkservice.service.ITParkEscapeService;
import cn.tnar.parkservice.util.DateUtil1;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static cn.tnar.parkservice.util.ExcelUtil.isNull;


/**
 * @author gl
 * @date 2018-10-22
 */
@Service
public class TParkEscapeServiceImpl extends ServiceImpl<TParkEscapeMapper, TParkEscape> implements ITParkEscapeService {

    @Override
    public List<TParkEscapeDto> getTParkEscapeList(Long startTime, Long endTime, String carId, String chargemanno, String parkCode) {
        return baseMapper.pageTParkEscapeList(startTime, endTime, carId, chargemanno, parkCode);
    }

    @Override
    public void exportTParkEscape(Long startTime, Long endTime, List<TParkEscapeDto> list, HttpServletResponse response) throws IOException, ParseException {

        String exportFileName = startTime.toString().substring(0, 8) + "-" + endTime.toString().substring(0, 8) + "逃逸名单.xlsx";

        //导出文件路径
//        String basePath = "C:\\Users\\Administrator\\Desktop\\"+exportFileName;

        // 声明一个工作薄
        XSSFWorkbook workBook = null;
        workBook = new XSSFWorkbook();
        // 生成一个表格
        XSSFSheet sheet = workBook.createSheet();

        CellStyle cellStyle = workBook.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        XSSFFont font = workBook.createFont();
        font.setFontName("黑体");
        font.setFontHeightInPoints((short) 16);

        workBook.setSheetName(0, "逃逸列表");
        // 创建表格标题行 第一行
        XSSFRow titleRow = sheet.createRow(0);
        String[] title = {"序号", "车牌号", "收费员", "停车场", "停车区域", "入场时间", "出场时间", "停车时长", "应收金额", "实收金额", "是否缴费"};
        for (int i = 0; i < title.length; i++) {
            XSSFCell cell = titleRow.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(cellStyle);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //插入需导出的数据
        for (int i = 0; i < list.size(); i++) {
            XSSFRow row = sheet.createRow(i + 1);
            sheet.setColumnWidth(7, "156小时59分" .length() * 512);
            for (int j = 0; j < title.length; j++) {
//                row.createCell(j).setCellValue(str);
                switch (j) {
                    case 0:
                        row.createCell(j).setCellValue(i + 1);
                        break;
                    case 1:
                        row.createCell(j).setCellValue(list.get(i).getCarId());
                        break;
                    case 2:
                        row.createCell(j).setCellValue(list.get(i).getName());
                        break;
                    case 3:
                        row.createCell(j).setCellValue(list.get(i).getParkname());
                        sheet.setColumnWidth(j, isNull(list.get(i).getParkname()).length() * 512);
                        break;
                    case 4:
                        row.createCell(j).setCellValue(list.get(i).getRegionname());
                        sheet.setColumnWidth(j, isNull(list.get(i).getRegionname()).length() * 512);
                        break;
                    case 5:
                        Date date = DateUtil1.parseDate(String.valueOf(list.get(i).getIntime()), DateUtil1.YYYYMMDDHHMMSS);
                        String format = DateUtil1.format(date, DateUtil1.YYYY_MM_DD_HH_MM_SS);
                        row.createCell(j).setCellValue(format);
                        sheet.setColumnWidth(j, isNull(format).length() * 324);
                        break;
                    case 6:
                        Date outDate = DateUtil1.parseDate(String.valueOf(list.get(i).getOuttime()), DateUtil1.YYYYMMDDHHMMSS);
                        String out = DateUtil1.format(outDate, DateUtil1.YYYY_MM_DD_HH_MM_SS);
                        row.createCell(j).setCellValue(out);
                        sheet.setColumnWidth(j, isNull(out).length() * 324);
                        break;
                    case 7:
                        Integer integer = new Integer(list.get(i).getParktime());
                        int hour = integer / 60;
                        int minute = integer % 60;
                        int day = hour / 24;
                        hour=hour %24;
                        row.createCell(j).setCellValue( (day == 0 ? "" : day + "天") +(hour == 0 ? "" : hour + "小时") + (minute == 0 ? "" : minute + "分"));
                        break;
                    case 8:
                        row.createCell(j).setCellValue(new BigDecimal(list.get(i).getParkamt()).doubleValue());
                        break;
                    case 9:
                        row.createCell(j).setCellValue(new BigDecimal(list.get(i).getChargeamt()).doubleValue());
                        break;
                    case 10:

                        if ("2" .equals(list.get(i).getPaystate())) {
                            row.createCell(j).setCellValue("是");
                        } else {
                            row.createCell(j).setCellValue("否");
                        }
                        break;
                    default:
                }
            }
        }
        //web浏览通过MIME类型判断文件是excel类型
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        // 对文件名进行处理。防止文件名乱码
        String fileName = new String(exportFileName.getBytes(), "ISO8859-1");
        // Content-disposition属性设置成以附件方式进行下载
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();

    }

    @Override
    public List<String> selectCustIds(String custId) {
        return baseMapper.selectCustIds(custId);
    }

    @Override
    public List<OverduePaymentExportDto> getOverduePaymentList(String carId, Long startTime, Long endTime, String chargemanno, String parkCode) {
        return baseMapper.getOverduePaymentList(carId, startTime, endTime, chargemanno, parkCode);
    }

    @Override
    public List<BadDebtExportDto> getBadDebtList(String carId, Long startTime, Long endTime, String chargemanno, String parkCode) {
        return baseMapper.getBadDebtList(carId, startTime, endTime, chargemanno, parkCode);
    }


    public static void main(String[] args) {
//        Long i = 20181012301010;
//        System.out.println(i/60);
//        System.out.println(i%60);
    }
}
