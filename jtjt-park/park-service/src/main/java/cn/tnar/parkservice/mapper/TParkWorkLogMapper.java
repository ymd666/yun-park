package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.dto.TParkWorkLog;
import cn.tnar.parkservice.model.request.ParkShiftRequset;
import cn.tnar.parkservice.model.request.ParkWorkLogRequset;
import cn.tnar.parkservice.model.response.ParkShiftResponse;
import cn.tnar.parkservice.model.response.TParkWorkLogResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Repository
@Mapper
public interface TParkWorkLogMapper extends BaseMapper<TParkWorkLog> {


    List<TParkWorkLogResponse> query(Page<TParkWorkLogResponse> page, @Param("req") ParkWorkLogRequset req);

    List<TParkWorkLogResponse> queryNew(Page<TParkWorkLogResponse> page, @Param("req") ParkWorkLogRequset req);

    List<TParkWorkLogResponse> queryZZ(@Param("req") ParkWorkLogRequset req);


    List<TParkWorkLogResponse> queryTollNo(@Param("parkCode") String parkCode, @Param("name") String name);

    List<ParkShiftResponse> getShiftDetail(Page<ParkShiftResponse> page, @Param("req") ParkShiftRequset req);

//    @Select("SELECT wl.id,wl.park_code,wl.toll_no,wl.on_duty,wl.off_duty,wl.isvalid,wl.on_photo,wl.off_photo," +
//            " pt.`name`,pt.toll_type," +
//            " pi.`name` as park_name," +
//            " td.item " +
//            " FROM t_park_work_log wl" +
//            " LEFT JOIN t_park_toll pt ON pt.`no` = wl.toll_no" +
//            " LEFT JOIN t_park_info pi ON pi.park_code = wl.park_code" +
//            " LEFT JOIN t_dictionary td ON td.cval = wl.isvalid" +
//            " where td.category_en = 'work_flag'" +
//            " <when  test='req.parkCode!=\"\"'> and  wl.park_code = #{req.parkCode}</when>" +
//            " <when  test='req.name!=\"\"'> and  pt.`name` like '%${req.name}%' </when>" +
//            " <when  test='req.isvalid>0'> and  wl.isvalid = #{req.isvalid}</when>" +
//            " <when  test='req.starTime>0 and req.endTime>0'> and  wl.on_duty  BETWEEN  #{req.starTime} AND #{req.endTime} </when>")
//    List<TParkWorkLogResponse> queryParkWorkLog(Page<TParkWorkLogResponse> page, @Param("req") ParkWorkLogRequset req);
}
