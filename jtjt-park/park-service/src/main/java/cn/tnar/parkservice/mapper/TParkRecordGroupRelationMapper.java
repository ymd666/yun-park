package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.entity.TParkRecordGroupRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月10日 14:11:42
 */
@Mapper
@QuerySupport
@Repository
public interface TParkRecordGroupRelationMapper extends BaseMapper<TParkRecordGroupRelation> {

    @Select("SELECT * from t_park_record_group_relation where parking_record_serialno not in \n" +
            "(SELECT serialno as parking_record_serialno from t_park_day_parking_record)")
    List<TParkRecordGroupRelation> queryNeedTransRelations();
}
