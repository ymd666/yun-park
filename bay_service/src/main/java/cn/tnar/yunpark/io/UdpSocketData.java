package cn.tnar.yunpark.io;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * UDP包及Socket
 * Created by tieh on 2017/6/22.
 */
public class UdpSocketData {
    DatagramSocket socket;
    DatagramPacket packet;

    public UdpSocketData(DatagramSocket socket, DatagramPacket packet) {
        this.socket = socket;
        this.packet = packet;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public void setSocket(DatagramSocket socket) {
        this.socket = socket;
    }

    public DatagramPacket getPacket() {
        return packet;
    }

    public void setPacket(DatagramPacket packet) {
        this.packet = packet;
    }
}
