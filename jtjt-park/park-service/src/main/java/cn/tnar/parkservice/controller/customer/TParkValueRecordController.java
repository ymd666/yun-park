package cn.tnar.parkservice.controller.customer;

import cn.tnar.parkservice.service.ITParkValueRecordService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 19:17:02
 */
@RestController
@RequestMapping("service/tParkValueRecord")
@Api(tags = "停车价值记录TParkValueRecordController")
@Slf4j
public class TParkValueRecordController {

    @Autowired
    @Qualifier("tParkValueRecordServiceImpl")
    private ITParkValueRecordService itParkValueRecordService;

    private static final ResultJson RESULT_JSON = new ResultJson();


    /**
     * 计费分析页面选择下拉框，分页显示停车价值记录
     * {
     * "id":325,
     * "type":0,
     * "classify":2,
     * "pageIndex":0,
     * "pageSize":10,
     * "start_date":20180101,
     * "end_date":20190910
     * }
     *
     * @param json
     * @return
     */
    @PostMapping("/getRecordByGroupId")
    public ResultJson getRecordByGroupId(@RequestBody String json) {
        try {
            return itParkValueRecordService.getRecordByGroupId(json);
        } catch (Exception e) {
            log.error("error", e);
            return RESULT_JSON.setCode(ResultCode.FAIL)
                    .setMsg(ResultCode.FAIL_MSG)
                    .setData(false);
        }
    }

}
