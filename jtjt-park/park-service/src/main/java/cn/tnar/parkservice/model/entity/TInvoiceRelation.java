package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_invoice_relation")
public class TInvoiceRelation extends Model<TInvoiceRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 开票流水号
     */
    @TableField("invoice_apply_sno")
    private String invoiceApplySno;
    /**
     * 订单号
     */
    @TableField("order_no")
    private String orderNo;
    /**
     * 停车场编号
     */
    @TableField("park_code")
    private String parkCode;
    /**
     * 开票金额
     */
    private BigDecimal amount;
    /**
     * 停车场名称
     */
    @TableField("park_name")
    private String parkName;
    private Integer parktime;
    private String car_id;


    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceApplySno() {
        return invoiceApplySno;
    }

    public void setInvoiceApplySno(String invoiceApplySno) {
        this.invoiceApplySno = invoiceApplySno;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public Integer getParktime() {
        return parktime;
    }

    public void setParktime(Integer parktime) {
        this.parktime = parktime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TInvoiceRelation{" +
                ", id=" + id +
                ", invoiceApplySno=" + invoiceApplySno +
                ", orderNo=" + orderNo +
                ", parkCode=" + parkCode +
                ", amount=" + amount +
                ", parkName=" + parkName +
                ", parktime=" + parktime +
                "}";
    }
}

