package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * @Author: 陈亮
 * @Date: 2018/9/27 17:54
 */
@Data
public class AliOssResponse {

    private String url;
    private long size;
    private long photoId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
