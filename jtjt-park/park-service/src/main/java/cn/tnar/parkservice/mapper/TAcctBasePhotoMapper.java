package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.TAcctBasePhoto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface TAcctBasePhotoMapper extends BaseMapper<TAcctBasePhoto> {

}
