package cn.tnar.parkservice.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by tieh on 2017/11/9.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryInvoiceResponse<T> {
    private int code;
    private String msg;
    private T data;
    private String sign;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
