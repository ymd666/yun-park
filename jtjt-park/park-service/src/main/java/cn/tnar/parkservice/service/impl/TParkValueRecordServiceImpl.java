package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.mapper.TParkValueRecordMapper;
import cn.tnar.parkservice.model.dto.TParkValueRecordDto;
import cn.tnar.parkservice.model.entity.TCarFeesroleinfo;
import cn.tnar.parkservice.model.entity.TParkMemberGroup;
import cn.tnar.parkservice.model.entity.TParkValueRecord;
import cn.tnar.parkservice.model.request.ParkValueReqParam;
import cn.tnar.parkservice.service.ITCarFeesroleinfoService;
import cn.tnar.parkservice.service.ITParkMemberGroupService;
import cn.tnar.parkservice.service.ITParkValueRecordService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 19:39:37
 */
@Service("tParkValueRecordServiceImpl")
public class TParkValueRecordServiceImpl extends ServiceImpl<TParkValueRecordMapper, TParkValueRecord> implements ITParkValueRecordService {
    @Autowired
    @Qualifier("tParkMemberGroupServiceImpl")
    private ITParkMemberGroupService itParkMemberGroupService;

    @Autowired
    @Qualifier("tCarFeesroleinfoServiceImpl")
    private ITCarFeesroleinfoService itCarFeesroleinfoService;

//    @Autowired
//    @Qualifier("tParkValueRecordServiceImpl")
//    private ITParkValueRecordService  itParkValueRecordService;

    @Autowired
    private TParkValueRecordMapper tParkValueRecordMapper;


    private static final ResultJson RESULT_JSON = new ResultJson();


    /**
     * 计费分析页面选择下拉框，分页显示停车价值记录
     *
     * @param json
     * @return
     */
    @Override
    public ResultJson getRecordByGroupId(String json) {
        ParkValueReqParam parkValueReqParam = JSON.parseObject(json, ParkValueReqParam.class);
        Long id = parkValueReqParam.getId(); //卡组id
        int type = parkValueReqParam.getType(); //卡组类型
        int classify = parkValueReqParam.getClassify(); //类别
        Long pageIndex = parkValueReqParam.getPageIndex(); //当前页面索引
        Long pageSize = parkValueReqParam.getPageSize(); //页面容量
        Long start_date = parkValueReqParam.getStartDate(); //开始日期
        Long end_date = parkValueReqParam.getEndDate(); //结束日期
        if (!(classify == Constant.VISITOR_CAR_CLASSIFY || classify == Constant.CUSTOM_CAR_CLASSIFY)) { //不是访客 类型 或者 自定义卡组类型
            return new ResultJson().setData(null).setCode(900).setMsg("只能选自定义卡组或者访客卡组");
        }
        String ids = "";
        if (type == 1) { //卡证
            ids = String.valueOf(id);
        } else if (type == 0) { //目录
            //获取目录下的所有的子卡
            List<String> idList = new ArrayList<String>();
            idList = itParkMemberGroupService.selectByParentId(idList, id);
            ids = idList.parallelStream()
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.joining(","));
        }

        Page<TParkValueRecordDto> page = new Page<>(pageIndex, pageSize);
        List<TParkValueRecordDto> result = tParkValueRecordMapper.getRecordByGroupId(page, ids, start_date, end_date);
//        QueryWrapper<TParkValueRecord> queryWrapper = new QueryWrapper<>();
//        queryWrapper.inSql("group_id", ids).orderByAsc("cur_time");
//        IPage<TParkValueRecord> page = this.page(new Page<>(0, 10), queryWrapper);
//        List<TParkValueRecordDto> records = page.getRecords().parallelStream().map(x -> {
//            TParkValueRecordDto tParkValueRecordDto = new TParkValueRecordDto();
//            BeanUtils.copyProperties(x, tParkValueRecordDto);
//            Map<String, Object> map = getGroupNameById(tParkValueRecordDto.getGroupId());
//            tParkValueRecordDto.setGroupName((String) (map.get("name")));
//            tParkValueRecordDto.setMonthValue((BigDecimal) (map.get("monthValue")));
//            return tParkValueRecordDto;
//        }).collect(Collectors.toList());
//
//        IPage<TParkValueRecordDto> recordDtoIPage = new Page<>();
//        BeanUtils.copyProperties(page, recordDtoIPage);
//        recordDtoIPage.setRecords(records);
        page.setRecords(result);
        page.setTotal(result.size());
        return new ResultJson().setData(page).setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
    }


    /**
     * 根据卡组的id获取卡组名称
     *
     * @return String
     */
    public Map<String, Object> getGroupNameById(Long id) {
        QueryWrapper<TParkMemberGroup> queryWrapper = new QueryWrapper<TParkMemberGroup>();
        queryWrapper.select("name,feesroleinfo_id").eq("id", id).last("limit 1");
        TParkMemberGroup one = itParkMemberGroupService.getOne(queryWrapper);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (one != null) {
            resultMap.put("name", one.getName());
            Long feesroleinfoId = one.getFeesroleinfoId();
            QueryWrapper<TCarFeesroleinfo> wrapper = new QueryWrapper<>();
            wrapper.select("month_value").eq("id", feesroleinfoId);
            TCarFeesroleinfo feesroleinfo = itCarFeesroleinfoService.getOne(wrapper);
            if (feesroleinfo != null) {
                resultMap.put("monthValue", feesroleinfo.getMonthValue());
            }
        }
        return resultMap;
    }


    /**
     * 获取失败返回消息体
     *
     * @param data
     * @return
     */
    private ResultJson getFailResultJson(Object data) {
        return RESULT_JSON.setCode(ResultCode.FAIL)
                .setMsg(ResultCode.FAIL_MSG).setData(data);
    }

    /**
     * 获取成功返回消息体
     *
     * @param data
     * @return
     */
    private ResultJson getSuccessResultJson(Object data) {
        return RESULT_JSON.setCode(ResultCode.SUCCESS)
                .setMsg(ResultCode.SUCCESS_MSG).setData(data);
    }
}
