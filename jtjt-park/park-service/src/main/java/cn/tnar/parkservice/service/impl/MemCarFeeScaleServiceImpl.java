package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.config.ExceptionConst;
import cn.tnar.parkservice.exception.CustomException;
import cn.tnar.parkservice.mapper.MemCarFeeScaleMapper;
import cn.tnar.parkservice.mapper.TParkInfoMapper;
import cn.tnar.parkservice.model.entity.MemCarFeeScale;
import cn.tnar.parkservice.model.request.MemCarFeeScaleRequest;
import cn.tnar.parkservice.service.MemCarFeeScaleService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/9/26
 * @Description:
 */
@Service
@Slf4j
public class MemCarFeeScaleServiceImpl extends ServiceImpl<MemCarFeeScaleMapper,MemCarFeeScale> implements MemCarFeeScaleService {

    @Autowired
    private TParkInfoMapper tParkInfoMapper;

    @Autowired
    private MemCarFeeScaleMapper memCarFeeScaleMapper;


    @Override
    public ResultJson query(Map<String,Object> request) {
        Integer pageNo = (Integer)request.get("pageNo");
        Integer pageSize = (Integer)request.get("pageSize");
        QueryWrapper<MemCarFeeScale> qw = new QueryWrapper<>();
        qw.eq("delflag",1);
        if (!"".equals(request.get("cardType").toString())){
            qw.eq("card_type",Integer.parseInt(request.get("cardType").toString()));
        }
        if (!"".equals(request.get("carType").toString())){
            qw.eq("car_type",Integer.parseInt(request.get("carType").toString()));
        }
        qw.eq("cust_id",request.get("custId").toString());
        Page<MemCarFeeScale> page = new Page<>(pageNo,pageSize);
        IPage<MemCarFeeScale> memCarFeeScaleIPage = memCarFeeScaleMapper.selectPage(page, qw);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(memCarFeeScaleIPage);
    }

    @Override
    public ResultJson save(MemCarFeeScaleRequest request) {
        MemCarFeeScale memCarFeeScale = new MemCarFeeScale();
        try {
            BeanUtils.copyProperties(request,memCarFeeScale);
        } catch (BeansException e) {
            log.error(e.getMessage());
        }
        memCarFeeScale.setDelflag(1);
        memCarFeeScale.setNodeId(1);
        memCarFeeScaleMapper.insert(memCarFeeScale);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(true);
    }

    @Override
    public ResultJson update(MemCarFeeScaleRequest request) {
        MemCarFeeScale memCarFeeScale = memCarFeeScaleMapper.selectById(request.getId());
        if (memCarFeeScale.getDelflag() == 2){
            log.error(ExceptionConst.ERROR_RECORD_NOT_FOUND);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(ExceptionConst.ERROR_RECORD_NOT_FOUND).setData(false);
        }else {
            try {
                BeanUtils.copyProperties(request,memCarFeeScale);
            } catch (BeansException e) {
                log.error(e.getMessage());
                throw new CustomException(ExceptionConst.KESB_UPDATE_ERROR);
            }
            memCarFeeScaleMapper.updateById(memCarFeeScale);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(true);
        }
    }


}
