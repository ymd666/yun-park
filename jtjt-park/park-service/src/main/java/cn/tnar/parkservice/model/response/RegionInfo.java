package cn.tnar.parkservice.model.response;

import lombok.Data;


@Data
public class RegionInfo {

    private String name;
    private String regionCode;
}

