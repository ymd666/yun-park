package cn.tnar.yunpark.service.impl;


import cn.tnar.yunpark.service.BayService;
import cn.tnar.yunpark.util.HttpToolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("bayService")
public class BayServiceImpl implements BayService {

    Logger log = LoggerFactory.getLogger(BayServiceImpl.class);

    @Value("${tnar.kesb.server}")
    private String server;

    @Value("${tnar.kesb.privilege-key}")
    private String privilegeKey;

    /**
     * 批量同步地磁状态
     *
     * @param params 权限 key privilegekey  , 数量 baynum  ,  数据串	datas --格式：地磁编号,状态,更新日期|地磁编号,状态,更新日期|地磁编号,状态,更新日期
     */
    @Override
    public void BatchSyncBayStatus(Map<String, String> params) {
        params.put("privilegekey", privilegeKey);

        String serviceId = "87202019";
        String paramsStr = HttpToolService.onRequestFormat(params, serviceId);
        log.debug("Method-BatchSyncBayStatus params:" + paramsStr);
        String result = HttpToolService.executePostByClient(server, paramsStr);
        log.info("Method-BatchSyncBayStatus result:" + result);
    }

}
