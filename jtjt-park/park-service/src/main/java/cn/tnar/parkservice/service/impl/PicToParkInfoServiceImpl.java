package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.PicToParkInfoMapper;
import cn.tnar.parkservice.model.dto.AcctBasePhotoDto;
import cn.tnar.parkservice.service.PicToParkInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: ZhangZhibiao
 * @Date:2018-11-15
 */
@Service
public class PicToParkInfoServiceImpl implements PicToParkInfoService {
    private static final Logger log = LoggerFactory.getLogger(PicToParkInfoServiceImpl.class);
    @Autowired
    PicToParkInfoMapper picToParkInfoMapper;

    @Override
    public int insertPicToParkInfo(String photoKey, String parkCode) {
        return picToParkInfoMapper.insertPicToParkInfo(photoKey, parkCode);
    }

    @Override
    public String getPhotoKeyFrom(String parkCode) {
        return picToParkInfoMapper.getPhotoKeyFrom(parkCode);
    }

    @Override
    public int selectId(String parkCode) {
        return  picToParkInfoMapper.selectId(parkCode);
    }

    @Override
    public int insertPicToBasePhoto(AcctBasePhotoDto dto) {
        return picToParkInfoMapper.insertPicToBasePhoto(dto);
    }

    @Override
    public int delPhotoF(String parkCode) {
        return picToParkInfoMapper.delPhotoF(parkCode);
    }

    @Override
    public int delPhotoX(int id) {
        return picToParkInfoMapper.delPhotoX(id);
    }

}
