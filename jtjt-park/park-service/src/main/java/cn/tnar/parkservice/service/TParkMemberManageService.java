package cn.tnar.parkservice.service;

import cn.tnar.parkservice.util.common.ResultJson;

import java.util.Map;

public interface TParkMemberManageService {


    ResultJson query(Map<String,Object> param);
}
