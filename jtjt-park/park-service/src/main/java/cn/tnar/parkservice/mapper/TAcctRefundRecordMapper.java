package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TAcctRefundRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 11:26:47
 */
@Mapper
@Repository
public interface TAcctRefundRecordMapper extends BaseMapper<TAcctRefundRecord> {
}
