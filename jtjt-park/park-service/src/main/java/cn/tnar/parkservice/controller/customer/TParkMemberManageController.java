package cn.tnar.parkservice.controller.customer;

import cn.tnar.parkservice.service.TParkMemberManageService;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/9/9
 * @Description:
 */
@Api(tags = "会员管理（张鑫）")
@RestController
@RequestMapping("service/member")
public class TParkMemberManageController {

    @Autowired
    private TParkMemberManageService tParkMemberManageService;

    @ApiOperation("会员管理-查询")
    @PostMapping("/query")
    public ResultJson query(@RequestBody String json){
        Map param = JSON.parseObject(json, Map.class);
        return tParkMemberManageService.query(param);
    }

}
