package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zzb
 * @since 2018-12-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "TParkRegion对象", description = "")
public class TParkRegion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer nodeId;

    @ApiModelProperty(value = " NOT NULL")
    private String parkCode;

    private String regionCode;

    @ApiModelProperty(value = " NOT NULL")
    private String name;

    private Integer spaceType;

    private Long feeindex;

    private String feedesc;

    private Integer freetime;

    private Integer floorNum;

    private Integer spaceCount;

    private String orderNum;

    @ApiModelProperty(value = "记录修改人  NOT NULL")
    private Long userId;

    @ApiModelProperty(value = "yyyyMMddHHmmss")
    private Long utime;

    private Integer specialCount;

    private Integer maxTempCount;

    private Integer freeCount;

    private Integer logon;

    private String parentRegionCode;


}
