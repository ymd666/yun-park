package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.TParkGroupSeatinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface TParkGroupSeatinfoMapper extends BaseMapper<TParkGroupSeatinfo> {

    @Select("select s.agent_id,s.park_code,s.region_code,f.car_type,f.card_type,g.agent_id,\n" +
            "\t\t(select COUNT(*) from t_park_group_seatinfo s left join t_car_feesroleinfo f ON s.fees_roleid = f.id \n" +
            "\t\twhere s.group_id = #{groupId}) AS total_record,g.fee_amount,s.id as seat_id,\n" +
            "\t\ts.start_date,s.end_date,s.fees_roleid,s.remark,s.buy_num,s.car_state,f.role_name,f.month_value \n" +
            "\t\tfrom t_park_group_seatinfo s \n" +
            "\t\tleft join  t_car_feesroleinfo f on s.fees_roleid = f.id \n" +
            "\t\tleft join  t_park_group_memberinfo g on g.id = s.group_id \n" +
            "\t\twhere s.group_id =#{groupId}")
    List<Map<String, String>> queryCarSeat(@Param("page") Page page, @Param("groupId") Long groupId);

}
