package cn.tnar.parkservice.model.dto;


import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单
 */
@Data
public class TTradeCommodityOrder {

    private long id;
    private long nodeId;
    private long custId;
    private String traceIndex;
    private String orderNumber;
    private String tradeDigestCode;
    private String usingobj;
    private String commCode;
    private String commname;
    private long commtype;
    private long commprovider;
    private BigDecimal orderamout;
    private String deliveryPlace;
    private String deliveryArea;
    private long tradetype;
    private long tradenumber;
    private BigDecimal tradeamount;
    private BigDecimal refundamout;
    private long orderstate;
    private String lcId;
    private String logisticsnumber;
    private String recvaddress;
    private String recvname;
    private String recvmobile;
    private String postCode;
    private long invoice;
    private String invoiceTitle;
    private long invoiceType;
    private String invoiceContent;
    private long ordertime;
    private long endtime;
    private long sendtime;
    private long recvtime;
    private String description;
    private String remark;
    private String batchNumber;
    private long delflag;
    private BigDecimal cloudTradeamt;
    private BigDecimal agentTradeamt;
    private long syncflag;
    private long assetsType;
    private BigDecimal activityCouponAmt;
    //  private BigDecimal eleccouponAmt;
    private long eleccoupontype;
    private long payType;
    private long payForCloud;
    private long timeRelateIntime;
    private BigDecimal timeRelateAmt;
    private BigDecimal feerate;
    private long clearState;
    private String thirdorderNo;
    private String voucherEnd;
    private String voucherStart;
    private int mergeFlag ;
    private int orderType ;
    private int updateTime;
    private BigDecimal feeamt;


}
