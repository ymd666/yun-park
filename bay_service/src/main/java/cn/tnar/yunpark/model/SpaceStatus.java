package cn.tnar.yunpark.model;

/**
 * 车位状态(车来，车走，异常)
 * Created by tieh on 2017/6/14.
 */
public class SpaceStatus implements KesbStatus {
    /**
     * 地磁编号(平台上面叫mac)
     */
    private String sensorId;

    /**
     * LBM格式的车位状态("1"-无车，"2"-有车，"95"-离线)
     */
    private String status;

    /**
     * 事件时间, yyyyMMddHHmmss
     */
    private String time;

    public SpaceStatus() {
    }

    public SpaceStatus(String sensorId, String status, String time) {
        this.sensorId = sensorId;
        this.status = status;
        this.time = time;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public void setLbmInStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String getType() {
        return "0";
    }

    @Override
    public String toKesbString() {
        return getSensorId() + "," + toKesbStatus(getStatus())
                + "," + getTime();
    }

    /**
     * 车位状态转换，LBM格式(1无车2有车95异常) -> (KESB更新状态接口API入参格式："00"-无车，"01"-有车，"99"-异常)
     *
     * @return
     */
    public static String toKesbStatus(String lbmStatus) {
        if ("1".equals(lbmStatus)) {
            return "00";
        }
        if ("2".equals(lbmStatus)) {
            return "01";
        }
        return "95";
    }

    @Override
    public String toString() {
        return "SpaceStatus{" +
                "id='" + sensorId + '\'' +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
