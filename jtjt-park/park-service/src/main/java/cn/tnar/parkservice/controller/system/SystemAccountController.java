package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.config.ExceptionConst;
import cn.tnar.parkservice.exception.CustomException;
import cn.tnar.parkservice.model.entity.TAcctCustInfo;
import cn.tnar.parkservice.model.entity.User;
import cn.tnar.parkservice.model.request.TAcctCustInfoRequest;
import cn.tnar.parkservice.service.ITAcctCustInfoService;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/22
 * @Description:
 */
@Api(tags = "系统账号（张鑫）")
@RestController
@RequestMapping("service/systemAccount")
public class SystemAccountController {

    private Logger log = LoggerFactory.getLogger(SystemAccountController.class);

    @Autowired
    private KesbApiService kesbApiService;

    @Autowired
    private ITAcctCustInfoService tAcctCustInfoServiceImpl;

    @Autowired
    private ITAcctCustInfoService itAcctCustInfoService;

    @ApiOperation("添加账号")
    @PostMapping("/addAccount")
    public ResultJson addAccount(@RequestBody Map<String, Object> param) {
        try {
            log.info("添加账号：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_SYSTEM_ACCOUNT_ADD, param);
            log.info("添加账号：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("添加账号C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改账号")
    @PostMapping("/updateAccount")
    public ResultJson updateAccount(@RequestBody Map<String, Object> param) {
        try {
            log.info("修改账号：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_SYSTEM_ACCOUNT_UPDATE, param);
            log.info("修改账号：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("修改账号C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


    @ApiOperation("查询所有账号")
    @PostMapping("/queryAccount")
    public ResultJson queryAccount(@RequestBody TAcctCustInfoRequest request) {
        try {
           return itAcctCustInfoService.querySysAccount(request);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG).setData(null);
        }
    }

    @ApiOperation(value = "系统账号启用/禁用")
    @PostMapping(value = "/changeState")
    public ResultJson changeState(@RequestParam long custId, @RequestParam int num) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Integer format = Integer.parseInt(sdf.format(new Date()));
        TAcctCustInfo info = new TAcctCustInfo();
        info.setCustStatus(num);
        info.setCustStatusdate(format);
        Wrapper<TAcctCustInfo> wrapper = new QueryWrapper<TAcctCustInfo>()
                .eq("cust_id", custId);
        boolean ret = tAcctCustInfoServiceImpl.update(info, wrapper);
        if (ret) {
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(ret);
        } else {
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG).setData(ret);
        }
    }

    @ApiOperation("修改密码")
    @PostMapping("/updatePassword")
    public ResultJson updatePassword(@RequestBody Map<String, Object> param){
        try {
            Long cust_id = Long.valueOf(param.get("cust_id").toString());
            String password = itAcctCustInfoService.queryPassword(cust_id);
            String old_pwd = param.get("old_pwd").toString();
            if (!old_pwd.equals(password)){
                throw new CustomException(ExceptionConst.ERROR_PASSWORD);
            }
            param.remove("account");
            param.remove("old_pwd");
            log.info("updatePassword-修改密码:c接口请求参数,param======>" +param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_SYSTEM_ACCOUNT_UPDATEPASSWORD, param);
            log.info("updatePassword-修改密码:c接口返回参数,param======>" + result);
             return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询机构角色")
    @PostMapping("/queryOrgRole")
    public ResultJson queryOrgRole(@RequestBody Map<String, Object> param){
        try {
            log.info("查询机构角色：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_QUERY_ORG_ROLE, param);
            log.info("查询机构角色：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        }catch (KesbException e) {
            log.error("查询机构角色C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询机构角色人员")
    @PostMapping("/queryUser")
    public ResultJson queryUser(@RequestBody Map<String, Object> param){
        try {
            log.info("查询机构角色人员：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_QUERY_ROLE_USER, param);
            log.info("查询机构角色人员：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("查询机构角色人员C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("系统账号-修改机构角色人员")
    @PostMapping("/updateRole")
    public ResultJson updateRole(@RequestBody Map<String,Object> param){
        try {
            log.info("系统账号-修改机构角色人员：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_UPDATE_ORT_ROLE, param);
            log.info("系统账号-修改机构角色人员：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("系统账号-修改机构角色人员C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


}
