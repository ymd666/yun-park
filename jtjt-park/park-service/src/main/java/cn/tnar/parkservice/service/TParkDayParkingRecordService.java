package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkDayParkingRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月10日 11:46:40
 */
public interface TParkDayParkingRecordService extends IService<TParkDayParkingRecord> {
}
