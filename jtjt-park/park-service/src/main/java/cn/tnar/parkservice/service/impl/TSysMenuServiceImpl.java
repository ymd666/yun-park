package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TSysMenuMapper;
import cn.tnar.parkservice.model.dto.MenuDto;
import cn.tnar.parkservice.model.entity.TSysMenu;
import cn.tnar.parkservice.service.ITSysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Service
public class TSysMenuServiceImpl extends ServiceImpl<TSysMenuMapper, TSysMenu> implements ITSysMenuService {

    @Autowired
    private TSysMenuMapper menuMapper;


    @Override
    public List<MenuDto> selectMenu(String CustId) {
        List<TSysMenu> menuList = menuMapper.selectMenu(CustId);
        List<MenuDto> menuDtoList = new ArrayList<>();
        //是否有根节点的标识
        boolean flag = false;
        for (TSysMenu item : menuList){
            MenuDto dto = new MenuDto();
            dto.setAudit_flag(item.getAuditFlag());
            dto.setBiz_type(item.getBizType());
            dto.setDepends(item.getDepends());
            dto.setFunc_code(item.getFuncCode());
            dto.setIcon(item.getIcon());
            dto.setId(item.getId());
            dto.setMenu_flag(item.getMenuFlag());
            dto.setMenu_id(item.getMenuId());
            dto.setMenu_name(item.getMenuName());
            dto.setMenu_name_en(item.getMenuNameEn());
            dto.setMtd_code(item.getMtdCode());
            dto.setParent_menu_id(item.getParentMenuId());
            dto.setSerial_no(item.getSerialNo());
            dto.setUrl(item.getUrl());
            dto.setObject_code(item.getObjectCode());
            dto.setModify_oper(item.getModifyOper());
            dto.setModify_time(item.getModifyTime());
            dto.setObj_code(item.getObjCode());
            dto.setPlatformType(item.getPlatformType());
            menuDtoList.add(dto);
            if (dto.getParent_menu_id().compareTo(1L) == 0 && dto.getPlatformType().compareTo(2) == 0 && "YGTROOT".equals(dto.getMenu_name_en())) {
                flag = true;
            }
        }
        if (!flag) { //返回的list中没有云岗亭根节点，则获取根节点
            TSysMenu root = menuMapper.selectYgtMenu();
            MenuDto rootDto = new MenuDto();
            rootDto.setAudit_flag(root.getAuditFlag());
            rootDto.setBiz_type(root.getBizType());
            rootDto.setDepends(root.getDepends());
            rootDto.setFunc_code(root.getFuncCode());
            rootDto.setIcon(root.getIcon());
            rootDto.setId(root.getId());
            rootDto.setMenu_flag(root.getMenuFlag());
            rootDto.setMenu_id(root.getMenuId());
            rootDto.setMenu_name(root.getMenuName());
            rootDto.setMenu_name_en(root.getMenuNameEn());
            rootDto.setMtd_code(root.getMtdCode());
            rootDto.setParent_menu_id(root.getParentMenuId());
            rootDto.setSerial_no(root.getSerialNo());
            rootDto.setUrl(root.getUrl());
            rootDto.setObject_code(root.getObjectCode());
            rootDto.setModify_oper(root.getModifyOper());
            rootDto.setModify_time(root.getModifyTime());
            rootDto.setObj_code(root.getObjCode());
            rootDto.setPlatformType(root.getPlatformType());
            menuDtoList.add(0, rootDto);
        }
        return menuDtoList;
    }

    @Override
    public TSysMenu selectYunRoot() {
        return menuMapper.selectYunRoot();
    }

    @Override
    public List<TSysMenu> selectSubByParentId(Long id) {
        return menuMapper.selectSubByParentId(id);
    }
}
