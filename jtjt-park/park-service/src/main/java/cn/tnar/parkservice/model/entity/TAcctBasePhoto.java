package cn.tnar.parkservice.model.entity;



import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_acct_base_photo")
public class TAcctBasePhoto extends Model<TAcctBasePhoto> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("node_id")
    private Integer nodeId;
    @TableField("object_id")
    private Long objectId;
    private Integer filetype;
    private String filekey;
    @TableField("file_name")
    private String fileName;
    @TableField("yun_name")
    private String yunName;
    @TableField("file_url")
    private String fileUrl;
    @TableField("file_desc")
    private String fileDesc;
    private Long addtime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Integer getFiletype() {
        return filetype;
    }

    public void setFiletype(Integer filetype) {
        this.filetype = filetype;
    }

    public String getFilekey() {
        return filekey;
    }

    public void setFilekey(String filekey) {
        this.filekey = filekey;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getYunName() {
        return yunName;
    }

    public void setYunName(String yunName) {
        this.yunName = yunName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public Long getAddtime() {
        return addtime;
    }

    public void setAddtime(Long addtime) {
        this.addtime = addtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TAcctBasePhoto{" +
        ", id=" + id +
        ", nodeId=" + nodeId +
        ", objectId=" + objectId +
        ", filetype=" + filetype +
        ", filekey=" + filekey +
        ", fileName=" + fileName +
        ", yunName=" + yunName +
        ", fileUrl=" + fileUrl +
        ", fileDesc=" + fileDesc +
        ", addtime=" + addtime +
        "}";
    }
}
