package cn.tnar.yunpark.careyes.tcp.model;

import cn.tnar.yunpark.careyes.CarEyesUtil;
import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * package: cn.tnar.yunpark.careyes.tcp.model
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/10 10:37
 * desc   : HeartBeat
 */
public class HeartBeat {

    private byte type;
    private int len;
    private int vbattry;
    private int province;
    private int city;
    private int area;
    private int street;
    private int parkId;
    private int routerId;
    private int sensorId;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minutes;
    private int second;
    private double adc;
    private int sequence;


    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public int getVbattry() {
        return vbattry;
    }

    public void setVbattry(int vbattry) {
        this.vbattry = vbattry;
    }

    public int getProvince() {
        return province;
    }

    public void setProvince(int province) {
        this.province = province;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getStreet() {
        return street;
    }

    public void setStreet(int street) {
        this.street = street;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public int getRouterId() {
        return routerId;
    }

    public void setRouterId(int routerId) {
        this.routerId = routerId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public double getAdc() {
        return adc;
    }

    public void setAdc(double adc) {
        this.adc = adc;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }


    public String fullToString() {
        return "HeartBeat{" +
                "type=" + type +
                ", len=" + len +
                ", vbattry=" + vbattry +
                ", province=" + province +
                ", city=" + city +
                ", area=" + area +
                ", street=" + street +
                ", parkId=" + parkId +
                ", routerId=" + routerId +
                ", sensorId=" + sensorId +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minutes=" + minutes +
                ", second=" + second +
                ", adc=" + adc +
                ", sequence=" + sequence +
                '}';
    }

    @Override
    public String toString() {
        return "HeartBeat{" +
                "type=" + type +
                ", len=" + len +
                ", province=" + province +
                ", city=" + city +
                ", area=" + area +
                ", street=" + street +
                ", parkId=" + parkId +
                ", routerId=" + routerId +
                ", adc=" + adc +
                ", sequence=" + sequence +
                '}';
    }

    public static HeartBeat parse(ByteBuf byteBuf) {
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        HeartBeat hb = new HeartBeat();
        buf.skipBytes(2);
        hb.setType(buf.readByte());
        hb.setLen(buf.readByte());
        buf.skipBytes(1);
        //buf.skipBytes(1);
        hb.setRouterId(buf.readByte());
        buf.skipBytes(4);
        hb.setProvince(buf.readByte());
        hb.setCity(buf.readByte());
        hb.setArea(buf.readByte());
        hb.setStreet(buf.readByte());
        hb.setParkId(buf.readByte());
        short adcs = buf.readShort();
        double adcd = (adcs / 4095) * 3.3 * 2;
        hb.setAdc(adcd);
        buf.skipBytes(1);
        hb.setSequence(buf.readByte());
        return hb;
    }

    public static HeartBeat parse2(ByteBuf byteBuf) {
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        HeartBeat hb = new HeartBeat();
        buf.skipBytes(2);
        hb.setType(buf.readByte());
        hb.setLen(buf.readByte());
        buf.skipBytes(1);
        buf.skipBytes(1);
        //hb.setRouterId(buf.readByte());
        buf.skipBytes(3);
        hb.setVbattry(buf.readByte());
        hb.setProvince(buf.readByte());
        hb.setCity(buf.readByte());
        hb.setArea(buf.readByte());
        hb.setStreet(buf.readByte());
        hb.setParkId(buf.readByte());
        hb.setRouterId(buf.readByte());
        hb.setSensorId(buf.readByte());
        buf.skipBytes(2);
        hb.setYear(CarEyesUtil.bcd2Int(buf.readByte()));
        hb.setMonth(CarEyesUtil.bcd2Int(buf.readByte()));
        hb.setDay(CarEyesUtil.bcd2Int(buf.readByte()));
        hb.setHour(CarEyesUtil.bcd2Int(buf.readByte()));
        hb.setMinutes(CarEyesUtil.bcd2Int(buf.readByte()));
        hb.setSecond(CarEyesUtil.bcd2Int(buf.readByte()));
        buf.skipBytes(7);
        hb.setSequence(buf.readByte());
        return hb;
    }
}
