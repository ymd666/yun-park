package cn.tnar.parkservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @Author: 陈亮
 * @Date: 2018/9/26 15:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AliOSSFiles {

    private String filename;
    private Long filesize;
    private Date uploadTime;

    public AliOSSFiles(String s, long size, Date lastModified) {

    }
}
