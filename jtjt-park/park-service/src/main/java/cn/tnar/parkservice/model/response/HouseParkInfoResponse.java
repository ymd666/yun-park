package cn.tnar.parkservice.model.response;

import lombok.Data;

@Data
public class HouseParkInfoResponse {

    private String parkCode;

    private String houseName;
}
