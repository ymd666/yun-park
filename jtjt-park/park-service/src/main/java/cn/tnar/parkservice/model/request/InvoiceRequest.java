package cn.tnar.parkservice.model.request;

/**
 * @Description:
 * @author: yl
 * @date 2018/10/16 14:23
 */
public class InvoiceRequest {

    /**
     * "开票起始时间",example = "20180925140107"
     */
    private Long startTime;

    /**
     * "开票结束时间",example = "20180925150107"
     * */
    private Long endTime;

    /**
     *  "发票类型:0:正数发票（蓝票） 1：负数发票（红票）",example = "0"
     */
    private String invoiceType;

    /**
     *   "发票状态 00开具成功 02空白发票作废 03:已开发票作废",example = "00"
     */
    private String invoiceStatus;

    /**
     * 停车场编号
     */
    private String park_code;

    /**
     *  "页码",example = "1"
     */
    private Integer pageNo;

    /**
     *  "页面显示条数",example = "10"
     */
    private Integer pageSize;


    public String getPark_code() {
        return park_code;
    }

    public void setPark_code(String park_code) {
        this.park_code = park_code;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
