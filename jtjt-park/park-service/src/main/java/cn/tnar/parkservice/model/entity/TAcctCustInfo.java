package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_acct_cust_info")
public class TAcctCustInfo extends Model<TAcctCustInfo> {

    private static final long serialVersionUID = 1L;

    @TableField("node_id")
    private Integer nodeId;
    @TableId("cust_id")
    private Long custId;
    private String name;
    /**
     * 数据字典 客户类别:  6-客户号 8-企业号 4-柜员号 1-平台管理员
     */
    @ApiParam(value = "数据字典 客户类别:  6-客户号 8-企业号 4-柜员号 1-平台管理员")
    @TableField("cust_type")
    private Integer custType;
    /**
     * 数据字典 客户级别:  1-注册 2-铜牌 3-银牌 4-金牌 5-钻石
     */
    @ApiParam(value = "数据字典 客户级别:  1-注册 2-铜牌 3-银牌 4-金牌 5-钻石")
    @TableField("cust_level")
    private Integer custLevel;
    /**
     * 数据字典 客户状态:	 1-未激活 2-正常 3-冻结 4-注销 
     */
    @ApiParam(value = "数据字典 客户状态:	 1-未激活 2-正常 3-冻结 4-注销 ")
    @TableField("cust_status")
    private Integer custStatus;
    @TableField("region_code")
    private String regionCode;
    @TableField("business_code")
    private String businessCode;
    @TableField("open_agentid")
    private Long openAgentid;
    /**
     * 开户日期
     */
    @ApiParam(value = "开户日期")
    @TableField("cust_opendate")
    private Integer custOpendate;
    /**
     * 默认-0，状态变更日期
     */
    @ApiParam(value = "默认-0，状态变更日期")
    @TableField("cust_statusdate")
    private Integer custStatusdate;
    @TableField("status_agentid")
    private Long statusAgentid;
    private String promotekey;
    /**
     * 开户途径:1-微信，2-支付宝，3-APP，4-平台，5-其他
     */
    @ApiParam(value = "开户途径:1-微信，2-支付宝，3-APP，4-平台，5-其他")
    @TableField("open_channel")
    private Integer openChannel;


    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCustType() {
        return custType;
    }

    public void setCustType(Integer custType) {
        this.custType = custType;
    }

    public Integer getCustLevel() {
        return custLevel;
    }

    public void setCustLevel(Integer custLevel) {
        this.custLevel = custLevel;
    }

    public Integer getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(Integer custStatus) {
        this.custStatus = custStatus;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public Long getOpenAgentid() {
        return openAgentid;
    }

    public void setOpenAgentid(Long openAgentid) {
        this.openAgentid = openAgentid;
    }

    public Integer getCustOpendate() {
        return custOpendate;
    }

    public void setCustOpendate(Integer custOpendate) {
        this.custOpendate = custOpendate;
    }

    public Integer getCustStatusdate() {
        return custStatusdate;
    }

    public void setCustStatusdate(Integer custStatusdate) {
        this.custStatusdate = custStatusdate;
    }

    public Long getStatusAgentid() {
        return statusAgentid;
    }

    public void setStatusAgentid(Long statusAgentid) {
        this.statusAgentid = statusAgentid;
    }

    public String getPromotekey() {
        return promotekey;
    }

    public void setPromotekey(String promotekey) {
        this.promotekey = promotekey;
    }

    public Integer getOpenChannel() {
        return openChannel;
    }

    public void setOpenChannel(Integer openChannel) {
        this.openChannel = openChannel;
    }

    @Override
    protected Serializable pkVal() {
        return this.custId;
    }

    @Override
    public String toString() {
        return "TAcctCustInfo{" +
        ", nodeId=" + nodeId +
        ", custId=" + custId +
        ", name=" + name +
        ", custType=" + custType +
        ", custLevel=" + custLevel +
        ", custStatus=" + custStatus +
        ", regionCode=" + regionCode +
        ", businessCode=" + businessCode +
        ", openAgentid=" + openAgentid +
        ", custOpendate=" + custOpendate +
        ", custStatusdate=" + custStatusdate +
        ", statusAgentid=" + statusAgentid +
        ", promotekey=" + promotekey +
        ", openChannel=" + openChannel +
        "}";
    }
}
