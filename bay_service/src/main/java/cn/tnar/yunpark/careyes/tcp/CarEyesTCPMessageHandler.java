package cn.tnar.yunpark.careyes.tcp;

import cn.tnar.yunpark.careyes.CarEyesUtil;
import cn.tnar.yunpark.careyes.tcp.model.HeartBeat;
import cn.tnar.yunpark.careyes.tcp.model.Status;
import cn.tnar.yunpark.model.SpaceSensorStatus;
import cn.tnar.yunpark.model.SpaceStatus;
import cn.tnar.yunpark.service.SensorEventDispatcher;
import cn.tnar.yunpark.util.ByteTool;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * author : CaineZhu
 * date   : 2017/12/22
 * desc   : logic handler
 */


    public class CarEyesTCPMessageHandler extends SimpleChannelInboundHandler<Object> {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private SensorEventDispatcher dispatcher;
    private static final DateFormat LBM_DATA_TIME = new SimpleDateFormat("yyyyMMddHHmmss");

    public CarEyesTCPMessageHandler(SensorEventDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext context, Object obj) throws Exception {
        if (obj instanceof Status) {
            Status msg = (Status) obj;
            logger.info("handler receiver:{}", msg);
            Calendar calendar = Calendar.getInstance();
            calendar.set(msg.getYear() + 2000, msg.getMonth() - 1, msg.getDay(), msg.getHour(), msg.getMinutes(), msg.getSecond());
            String dateTime = LBM_DATA_TIME.format(calendar.getTime());
            String sensorId = ByteTool.bytes2StringWithOutBlank((byte) msg.getProvince(), (byte) msg.getCity(), (byte) msg.getArea(), (byte) msg.getStreet(), (byte) msg.getParkId(), (byte) msg.getRouterId(), (byte) msg.getSensorId());
            SpaceStatus event = new SpaceStatus(sensorId, CarEyesUtil.toLbmStatus(msg.getStatus()), dateTime);
             dispatcher.dispatch(event);
        } else if (obj instanceof HeartBeat) {
            HeartBeat msg = (HeartBeat) obj;
            logger.info("handler receiver:{}", msg.fullToString());
            String sensorId = ByteTool.bytes2StringWithOutBlank((byte) msg.getProvince(), (byte) msg.getCity(), (byte) msg.getArea(), (byte) msg.getStreet(), (byte) msg.getParkId(), (byte) msg.getRouterId(), (byte) msg.getSensorId());
            SpaceSensorStatus sensorStatus = new SpaceSensorStatus(sensorId, msg.getVbattry(), String.valueOf(msg.getVbattry()), 0, "0");
            dispatcher.dispatch(sensorStatus);
            logger.info("1111:{}",sensorStatus);
        }
    }
}
