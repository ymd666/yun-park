package cn.tnar.parkservice.model.response;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;


/**
 * <p>
 * <p>
 * </p>
 *
 * @author clarck
 * @since 2018-10-05
 */
@Data
@TableName("t_park_visit_history")
public class TParkVisitHistoryResponse extends Model<TParkVisitHistoryResponse> {

    private Integer id;

    private String parkName;

    private String parkCode;

    private String visitor;

    private String mobile;

    private String carId;

    private Byte carType;

    private String applyName;

    private String applyMobile;

    private Long visitTimeStart;

    private Long visitTimeEnd;

    private Integer visitState;

    private Long entranceTime;

    private String remarks;

    private Long applyTime;

    private Long applyId;

    private Byte applyStatus;

    private Long accessTime;

    private String operId;

    private Long feesroleinfoId;

    private String feesroleinfoName;
}
