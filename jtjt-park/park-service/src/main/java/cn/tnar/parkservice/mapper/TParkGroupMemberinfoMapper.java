package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkGroupMemberinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Repository
@Mapper
public interface TParkGroupMemberinfoMapper extends BaseMapper<TParkGroupMemberinfo> {

    List<Map<String, Object>> weChatSelect(@Param("telNo") String telNo, @Param("carId") String carId,@Param("memberGroupId")Long memberGroupId);

    List<Map<String, Object>> weChatSelectInfo(@Param("memberinfoId") Long memberinfoId);

    Map<String,Object> getGroupByMember(@Param("memberinfoId") Long memberinfoId);

    Map<String,Object> getSecurityAccount(@Param("custId") Long custId);

    Map<String, Object> getAggregateInfo(@Param("custId")Long custId);

    String getMerchantKey(@Param("custId")Long custId, @Param("merchantId")String merchantId, @Param("posId")String posId);
}
