package cn.tnar.yunpark.careyes.tcp.model;

import cn.tnar.yunpark.careyes.BCCTool;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * package: cn.tnar.yunpark.careyes.nbiot.model
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/9 10:07
 * desc   : AckMessage
 */
public class AckMessage {

    private int len = 0X0E;
    private int type = 0X27;
    private int routerId;
    private int sequence;

    public int getRouterId() {
        return routerId;
    }

    public void setRouterId(int routerId) {
        this.routerId = routerId;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public ByteBuf toArray() {

        ByteBuf buf = Unpooled.buffer(this.len);
        //HEADER
        buf.writeByte(0XFF);
        buf.writeByte(0XFE);
        //type
        buf.writeByte(this.type);
        buf.writeByte(this.len);
        buf.writeByte(this.routerId);
        buf.writeByte(0x88);
        buf.writeByte(0x02);
        buf.writeByte(0x09);
        buf.writeByte(0x07);
        buf.writeByte(0x00);
        buf.writeByte(this.sequence);
        buf.writeByte(BCCTool.bccCalc(buf));
        buf.writeByte(0xAB);
        buf.writeByte(0xAA);
        return buf;
    }
}
