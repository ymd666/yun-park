package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkStallMapper;
import cn.tnar.parkservice.model.response.TParkStallResponse;
import cn.tnar.parkservice.service.TParkStallService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/9/23
 * @Description:
 */
@Service
public class TParkStallServiceImpl implements TParkStallService {

    @Autowired
    private TParkStallMapper tParkStallMapper;

    @Override
    public ResultJson query(Map<String, Object> param) {
        String pageNoStr= (String)param.get("pageNo");
        String pageSizeStr = (String)param.get("pageSize");
        Long pageNo = Long.valueOf(pageNoStr);
        Long pageSize = Long.valueOf(pageSizeStr);
        Page<TParkStallResponse> page = new Page<>(pageNo,pageSize);
        List<TParkStallResponse> response =tParkStallMapper.query(page,param);
        page.setRecords(response);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(page);
    }
}
