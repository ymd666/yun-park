package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.dto.ExportDto;
import cn.tnar.parkservice.util.common.ResultJson;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;


public interface ExportService {
    void getVisit(ExportDto exportDto, HttpServletResponse response);

    void getWhiteBlack(ExportDto exportDto, HttpServletResponse response);

    void getOther(ExportDto exportDto, HttpServletResponse response);

    void getVisitApply(ExportDto exportDto, HttpServletResponse response) throws ParseException;

    void getVisitJurisdiction(ExportDto exportDto, HttpServletResponse response) throws ParseException;

    void down(String fileName, HttpServletResponse response);

    void queryOrder(ExportDto exportDto, HttpServletResponse response);

    void queryArrearage(ExportDto exportDto, HttpServletResponse response);

    void queryAbNormal(ExportDto exportDto, HttpServletResponse response);

    void queryRecord(ExportDto exportDto, HttpServletResponse response);

    ResultJson importMember(MultipartFile file, String json);

    ResultJson importDeferMember(MultipartFile file, String json);

    ResultJson importWhiteMember(MultipartFile file, String json);

    ResultJson importDeferWhiteMember(MultipartFile file, String json);


    void getPaymentQuery(ExportDto exportDto, HttpServletResponse response);

    void getTollShift(ExportDto exportDto, HttpServletResponse response);

    void getDayStatistics(ExportDto exportDto, HttpServletResponse response);

    void getAttendanceStatistics(ExportDto exportDto, HttpServletResponse response);

    void getTollAccount(ExportDto exportDto, HttpServletResponse response);

    void queryInvoiceStatistics(ExportDto exportDto, HttpServletResponse response);

    void queryFinancialStatements(ExportDto exportDto, HttpServletResponse response);

}
