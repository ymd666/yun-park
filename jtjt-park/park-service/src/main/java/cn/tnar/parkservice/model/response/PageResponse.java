package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.util.List;

/**
 * @Author: xinZhang
 * @Date: 2019/9/23
 * @Description:
 */
@Data
public class PageResponse {

    private Integer current;//当前页

    private Integer size;//每页显示记录条数

    private Integer pages;//总页数

    private List<?> records;//每页显示的数据

    private Integer star;//开始数据

    private Long total; //总记录数

}
