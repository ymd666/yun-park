package cn.tnar.parkservice.config;

import cn.tnar.pms.kesb.KesbClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by zk on 2019/5/20.
 */

@Configuration
public class KesbClientcofig {

    @Bean
    public KesbClient kesbClient(@Value("${api.path}") String KesbClientUrl){
        return new KesbClient(KesbClientUrl);
    }


}
