package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;


@Data
public class TParkGroupMemberinfoRequest {

    @ApiModelProperty(value = "会员姓名")
    private String ownerName;

    @ApiModelProperty(value = "房号")
    private String roomNo;

    @ApiModelProperty(value = "停车场编号")
    private String parkCode;

    private Long agentId;

    @ApiModelProperty(value = "联系方式")
    private String telNo;

    @ApiModelProperty(value = "到期日期-起始日期")
    private Integer startDate;

    @ApiModelProperty(value = "到期日期-结束日期")
    private Integer endDate;

    @ApiModelProperty(value = "车牌号")
    private String carId;

    @ApiModelProperty(value = "车辆类型")
    private Integer carType;

    private String remark;

    @ApiModelProperty(value = "会员分组表id")
    private Long memberGroupId;

    @ApiModelProperty(value = "同组车会员信息表id")
    private Long groupMemberinfoId;

    @ApiModelProperty(value = "分页-当前页")
    private Integer pageNo;
    @ApiModelProperty(value = "分页-显示条数")
    private Integer pageSize;

    @ApiModelProperty(value = "查询类型-全部、正常、暂停、过期0-3")
    private Integer type;

    @ApiModelProperty(value = "拼接-同组车会员信息表id")
    private String memberinfoIds;

    @ApiModelProperty(value = "暂停天数")
    private Integer days;

    @ApiModelProperty(value = "延期日期")
    private String date;

    @ApiModelProperty(value = "操作员")
    private Long operId;

    @ApiModelProperty(value = "白名单请求/黑名单请求")
    private Integer fun;

    @ApiModelProperty(value = "停车场custid")
    private Long custId;

    @ApiModelProperty(value = "会员状态 1生效 2 失效")
    private Integer member_type;
    @ApiModelProperty(value = "过期时间")
    private Long expireTime;
    @ApiModelProperty(value = "购买份数")
    private Integer buyNum;
    @ApiModelProperty(value = "收费总额")
    private BigDecimal feeAmount;
    @ApiModelProperty(value = "支付渠道： 1.微信")
    private Integer payMentMethod;
    @ApiModelProperty(value = "聚合支付标识： 1.聚合支付  0.微信支付")
    private Integer aggregatePayflag=1;
    @ApiModelProperty(value = "微信唯一标识ID")
    private String openId;
    @ApiModelProperty(value = "审核状态:0通过 1不通过")
    private String look;

    private String feesRoleid;

    @ApiModelProperty(value = "开始时间-起始日期")
    private Integer start;

    @ApiModelProperty(value = "开始时间-结束日期")
    private Integer end;
}
