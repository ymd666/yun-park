package cn.tnar.parkservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description: 新增会员分组请求实体
 * @date 2019年07月30日 14:22:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class MemberGroupReq {

    private Integer id;

    private Long no; //卡组no 新增时为空，更新时需有值

    private String name; //卡组名称
    /**
     * 当新增0目录的时候无需关联停车场，parkCodes就为空
     */
    private Integer type; //卡组类型 0 目录 1卡证

    private String parkCodes; //多个停车场编号逗号隔开

    private Long parentId; //父id，前端传递，如果是目录则为0，如果是卡证则传递当前选中的目录

    private Integer classify; //类别：0 根节点 1 临停车目录或卡组  2 访客车目录或卡组 3 白名单目录或者卡组 4 黑名单目录或者卡组 5 用户自定义目录或卡组

    private MCardLevelRelations mCardLevelRelations; //月卡层级关系

}
