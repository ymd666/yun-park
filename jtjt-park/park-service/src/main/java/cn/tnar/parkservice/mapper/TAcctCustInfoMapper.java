package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.entity.TAcctCustInfo;
import cn.tnar.parkservice.model.request.TAcctCustInfoRequest;
import cn.tnar.parkservice.model.response.TAcctCustInfoResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@QuerySupport
@Repository
@Mapper
public interface TAcctCustInfoMapper extends BaseMapper<TAcctCustInfo> {

    /**
     * 注册渠道分析 按月查询
     * @return
     */
    public List<Map<String,Object>> getStatisNumGroupForDays(@Param("beginWith") int beginWith, @Param("endWith") int endWith);

    /**
     * 注册渠道分析 按年查询
     * @return
     */
    public List<Map<String,Object>> getStatisNumGroupForMonth(@Param("beginWith") int beginWith, @Param("endWith") int endWith);

    /**
     * 各渠道总用户
     * @param beginWith
     * @param endWith
     * @return
     */
    public List<Map<String,Object>> getNumGroupByChannel(@Param("beginWith") int beginWith, @Param("endWith") int endWith);

    /**
     * 活跃用户
     * @param beginWith
     * @param endWith
     * @return
     */
    public List<Map<String,Object>> getActiveNumByTime(@Param("beginWith") int beginWith,
                                                       @Param("endWith") int endWith,
                                                       @Param("limitTime") long limitTime);

    /**
     * 普通用户
     * @param beginWith
     * @param endWith
     * @return
     */
    public List<Map<String,Object>> getCommonNumByTime(@Param("beginWith") int beginWith,
                                                       @Param("endWith") int endWith,
                                                       @Param("limitBegin") long limitBegin,
                                                       @Param("limitEnd") long limitEnd);


    /**
     * 僵尸用户
     * @param beginWith
     * @param endWith
     * @return
     */
    public List<Map<String,Object>> getInactiveNumByTime(@Param("beginWith") int beginWith,
                                                         @Param("endWith") int endWith,
                                                         @Param("limitTime") long limitTime);


    /**
     * 查询系统账号详情列表
     */
    List<TAcctCustInfoResponse> querySysAccount(Page page, @Param("param") TAcctCustInfoRequest request);
}
