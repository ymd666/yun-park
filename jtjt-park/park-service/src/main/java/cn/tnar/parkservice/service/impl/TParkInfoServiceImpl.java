package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.mapper.THouseIdentityApplyMapper;
import cn.tnar.parkservice.mapper.TParkDayParkingRecordMapper;
import cn.tnar.parkservice.mapper.TParkInfoMapper;
import cn.tnar.parkservice.mapper.TParkSerialnoReportLogMapper;
import cn.tnar.parkservice.model.dto.OrderAmountDto;
import cn.tnar.parkservice.model.entity.TBerthCount;
import cn.tnar.parkservice.model.entity.TParkInfo;
import cn.tnar.parkservice.model.request.CurrentViewRequset;
import cn.tnar.parkservice.model.response.ParkInfoInResponse;
import cn.tnar.parkservice.model.response.THouseIdentityApplyResponse;
import cn.tnar.parkservice.model.response.TparkInfoBaseResponse;
import cn.tnar.parkservice.service.ITParkInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zzb
 * @since 2018-12-17
 */
@Service("tParkInfoServiceImpl")
public class TParkInfoServiceImpl extends ServiceImpl<TParkInfoMapper, TParkInfo> implements ITParkInfoService {

    @Autowired
    private TParkInfoMapper mapper;

    @Autowired
    private THouseIdentityApplyMapper applyMapper;

    @Autowired
    private TParkDayParkingRecordMapper parkDayParkingRecordMapper;

    @Autowired
    private TParkSerialnoReportLogMapper parkSerialnoReportLogMapper;


    @Override
    public TParkInfo selectByParkCode(String parkCode) {
        return mapper.selectByParkCode(parkCode);
    }

    @Override
    public List<TParkInfo> selectParkNo(long custId) {
        List<TParkInfo> parkNoList = mapper.selectParkNo("");
        List<TParkInfo> monthCardList = new ArrayList<>();

        THouseIdentityApplyResponse apply = applyMapper.selectByCustId(custId);


        if (apply != null && apply.getApplyResult() == 5 && apply.getPayMode() == 2) {

            String activtyTime = applyMapper.getActivtyTime(apply.getCheapId());
            Long activtyTimeEnd = Long.valueOf(activtyTime.split("-")[1]);
            Long now = Long.valueOf(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            if (now < activtyTimeEnd) {
                List<TParkInfo> tParkInfo = mapper.selectParkNo(apply.getHouseId().toString());
                if (tParkInfo == null || tParkInfo.size() == 0) {
                    return parkNoList;
                }
                parkNoList.addAll(tParkInfo);
            }

        }

        for (int i = 0; i < parkNoList.size(); i++) {
            String parkCode = parkNoList.get(i).getParkCode();
            Integer total = mapper.countSeatTotal(parkCode);
            Integer use = mapper.countSeatUse(parkCode);
            Integer free = total - use;
            if (free < 0) {
                free = 0;
            }
            parkNoList.get(i).setCapacityNum(total);
            parkNoList.get(i).setFreeNum(free);
            //月卡是否支持购买开关 1：可以购买  2：不能购买
            if (parkNoList.get(i).getMembercarflag() == 1) {
                monthCardList.add(parkNoList.get(i));
            }
        }

        return monthCardList;
    }

    @Override
    public List<TParkInfo> selectPtype(String parkCode, int ptype) {
        return mapper.selectPtype(parkCode, ptype);
    }

    @Override
    public String getPosId(String custId) {
        return mapper.getPosId(custId);
    }

    @Override
    public List<ParkInfoInResponse> getParkInfoIn(String code) {
        return mapper.getParkInfoIn(code);
    }

    @Override
    public List<String> getParkCodeByCustId(String custId) {
        return mapper.getParkCodeByCustId(custId);
    }

    @Override
    public String getParkCodeByCustIdAndParkName(String custId, String parkName) {
        return mapper.getParkCodeByCustIdAndParkName(custId, parkName);
    }

    @Override
    public Map<String, Integer> queryParkInfoByParkCode(String parkCode) {

        return mapper.queryParkInfoByParkCode(parkCode);
    }

    @Override
    public Map<String, Object> statisGroupByParkType(CurrentViewRequset currentViewRequset) {
        return parkDayParkingRecordMapper.statisGroupByParkType(currentViewRequset);
    }

    @Override
    public List<Map<String, Object>> statisGroupByCarType(CurrentViewRequset currentViewRequset) {
        return parkDayParkingRecordMapper.statisGroupByCarType(currentViewRequset);
    }

    @Override
    public Map<String, Object> statisGroupByPayChannel(CurrentViewRequset currentViewRequset) {
        return parkSerialnoReportLogMapper.statisGroupByPayChannel(currentViewRequset);
    }

    @Override
    public Integer selectTransCount(String beginTime, String endTime, String parkCode, Integer ptype) {
        return mapper.selectTransCount(beginTime, endTime, ptype, parkCode);
    }

    @Override
    public Integer queryBerthInOrOut(Integer ptype, String parkCode) {
        return mapper.queryBerthInOrOut(ptype, parkCode);
    }

    @Override
    public Integer countTotal(String code) {
        return mapper.countTotal(code);
    }

    @Override
    public Integer queryRecordIn(String beginTime, String nowStr, String parkCode) {
        return mapper.queryRecordIn(beginTime, nowStr, parkCode);
    }

    @Override
    public Integer queryRecordOut(String beginTime, String nowStr, String parkCode) {
        return mapper.queryRecordOut(beginTime, nowStr, parkCode);
    }

    @Override
    public TBerthCount selectBerthCount(TBerthCount entity) {
        Long startTime = Long.valueOf(String.valueOf(entity.getStartTime()).substring(0, 8));
        Long endTime = Long.valueOf(String.valueOf(entity.getEndTime()).substring(0, 8));
        TBerthCount tBerthCount = mapper.selectTBerthCount(entity.getPtype(), startTime, endTime, entity.getParkCode());
        if (tBerthCount == null) {
            return new TBerthCount();
        }
        return tBerthCount;
    }

    @Override
    public Integer queryOccupyBerthNum(TBerthCount entity) {
        return mapper.queryOccupyBerthNum(entity);
    }

    @Override
    public List<OrderAmountDto> statisticToday(CurrentViewRequset currentViewRequset) {
        List<OrderAmountDto> results = new ArrayList<>();
        List<OrderAmountDto> amountDtos = mapper.statisticToday(currentViewRequset);
        for (int i = 0; i < 24; i++) {
            OrderAmountDto amountDto = new OrderAmountDto();
            amountDto.setTime(i);
            for (OrderAmountDto dto : amountDtos) {
                if (dto != null && dto.getTime() == i) {
                    amountDto.setNum(dto.getNum());
                }
            }
            results.add(amountDto);
        }
        return results;
    }

    @Override
    public List<Map<String, Object>> queryParkByPtype(int ptype) {
        return parkSerialnoReportLogMapper.queryParkByPtype(ptype);
    }

    @Override
    public List<Map<String, Object>> queryParkByPtypeCustId(int ptype, String custId) {
        return parkSerialnoReportLogMapper.queryParkByPtypeCustId(ptype, custId);
    }

    @Override
    public List<TparkInfoBaseResponse> selectBase() {
        return mapper.selectBase();
    }

    @Override
    public Map<String, Object> getTodayProject(String parkCode, String beginTime, String endTime, int type, int ptype) {
        return mapper.getTodayProject(parkCode, beginTime, endTime, type, ptype);
    }

    @Override
    public Integer countByOpenChannel(int open_channel) {
        return mapper.countByOpenChannel(open_channel);
    }

    @Override
    public List<Map<String, String>> queryAllParks(Map<String, Object> param) {
        String parkName = "";
        if(param.get("parkName")!=null){
             parkName= Optional.ofNullable(param.get("parkName").toString()).orElse("");
        }
        return mapper.queryAllParks(parkName);
    }

    /**
     * 根据停车场编号获取停车场名称
     *
     * @param parkCode
     * @return
     */
    @Override
    public String getParkNameByParkCode(String parkCode) {
        QueryWrapper<TParkInfo> queryWrapper = new QueryWrapper<TParkInfo>();
        queryWrapper.select("name")
                .eq("park_code", parkCode)
                .last("limit 1");
        TParkInfo one = mapper.selectOne(queryWrapper);
        return one != null ? one.getName() : null;
    }

    @Override
    public List<TparkInfoBaseResponse> selectByPackCode(int ptype, String parkCode) {

        return mapper.selectByPackCode(ptype, parkCode);
    }
}
