package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TSysSnogeneral;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zzb
 * @since 2019-03-05
 */
public interface ITSysSnogeneralService extends IService<TSysSnogeneral> {

    /**
     * 根据cust_id ，操作类型，step获取下一个编号
     *
     * @param operType
     * @return Long
     */
    Long GetNextSno(int operType) throws Exception;


    /**
     * 根据 日期+cust_id ，操作类型，step获取下一个序列号
     *
     * @param operType
     * @return Long
     */
    String GetNextPrefix(int operType,String prefix) throws Exception;

}
