package cn.tnar.parkservice.controller.customer;

import cn.tnar.parkservice.model.dto.TParkOfflineCouponsDto;
import cn.tnar.parkservice.model.entity.TParkDayParkingRecord;
import cn.tnar.parkservice.model.entity.TParkOfflineCoupons;
import cn.tnar.parkservice.service.*;
import cn.tnar.parkservice.util.RedisUtil;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年11月19日 09:22:32
 */
@Api(tags = "车道卡组计费会员同步controller")
@RestController
@RequestMapping("/flyosSync")
@Slf4j
public class flyosSyncController {


    @Autowired
    private ITParkMemberGroupService itParkMemberGroupService;

    @Autowired
    private ITParkFeeIndexService itParkFeeIndexService;

    @Autowired
    private ITParkGroupMemberinfoService itParkGroupMemberinfoService;

    @Autowired
    private ITParkMemberGroupRelationService itParkMemberGroupRelationService;

    @Autowired
    private ITParkOfflineCouponsService itParkOfflineCouponsService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private ITParkVisitHistoryService tParkVisitHistoryService;


    private static final ResultJson RESULT_JSON = new ResultJson();


    @PostMapping("/syncMemberGroup")
    @ApiOperation("车道主动调用岗亭接口同步卡组信息")
    public ResultJson syncToFlyos(@RequestBody String json) {
        return itParkMemberGroupService.syncToFlyos(json);
    }

    @PostMapping("/syncFeeIndexAndCalc")
    @ApiOperation("车道主动调用岗亭接口同步计费索引及计费规则")
    public ResultJson syncFeeIndexAndCalc(@RequestBody Map<String, String> map) {
        return itParkFeeIndexService.syncFeeIndexAndCalc(map);
    }


    @ApiOperation("车道主动调用岗亭接口同步会员")
    @PostMapping("/syncMemberInfo")
    public ResultJson syncMemberInfo(@RequestBody String json) {
        return itParkGroupMemberinfoService.syncMemberInfo(json);
    }


    /**
     * @param json
     * @return
     */
    @PostMapping("/pushParkRecord")
    @ApiOperation("车道推送停车流水数据到redis")
    public ResultJson pushParkRecord(@RequestBody String json) {
        try {
            TParkDayParkingRecord parkingRecord = JSON.parseObject(json, TParkDayParkingRecord.class);
            //根据车牌号获取用户，进而获取用户卡组关联表，获取最新卡组id
            String carId = parkingRecord.getCarId();
            Long groupId = itParkMemberGroupRelationService.getGroupIdByCarNo(carId);
            String ymd = String.valueOf(parkingRecord.getOuttime()).substring(0, 8);
            String key = groupId + "_" + ymd;
            redisUtil.lpush(key, JSON.toJSONString(parkingRecord));
            return RESULT_JSON.setCode(ResultCode.SUCCESS)
                    .setMsg(ResultCode.SUCCESS_MSG)
                    .setData(true);
        } catch (Exception e) {
            log.error("error", e);
            return RESULT_JSON.setCode(ResultCode.FAIL)
                    .setMsg(ResultCode.FAIL_MSG)
                    .setData(false);
        }
    }

    @PostMapping("/syncOfflineCoupons")
    @ApiOperation("车道主动同步线下优惠券")
    public ResultJson syncOfflineCoupons(@RequestBody String json) {
        try {
            Map map = JSON.parseObject(json, Map.class);
            String parkCode = map.get("parkCode").toString();
            //查询当前停车场的线下优惠券
            QueryWrapper<TParkOfflineCoupons>
                    wrapper = new QueryWrapper<>();
            wrapper.eq("park_code", parkCode).orderByAsc("create_time");
            List<TParkOfflineCoupons> list = itParkOfflineCouponsService.list(wrapper);
            List<TParkOfflineCouponsDto> tParkOfflineCouponsDtos = null;
            if (!CollectionUtils.isEmpty(list)) {
                tParkOfflineCouponsDtos = list.parallelStream().filter(x -> x != null)
                        .map(x -> new TParkOfflineCouponsDto().setFree(x.getFree())
                                .setTime_discont(x.getTimeDiscont())
                                .setMoney_discont(x.getMoneyDiscont())
                                .setState(x.getState())
                                .setTime_userdefine(x.getTimeUserdefine())
                                .setMoney_userdefine(x.getMoneyUserdefine())
                                .setStart_time(x.getCreateTime())
                                .setEnd_time(0L)
                                .setCounpon_name(x.getParkName())
                                .setId(x.getId()))
                        .collect(Collectors.toList());
            }
            return new ResultJson().setCode(ResultCode.SUCCESS)
                    .setMsg(ResultCode.SUCCESS_MSG)
                    .setData(tParkOfflineCouponsDtos);
        } catch (Exception e) {
            log.error("车道主动同步线下优惠券异常", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG).setData(null);
        }
    }

    @ApiOperation("车道手动同步访客车")
    @GetMapping("/combineVisit")
    public ResultJson combineVisit(@RequestParam String parkCode){
        return tParkVisitHistoryService.combineVisit(parkCode);
    }

}
