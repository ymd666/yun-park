package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkRegion;
import cn.tnar.parkservice.model.response.RegionInfo;
import cn.tnar.parkservice.model.response.VisitCardResponse;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zzb
 * @since 2018-12-08
 */
public interface ITParkRegionService extends IService<TParkRegion> {

    List<RegionInfo> queryCardAndRegion(@Param("regions") List<String> regions);

    List<String> getName(String regionCodes);
}
