package cn.tnar.yunpark.xunlang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author : CaineZhu E-mail:CaineZhu@me.com
 * @version : V1.0
 * @创建时间 ： 2017年5月2日 上午11:05:34
 * @类说明 : byte辅助类
 */
public class ByteTool {

    static Logger logger = LoggerFactory.getLogger(ByteTool.class);

    // short 转byte //低位在前高位在后
    public static byte[] shortToByteArray(short s) {
        byte[] targets = new byte[2];
        for (int i = 0; i < 2; i++) {
            int offset = (targets.length - 1 - i) * 8;
            targets[i] = (byte) ((s >>> offset) & 0xff);
        }
        return targets;
    }

    public static short byte2Short(byte... b) {
        return (short) (((b[0] & 0xff) << 8) | (b[1] & 0xff));
    }

    // short 转short //高位在前低位在后
    public static short convertShort(short value) {
        short mask = 0x00FF;
        short b, c;
        b = (short) ((value >> 8) & mask);
        c = (short) ((value << 8) & (~mask));
        value = (short) (b | c);
        return value;
    }

    // int 转byte //低位在前高位在后
    public static byte[] int2bytes(int num) {
        byte[] result = new byte[4];
        result[0] = (byte) ((num >>> 24) & 0xff);// 说明一
        result[1] = (byte) ((num >>> 16) & 0xff);
        result[2] = (byte) ((num >>> 8) & 0xff);
        result[3] = (byte) ((num >>> 0) & 0xff);
        return result;
    }

    public static int byte2Int(byte[] res) {
        // 一个byte数据左移24位变成0x??000000，再右移8位变成0x00??0000
        int targets = (res[3] & 0xff) | ((res[2] << 8) & 0xff00) // | 表示安位或
                | ((res[1] << 24) >>> 8) | (res[0] << 24);
        return targets;
    }


    public static String bytes2String(byte... bs) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bs) {
            // System.out.print(checkLen(b,2)+" ");
            sb.append(checkLen(b, 2) + " ");
        }
        return sb.toString();
    }

    public static String bytes2StringWithOutBlank(byte... bs) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bs) {
            // System.out.print(checkLen(b,2)+" ");
            sb.append(checkLen(b, 2));
        }
        return sb.toString();
    }

    public static String checkLen(String source, int len) {
        StringBuilder sb = new StringBuilder();
        for (int i = len - source.length(); i > 0; i--) {
            sb.append("0");
        }
        return sb.append(source).toString();
    }

    public static String checkLen(byte source, int len) {
        return checkLen(Integer.toHexString(source & 0xff), len);
    }

    public static byte[] timeStamp2Bytes(long timeStamp) {
        String now = Long.toString(timeStamp);
//		char[] arr = now.toCharArray();
//		byte[] by = new byte[13];
//		for(int i = 0;i<by.length;i++) {
//			by[i] = (byte) Integer.parseInt(arr[i]+"");
//		}
        return now.getBytes();
    }

    public static long bytes2Long(byte[] byteNum) {
        long num = 0;
        for (int ix = 0; ix < 8; ++ix) {
            num <<= 8;
            num |= (byteNum[ix] & 0xff);
        }
        return num;
    }

}
