package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.model.dto.CloudParkBilling;
import cn.tnar.parkservice.model.dto.CloudParkFeeIndex;
import cn.tnar.parkservice.model.entity.TParkFee;
import cn.tnar.parkservice.model.entity.TParkFeeIndex;
import cn.tnar.parkservice.service.*;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/23
 * @Description:
 */
@Api(tags = "计费索引及计费信息管理（张鑫）")
@RestController
@RequestMapping("service/feeIndex")
public class TParkFeeIndexController {

    private Logger log = LoggerFactory.getLogger(TParkFeeIndexController.class);

    @Autowired
    private KesbApiService kesbApiService;

    @Autowired
    @Qualifier("tParkFeeServiceImpl")
    private ITParkFeeService itParkFeeService;

    @Autowired
    @Qualifier("tParkFeeIndexServiceImpl")
    private ITParkFeeIndexService itParkFeeIndexService;


    @Autowired
    private MqService mqService;

    @Autowired
    @Qualifier("tParkMemberGroupServiceImpl")
    private ITParkMemberGroupService itParkMemberGroupService;

    @ApiOperation("添加停车场计费索引")
    @PostMapping("/add")
    public ResultJson addFeeIndex(@RequestBody Map<String, Object> param) {
        try {
            Map<String, String> result = kesbApiService.queryKesbApiByNestedMap(Constant.C_PARK_FEE_INDEX_ADD, param);
            if (result != null && result.size() > 0) {
                Long id = Long.valueOf(result.get("id"));
                if (id != null) {
                    TParkFeeIndex feeIndex = itParkFeeIndexService.getById(id);
                    if (feeIndex != null) {
                        String parkCode = feeIndex.getParkCode();
                        CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
                        cloudParkFeeIndex.setId(feeIndex.getId());
                        cloudParkFeeIndex.setName(feeIndex.getName());
                        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                        cloudParkFeeIndex.setDelflag(1);
                        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                        // mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.FEEINDEX_QUEUENAME_PREFIX + parkCode, JSON.toJSONString(cloudParkFeeIndex));
                    }
                }
            }
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("删除停车场计费索引")
    @PostMapping("/delete")
    public ResultJson deleteFeeIndex(@RequestBody Map<String, Object> param) {
        try {
            String parkCode = "";
            CloudParkFeeIndex cloudParkFeeIndex = null;
            //是否下发删除计费索引
            boolean flag = false;
            if (param != null && param.size() > 0) {
                Long id = Long.valueOf((String) param.get("id"));
                if (id != null) {
                    TParkFeeIndex feeIndex = itParkFeeIndexService.getById(id);
                    if (feeIndex != null) {
                        String regionCodes = itParkMemberGroupService.getRegionCodeByFeeIndexId(id);
                        if (StringUtils.isNotBlank(regionCodes)) {
                            flag = true; //需要下发
                        }
                        parkCode = feeIndex.getParkCode();
                        cloudParkFeeIndex = new CloudParkFeeIndex();
                        cloudParkFeeIndex.setId(feeIndex.getId());
                        cloudParkFeeIndex.setName(feeIndex.getName());
                        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                        cloudParkFeeIndex.setDelflag(2);//删除
                        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                        cloudParkFeeIndex.setRegionCodes(regionCodes);//设置区域编号
                    }
                }
            }
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_FEE_INDEX_DELETE, param);
            if (result != null && result.size() > 0 && StringUtils.isNotBlank(result.get("id")) && cloudParkFeeIndex != null) {
                if (flag) { //当前计费索引绑定了卡证，需要下发
                    log.info("下发【删除】计费索引到mq,消息体:" + JSON.toJSONString(cloudParkFeeIndex));
                    mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.FEEINDEX_QUEUENAME_PREFIX + parkCode, JSON.toJSONString(cloudParkFeeIndex));
                }
            }
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场计费索引")
    @PostMapping("/update")
    public ResultJson updateFeeIndex(@RequestBody Map<String, Object> param) {
        try {
            boolean flag = false;//是否下发
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_FEE_INDEX_UPDATE, param);
            if (result != null && result.size() > 0) {
                Long id = Long.valueOf(result.get("id"));
                if (id != null) {
                    TParkFeeIndex feeIndex = itParkFeeIndexService.getById(id);
                    if (feeIndex != null) {
                        String regionCodes = itParkMemberGroupService.getRegionCodeByFeeIndexId(id);
                        if (StringUtils.isNotBlank(regionCodes)) {
                            flag = true;
                        }
                        String parkCode = feeIndex.getParkCode();
                        CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
                        cloudParkFeeIndex.setId(feeIndex.getId());
                        cloudParkFeeIndex.setName(feeIndex.getName());
                        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                        cloudParkFeeIndex.setDelflag(3);//修改
                        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                        cloudParkFeeIndex.setRegionCodes(regionCodes);
                        if (flag) {
                            log.info("下发【修改】计费索引到mq,消息体:" + JSON.toJSONString(cloudParkFeeIndex));
                            mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.FEEINDEX_QUEUENAME_PREFIX + parkCode, JSON.toJSONString(cloudParkFeeIndex));
                        }
                    }
                }
            }
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场计费索引")
    @PostMapping("/query")
    public ResultJson queryFeeIndex(@RequestBody Map<String, Object> param) {
        try {
            log.info("查询停车场计费索引：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_FEE_INDEX_QUERY, param);
            log.info("查询停车场计费索引：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("查询停车场计费索引:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


    @ApiOperation("添加停车场计费信息")
    @PostMapping("/addFeeInfo")
    public ResultJson addFeeInfo(@RequestBody Map<String, Object> param) {
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_FEE_INFO_ADD, param);
            boolean flag = false; //是否下发
            if (result != null && result.size() > 0) {
                Long id = Long.valueOf(result.get("id"));
                if (id != null) {
                    TParkFee tParkFee = itParkFeeService.getById(id);
                    if (tParkFee != null) {
                        QueryWrapper<TParkFee> wrapper = new QueryWrapper<TParkFee>();
                        wrapper.eq("index_id", tParkFee.getIndexId());
                        //下发计费规则
                        List<TParkFee> list = itParkFeeService.list(wrapper);
                        //获取parkCode
                        String parkCode = "";
                        TParkFeeIndex feeIndex = itParkFeeIndexService.getById(tParkFee.getIndexId());
                        parkCode = feeIndex != null ? feeIndex.getParkCode() : "";

                        String regionCodes = itParkMemberGroupService.getRegionCodeByFeeIndexId(tParkFee.getIndexId());
                        if (StringUtils.isNotBlank(regionCodes)) {
                            flag = true;
                        }
                        CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
                        cloudParkFeeIndex.setId(feeIndex.getId());
                        cloudParkFeeIndex.setName(feeIndex.getName());
                        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                        cloudParkFeeIndex.setDelflag(1);//新增
                        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                        cloudParkFeeIndex.setRegionCodes(regionCodes);

                        CloudParkBilling cloudParkBilling = new CloudParkBilling();
                        cloudParkBilling.setId(tParkFee.getId());
                        cloudParkBilling.setIndex_id(tParkFee.getIndexId());
                        cloudParkBilling.setCartype(tParkFee.getCartype());
                        cloudParkBilling.setFreetime1(tParkFee.getFreetime1());
                        cloudParkBilling.setMonth(tParkFee.getMonth().floatValue());
                        cloudParkBilling.setT_limit(tParkFee.getTLimit().floatValue());
                        cloudParkBilling.setFreetime(tParkFee.getFreetime());
                        cloudParkBilling.setD_type(tParkFee.getDType());
                        cloudParkBilling.setD_preamt(tParkFee.getDPreamt().floatValue());
                        cloudParkBilling.setD_once(tParkFee.getDOnce().floatValue());
                        cloudParkBilling.setD_limit(tParkFee.getDLimit().floatValue());
                        cloudParkBilling.setD_begin(tParkFee.getDBegin().longValue());
                        cloudParkBilling.setD_end(tParkFee.getDEnd());
                        cloudParkBilling.setD_unittime1(tParkFee.getDUnittime1());
                        cloudParkBilling.setD_unittime2(tParkFee.getDUnittime2());
                        cloudParkBilling.setD_unittime3(tParkFee.getDUnittime3());
                        cloudParkBilling.setD_unittime4(tParkFee.getDUnittime4());
                        cloudParkBilling.setD_cycletime1(tParkFee.getDCycletime1());
                        cloudParkBilling.setD_cycletime2(tParkFee.getDCycletime2());
                        cloudParkBilling.setD_cycletime3(tParkFee.getDCycletime3());
                        cloudParkBilling.setD_cycletime4(tParkFee.getDCycletime4());
                        cloudParkBilling.setD_price1(tParkFee.getDPrice1().floatValue());
                        cloudParkBilling.setD_price2(tParkFee.getDPrice2().floatValue());
                        cloudParkBilling.setD_price3(tParkFee.getDPrice3().floatValue());
                        cloudParkBilling.setD_price4(tParkFee.getDPrice4().floatValue());
                        cloudParkBilling.setN_price1(tParkFee.getNPrice1().floatValue());
                        cloudParkBilling.setN_price2(tParkFee.getNPrice2().floatValue());
                        cloudParkBilling.setN_price3(tParkFee.getNPrice3().floatValue());
                        cloudParkBilling.setN_price4(tParkFee.getNPrice4().floatValue());
                        cloudParkBilling.setN_begin(tParkFee.getNBegin());
                        cloudParkBilling.setN_end(tParkFee.getNEnd());
                        cloudParkBilling.setN_limit(tParkFee.getNLimit().floatValue());
                        cloudParkBilling.setN_once(tParkFee.getNOnce().floatValue());
                        cloudParkBilling.setN_preamt(tParkFee.getNPreamt().floatValue());
                        cloudParkBilling.setN_type(tParkFee.getNType());
                        cloudParkBilling.setN_unittime1(tParkFee.getNUnittime1());
                        cloudParkBilling.setN_unittime2(tParkFee.getNUnittime2());
                        cloudParkBilling.setN_unittime3(tParkFee.getNUnittime3());
                        cloudParkBilling.setN_unittime4(tParkFee.getNUnittime4());
                        cloudParkBilling.setN_cycletime1(tParkFee.getNCycletime1());
                        cloudParkBilling.setN_cycletime2(tParkFee.getNCycletime2());
                        cloudParkBilling.setN_cycletime3(tParkFee.getNCycletime3());
                        cloudParkBilling.setN_cycletime4(tParkFee.getNCycletime4());
                        cloudParkBilling.setWorktype(tParkFee.getWorktype());
                        cloudParkBilling.setCount(list.size());
                        cloudParkBilling.setPark_code(parkCode);
                        //cloudParkBilling.setLicensekey("");
                        cloudParkBilling.setUser_type(tParkFee.getUserType());
                        cloudParkBilling.setDelflag(1);//新增

                        //cloudParkBilling.setCarplatecolor(1);
                        cloudParkBilling.setNewCarType(tParkFee.getCartype());
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("FeeIndex", cloudParkFeeIndex);
                        jsonObject.put("Billing", cloudParkBilling);
                        JSONArray jsonArray = new JSONArray();
                        jsonArray.add(jsonObject);
                        if (flag) {
                            log.info("下发【新增】计费规则到mq,消息体:" + jsonArray.toJSONString());
                            mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.TPARKFEE_QUEUENAME_PREFIX + parkCode, jsonArray.toJSONString());
                        }
                    }
                }
            }
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (
                KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }

    }

    @ApiOperation("删除停车场计费信息")
    @PostMapping("/deleteFeeInfo")
    public ResultJson deleteFeeInfo(@RequestBody Map<String, Object> param) {
        try {
            String parkCode = "";
            CloudParkBilling cloudParkBilling = null;
            JSONObject jsonObject = null;
            JSONArray jsonArray = null;
            boolean flag = false;
            if (param != null && param.size() > 0) {
                Long id = Long.valueOf((String) param.get("id"));
                if (id != null) {
                    TParkFee tParkFee = itParkFeeService.getById(id);
                    if (tParkFee != null) {
                        QueryWrapper<TParkFee> wrapper = new QueryWrapper<TParkFee>();
                        wrapper.eq("index_id", tParkFee.getIndexId());
                        //下发计费规则
                        List<TParkFee> list = itParkFeeService.list(wrapper);
                        TParkFeeIndex feeIndex = itParkFeeIndexService.getById(tParkFee.getIndexId());
                        parkCode = feeIndex != null ? feeIndex.getParkCode() : "";

                        String regionCodes = itParkMemberGroupService.getRegionCodeByFeeIndexId(tParkFee.getIndexId());
                        if (StringUtils.isNotBlank(regionCodes)) {
                            flag = true;
                        }
                        CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
                        cloudParkFeeIndex.setId(feeIndex.getId());
                        cloudParkFeeIndex.setName(feeIndex.getName());
                        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                        cloudParkFeeIndex.setDelflag(2);//删除
                        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                        cloudParkFeeIndex.setRegionCodes(regionCodes);

                        cloudParkBilling = new CloudParkBilling();
                        cloudParkBilling.setId(tParkFee.getId());
                        cloudParkBilling.setIndex_id(tParkFee.getIndexId());
                        cloudParkBilling.setCartype(tParkFee.getCartype());
                        cloudParkBilling.setFreetime1(tParkFee.getFreetime1());
                        cloudParkBilling.setMonth(tParkFee.getMonth().floatValue());
                        cloudParkBilling.setT_limit(tParkFee.getTLimit().floatValue());
                        cloudParkBilling.setFreetime(tParkFee.getFreetime());
                        cloudParkBilling.setD_type(tParkFee.getDType());
                        cloudParkBilling.setD_preamt(tParkFee.getDPreamt().floatValue());
                        cloudParkBilling.setD_once(tParkFee.getDOnce().floatValue());
                        cloudParkBilling.setD_limit(tParkFee.getDLimit().floatValue());
                        cloudParkBilling.setD_begin(tParkFee.getDBegin().longValue());
                        cloudParkBilling.setD_end(tParkFee.getDEnd());
                        cloudParkBilling.setD_unittime1(tParkFee.getDUnittime1());
                        cloudParkBilling.setD_unittime2(tParkFee.getDUnittime2());
                        cloudParkBilling.setD_unittime3(tParkFee.getDUnittime3());
                        cloudParkBilling.setD_unittime4(tParkFee.getDUnittime4());
                        cloudParkBilling.setD_cycletime1(tParkFee.getDCycletime1());
                        cloudParkBilling.setD_cycletime2(tParkFee.getDCycletime2());
                        cloudParkBilling.setD_cycletime3(tParkFee.getDCycletime3());
                        cloudParkBilling.setD_cycletime4(tParkFee.getDCycletime4());
                        cloudParkBilling.setD_price1(tParkFee.getDPrice1().floatValue());
                        cloudParkBilling.setD_price2(tParkFee.getDPrice2().floatValue());
                        cloudParkBilling.setD_price3(tParkFee.getDPrice3().floatValue());
                        cloudParkBilling.setD_price4(tParkFee.getDPrice4().floatValue());
                        cloudParkBilling.setN_price1(tParkFee.getNPrice1().floatValue());
                        cloudParkBilling.setN_price2(tParkFee.getNPrice2().floatValue());
                        cloudParkBilling.setN_price3(tParkFee.getNPrice3().floatValue());
                        cloudParkBilling.setN_price4(tParkFee.getNPrice4().floatValue());
                        cloudParkBilling.setN_begin(tParkFee.getNBegin());
                        cloudParkBilling.setN_end(tParkFee.getNEnd());
                        cloudParkBilling.setN_limit(tParkFee.getNLimit().floatValue());
                        cloudParkBilling.setN_once(tParkFee.getNOnce().floatValue());
                        cloudParkBilling.setN_preamt(tParkFee.getNPreamt().floatValue());
                        cloudParkBilling.setN_type(tParkFee.getNType());
                        cloudParkBilling.setN_unittime1(tParkFee.getNUnittime1());
                        cloudParkBilling.setN_unittime2(tParkFee.getNUnittime2());
                        cloudParkBilling.setN_unittime3(tParkFee.getNUnittime3());
                        cloudParkBilling.setN_unittime4(tParkFee.getNUnittime4());
                        cloudParkBilling.setN_cycletime1(tParkFee.getNCycletime1());
                        cloudParkBilling.setN_cycletime2(tParkFee.getNCycletime2());
                        cloudParkBilling.setN_cycletime3(tParkFee.getNCycletime3());
                        cloudParkBilling.setN_cycletime4(tParkFee.getNCycletime4());
                        cloudParkBilling.setWorktype(tParkFee.getWorktype());
                        cloudParkBilling.setCount(list.size());
                        cloudParkBilling.setPark_code(parkCode);
                        //cloudParkBilling.setLicensekey("");
                        cloudParkBilling.setUser_type(tParkFee.getUserType());
                        cloudParkBilling.setDelflag(2);//删除
                        //cloudParkBilling.setCarplatecolor(1);
                        cloudParkBilling.setNewCarType(tParkFee.getCartype());
                        jsonObject = new JSONObject();
                        jsonObject.put("FeeIndex", cloudParkFeeIndex);
                        jsonObject.put("Billing", cloudParkBilling);
                        jsonArray = new JSONArray();
                        jsonArray.add(jsonObject);
                    }
                }
            }

            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_FEE_INFO_DELETE, param);
            if (result != null && result.size() > 0 && StringUtils.isNotBlank(result.get("id")) && jsonArray != null) {
                if (flag) {
                    log.info("下发【删除】计费规则到mq,消息体:" + jsonArray.toJSONString());
                    mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.TPARKFEE_QUEUENAME_PREFIX + parkCode, jsonArray.toJSONString());
                }
            }
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场计费信息")
    @PostMapping("/updateFeeInfo")
    public ResultJson updateFeeInfo(@RequestBody Map<String, Object> param) {
        try {
            String parkCode = "";
            CloudParkBilling cloudParkBilling = null;
            JSONObject jsonObject = null;
            JSONArray jsonArray = null;
            Long id = null;
            boolean flag = false; //是否下发
            String regionCodes = "";
            if (param != null && param.size() > 0) {
                id = Long.valueOf((String) param.get("id"));
                if (id != null) {
                    TParkFee tParkFee = itParkFeeService.getById(id);
                    if (tParkFee != null) {
                        QueryWrapper<TParkFee> wrapper = new QueryWrapper<TParkFee>();
                        wrapper.eq("index_id", tParkFee.getIndexId());
                        //下发计费规则
                        List<TParkFee> list = itParkFeeService.list(wrapper);
                        TParkFeeIndex feeIndex = itParkFeeIndexService.getById(tParkFee.getIndexId());
                        regionCodes = itParkMemberGroupService.getRegionCodeByFeeIndexId(tParkFee.getIndexId());
                        if (StringUtils.isNotBlank(regionCodes)) {
                            flag = true;
                        }
                        CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
                        cloudParkFeeIndex.setId(feeIndex.getId());
                        cloudParkFeeIndex.setName(feeIndex.getName());
                        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                        cloudParkFeeIndex.setDelflag(2);//删除
                        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                        cloudParkFeeIndex.setRegionCodes(regionCodes);

                        parkCode = feeIndex != null ? feeIndex.getParkCode() : "";
                        cloudParkBilling = new CloudParkBilling();
                        cloudParkBilling.setId(tParkFee.getId());
                        cloudParkBilling.setIndex_id(tParkFee.getIndexId());
                        cloudParkBilling.setCartype(tParkFee.getCartype());
                        cloudParkBilling.setFreetime1(tParkFee.getFreetime1());
                        cloudParkBilling.setMonth(tParkFee.getMonth().floatValue());
                        cloudParkBilling.setT_limit(tParkFee.getTLimit().floatValue());
                        cloudParkBilling.setFreetime(tParkFee.getFreetime());
                        cloudParkBilling.setD_type(tParkFee.getDType());
                        cloudParkBilling.setD_preamt(tParkFee.getDPreamt().floatValue());
                        cloudParkBilling.setD_once(tParkFee.getDOnce().floatValue());
                        cloudParkBilling.setD_limit(tParkFee.getDLimit().floatValue());
                        cloudParkBilling.setD_begin(tParkFee.getDBegin().longValue());
                        cloudParkBilling.setD_end(tParkFee.getDEnd());
                        cloudParkBilling.setD_unittime1(tParkFee.getDUnittime1());
                        cloudParkBilling.setD_unittime2(tParkFee.getDUnittime2());
                        cloudParkBilling.setD_unittime3(tParkFee.getDUnittime3());
                        cloudParkBilling.setD_unittime4(tParkFee.getDUnittime4());
                        cloudParkBilling.setD_cycletime1(tParkFee.getDCycletime1());
                        cloudParkBilling.setD_cycletime2(tParkFee.getDCycletime2());
                        cloudParkBilling.setD_cycletime3(tParkFee.getDCycletime3());
                        cloudParkBilling.setD_cycletime4(tParkFee.getDCycletime4());
                        cloudParkBilling.setD_price1(tParkFee.getDPrice1().floatValue());
                        cloudParkBilling.setD_price2(tParkFee.getDPrice2().floatValue());
                        cloudParkBilling.setD_price3(tParkFee.getDPrice3().floatValue());
                        cloudParkBilling.setD_price4(tParkFee.getDPrice4().floatValue());
                        cloudParkBilling.setN_price1(tParkFee.getNPrice1().floatValue());
                        cloudParkBilling.setN_price2(tParkFee.getNPrice2().floatValue());
                        cloudParkBilling.setN_price3(tParkFee.getNPrice3().floatValue());
                        cloudParkBilling.setN_price4(tParkFee.getNPrice4().floatValue());
                        cloudParkBilling.setN_begin(tParkFee.getNBegin());
                        cloudParkBilling.setN_end(tParkFee.getNEnd());
                        cloudParkBilling.setN_limit(tParkFee.getNLimit().floatValue());
                        cloudParkBilling.setN_once(tParkFee.getNOnce().floatValue());
                        cloudParkBilling.setN_preamt(tParkFee.getNPreamt().floatValue());
                        cloudParkBilling.setN_type(tParkFee.getNType());
                        cloudParkBilling.setN_unittime1(tParkFee.getNUnittime1());
                        cloudParkBilling.setN_unittime2(tParkFee.getNUnittime2());
                        cloudParkBilling.setN_unittime3(tParkFee.getNUnittime3());
                        cloudParkBilling.setN_unittime4(tParkFee.getNUnittime4());
                        cloudParkBilling.setN_cycletime1(tParkFee.getNCycletime1());
                        cloudParkBilling.setN_cycletime2(tParkFee.getNCycletime2());
                        cloudParkBilling.setN_cycletime3(tParkFee.getNCycletime3());
                        cloudParkBilling.setN_cycletime4(tParkFee.getNCycletime4());
                        cloudParkBilling.setWorktype(tParkFee.getWorktype());
                        cloudParkBilling.setCount(list.size());
                        cloudParkBilling.setPark_code(parkCode);
                        //cloudParkBilling.setLicensekey("");
                        cloudParkBilling.setUser_type(tParkFee.getUserType());
                        cloudParkBilling.setDelflag(2);//删除
                        //cloudParkBilling.setCarplatecolor(1);
                        cloudParkBilling.setNewCarType(tParkFee.getCartype());
                        jsonObject = new JSONObject();
                        jsonObject.put("FeeIndex", cloudParkFeeIndex);
                        jsonObject.put("Billing", cloudParkBilling);
                        jsonArray = new JSONArray();
                        jsonArray.add(jsonObject);
                    }
                }
            }
            //更新成功
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_FEE_INFO_UPDATE, param);
            //发送删除计费规则的mq消息
            if (result != null && result.size() > 0 && StringUtils.isNotBlank(result.get("id")) && jsonArray != null) {
                if (flag) {
                    log.info("下发【更新】计费规则到mq,[1先删除]消息体:" + jsonArray.toJSONString());
                    mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.TPARKFEE_QUEUENAME_PREFIX + parkCode, jsonArray.toJSONString());
                }
            }
            if (id != null) {
                TParkFee tParkFee = itParkFeeService.getById(id);
                if (tParkFee != null) {
                    QueryWrapper<TParkFee> wrapper = new QueryWrapper<TParkFee>();
                    wrapper.eq("index_id", tParkFee.getIndexId());
                    //下发计费规则
                    List<TParkFee> list = itParkFeeService.list(wrapper);
                    //获取parkCode
//                    String parkCode = "";
                    TParkFeeIndex feeIndex = itParkFeeIndexService.getById(tParkFee.getIndexId());

                    CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
                    cloudParkFeeIndex.setId(feeIndex.getId());
                    cloudParkFeeIndex.setName(feeIndex.getName());
                    cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
                    cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
                    cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
                    cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
                    cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
                    cloudParkFeeIndex.setUtime(feeIndex.getUtime());
                    cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
                    cloudParkFeeIndex.setDelflag(1);//新增
                    cloudParkFeeIndex.setCycle(feeIndex.getCycle());
                    cloudParkFeeIndex.setRegionCodes(regionCodes);


                    parkCode = feeIndex != null ? feeIndex.getParkCode() : "";
                    cloudParkBilling = new CloudParkBilling();
                    cloudParkBilling.setId(tParkFee.getId());
                    cloudParkBilling.setIndex_id(tParkFee.getIndexId());
                    cloudParkBilling.setCartype(tParkFee.getCartype());
                    cloudParkBilling.setFreetime1(tParkFee.getFreetime1());
                    cloudParkBilling.setMonth(tParkFee.getMonth().floatValue());
                    cloudParkBilling.setT_limit(tParkFee.getTLimit().floatValue());
                    cloudParkBilling.setFreetime(tParkFee.getFreetime());
                    cloudParkBilling.setD_type(tParkFee.getDType());
                    cloudParkBilling.setD_preamt(tParkFee.getDPreamt().floatValue());
                    cloudParkBilling.setD_once(tParkFee.getDOnce().floatValue());
                    cloudParkBilling.setD_limit(tParkFee.getDLimit().floatValue());
                    cloudParkBilling.setD_begin(tParkFee.getDBegin().longValue());
                    cloudParkBilling.setD_end(tParkFee.getDEnd());
                    cloudParkBilling.setD_unittime1(tParkFee.getDUnittime1());
                    cloudParkBilling.setD_unittime2(tParkFee.getDUnittime2());
                    cloudParkBilling.setD_unittime3(tParkFee.getDUnittime3());
                    cloudParkBilling.setD_unittime4(tParkFee.getDUnittime4());
                    cloudParkBilling.setD_cycletime1(tParkFee.getDCycletime1());
                    cloudParkBilling.setD_cycletime2(tParkFee.getDCycletime2());
                    cloudParkBilling.setD_cycletime3(tParkFee.getDCycletime3());
                    cloudParkBilling.setD_cycletime4(tParkFee.getDCycletime4());
                    cloudParkBilling.setD_price1(tParkFee.getDPrice1().floatValue());
                    cloudParkBilling.setD_price2(tParkFee.getDPrice2().floatValue());
                    cloudParkBilling.setD_price3(tParkFee.getDPrice3().floatValue());
                    cloudParkBilling.setD_price4(tParkFee.getDPrice4().floatValue());
                    cloudParkBilling.setN_price1(tParkFee.getNPrice1().floatValue());
                    cloudParkBilling.setN_price2(tParkFee.getNPrice2().floatValue());
                    cloudParkBilling.setN_price3(tParkFee.getNPrice3().floatValue());
                    cloudParkBilling.setN_price4(tParkFee.getNPrice4().floatValue());
                    cloudParkBilling.setN_begin(tParkFee.getNBegin());
                    cloudParkBilling.setN_end(tParkFee.getNEnd());
                    cloudParkBilling.setN_limit(tParkFee.getNLimit().floatValue());
                    cloudParkBilling.setN_once(tParkFee.getNOnce().floatValue());
                    cloudParkBilling.setN_preamt(tParkFee.getNPreamt().floatValue());
                    cloudParkBilling.setN_type(tParkFee.getNType());
                    cloudParkBilling.setN_unittime1(tParkFee.getNUnittime1());
                    cloudParkBilling.setN_unittime2(tParkFee.getNUnittime2());
                    cloudParkBilling.setN_unittime3(tParkFee.getNUnittime3());
                    cloudParkBilling.setN_unittime4(tParkFee.getNUnittime4());
                    cloudParkBilling.setN_cycletime1(tParkFee.getNCycletime1());
                    cloudParkBilling.setN_cycletime2(tParkFee.getNCycletime2());
                    cloudParkBilling.setN_cycletime3(tParkFee.getNCycletime3());
                    cloudParkBilling.setN_cycletime4(tParkFee.getNCycletime4());
                    cloudParkBilling.setWorktype(tParkFee.getWorktype());
                    cloudParkBilling.setCount(list.size());
                    cloudParkBilling.setPark_code(parkCode);
                    //cloudParkBilling.setLicensekey("");
                    cloudParkBilling.setUser_type(tParkFee.getUserType());
                    cloudParkBilling.setDelflag(1);//新增

                    //cloudParkBilling.setCarplatecolor(1);
                    cloudParkBilling.setNewCarType(tParkFee.getCartype());
                    jsonObject = new JSONObject();
                    jsonObject.put("FeeIndex", cloudParkFeeIndex);
                    jsonObject.put("Billing", cloudParkBilling);
                    jsonArray = new JSONArray();
                    jsonArray.add(jsonObject);
                    if (flag) {
                        log.info("下发【更新】计费规则到mq,[2后新增]消息体:" + jsonArray.toJSONString());
                        mqService.convertAndSend(Constant.EXCHANGE_NAME, Constant.TPARKFEE_QUEUENAME_PREFIX + parkCode, jsonArray.toJSONString());
                    }
                }
            }
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场计费信息")
    @PostMapping("/queryFeeInfo")
    public ResultJson queryFeeInfo(@RequestBody Map<String, Object> param) {
        try {
            log.info("查询停车场计费信息：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_FEE_INFO_QUERY, param);
            log.info("查询停车场计费信息：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("查询停车场计费信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("停车场计费规则试算")
    @PostMapping("/ruleTest")
    public ResultJson ruleTest(@RequestBody Map<String, Object> param) {
        try {
            log.info("停车场计费规则试算：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_FEE_RULE_TEST, param);
            log.info("停车场计费规则试算：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("停车场计费规则试算:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("全国区域查询")
    @PostMapping("/region")
    public ResultJson regionQuery(@RequestBody Map<String, Object> param) {
        try {
            log.info("全国区域查询：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_COUNTRY_REGION_QUERY, param);
            log.info("全国区域查询：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("全国区域查询:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


}
