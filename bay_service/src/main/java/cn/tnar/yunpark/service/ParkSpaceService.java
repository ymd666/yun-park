package cn.tnar.yunpark.service;

import cn.tnar.yunpark.model.ParkSpace;

/**
 * 停车场车位信息服务
 * Created by tieh on 2017/6/14.
 */
public interface ParkSpaceService {
    ParkSpace getSpaceInfoBySensorId(String sensorId);
}
