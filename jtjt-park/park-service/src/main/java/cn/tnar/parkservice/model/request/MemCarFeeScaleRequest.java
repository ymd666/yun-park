package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: xinZhang
 * @Date: 2019/9/26
 * @Description:
 */
@ApiModel("新增套餐收费策略")
@Data
public class MemCarFeeScaleRequest {

    @ApiModelProperty(name = "数据记录id，修改记录时使用,新增不传",example = "10")
    private Long id;

    @ApiModelProperty(name = "收费标准名称",example = "")
    private String roleName;

    @ApiModelProperty(name = "车辆类型 1: 小型车 2: 中型车 3: 大型车 4: 电动车 5: 摩托车",example = "1")
    private Integer carType;

    @ApiModelProperty(name = "月卡类型 1: 月卡 2: 季卡 3: 半年卡 4: 年卡",example = "1")
    private Integer cardType;

    @ApiModelProperty(name = "运营商/停车场 cust_id",example = "")
    private Long custId;

    @ApiModelProperty(name = "区域id",example = "")
    private String regionCode;

    @ApiModelProperty(name = "收费金额",example = "")
    private BigDecimal monthValue;

    @ApiModelProperty(name = "开始时间",example = "20190901000000")
    private String startTime;

    @ApiModelProperty(name = "结束时间",example = "20190902000000")
    private String endTime;

    @ApiModelProperty(name = "备注",example = "")
    private String remark;

    @ApiModelProperty(name = "删除标识 1.正常 2.删除 新增时传1",example = "1")
    private Integer delflag;
}
