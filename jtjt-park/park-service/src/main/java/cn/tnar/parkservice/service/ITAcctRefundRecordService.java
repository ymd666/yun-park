package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TAcctRefundRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 11:27:38
 */
public interface ITAcctRefundRecordService extends IService<TAcctRefundRecord> {
}
