package cn.tnar.yunpark.huasai;

import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * Created by tieh on 2016/9/23.
 */
public class HuasaiHostMessage {
    HuasaiMessageHeader header;
    byte alarm;
    byte voltage;
    byte temp;

    public HuasaiMessageHeader getHeader() {
        return header;
    }

    public void setHeader(HuasaiMessageHeader header) {
        this.header = header;
    }

    public byte getAlarm() {
        return alarm;
    }

    public void setAlarm(byte alarm) {
        this.alarm = alarm;
    }

    public byte getVoltage() {
        return voltage;
    }

    public void setVoltage(byte voltage) {
        this.voltage = voltage;
    }

    public byte getTemp() {
        return temp;
    }

    public void setTemp(byte temp) {
        this.temp = temp;
    }

    public static HuasaiHostMessage parse(ByteBuf buf) {
        ByteBuf b = buf.order(ByteOrder.LITTLE_ENDIAN);
        HuasaiHostMessage msg = new HuasaiHostMessage();
        msg.setHeader(HuasaiMessageHeader.parse(buf));
        msg.setAlarm(b.readByte());
        msg.setVoltage(b.readByte());
        msg.setTemp(b.readByte());
        b.skipBytes(3);
        return msg;
    }
}
