import cn.tnar.yunpark.huasai.CRC16;
import cn.tnar.yunpark.huasai.HuasaiAck;
import cn.tnar.yunpark.huasai.HuasaiMessageDecoder;
import cn.tnar.yunpark.huasai.nbiot.HuasaiException;
import cn.tnar.yunpark.huasai.nbiot.HuasaiNbiotAck;
import cn.tnar.yunpark.huasai.nbiot.HuasaiNbiotMessage;
import cn.tnar.yunpark.util.Util;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * Created by tieh on 2017/6/17.
 */
public class TestHuasai {
    // RX HOST <=  21 7a 04 1a 00 01 00 00 00 17 0d 7a 01 4c 59 00 00 00 00 00 81 32 00 53 46
    @Test
    public void testSuzouCrc() {
        String p = "21 7a 04 1a 00 01 00 00 00 17 0d 7a 01 4c 59 00 00 00 00 00 81 32 00 53 46";
        String hex = p.replaceAll(" ", "");
        byte[] bs = Util.hexToBytes(hex);
        ByteBuf buf = Unpooled.wrappedBuffer(bs);
        assertTrue(HuasaiMessageDecoder.checkCrc(buf, 0, bs.length - 2));
    }


    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // TX HOST ACK =>  00 00 00 77 00 70 00 00 32 30 31 37 30 36 32 33 30 31 34 32 31 37 c6 87
    @Test
    public void testSuzouCrcAck() throws ParseException {
        HuasaiAck ack = new HuasaiAck(0);
        ack.setDate(DATE_FORMAT.parse("2017-06-23 01:42:17"));
        ByteBuf ackBuf = ack.encode();
        assertEquals("00000077007000003230313730363233303134323137c687", Util.toHex(ackBuf));
    }

    @Test
    // TX NBIOT 210E001700000100000001000000000000E2AB0000060104509F0B0000EB4C
              //210B0016000001000000010000000044E503000000000000010801042991070000049D
    public void tesNbiotRcvCRC() throws HuasaiException {
        String p = "210B0016000001000000010000000044E503000000000000010801042991070000049D";
        byte[] bs = Util.hexToBytes(p);
        HuasaiNbiotMessage msg = HuasaiNbiotMessage.decode(bs, bs.length);
        assertNotNull(msg);
    }

    @Test
    // RX NBIOT 77 00 70 00 01 00 00 00 00 00 20 17 06 23 10 39 34 26 1C
    public void testCrc6() {
        HuasaiNbiotAck ack = new HuasaiNbiotAck(1);
        ack.setTime(LocalDateTime.parse("2017-06-23T10:39:34"));
        assertEquals("7700700001000000000020170623103934261C", Util.toHex(ack.encode()));
    }

    @Test
    public void testNbiotCRC() {
        String p = "210E00170000010000000100000000594D3F560000070104509F0E0000"; //9997
        byte[] bs = Util.hexToBytes(p);
        System.out.println(String.format("%04x", CRC16.calc(bs)));
    }

    @Test
    public void testRssiConvert() {
        for(int i = 0; i <= 31; i++) {
            System.out.println(String.format("%d => %d%%", i, (31 - i) * 100 / 31));
        }
    }
}
