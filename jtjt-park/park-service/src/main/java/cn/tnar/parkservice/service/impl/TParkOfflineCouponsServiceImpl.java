package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkOfflineCouponsMapper;
import cn.tnar.parkservice.model.entity.TParkOfflineCoupons;
import cn.tnar.parkservice.service.ITParkOfflineCouponsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 16:31:36
 */
@Service("tParkOfflineCouponsServiceImpl")
public class TParkOfflineCouponsServiceImpl extends ServiceImpl<TParkOfflineCouponsMapper, TParkOfflineCoupons> implements ITParkOfflineCouponsService {

}
