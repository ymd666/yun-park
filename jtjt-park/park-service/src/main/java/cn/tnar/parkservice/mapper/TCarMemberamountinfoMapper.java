package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.entity.TCarMemberamountinfo;
import cn.tnar.parkservice.model.response.RecordResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-08-29
 */
@Mapper
@QuerySupport
@Repository
public interface TCarMemberamountinfoMapper extends BaseMapper<TCarMemberamountinfo> {

    List<RecordResponse> getAmountInfo(Page<RecordResponse> page, @Param("memberInfoId") Long memberInfoId);
    List<RecordResponse> getStopRecord(Page<RecordResponse> page, @Param("memberInfoId") Long memberInfoId);
}
