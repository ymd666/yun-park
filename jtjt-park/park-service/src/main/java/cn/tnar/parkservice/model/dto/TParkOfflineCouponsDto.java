package cn.tnar.parkservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 16:37:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class TParkOfflineCouponsDto {
    private Integer free;
    private String time_discont;
    private String money_discont;
    private Integer state;
    private Integer time_userdefine;
    private Integer money_userdefine;
    private Long start_time; //create_time
    private Long end_time;  //0
    private String counpon_name; //parkName
    private Long id;
    private Integer status; //0 新增 1 修改 2 删除

}
