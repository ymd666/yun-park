package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.response.TOrderResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TParkOrderMapper {

    List<Map<String,String>> getDailyParkRecord(Page page,@Param("param") Map<String,Object> map);

    List<Map<String,String>> getHisParkRecord(Page page, @Param("param") Map<String, Object> map);

    List<Map<String,Object>> getParkingRecord(Page page, @Param("param") Map<String, Object> map);

    Map<String,Object> getAmount(@Param("batchno") String batchno);

    Map<String,Object> getMemCarAmt(@Param("batchno") String batchno);

    Map<String,Object> getCouponAmt(@Param("batchno") String batchno);

    List<TOrderResponse> queryDayOrder(@Param("param") Map<String,String> param);

    List<TOrderResponse> queryHisOrder(@Param("param") Map<String,String> param);

    Map<String,Object> getAccount(@Param("param") Map<String,String> param);

    Map<String,Object> getDailyAmount(@Param("batchno") String batchno);

    Map<String,Object> getDailyMemCarAmt(@Param("batchno") String batchno);

    Map<String,Object> getDailyCouponAmt(@Param("batchno") String batchno);

}
