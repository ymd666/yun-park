package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.TDictionary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface TDictionaryMapper extends BaseMapper<TDictionary> {

    List<Map<String, Object>> batchQuery(@Param("category") List<String> category);
}
