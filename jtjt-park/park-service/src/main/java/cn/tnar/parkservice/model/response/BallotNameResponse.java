package cn.tnar.parkservice.model.response;

import lombok.Data;

@Data
public class BallotNameResponse {

    private String custName;

    private String houseNum;

    private String phone;
}
