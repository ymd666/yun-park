package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.QianTuOutLogDto;
import cn.tnar.parkservice.model.entity.TParkDayParkingRecord;
import cn.tnar.parkservice.model.entity.UpParkingRecord;
import cn.tnar.parkservice.model.request.CurrentViewRequset;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Mapper
@QuerySupport
@Repository
public interface TParkDayParkingRecordMapper extends BaseMapper<TParkDayParkingRecord> {

    void updateTParkDayParkingRecord(@Param("plateNumber") String plateNumber, @Param("timeIn") String timeIn, @Param("timeOut") String timeOut);

    @Select("select serialno as iden , park_code,intime as inTime ,outtime as outTime,cartype as carType ,car_id as plateNum from t_park_day_parking_record where  tracetype =4 AND ((DATE_FORMAT(intime,'%Y%m%d%H%i')>=#{beginTime} and DATE_FORMAT(intime,'%Y%m%d%H%i') < #{nowStr} )or (DATE_FORMAT(outtime,'%Y%m%d%H%i')>=#{beginTime} and DATE_FORMAT(outtime,'%Y%m%d%H%i')< #{nowStr} ))  order by park_code")
    List<QianTuOutLogDto> queryRecord(@Param("beginTime") String beginTime, @Param("nowStr") String nowStr);

    @Select("select serialno as iden , park_code,intime as inTime ,outtime as outTime,cartype as carType ,car_id as plateNum from t_park_day_parking_record where  park_code in(${parkCode}) AND ((DATE_FORMAT(intime,'%Y%m%d%H%i')>=#{beginTime} and DATE_FORMAT(intime,'%Y%m%d%H%i') < #{nowStr} )or (DATE_FORMAT(outtime,'%Y%m%d%H%i')>=#{beginTime} and DATE_FORMAT(outtime,'%Y%m%d%H%i')< #{nowStr} ))  order by park_code")
    List<QianTuOutLogDto> queryRecordInfo(@Param("parkCode") String parkCode, @Param("beginTime") String beginTime, @Param("nowStr") String nowStr);


    @Select("select serialno as iden , park_code,intime as inTime ,outtime as outTime,cartype as carType ,car_id as plateNum from t_park_day_parking_record where id=#{id}")
    List<QianTuOutLogDto> queryRecordById(@Param("id") long id);

    @Select("SELECT  IFNULL(sum(space_count),0)  FROM `t_park_region`  where park_code=#{code} ")
    Integer countSeatTotal(@Param("code") String code);

    @Select("SELECT  IFNULL(sum(free_num),0)  FROM `t_park_info`  where park_code=#{code} ")
    Integer countSeatSpace(@Param("code") String code);

    @Select("SELECT count(1) FROM `t_park_day_parking_record` WHERE result=90 and park_code=#{code}")
    Integer countSeatUse(@Param("code") String code);

    @Select("select count(1) from t_park_day_parking_record where (DATE_FORMAT(intime,'%Y%m%d%H%i')>=#{beginTime} and DATE_FORMAT(intime,'%Y%m%d%H%i') < #{nowStr} ) and park_code =#{parkCode} and outoperate <>6")
    Integer queryRecordIn(@Param("beginTime") String beginTime, @Param("nowStr") String nowStr, @Param("parkCode") String parkCode);

    @Select("select count(1) from t_park_day_parking_record where  (DATE_FORMAT(outtime,'%Y%m%d%H%i')>=#{beginTime} and DATE_FORMAT(outtime,'%Y%m%d%H%i')< #{nowStr} )  and park_code =#{parkCode} and outoperate <>6")
    Integer queryRecordOut(@Param("beginTime") String beginTime, @Param("nowStr") String nowStr, @Param("parkCode") String parkCode);


    @Select("select count(1) from t_park_day_parking_record where  ( (DATE_FORMAT(intime,'%Y%m%d%H%i%s')<= #{beginTime} and DATE_FORMAT(outtime,'%Y%m%d%H%i%s') = 0) or  (DATE_FORMAT(intime,'%Y%m%d%H%i%s')<= #{beginTime} and DATE_FORMAT(outtime,'%Y%m%d%H%i%s') > #{beginTime}) )  and park_code =#{parkCode} and outoperate <>6")
    Integer queryZero(@Param("beginTime") String beginTime, @Param("parkCode") String parkCode);


    @Select("select park_code from t_park_info where delflag =1 ")
    List<String> queryParkCodes();

    List<UpParkingRecord> queryRecordLimitByTime(@Param("direction") int direction, @Param("start") String start, @Param("end") String end);

    /**
     * 今日收入情况分析
     * t_park_info  ptype 1路内 2路外
     *
     * @param req
     * @return
     */
    Map<String, Object> statisGroupByParkType(@Param("req") CurrentViewRequset req);

    /**
     * 今日进出场车辆分析
     *
     * @param req
     * @return
     */

    List<Map<String, Object>> statisGroupByCarType(@Param("req") CurrentViewRequset req);


}