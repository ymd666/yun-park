package cn.tnar.yunpark.careyes.nbiot.model;

import cn.tnar.yunpark.util.ByteTool;
import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * package: cn.tnar.yunpark.careyes.nbiot.model
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/12 17:03
 * desc   : HeartBeat
 */
public class HeartBeat extends Message {

    private int deviceType;
    private int len;
    private String sensorId;
    private byte[] sensorIdBytes = new byte[6];
    private int city;
    private int area;
    private String parkId;
    private byte[] parkIdBytes = new byte[4];
    private int csq;
    private int vbattery;
    private int sequence;

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public byte[] getSensorIdBytes() {
        return sensorIdBytes;
    }

    public void setSensorIdBytes(byte[] sensorIdBytes) {
        this.sensorIdBytes = sensorIdBytes;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }

    public byte[] getParkIdBytes() {
        return parkIdBytes;
    }

    public void setParkIdBytes(byte[] parkIdBytes) {
        this.parkIdBytes = parkIdBytes;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getCsq() {
        return csq;
    }

    public void setCsq(int csq) {
        this.csq = csq;
    }

    public int getVbattery() {
        return vbattery;
    }

    public void setVbattery(int vbattery) {
        this.vbattery = vbattery;
    }

    @Override
    public String toString() {
        return "HeartBeat{" +
                "deviceType=" + deviceType +
                ", len=" + len +
                ", sensorId='" + sensorId + '\'' +
                ", sensorIdBytes=" + Arrays.toString(sensorIdBytes) +
                ", city=" + city +
                ", area=" + area +
                ", parkId='" + parkId + '\'' +
                ", parkIdBytes=" + Arrays.toString(parkIdBytes) +
                ", csq=" + csq +
                ", vbattery=" + vbattery +
                ", sequence=" + sequence +
                '}';
    }

    public static HeartBeat parse(ByteBuf byteBuf) {
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        HeartBeat t = new HeartBeat();
        buf.skipBytes(2);
        buf.skipBytes(1);
        buf.skipBytes(1);
        t.setDeviceType(buf.readByte());
        byte[] sensorIdBytes = new byte[6];
        buf.readBytes(sensorIdBytes);
//        String sid = ByteTool.bytes2StringWithOutBlank(sensorIdBytes);
        t.setSensorId(ByteTool.bytes2StringWithOutBlank(sensorIdBytes));
        t.setSensorIdBytes(sensorIdBytes);
        buf.skipBytes(1);
        t.setCity(buf.readByte());
        t.setArea(buf.readByte());
        byte[] parkIdBytes = new byte[4];
        buf.readBytes(parkIdBytes);
        t.setParkIdBytes(parkIdBytes);
        t.setParkId(ByteTool.bytes2StringWithOutBlank(parkIdBytes));
        buf.skipBytes(6);
        buf.skipBytes(6);
        t.setCsq(buf.readByte() & 0xff);
        buf.skipBytes(1);
        buf.skipBytes(1);
        t.setVbattery(buf.readByte() & 0xff);
        buf.skipBytes(1);
        t.setSequence(buf.readByte());
        return t;
    }
}
