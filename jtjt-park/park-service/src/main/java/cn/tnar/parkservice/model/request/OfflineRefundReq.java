package cn.tnar.parkservice.model.request;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 10:59:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("线下退款请求体")
public class OfflineRefundReq {

    @ApiModelProperty("开始日期")
    private Integer startDate;

    @ApiModelProperty("截止日期")
    private Integer endDate;

    @ApiModelProperty("退款金额")
    private BigDecimal refoundAmt;

    @ApiModelProperty("停车场的custId")
    private Long custId;

    @ApiModelProperty("停车场的parkCode")
    private String parkCode;

    @ApiModelProperty("套餐id")
    private Long feesRoleId;

    @ApiModelProperty("套餐名称")
    private String feesRoleName;

    @ApiModelProperty("套餐金额")
    private BigDecimal monthValue;

    @ApiModelProperty("操作员id")
    private Long operId;

    @ApiModelProperty("区域编号")
    private String regionCode;

    @ApiModelProperty("会员id")
    private Long memberId;

    @ApiModelProperty("车位id")
    private Long seatId;

    @ApiModelProperty("车牌号")
    private String carId;

    @ApiModelProperty("客户号")
    private Long customer_custId;

    @ApiModelProperty("手机号")
    private String phone;


    public static void main(String[] args) {
        OfflineRefundReq offlineRefundReq = new OfflineRefundReq();
        offlineRefundReq.setStartDate(0);
        offlineRefundReq.setEndDate(0);
        offlineRefundReq.setRefoundAmt(new BigDecimal("0"));
        offlineRefundReq.setCustId(0L);
        offlineRefundReq.setParkCode("1");
        offlineRefundReq.setFeesRoleId(0L);
        offlineRefundReq.setFeesRoleName("");
        offlineRefundReq.setMonthValue(new BigDecimal("0"));
        offlineRefundReq.setOperId(0L);
        offlineRefundReq.setRegionCode("");
        offlineRefundReq.setMemberId(0L);
        offlineRefundReq.setSeatId(0L);
        offlineRefundReq.setCarId("");
        offlineRefundReq.setCustomer_custId(0L);
        offlineRefundReq.setPhone("");


        System.out.println(JSON.toJSONString(offlineRefundReq));
    }
}
