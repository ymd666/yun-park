package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkCalendar;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月04日 19:50:47
 */

public interface ITParkCalendarService extends IService<TParkCalendar> {


}
