package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class QianTuOutLogDto {

    //车牌号码
    private String plateNum;

    //进场时间
    private long inTime;

    //出场时间
    private long outTime;

    //车类型
    private String carType;

    private String parkCode;

    //记录唯一标识    流水号
    private String iden;

    private Integer pageNum;

    private Integer pageSize;
}
