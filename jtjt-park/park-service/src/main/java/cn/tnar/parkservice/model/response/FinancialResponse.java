package cn.tnar.parkservice.model.response;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class FinancialResponse implements Serializable {
    private BigDecimal auto_pay_time_relate_account;
    private BigDecimal balance_actual_account;
    private long balance_total_cnt;
    private long charge_amt_cnt;
    private BigDecimal citycard_actual_account;
    private BigDecimal citycard_third_actual_account;
    private long citycard_total_cnt;
    private BigDecimal clear_amt;
    private long clear_cnt;
    private BigDecimal elec_coupon_amt;
    private long elec_coupon_cnt;
    private BigDecimal endbalance;
    private long feeCnt;
    private BigDecimal feeRate;
    private BigDecimal feeamt;
    private BigDecimal lastbalance;
    private BigDecimal monthcard_amt;
    private long monthcard_count;
    private BigDecimal new_actual_amt;
    private long new_actual_cnt;
    private BigDecimal new_citycard_amt;
    private long new_citycard_cnt;
    private BigDecimal new_clear_elec_amt;
    private long new_clear_elec_cnt;
    private BigDecimal new_elec_amt;
    private long new_elec_cnt;
    private BigDecimal new_ex_actual_amt;
    private long new_ex_actual_cnt;
    private BigDecimal new_ex_epay_amt;
    private long new_ex_epay_cnt;
    private BigDecimal new_freepay_amt;
    private long new_freepay_cnt;
    private BigDecimal new_inner_card_amt;
    private long new_inner_card_cnt;
    private long new_normal_cnt;
    private BigDecimal new_offline_amt;
    private long new_offline_cnt;
    private BigDecimal noclear_ec_amt;
    private long noclear_ec_count;
    private long occur_date;
    private BigDecimal offline_coupon_amt;
    private long offline_coupon_cnt;
    private BigDecimal owner_pay_time_relate_account;
    private BigDecimal reveral_amt;
    private long reveral_cnt;
    private BigDecimal reveral_fee_amt;
    private long reveral_fee_cnt;
    private BigDecimal self_third_weixin_account;
    private BigDecimal self_third_zhifubao_account;
    private BigDecimal self_weixin_account;
    private BigDecimal self_zhifubao_account;
    private long selfaccount_flag;
    private long shdm;
    private BigDecimal sumMember_account;
    private long sumMember_cnt;
    private String tollname;
    private BigDecimal total_actual_account;
    private long total_car_cnt;
    private long trans_cnt;
    private BigDecimal transamt;
    private BigDecimal yu_epay_amt;
    private long self_total_cnt;
    private long yu_epay_cnt;


}
