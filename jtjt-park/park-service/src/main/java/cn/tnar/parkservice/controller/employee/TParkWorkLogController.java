package cn.tnar.parkservice.controller.employee;


import cn.tnar.parkservice.model.request.ParkShiftRequset;
import cn.tnar.parkservice.model.request.ParkWorkLogRequset;
import cn.tnar.parkservice.model.response.ParkShiftResponse;
import cn.tnar.parkservice.model.response.TParkWorkLogResponse;
import cn.tnar.parkservice.service.ITParkWorkLogService;
import cn.tnar.parkservice.util.ExcelUtil;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;


/**
 * @author Tiyer.Tao
 * @since 2018-09-18
 */
@Api(tags = "出勤统计(陶锐)")
@RestController
@RequestMapping("service/tParkWorkLog")
public class TParkWorkLogController {

    private ResultJson json = new ResultJson();

    @Autowired
    @Qualifier("tParkWorkLogServiceImpl")
    private ITParkWorkLogService it_park_work_logService;

    @ApiOperation(value = "出勤统计查询 - t_park_work_log")
    @PostMapping(value = "queryParkWorkLog")
    public ResultJson queryParkWorkLog(@RequestBody ParkWorkLogRequset parkWorkLogRequset) {
        //isvalid 1 正常  2未上班  3迟到   4代班
//        if(parkWorkLogRequset.getStarTime()>0){
//            parkWorkLogRequset.setStarTime(Long.valueOf(parkWorkLogRequset.getStarTime() + "000000"));
//        }
//        if(parkWorkLogRequset.getEndTime()>0){
//            parkWorkLogRequset.setEndTime(Long.valueOf(parkWorkLogRequset.getEndTime() + "235959"));
//        }

        Page<TParkWorkLogResponse> list = it_park_work_logService.query(parkWorkLogRequset);
        if (list != null && list.getSize() > 0) {
            json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } else {
            json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.FAIL_MSG).setData(null);
        }
        return json;
    }


    @ApiOperation(value = "出勤统计查询(zzb) - t_park_work_log")
    @PostMapping(value = "queryParkWorkLogNew")
    public ResultJson queryParkWorkLogNew(@RequestBody ParkWorkLogRequset parkWorkLogRequset) {
        //isvalid 1 正常  2未上班  3迟到   4代班
        /*if(parkWorkLogRequset.getStarTime()>0){
            parkWorkLogRequset.setStarTime(Long.valueOf(parkWorkLogRequset.getStarTime() + "000000"));
        }
        if(parkWorkLogRequset.getEndTime()>0){
            parkWorkLogRequset.setEndTime(Long.valueOf(parkWorkLogRequset.getEndTime() + "235959"));
        }*/

        Page<TParkWorkLogResponse> list = it_park_work_logService.queryNew(parkWorkLogRequset);
        if (list != null && list.getSize() > 0) {
            json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } else {
            json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.FAIL_MSG).setData(null);
        }
        return json;
    }

    @ApiOperation(value = "工作排班详情 - t_park_shift")
    @PostMapping(value = "queryWorkShiftDetail")
    public ResultJson queryWorkShiftDetail(@RequestBody ParkShiftRequset parkShiftRequset) {
        Page<ParkShiftResponse> list = it_park_work_logService.getShiftDetail(parkShiftRequset);
        if (list != null && list.getSize() > 0) {
            json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } else {
            json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.FAIL_MSG).setData(null);
        }
        return json;
    }

    @ApiOperation(value = "考勤导出 - t_park_work_log")
    @RequestMapping(value = "excelOut")
    public void excelOut(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> reqMap) throws Exception {
        ParkWorkLogRequset parkWorkLogRequset = new ParkWorkLogRequset();
        String endtime = reqMap.get("endTime").toString().equals("") ? "0" : reqMap.get("endTime").toString();
        String isvalid = reqMap.get("isvalid").toString().equals("") ? "0" : reqMap.get("isvalid").toString();
        String startime = reqMap.get("starTime").toString().equals("") ? "0" : reqMap.get("starTime").toString();

        parkWorkLogRequset.setCurrent(Integer.parseInt(reqMap.get("current").toString()));
        parkWorkLogRequset.setEndTime(new Long(endtime));
        parkWorkLogRequset.setIsvalid(Integer.parseInt(isvalid));
        parkWorkLogRequset.setName(reqMap.get("name").toString());
        parkWorkLogRequset.setParkCode(reqMap.get("parkCode").toString());
        parkWorkLogRequset.setSize(Integer.parseInt(reqMap.get("size").toString()));
        parkWorkLogRequset.setStarTime(new Long(startime));
        //获取数据
        Page<TParkWorkLogResponse> list = it_park_work_logService.queryNew(parkWorkLogRequset);


        //excel标题
        String[] title = {"序号", "停车场名称", "姓名", "账号", "角色名称", "出勤日期", "上班时间", "上班时间", "签到时间", "签出时间", "上班状态", "代班人"
        };
        //excel文件名
        String fileName = "出勤统计.xlsx";

        //sheet名
        String sheetName = "出勤统计";
        String[][] content = new String[list.getRecords().size()][];
        for (int i = 0; i < list.getRecords().size(); i++) {

            TParkWorkLogResponse obj = list.getRecords().get(i);
            content[i] = new String[title.length];
            content[i][0] = String.valueOf(i + 1);
            content[i][1] = obj.getParkName();
            content[i][2] = obj.getName();
            content[i][3] = obj.getLoginname();

            String tollType = obj.getTollType();
            if (("").equals(tollType) || tollType == null) {
                tollType = "";
            } else {
                switch (tollType) {
                    case "1":
                        tollType = "岗亭收费员";
                        break;
                    case "2":
                        tollType = "中央收费员";
                        break;
                    case "3":
                        tollType = "管理人员";
                        break;
                    case "4":
                        tollType = "路边收费员";
                        break;
                    case "5":
                        tollType = "巡查人员";
                        break;

                    default:
                        tollType = "";
                        break;
                }
            }

            content[i][4] = tollType;
            String onDuty = obj.getOnDuty();
            String offDuty = obj.getOffDuty();
            if (("").equals(onDuty) || onDuty == null || ("0").equals(onDuty) ) {
                content[i][5] = "-";
                content[i][8] = "-";
            } else {
                content[i][5] = onDuty.substring(0, 4) + "-" + onDuty.substring(4, 6) + "-" + onDuty.substring(6, 8);
                content[i][8] = onDuty.substring(8, 10) + ":" + onDuty.substring(10, 12) + ":" + onDuty.substring(12, 14);
            }

            if (("").equals(offDuty) || offDuty == null || ("0").equals(offDuty)) {
                content[i][9] = "-";

            } else {
                content[i][9] = offDuty.substring(8, 10) + ":" + offDuty.substring(10, 12) + ":" + offDuty.substring(12, 14);
            }

            String beginTime = obj.getBeginTime();
            String endTime = obj.getEndTime();

            if (!"".equals(beginTime) && !"".equals(endTime) && beginTime.length() == 7 && endTime.length() == 7) {
                beginTime = beginTime.substring(1, 3) + ":" + beginTime.substring(3, 5) + ":" + beginTime.substring(5, 7);
                endTime = endTime.substring(1, 3) + ":" + endTime.substring(3, 5) + ":" + endTime.substring(5, 7);
            }
            content[i][6] = beginTime;
            content[i][7] = endTime;
            content[i][10] = obj.getStatusName();

            String insteadMan = obj.getInsteadMan();
            if (("").equals(insteadMan) || insteadMan == null) {
                content[i][11] = "-";
            } else {
                content[i][11] = obj.getInsteadMan();
            }
        }

        //创建HSSFWorkbook
        XSSFWorkbook wb = ExcelUtil.getXSSFWorkbook(sheetName, title, content, null);

        //响应到客户端
        try {
            this.setResponseHeader(response, fileName);
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //发送响应流方法
    public void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            response.reset();
            response.setContentType("application/ms-excel");
            response.setCharacterEncoding("utf-8");

            try {
                fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}

