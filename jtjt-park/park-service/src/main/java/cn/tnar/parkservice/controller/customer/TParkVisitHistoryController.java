package cn.tnar.parkservice.controller.customer;


import cn.tnar.parkservice.service.ITParkVisitHistoryService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zzb
 * @since 2019-08-28
 */
@Api(tags = "月卡-访客车(张志彪)")
@RestController
@RequestMapping("service/tParkVisitHistory")
public class TParkVisitHistoryController {

    @Autowired
    private ITParkVisitHistoryService itParkVisitHistoryService;

    @ApiOperation(value = "卡组-查询访客")
    @PostMapping(value = "select")
    private ResultJson select(@RequestBody String json) {
        return itParkVisitHistoryService.select(json);
    }

}

