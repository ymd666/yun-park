package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * Created by fly on 2018/10/5.
 */
public class TBerthCount extends Model<TBerthCount> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer capacityNum;//泊位数

    private Double totalShouldAccount;//应收

    private Double totalActualAccount;//实收

    private Double totalCouponAccount;//优惠券抵用金券

    private Integer occupyBerthNum;//占用泊位数

    private Integer transBerthNum;//流转次数

    private Integer ptype;//1-路内 2 -路外

    private Integer idle;//1-空闲 2-占用

    private Long intime;//时间

    private Long startTime;//开始时间

    private Long endTime;//结束时间

    private String parkCode;//停车场编号

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public Integer getCapacityNum() {
        return capacityNum;
    }

    public void setCapacityNum(Integer capacityNum) {
        this.capacityNum = capacityNum;
    }

    public Double getTotalShouldAccount() {
        return totalShouldAccount;
    }

    public void setTotalShouldAccount(Double totalShouldAccount) {
        this.totalShouldAccount = totalShouldAccount;
    }

    public Double getTotalActualAccount() {
        return totalActualAccount;
    }

    public void setTotalActualAccount(Double totalActualAccount) {
        this.totalActualAccount = totalActualAccount;
    }

    public Double getTotalCouponAccount() {
        return totalCouponAccount;
    }

    public void setTotalCouponAccount(Double totalCouponAccount) {
        this.totalCouponAccount = totalCouponAccount;
    }

    public Integer getOccupyBerthNum() {
        return occupyBerthNum;
    }

    public void setOccupyBerthNum(Integer occupyBerthNum) {
        this.occupyBerthNum = occupyBerthNum;
    }

    public Integer getTransBerthNum() {
        return transBerthNum;
    }

    public void setTransBerthNum(Integer transBerthNum) {
        this.transBerthNum = transBerthNum;
    }

    public Integer getPtype() {
        return ptype;
    }

    public void setPtype(Integer ptype) {
        this.ptype = ptype;
    }

    public Integer getIdle() {
        return idle;
    }

    public void setIdle(Integer idle) {
        this.idle = idle;
    }

    public Long getIntime() {
        return intime;
    }

    public void setIntime(Long intime) {
        this.intime = intime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
