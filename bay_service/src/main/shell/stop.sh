#!/usr/bin/env bash
name=bay-service

pid=`ps aux | grep $name | grep -v grep | awk 'NR==1{print $2}'`
if [ -z $pid ]
then
    echo $name not running
else
    echo killing $pid
    kill $pid
fi
