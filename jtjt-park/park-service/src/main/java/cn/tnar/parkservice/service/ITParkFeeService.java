package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkFee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:38:31
 */
public interface ITParkFeeService extends IService<TParkFee> {

}
