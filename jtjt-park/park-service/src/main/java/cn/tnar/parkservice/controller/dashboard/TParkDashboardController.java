package cn.tnar.parkservice.controller.dashboard;

import cn.tnar.parkservice.model.request.CurrentViewRequset;
import cn.tnar.parkservice.service.ITParkInfoService;
import cn.tnar.parkservice.service.TParkDashboardService;
import cn.tnar.parkservice.util.DateUtil1;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author: xinZhang
 * @Date: 2019/8/23
 * @Description:
 */
@Api(tags = "首页数据统计（张鑫）")
@RestController
@RequestMapping("service/dashboard")
public class TParkDashboardController {

    private Logger logger = LoggerFactory.getLogger(TParkDashboardController.class);

    @Autowired
    private TParkDashboardService tParkDashboardService;

    @Autowired
    private ITParkInfoService itParkInfoService;

    @ApiOperation(value = "最近一周进出场统计")
    @PostMapping(value = "getWeekCount")
    public ResultJson getWeekCount(@RequestBody String json) {
        CurrentViewRequset req = JSON.parseObject(json, CurrentViewRequset.class);
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = -6; i <= 0; i++) {
            Map<String, Object> map = new HashMap<>();
            String date = week(i);
            String beginTime = date + "000000";
            String nowStr = week(i + 1) + "000000";
            Integer park_lot_cnt = itParkInfoService.countTotal(req.getParkCode());
            Integer in_car_count = itParkInfoService.queryRecordIn(beginTime, nowStr, req.getParkCode());
            Integer out_car_count = itParkInfoService.queryRecordOut(beginTime, nowStr, req.getParkCode());
            map.put("park_lot_cnt", park_lot_cnt);
            map.put("in_car_count", in_car_count);
            map.put("out_car_count", out_car_count);
            map.put("date", Integer.valueOf(date));
            map.put("shdm", i + 8);
            list.add(map);
        }
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
    }

    public static String week(int num) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, num);
        Date date = calendar.getTime();
        String yesterday = new SimpleDateFormat("yyyyMMdd").format(date);
        return yesterday;
    }

    @ApiOperation("首页统计")
    @PostMapping("/count")
    public ResultJson count(@RequestBody Map<String,Object> map){
        Date dayStartTime = DateUtil1.getDayStartTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String startTime = sdf.format(dayStartTime);
        String endTime = sdf.format(new Date());
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        return tParkDashboardService.count(map);
    }

    @ApiOperation("临停，访客，月租车统计-最近一周统计")
    @PostMapping("/weekCarCount")
    public ResultJson weekCarCount(@RequestBody CurrentViewRequset request){
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = -6; i <= 0; i++) {
            String date = week(i);
            String beginTime = date + "000000";
            String nowStr = week(i + 1) + "000000";
            Map<String,Object> map ;
            if (i == 0){
                 map= tParkDashboardService.weekCarCount(beginTime, nowStr, request.getParkCode(),true);
            }else {
                 map = tParkDashboardService.weekCarCount(beginTime, nowStr, request.getParkCode(),false);
            }
            map.put("date",date);
            map.put("index",i + 8);
            list.add(map);
        }
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
    }

    @ApiOperation("当天卡组类型进场统计")
    @PostMapping("/groupCarCount")
    public ResultJson groupCarCount(@RequestBody Map<String,Object> request){
        List<Map<String, Object>> maps = tParkDashboardService.groupCarCount(request);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(maps);
    }

    @ApiOperation("当天进出场自定义卡组分析")
    @PostMapping("/diyMemCarCount")
    public ResultJson diyMemCarCount(@RequestBody Map<String,Object> request){
        List<Map<String, Object>> list = tParkDashboardService.diyMemCarCount(request);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
    }
}
