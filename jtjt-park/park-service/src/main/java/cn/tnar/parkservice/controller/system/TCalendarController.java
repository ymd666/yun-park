package cn.tnar.parkservice.controller.system;


import cn.tnar.parkservice.service.ITCalendarService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zzb
 * @since 2019-03-11
 */
@Api(tags = "日历导入(张志彪)")
@RestController
@RequestMapping("t-calendar")
public class TCalendarController {

    @Autowired
    @Qualifier("tCalendarServiceImpl")
    private ITCalendarService itCalendarService;

    private ResultJson resultJson = new ResultJson();

    @ApiOperation(value = "查询是否存在该年份日历")
    @PostMapping(value = "query")
    public ResultJson query(@RequestParam("file") MultipartFile file) {
        return itCalendarService.query(file);
    }

    @ApiOperation(value = "下载该年份日历")
    @PostMapping(value = "downCal")
    public void downCal(HttpServletResponse response, @RequestParam("year") int year) {
        itCalendarService.downCal(response, year);
    }


    @ApiOperation(value = "导入日历")
    @PostMapping(value = "excelInCal")
    public ResultJson excelInCal(@RequestParam("file") MultipartFile file) {
        return itCalendarService.excelInCal(file);
    }


    @ApiOperation(value = "获取该年份日历")
    @PostMapping(value = "queryCal")
    public ResultJson queryCal(@RequestBody String json) {
        return itCalendarService.queryCal(json);
    }


}

