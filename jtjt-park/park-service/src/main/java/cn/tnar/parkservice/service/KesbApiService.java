package cn.tnar.parkservice.service;

import java.util.List;
import java.util.Map;

public interface KesbApiService{


    public Map<String,String> queryKesbApi(String serviceId, Map<String,Object> req);

    public List<Map<String, String>> queryKesbApiList(String serviceId, Map<String,Object> req);

    Map<String,String> queryKesbApiByNestedMap(String serviceId,Map<String,Object> req);
}
