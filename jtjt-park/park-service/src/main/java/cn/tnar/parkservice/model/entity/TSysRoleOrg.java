package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/10/10
 * @Description:
 */
@Data
@TableName("t_sys_role_org")
public class TSysRoleOrg {

    private Long id;

    private Integer auditFlag;

    private Integer maxAssignMembers;

    private Long modifyOper;

    private Long modifyTime;

    private Long orgId;

    private Long roleId;

    private Integer stat;

    private String remarks;
}
