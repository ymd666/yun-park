package cn.tnar.parkservice;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("cn.tnar.parkservice.mapper")
@EnableScheduling
public class ParkServiceApplication {

    private static final Logger logger = LoggerFactory.getLogger(ParkServiceApplication.class);

    @Bean
    public ApplicationRunner runner() {
        return args -> {
            logger.info("here we go!");
            logger.debug("debug we go!");
            logger.error("error we go!");
            logger.trace("trace we go!");
            logger.warn("warn we go!");
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(ParkServiceApplication.class, args);
    }

}
