package cn.tnar.parkservice.model.dto;

import lombok.Data;

/**
 * 车牌颜色构成
 *
 * @author Tiyer.Tao
 * @since 2018-10-15
 */
@Data
public class OrderAmountDto {

    private double num = 0;

    private int time;

    private double chargeamt = 0;

    private double eleamt = 0;

}
