package cn.tnar.parkservice.util.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.http.HttpStatus;

import java.io.Serializable;

@ApiModel(description = "数据传输对象")
public class ResultJson implements Serializable {
    private static final long serialVersionUID = 1L;
    private static volatile ResultJson instance = null;
    @ApiModelProperty("数据消息")
    private String msg;
    @ApiModelProperty("传输状态码")
    private Integer code;
    @ApiModelProperty("传输数据")
    private Object data;

    public ResultJson() {
    }

    public static class Builder {
        private String msg;

        private Integer code;

        private Object data;

        public Builder(Integer code) {
            this.code = code;
        }

        public Builder msg(String msg) {
            this.msg = msg;
            return this;
        }

        public Builder data(Object data) {
            this.data = data;
            return this;
        }

        public ResultJson build() {
            ResultJson resultJson = new ResultJson();
            return resultJson.setMsg(msg).setCode(code).setData(data);
        }
    }

    public static Builder success() {
        return new Builder(HttpStatus.SC_OK);
    }


    public static Builder clientError() {
        return new Builder(HttpStatus.SC_BAD_REQUEST);
    }

    public static Builder fail() {
        return new Builder(900);
    }

    public static Builder code(int code) {
        return new Builder(code);
    }

    public static ResultJson getInstance() {
        if (instance == null) {
            synchronized (ResultJson.class) {
                if (instance == null) {
                    instance = new ResultJson();
                }
            }
        }
        return instance;
    }

    public String getMsg() {
        return msg;
    }

    public ResultJson setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public ResultJson setCode(Integer code) {
        this.code = code;
        return this;
    }

    public Object getData() {
        return data;
    }

    public ResultJson setData(Object data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "resultJson [code=" + code + ", message=" + msg + ", data=" + data + "]";
    }

}
