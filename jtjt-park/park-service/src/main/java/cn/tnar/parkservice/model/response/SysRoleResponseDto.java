package cn.tnar.parkservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月30日 09:55:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class SysRoleResponseDto {

    private Long id;

    /**
     * 角色代码
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色类型 1系统管理角色，2业务岗位角色，3个人角色
     */
    private Integer roleType;

    /**
     * 角色允许分配最大人员数
     */
    private Integer maxAssignMembers;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 父节点id
     */
    private Long parentRoleId;

    /**
     * 子节点
     */
    private List<SysRoleResponseDto> childs = new ArrayList<SysRoleResponseDto>();

}
