package cn.tnar.parkservice.controller.order;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.exception.CustomException;
import cn.tnar.parkservice.service.ITParkMemberGroupService;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.service.TParkOrderService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/23
 * @Description:
 */
@Api(tags = "订单管理（张鑫）")
@RestController
@RequestMapping("service/order")
public class TParkOrderController {

    private Logger log = LoggerFactory.getLogger(TParkOrderController.class);

    @Autowired
    private KesbApiService kesbApiService;

    @Autowired
    private TParkOrderService tParkOrderService;

    @Autowired
    private ITParkMemberGroupService tParkMemberGroupService;


    @ApiOperation("正常订单查询")
    @PostMapping("/listInfo")
    public ResultJson listInfo(@RequestBody Map<String,String> request){
        if (StringUtils.isBlank(request.get("parkCode"))){
            throw new CustomException(Constant.MSG_PARKCODE_NULL);
        }
        try {
            return tParkOrderService.queryOrder(request);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("欠费记录流水查询")
    @PostMapping("/arrearage")
    public ResultJson arrearageList(@RequestBody String json){
        try {
            Map param = JSON.parseObject(json, Map.class);
            log.info("arrearage-欠费记录流水查询:c接口请求参数,param======>" +param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_ORDER_ARREARAGE, param);
            log.info("arrearage-欠费记录流水查询:c接口返回参数,param======>" + list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("欠费补交流水查询")
    @PostMapping("/arrearagePay")
    public ResultJson arrearagePay(@RequestBody String json){
        try {
            Map param = JSON.parseObject(json, Map.class);
            log.info("arrearagePay-欠费补交流水查询:c接口请求参数,param======>" +param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_ORDER_ARREARAGE_PAY, param);
            log.info("arrearagePay-欠费补交流水查询:c接口返回参数,param======>" + list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("异常订单查询")
    @PostMapping("/abNormal")
    public ResultJson abNormalList(@RequestBody String json){
        try {
            Map param = JSON.parseObject(json, Map.class);
            log.info("abNormal-异常订单查询:c接口请求参数,param======>" +param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_ORDER_ABNORMAL, param);
            log.info("abNormal-异常订单查询:c接口返回参数,param======>" + list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("坏账查询")
    @PostMapping("/badDebt")
    public ResultJson queryBadDebt(@RequestBody String json){
        try {
            Map param = JSON.parseObject(json, Map.class);
            log.info("badDebt-坏账查询:c接口请求参数,param======>" +param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_ORDER_BADDEBT, param);
            log.info("badDebt-坏账查询:c接口返回参数,param======>" + list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("停车流水列表查询")
    @PostMapping("/parkRecord")
    public ResultJson parkRecords(@RequestBody Map<String,Object> map){
        //通过卡组id查询卡证id集合
        List<Long> groupIds = new ArrayList<>();
//        List<String> groupId = tParkMemberGroupService.selectByParentId(groupIds, Long.valueOf(map.get("groupId").toString()));
        if ("4".equals(map.get("recordType").toString())){
            String groupId = map.get("groupId").toString();
            List<Long> longs = new ArrayList<>();
            if (StringUtils.isNotBlank(groupId)){
                 longs = tParkMemberGroupService.queryGroupIdsByParentId(groupIds, Long.valueOf(groupId), map.get("park_code").toString());
                 longs.add(Long.valueOf(groupId));
            }
            map.put("groupIds",longs);
            }
        return tParkOrderService.getParkRecord(map);
    }

    @ApiOperation("/停车中流水查询")
    @PostMapping("/parking")
    public ResultJson getParkingRecord(@RequestBody Map<String,Object> map){
        return tParkOrderService.getParkingRecord(map);
    }



}
