package cn.tnar.yunpark.puxd;

/**
 * Created by tieh on 2017/6/23.
 */
public class PuxdUtil {
    /**
     * 车位状态转换，普信达格式(0无车1有车8异常) -> LBM格式(01无车02有车99异常)
     */
    public static String toLbmStatus(int puxdStatus) {
        switch (puxdStatus) {
            case 0:
                return "1";
            case 1:
                return "2";
            default:
                return "99";
        }
    }
}
