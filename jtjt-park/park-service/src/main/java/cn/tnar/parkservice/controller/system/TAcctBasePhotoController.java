package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.model.dto.TAcctBasePhotoDto;
import cn.tnar.parkservice.model.entity.TAcctBasePhoto;
import cn.tnar.parkservice.service.ITAcctBasePhotoService;
import cn.tnar.parkservice.util.DtoUtil;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author clarck
 * @since 2018-09-18
 */
@Api(tags = "保存图片信息（陶锐）")
@RestController
@RequestMapping("service/tAcctBasePhoto")
public class TAcctBasePhotoController {

    @Autowired
    private ITAcctBasePhotoService it_acct_base_photoService;

    private ResultJson json = new ResultJson();
    
    @ApiOperation(value = "保存图片信息 - t_acct_base_photo")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody TAcctBasePhotoDto model) {
        TAcctBasePhoto entity = DtoUtil.convertObject(model, TAcctBasePhoto.class);
        entity.setAddtime(Long.valueOf(new SimpleDateFormat("yyyyMMhhHHmmss").format(new Date())));
        boolean flag = it_acct_base_photoService.save(entity);
        if (flag) {
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        } else {
            return json.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }
    }

    @ApiOperation(value = "根据数据库字段查询图片信息 object_id ,file_name,id等 - t_acct_base_photo")
    @PostMapping(value = "queryByCondition")
    public ResultJson queryByCondition(@RequestBody Map<String,Object> map) {
        List<TAcctBasePhoto> list = (List<TAcctBasePhoto>) it_acct_base_photoService.listByMap(map);
        if (list!=null) {
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } else {
            return json.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }
    }

}

