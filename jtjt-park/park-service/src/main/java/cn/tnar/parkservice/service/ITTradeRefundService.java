package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.request.TradeRefundRequset;
import cn.tnar.parkservice.model.response.TradeRefundResponse;
import cn.tnar.parkservice.model.entity.TTradeRefund;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

/**
 *  退款信息 服务类
 *
 * @author Tiyer.Tao
 * @since 2018-09-20
 */
public interface ITTradeRefundService extends IService<TTradeRefund> {

    /**
     * 退款信息查询
     * @param tradeRefundRequset
     * @return
     */
    public Page<TradeRefundResponse> getRefundByPage(TradeRefundRequset tradeRefundRequset);

    /**
     * 退款审核
     * @return
     */
    public boolean updateAuditRefund(TTradeRefund tradeRefund);

    /**
     * 退款申请
     * @param orderNumber
     * @param refundamount
     * @return
     */
    public  boolean updateTradeCommodityState(String orderNumber, BigDecimal refundamount);

    /**
     * 退款信息查询(导出List)
     * @param parkCode  停车场编码
     * @param carNo 车编号
     * @param orderNumber 订单编号
     * @param refundState 退款状态
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    List<TradeRefundResponse> getRefundList(String parkCode, String carNo, String orderNumber, Integer refundState, Long startTime, Long endTime);
}
