package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * @Description:停车场准入规则实体
 * @author: yl
 * @date 2018/9/13 15:09
 */
@TableName("t_park_access_rule")
public class AccessRule extends Model<AccessRule> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 准入规则名称
     */
    private String ruleName;
    /**
     * 准入停车场名称
     */
    private String parkName;
    /**
     * 准入停车场编号
     */
    private String parkCode;
    /**
     * 准入停车区域
     */
    private String regionCode;
    /**
     * 准入区域名称
     */
    private String regionName;
    /**
     * 准入出入口
     */
    private String accessGate;
    /**
     * 准入出入口名称
     */
    private String gateName;
    /**
     * 会员车位数
     */
    private Integer memberNum;

    /**
     * 临停车位数
     */
    private Integer temporaryNum;

    /**
     * 备注
     */
    private String remark;
    /**
     * 储值车位数
     */
    private Integer storeNum;

    /**
     * 预约车位数
     */
    private Integer bookingNum;

    /**
     * 访客车位数
     */
    private Integer visitorNum;

    /**
     * 月卡车颜色
     */
    private String memberColor;

    /**
     * 临停车颜色
     */
    private String temporaryColor;

    /**
     * 储值车颜色
     */
    private String storeColor;

    /**
     * 预约车颜色
     */
    private String bookingColor;

    /**
     * 访客车颜色
     */
    private String visitorColor;

    /**
     * 单双号限停
     */
    private Integer carstopLimit;

    /**
     * 启用状态
     */
    private Integer isUsing;
    /**
     * 准入车辆类型，如1-临时车、2-月卡车、3-储值车，4-内部储值车，5-免费车，6-内部车等
     */
    private String cardType;
    /**
     * 准入车牌颜色，1-蓝，3-黄，6-绿，7-白
     */
    private String carType;
    /**
     * 准入开始时间
     */
    private Long startTime;
    /**
     * 准入结束时间
     */
    private Long endTime;
    /**
     * 总车位数限制
     */
    private Integer totalNum;
    /**
     * 无牌车准入数
     */
    private Integer noplateNum;
    /**
     * 其他规则设置
     */
    private String otherRule;
    /**
     * 准入规则id
     */
    private Long accessRuleId;



    @Override
    protected Serializable pkVal() {
        return null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getAccessGate() {
        return accessGate;
    }

    public void setAccessGate(String accessGate) {
        this.accessGate = accessGate;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Integer getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(Integer memberNum) {
        this.memberNum = memberNum;
    }

    public Integer getTemporaryNum() {
        return temporaryNum;
    }

    public void setTemporaryNum(Integer temporaryNum) {
        this.temporaryNum = temporaryNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStoreNum() {
        return storeNum;
    }

    public void setStoreNum(Integer storeNum) {
        this.storeNum = storeNum;
    }

    public Integer getBookingNum() {
        return bookingNum;
    }

    public void setBookingNum(Integer bookingNum) {
        this.bookingNum = bookingNum;
    }

    public Integer getVisitorNum() {
        return visitorNum;
    }

    public void setVisitorNum(Integer visitorNum) {
        this.visitorNum = visitorNum;
    }

    public String getMemberColor() {
        return memberColor;
    }

    public void setMemberColor(String memberColor) {
        this.memberColor = memberColor;
    }

    public String getTemporaryColor() {
        return temporaryColor;
    }

    public void setTemporaryColor(String temporaryColor) {
        this.temporaryColor = temporaryColor;
    }

    public String getStoreColor() {
        return storeColor;
    }

    public void setStoreColor(String storeColor) {
        this.storeColor = storeColor;
    }

    public String getBookingColor() {
        return bookingColor;
    }

    public void setBookingColor(String bookingColor) {
        this.bookingColor = bookingColor;
    }

    public String getVisitorColor() {
        return visitorColor;
    }

    public void setVisitorColor(String visitorColor) {
        this.visitorColor = visitorColor;
    }

    public Integer getCarstopLimit() {
        return carstopLimit;
    }

    public void setCarstopLimit(Integer carstopLimit) {
        this.carstopLimit = carstopLimit;
    }

    public Integer getIsUsing() {
        return isUsing;
    }

    public void setIsUsing(Integer isUsing) {
        this.isUsing = isUsing;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getNoplateNum() {
        return noplateNum;
    }

    public void setNoplateNum(Integer noplateNum) {
        this.noplateNum = noplateNum;
    }

    public String getOtherRule() {
        return otherRule;
    }

    public void setOtherRule(String otherRule) {
        this.otherRule = otherRule;
    }
    public Long getAccessRuleId() {
        return accessRuleId;
    }

    public void setAccessRuleId(Long accessRuleId) {
        this.accessRuleId = accessRuleId;
    }

    @Override
    public String toString() {
        return "AccessRule{" +
                "id=" + id +
                ", ruleName='" + ruleName + '\'' +
                ", parkName='" + parkName + '\'' +
                ", parkCode='" + parkCode + '\'' +
                ", regionCode='" + regionCode + '\'' +
                ", regionName='" + regionName + '\'' +
                ", accessGate='" + accessGate + '\'' +
                ", gateName='" + gateName + '\'' +
                ", memberNum=" + memberNum +
                ", temporaryNum=" + temporaryNum +
                ", remark='" + remark + '\'' +
                ", storeNum=" + storeNum +
                ", bookingNum=" + bookingNum +
                ", visitorNum=" + visitorNum +
                ", memberColor='" + memberColor + '\'' +
                ", temporaryColor='" + temporaryColor + '\'' +
                ", storeColor='" + storeColor + '\'' +
                ", bookingColor='" + bookingColor + '\'' +
                ", visitorColor='" + visitorColor + '\'' +
                ", carstopLimit=" + carstopLimit +
                ", isUsing=" + isUsing +
                ", cardType='" + cardType + '\'' +
                ", carType='" + carType + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", totalNum=" + totalNum +
                ", noplateNum=" + noplateNum +
                ", otherRule='" + otherRule + '\'' +
                ", accessRuleId=" + accessRuleId +
                '}';
    }
}
