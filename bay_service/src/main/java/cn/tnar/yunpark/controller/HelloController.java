package cn.tnar.yunpark.controller;

import cn.tnar.yunpark.model.ParkSpace;
import cn.tnar.yunpark.service.ParkSpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @Autowired
    ParkSpaceService parkSpaceService;

    @RequestMapping("/hello")
    public String greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "hello";
    }

    @GetMapping("/space")
    public ParkSpace getSpaceInfo(@RequestParam("id") String id) {
        return parkSpaceService.getSpaceInfoBySensorId(id);
    }

}
