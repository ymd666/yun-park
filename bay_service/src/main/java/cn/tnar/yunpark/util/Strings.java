package cn.tnar.yunpark.util;

/**
 * Created by tieh on 2016/9/5.
 */
public class Strings {

    /**
     * Copy from Guava
     * Returns the given string if it is non-null; the empty string otherwise.
     *
     * @param string the string to test and possibly return
     * @return {@code string} itself if it is non-null; {@code ""} if it is null
     */
    public static String nullToEmpty(String string) {
        return (string == null) ? "" : string;
    }

    public static String nullToEmpty(String... values) {
        if (values == null || values.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String s : values) {
            if (!isNullOrEmpty(s)) {
                sb.append(s);
            }
        }
        return sb.toString();
    }

    /**
     * Copy from Guava
     * Returns the given string if it is nonempty; {@code null} otherwise.
     * s
     *
     * @param string the string to test and possibly return
     * @return {@code string} itself if it is nonempty; {@code null} if it is
     * empty or null
     */
    public static String emptyToNull(String string) {
        return isNullOrEmpty(string) ? null : string;
    }

    /**
     * Copy from Guava
     * Returns {@code true} if the given string is null or is the empty string.
     * <p/>
     * <p>Consider normalizing your string references with {@link #nullToEmpty}.
     * If you do, you can use {@link String#isEmpty()} instead of this
     * method, and you won't need special null-safe forms of methods like {@link
     * String#toUpperCase} either. Or, if you'd like to normalize "in the other
     * direction," converting empty strings to {@code null}, you can use {@link
     * #emptyToNull}.
     *
     * @param string a string reference to check
     * @return {@code true} if the string is null or is the empty string
     */
    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0; // string.isEmpty() in Java 6
    }


    /**
     * 检查是否全部非NULL非空
     *
     * @param values
     * @return 全部非NULL且非空返回true
     */
    public static boolean areNotNullOrEmpty(String... values) {
        boolean result = true;
        if (values == null || values.length == 0) {
            result = false;
        } else {
            for (String value : values) {
                result &= !isNullOrEmpty(value);
            }
        }
        return result;
    }
}
