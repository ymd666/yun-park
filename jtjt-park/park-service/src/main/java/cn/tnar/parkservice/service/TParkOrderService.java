package cn.tnar.parkservice.service;

import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Map;

public interface TParkOrderService {

    ResultJson getParkRecord(Map<String,Object> request);

    ResultJson getParkingRecord(Map<String,Object> request);

    /**
     * 查询订单
     */
    ResultJson queryOrder(Map<String,String> request);
}
