package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/8/29
 * @Description:
 */
@Data
public class TParkVisistApproveRequest {

    private Long id;

    private Integer status;

    private String beginTime;

    private String endTime;

    private Long groupId;

    private String operId;

    private String mobile;

    private String reason;

}
