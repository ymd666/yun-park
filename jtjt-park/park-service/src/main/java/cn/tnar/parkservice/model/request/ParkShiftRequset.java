package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询排班详情 请求类
 *
 * @author ouzhijie
 * @since 20181023
 */
@Data
public class ParkShiftRequset {

    @ApiModelProperty(value = "停车场编码", example = "420602200001")
    private String parkCode;

    @ApiModelProperty(value = "开始时间",example = "20181001")
    private Integer startTime;

    @ApiModelProperty(value = "结束时间",example = "20181031")
    private Integer endTime;

    @ApiModelProperty(value = "当前页", example = "1")
    private int current;

    @ApiModelProperty(value = "每页大小", example = "10")
    private int size;
}
