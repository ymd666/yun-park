package cn.tnar.parkservice.controller.common;


import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.model.dto.TDictionaryDto;
import cn.tnar.parkservice.model.entity.TDictionary;
import cn.tnar.parkservice.service.ITDictionaryService;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.util.DtoUtil;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/13
 * @Description:
 */

@Api(tags = "字典信息（张鑫）t_dictionary - Controller")
@RestController
@RequestMapping("service/tDictionary")
public class TDictionaryController {

    private Logger log = LoggerFactory.getLogger(TDictionaryController.class);
    @Autowired
//    @Qualifier("tDictionaryServiceImpl")
    private ITDictionaryService it_dictionaryService;

    @Autowired
    private KesbApiService kesbApiService;

    private ResultJson json = new ResultJson();


    @ApiOperation(value = "添加 - t_dictionary")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody TDictionaryDto model) {
        TDictionary entity = DtoUtil
                .convertObject(model, TDictionary.class);
        boolean flag = it_dictionaryService.save(entity);
        if (flag) {
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        } else {
            return json.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }
    }

    @ApiOperation(value = "修改 - t_dictionary")
    @PutMapping(value = "up/{id}")
    public ResultJson update(@RequestBody TDictionaryDto model) {
        TDictionary entity = DtoUtil.convertObject(model, TDictionary.class);
        boolean flag = it_dictionaryService.update(entity, null);
        if (flag) {
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        } else {
            return json.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }
    }

    @ApiOperation(value = "删除 - t_dictionary")
    @DeleteMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable Integer id) {
        boolean flag = it_dictionaryService.removeById(id);
        if (flag) {
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        } else {
            return json.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }
    }

    @ApiOperation(value = "通过类型编码查询字典类型列表 - t_dictionary")
    @GetMapping(value = "queryDicByCategory")
    public ResultJson queryDicByCategory(@RequestParam("categoryEn")String categoryEn) {
        QueryWrapper ew = new QueryWrapper();
        ew.eq("category_en",categoryEn);
        List<TDictionary> list = it_dictionaryService.list(ew);
        return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
    }

    @ApiOperation(value = "查询所有 - t_dictionary")
    @GetMapping(value = "query")
    public ResultJson query() {
        Page<TDictionary> page = new Page<TDictionary>();
        Page<TDictionary> list = page.setRecords(it_dictionaryService.list(null));
        return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
    }

    @ApiOperation(value = "字典查询C")
    @PostMapping("/queryDic")
    public ResultJson queryDic (@RequestBody Map<String,Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_QUERY_DICTIONARY, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation(value = "批量查询字典")
    @PostMapping("/batchQuery")
    public ResultJson batchQuery(@RequestBody Map<String,Object> param){
        List<String> category = (List<String>)param.get("categoryEns");
        return it_dictionaryService.batchQuery(category);
    }


}
