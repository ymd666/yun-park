package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.model.dto.TParkVisitDto;
import cn.tnar.parkservice.model.request.TParkVisitHistoryRequest;
import cn.tnar.parkservice.mapper.TParkVisitApplyMapper;
import cn.tnar.parkservice.model.entity.TParkVisitApply;
import cn.tnar.parkservice.service.ITParkVisitApplyService;
import cn.tnar.parkservice.util.DateUtils;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.client.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-10-05
 */
@Service
@Slf4j
public class TParkVisitApplyServiceImpl extends ServiceImpl<TParkVisitApplyMapper, TParkVisitApply> implements ITParkVisitApplyService {

    private  ResultJson resultJson = new ResultJson();
    @Autowired
    TParkVisitApplyMapper visitApplyMapper;

    @Override
    public ResultJson queryInfo(String requset) {
        TParkVisitDto dto = JSON.parseObject(requset, TParkVisitDto.class);
        if(dto==null || StringUtils.isBlank(dto.getCustId())){
            return resultJson.setCode(ResultCode.MISS_PARAM).setMsg(ResultCode.MISS_PARAM_MSG);
        }
        TParkVisitDto resp =  visitApplyMapper.queryInfo(dto.getCustId());
        return resultJson.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(JSON.toJSON(resp));
    }

    @Override
    public ResultJson apply(TParkVisitApply requset) {
        String now = DateUtils.getNowString(DateUtils.YYYYMMDDHHMMSS);
        requset.setApplyTime(Long.valueOf(now));
        requset.setApplyStatus(1);// 1:待审核
        boolean flag = this.saveOrUpdate(requset);
        if(flag){
            resultJson.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        }else{
            resultJson.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }
        return resultJson;
    }

    @Override
    public ResultJson audit(Map<String, Object> requset) {
        String parm1 = Optional.ofNullable(String.valueOf(requset.get("id"))).orElse("");
        String parm2 = Optional.ofNullable(String.valueOf(requset.get("apply_status"))).orElse("");
        String parm3 = Optional.ofNullable(String.valueOf(requset.get("oper_id"))).orElse("");
        if(org.apache.commons.lang.StringUtils.isBlank(parm1)|| org.apache.commons.lang.StringUtils.isBlank(parm2)|| org.apache.commons.lang.StringUtils.isBlank(parm3)){
            return resultJson.setCode(ResultCode.MISS_PARAM).setMsg(ResultCode.MISS_PARAM_MSG);
        }
//        TParkVisitApply apply = new TParkVisitApply();
        TParkVisitApply apply = visitApplyMapper.selectById(Long.valueOf(parm1));
        int applyStatus = Integer.valueOf(parm2);
        apply.setApplyStatus(applyStatus);
        apply.setOperId(parm3);
        // 1: 待审核2: 已通过3: 未通过
        if(applyStatus == 2){
            String now = DateUtils.getNowString(DateUtils.YYYYMMDDHHMMSS);
            apply.setAccessTime(Long.valueOf(now));
        }
        UpdateWrapper<TParkVisitApply> wrapper = new UpdateWrapper <>();
        wrapper.eq("id",Long.valueOf(String.valueOf(requset.get("id"))));
        boolean flag = this.update(apply,wrapper);
        if(!flag){
            resultJson.setCode(ResultCode.FAIL).setMsg(ResultCode.FAIL_MSG);
        }else{
            resultJson.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        }
        return resultJson;
    }


    @Override
    public ResultJson visiterApplyRecord(TParkVisitHistoryRequest request) {
        QueryWrapper<TParkVisitApply> qw = new QueryWrapper<>();
        qw.eq("park_code",request.getParkCode());
        qw.like(request.getMobile() != null,"apply_mobile",request.getMobile());
        qw.in("apply_status",request.getStatusList());
        qw.orderByDesc("apply_time");
        Page<TParkVisitApply> page = new Page<>(request.getPageNo(),request.getPageSize());
        IPage<TParkVisitApply> ipage = visitApplyMapper.selectPage(page, qw);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(ipage);
    }

    @Override
    public ResultJson rights(TParkVisitDto request) {
        QueryWrapper<TParkVisitApply> qw = new QueryWrapper<>();
        qw.eq("park_code",request.getParkCode());
        if(!StringUtils.isBlank(request.getCustId())){
            qw.eq("cust_id",request.getCustId());
        }
        if(!StringUtils.isBlank(request.getApplyName())){
            qw.eq("apply_name",request.getApplyName());
        }
        if(!StringUtils.isBlank(request.getApplyMobile())){
            qw.eq("apply_mobile",request.getApplyMobile());
        }
        TParkVisitApply visitApply = this.getOne(qw);
        if(null == visitApply){
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(0);
        }else{
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(JSON.toJSON(visitApply));
        }
    }

}
