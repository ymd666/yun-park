# yun-park

#### 介绍
城市级智慧停车系统，利用互联网，物联网和大数据技术，提供领先的路内路外停车智能化系统建设与停车运营关联系统解决方案，以智能停车业务为核心，在智慧停车管理系统、物联网智能硬件、互联网金融、新能源汽车服务和停车场库开发建设等领域综合布局，围绕智能静态交通领域提供专业的停车服务，极大的解决了车主停车难，找车难的问题及停车场管理人员的管理问题。集合多种如云对讲，高位视频，无人巡检车，地磁NB，Lora，监控中心，诱导屏，摄像头，Mass，道闸等的智能硬件设备，实现了路内停车管理智慧化(包括前端采集，缴费，取证等环节)，降低人工参与率，提高了泊位周转率，依据检测信息采集和改进管理，提高工作效率，节省人工成本提升车位利用率，防止跑冒滴漏，提高车位整体收益，支持线上电子支付（支付宝、微信、银联），提高车主停车体验，实现各个路段路内泊位数量及状态自动实时采集，发送至云平台实现诱导系统的发布，后期可对接天网系统实现路内停车信息收费自动关联车牌无人值守管理，最终达到了车位可查，可预约，可共享，可寻车，可无感，实现了车主停车有位，出场通畅，便捷缴费的目标。

#### 软件架构
软件架构说明
技术选型 springboot,springcloud,mybatis-plus,netty,mysql数据库，jna，阿里云oss，caffeine，flyway
,springCloud-Alibaba,redis,jwt,shiro,swagger2,poi,rabbitmq。

#### 使用说明

1.  flyos岗亭收费系统
2.  jtjt-park 停车场系统云平台
3.  bay-service  地磁数据采集系统

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
