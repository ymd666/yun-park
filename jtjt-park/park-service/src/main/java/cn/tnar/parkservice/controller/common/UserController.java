package cn.tnar.parkservice.controller.common;

import cn.tnar.parkservice.model.entity.User;
import cn.tnar.parkservice.service.ITParkInfoService;
import cn.tnar.parkservice.service.IUserService;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * UserController
 */
@RestController
@RequestMapping("")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ITParkInfoService itParkInfoService;


    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public ResultJson login(@RequestBody String json) throws IOException {
        User account = JSON.parseObject(json,User.class);
        return userService.userLogin(account);
    }

    @ApiOperation(value = "获取停车场和运营商信息")
    @PostMapping("/service/informationByAll")
    public ResultJson informationByAll(@RequestBody String json) throws IOException {
        return userService.informationByAll(json);
    }


    @ApiOperation(value = "查询菜单树")
    @PostMapping("/service/informationTree")
    public ResultJson informationTree() throws IOException {
        return userService.informationTree();
    }

}