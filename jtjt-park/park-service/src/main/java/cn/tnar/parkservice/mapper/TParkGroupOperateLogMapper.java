package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.TParkGroupOperateLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface TParkGroupOperateLogMapper extends BaseMapper<TParkGroupOperateLog> {

}
