package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkOfflineCoupons;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 16:30:23
 */
@Repository
@Mapper
public interface TParkOfflineCouponsMapper extends BaseMapper<TParkOfflineCoupons> {
}
