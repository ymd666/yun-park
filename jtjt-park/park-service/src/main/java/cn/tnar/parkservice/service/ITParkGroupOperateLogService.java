package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TParkGroupOperateLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface ITParkGroupOperateLogService extends IService<TParkGroupOperateLog> {

}
