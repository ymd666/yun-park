package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkMemberManageMapper;
import cn.tnar.parkservice.mapper.TParkVisitHistoryMapper;
import cn.tnar.parkservice.model.response.PageResponse;
import cn.tnar.parkservice.model.response.TMeberCardManageResponse;
import cn.tnar.parkservice.model.response.VisistRecordResponse;
import cn.tnar.parkservice.service.TParkMemberManageService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Author: xinZhang
 * @Date: 2019/9/11
 * @Description:
 */
@Service
public class TParkMemberManageServiceImpl implements TParkMemberManageService {

    @Autowired
    private TParkMemberManageMapper tParkMemberManageMapper;


    @Autowired
    private TParkVisitHistoryMapper tParkVisitHistoryMapper;

    @Override
    @Transactional
    public ResultJson query(Map<String,Object> param){
        Integer pageNoInt = (Integer) param.get("pageNo");
        Integer pageSizeInt = (Integer) param.get("pageSize");
        List<VisistRecordResponse> records = null;
        List<TMeberCardManageResponse> list = new ArrayList<>();
        Page<VisistRecordResponse> page1 = new Page<>();
        records = tParkVisitHistoryMapper.querySimpleRecord(page1, param);

        Page<TMeberCardManageResponse> page2 = new Page<>();
        List<TMeberCardManageResponse> list1 = tParkMemberManageMapper.queryOldMember(page2, param);
        list.addAll(list1);

        Page<TMeberCardManageResponse> page = new Page<>();
        List<TMeberCardManageResponse> list2 = tParkMemberManageMapper.queryNewMember(page, param);
        list.addAll(list2);

        TMeberCardManageResponse tMeberCardManageResponse = null;
        for (VisistRecordResponse visistRecordResponse : records) {
            tMeberCardManageResponse = new TMeberCardManageResponse();
            tMeberCardManageResponse.setId(visistRecordResponse.getId());
            tMeberCardManageResponse.setCar_id(visistRecordResponse.getCarId());
            tMeberCardManageResponse.setOwner_name(visistRecordResponse.getApplyName());
            tMeberCardManageResponse.setTel_no(visistRecordResponse.getApplyMobile());
            tMeberCardManageResponse.setRole_name(visistRecordResponse.getMemberCardName());
            tMeberCardManageResponse.setStart_date(visistRecordResponse.getVisitTimeStart());
            tMeberCardManageResponse.setEnd_date(visistRecordResponse.getVisitTimeEnd());
            tMeberCardManageResponse.setState(visistRecordResponse.getVisitState());
            list.add(tMeberCardManageResponse);
        }
        PageResponse page3 = getPage(pageNoInt, pageSizeInt, list);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(page3);
    }

    /**
     * 对list分页，排序
     *
     * @param pageNoInt
     * @param pageSizeInt
     * @param list
     * @return
     */
    private PageResponse getPage(Integer pageNoInt, Integer pageSizeInt, List<TMeberCardManageResponse> list) {
        PageResponse page = new PageResponse();
        //刚开始的页面为第一页
        if (page.getCurrent() == null) {
            page.setCurrent(1);
        } else {
            page.setCurrent(pageNoInt);
        }
        //设置每页数据条数
        page.setSize(pageSizeInt);
        //每页的开始记录数
        page.setStar((pageNoInt - 1) * (page.getSize()));
        //数据集合大小
        int count = list.size();
        //设置总记录数
        page.setTotal(Long.valueOf(String.valueOf(count)));
        //设置总页数
        page.setPages(count % pageSizeInt == 0 ? count / pageSizeInt : count / pageSizeInt + 1);
        //对list进行截取
        Integer formIndex = page.getStar();
        Integer tolIndex = count - page.getStar() > page.getSize() ? page.getStar() + pageSizeInt : count;
        //排序
        Collections.sort(list, Comparator.comparing(TMeberCardManageResponse::getStart_date, Comparator.nullsLast(Long::compareTo)).reversed());
        page.setRecords(list.subList(formIndex, tolIndex));
        return page;
    }
}
