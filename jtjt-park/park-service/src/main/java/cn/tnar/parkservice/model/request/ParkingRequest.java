package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @author xzhang
 * @date 2019/9/7
 */
@Data
public class ParkingRequest {
    private String custId;
    private String custSession;
    private String parkCode;
    private String carId;
    private String carType;
    private String mobile;
    private String openId;

}
