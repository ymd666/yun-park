package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 新增退款记录
 * @author Tiyer.Tao
 * @since 2018-09-08
 */
@Data
public class OrderRefundRequest {

    @ApiModelProperty(value = " 退款客户编号", example = "4010000120296")
    private Long custId;

    @ApiModelProperty(value = " 订单编号", example = "201508318010000000334420111000025000240030368")
    private String orderNumber;

    @ApiModelProperty(value = " 退款金额", example = "1")
    private BigDecimal refundamount;

    @ApiModelProperty(value = " 手续费费率", example = "20150830140048")
    private double feerate;

    @ApiModelProperty(value = " 退款原因", example = "")
    private String refundreason;

    @ApiModelProperty(value = " 操作员编号", example = "4010000120296")
    private long operId;

    @ApiModelProperty(value = " 备注", example = "")
    private String remark;

}

