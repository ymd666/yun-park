package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.OrderAmountDto;
import cn.tnar.parkservice.model.dto.TTradeCommodityOrder;
import cn.tnar.parkservice.model.request.OrderRequest;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@QuerySupport
@Repository
public interface TTradeCommodityOrderMapper extends BaseMapper<TTradeCommodityOrder> {

    @QuerySupport
    @Select("select * FROM t_trade_commodity_order")
    List<TTradeCommodityOrder> getTTradeCommodityOrderList(@Param(Constants.WRAPPER) Wrapper<TTradeCommodityOrder> entityWrapper);


    @Update("UPDATE t_trade_commodity_order set order_type =1 where pay_type =10 and DATE_FORMAT(ordertime,'%Y%m%d')=${orderTime}")
    void updateX(@Param("orderTime") String orderTime);

    List<Map<String,Object>> excelOut(@Param("parkCode") String parkCode, @Param("startTime") long startTime, @Param("endTime") long endTime);

    List<Map<String,Object>> excelOutInfo(@Param("commprovider") String commprovider, @Param("startTime") long startTime, @Param("endTime") long endTime);


    public List<OrderAmountDto> queryTradeAmountByDay(@Param("parkCode") String parkCode, @Param("startTime") long startTime, @Param("endTime") long endTime);

    public List<OrderAmountDto> queryTradeAmountByHis(@Param("parkCode") String parkCode, @Param("startTime") long startTime, @Param("endTime") long endTime);


    public List<OrderAmountDto> queryTradeAmountByMonth(@Param("parkCode") String parkCode, @Param("startTime") long startTime, @Param("endTime") long endTime);

    public List<OrderAmountDto> queryTradeAmountByYear(@Param("parkCode") String parkCode, @Param("startTime") long startTime, @Param("endTime") long endTime);


    public List<Map<String,Object>>  queryOrders(Page<Map<String, Object>> page, @Param("request") OrderRequest orderRequest);

    Map<String,Object> queryTradeAmountToday(@Param("parkCode") String parkCode, @Param("startTime") long startTime, @Param("endTime") long endTime);



}
