package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: xinZhang
 * @Date: 2019/10/12
 * @Description:
 */
@Data
@ApiModel("系统账号信息列表查询参数")
public class TAcctCustInfoRequest  {

    @ApiModelProperty(value = "分页-当前页",example = "1")
    private Integer pageNo;

    @ApiModelProperty(value = "分页-每页条数",example = "8")
    private Integer pageSize;

    @ApiModelProperty(value = "角色id",example = "")
    private Long roleId;

    @ApiModelProperty(value = "姓名",example = "")
    private String name;

    @ApiModelProperty(value = "运营商id",example = "8010000122568")
    private Long parentCustId;

    private List<Long> custIds;
}
