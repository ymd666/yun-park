package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @author xzhang
 * @date 2019/9/7
 */
@Data
public class RegRequest {
    private String carId;
    private String carType;
    private String mobileNo;
    private String code;
    private String openId;
    private String checkToken;
    private String dataValid;
    private String overtime;
    private String custSession;
}
