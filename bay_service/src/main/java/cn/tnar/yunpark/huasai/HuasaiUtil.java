package cn.tnar.yunpark.huasai;

/**
 * Created by tieh on 2017/6/22.
 */
public class HuasaiUtil {
    /**
     * 车位状态转换，华赛格式(0无车1有车6在线8离线) -> LBM格式(01无车02有车95离线)
     */
    public static String toLbmStatus(int huasaiStatus) {
        switch (huasaiStatus) {
            case 0:
                return "1";
            case 1:
                return "2";
            case 6:
                return null;    // 在线不报，没有意义
            case 8:
                return "95";
            default:
                return null;    // 未知状态不报
        }
    }
}
