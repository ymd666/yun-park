package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.util.List;

/**
 * @Author: xinZhang
 * @Date: 2019/9/11
 * @Description:
 */
@Data
public class TParkMemberCardResponse {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String memCardName;

    private String regionCode;

    private List<String> regionNames;
}
