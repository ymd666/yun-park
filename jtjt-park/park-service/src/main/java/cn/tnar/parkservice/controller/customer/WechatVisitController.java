package cn.tnar.parkservice.controller.customer;

import cn.tnar.parkservice.config.ExceptionConst;
import cn.tnar.parkservice.model.dto.TParkVisitDto;
import cn.tnar.parkservice.model.entity.TParkVisitApply;
import cn.tnar.parkservice.model.entity.TParkVisitHistory;
import cn.tnar.parkservice.service.ITParkInfoService;
import cn.tnar.parkservice.service.ITParkVisitApplyService;
import cn.tnar.parkservice.service.ITParkVisitHistoryService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Description:访客管理Controller
 * @author: Tiyer.Tao
 * @date 2019/8/23 15:43
 */
@RestController
@RequestMapping("wechat/tParkVisit")
@Api(tags = "访客管理（公众号） - Controller")
@Slf4j
public class WechatVisitController {


    @Autowired
    private ITParkInfoService parkInfoService;

    @Autowired
    private ITParkVisitApplyService tParkVisitApplyService;

    @Autowired
    private ITParkVisitHistoryService tParkVisitHistoryService;

    @ApiOperation(value = "访客管理（公众号） - 查询用户信息及访客权限")
    @PostMapping(value = "/query")
    public ResultJson query(@RequestBody String requset) {
        try {
            return tParkVisitApplyService.queryInfo(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_QUERY_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_QUERY_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 查询被访者是否有审核权限")
    @PostMapping(value = "/rights")
    public ResultJson rights(@RequestBody TParkVisitDto requset) {
        try {
            return tParkVisitApplyService.rights(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_QUERY_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_QUERY_ERROR+e.getMessage());
        }
    }


    @ApiOperation(value = "访客管理（公众号） - 被访者申请权限")
    @PostMapping(value = "/apply")
    public ResultJson apply(@RequestBody TParkVisitApply requset) {
        try {
            return tParkVisitApplyService.apply(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_APPLY_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_APPLY_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理 - 被访者审核权限")
    @PostMapping(value = "/audit")
    public ResultJson audit(@RequestBody Map<String,Object> requset) {
        try {
            return tParkVisitApplyService.audit(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_AUDIT_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_AUDIT_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 来访记录（访客）")
    @PostMapping(value = "/visitHistory")
    public ResultJson visitHistory(@RequestBody TParkVisitDto requset) {
        try {
            return tParkVisitHistoryService.visitHistory(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_HIS_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_HIS_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 来访记录 查询已绑定卡组信息 ")
    @PostMapping(value = "/queryBindCard")
    public ResultJson queryBindCard(@RequestBody TParkVisitDto requset) {
        try {
            return tParkVisitHistoryService.queryBindCard(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_HIS_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_HIS_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 预约停车")
    @PostMapping(value = "/reserve")
    public ResultJson reserve(@RequestBody TParkVisitHistory requset) {
        try {
            return tParkVisitHistoryService.reserve(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_RESERVE_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_RESERVE_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 预约停车")
    @PostMapping(value = "/updateReserve")
    public ResultJson updateReserve(@RequestBody TParkVisitHistory requset) {
        try {
            return tParkVisitHistoryService.updateReserve(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_RESERVE_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_RESERVE_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理 - 审核访客申请记录")
    @PostMapping(value = "/checkIn")
    public ResultJson checkIn(@RequestBody TParkVisitDto requset) {
        try {
            return tParkVisitHistoryService.checkIn(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_CHECK_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_CHECK_ERROR+e.getMessage());
        }

    }


    @ApiOperation(value = "访客管理 (公众号) - 撤销访客申请")
    @PostMapping(value = "/cancel")
    public ResultJson cancel(@RequestBody TParkVisitDto requset) {
        try {
            return tParkVisitHistoryService.cancel(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_VISIT_CHECK_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_VISIT_CHECK_ERROR+e.getMessage());
        }

    }

    @ApiOperation(value = "访客管理（公众号） - 预约记录（访客）")
    @PostMapping(value = "/applyHistory")
    public ResultJson applyHistory(@RequestBody TParkVisitDto requset) {
        try {
            return tParkVisitHistoryService.applyHistory(requset);
        }catch (Exception e){
            log.error(ExceptionConst.API_APPLY_HIS_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_APPLY_HIS_ERROR+e.getMessage());
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 根据ParkCode查询卡证")
    @PostMapping(value = "/queryCard")
    public ResultJson queryCard(@RequestBody TParkVisitDto requset) {
        try {
            String parkCode = requset.getParkCode();
            if(StringUtils.isBlank(parkCode)){
                return ResultJson.getInstance().setCode(ResultCode.MISS_PARAM).setMsg(ResultCode.MISS_PARAM_MSG);
            }else{
                return tParkVisitHistoryService.queryCard(parkCode);
            }
        }catch (Exception e){
            log.error(ExceptionConst.API_QUERY_CARD_ERROR,e.getMessage());
            return ResultJson.getInstance().setCode(ResultCode.FAIL).setMsg(ExceptionConst.API_QUERY_CARD_ERROR+e.getMessage());
        }
    }


    @ApiOperation(value = "访客管理（公众号） - 查询配置了计费规则的停车场信息 ")
    @PostMapping("/queryAllParks")
    public ResultJson queryAllParks(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> result = parkInfoService.queryAllParks(param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }
}
