package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月28日 17:26:46
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(description = "启用/禁用月卡分组请求体")
public class MemberGoupReqParam {

    private Long id;

    private Integer delFlag;

    private String parkCode;

    private String key;

    private String regionCodes;

    private Long feeIndexId;

    private Long feesRoleInfoId;

    private String custId;

    private String ids;


}
