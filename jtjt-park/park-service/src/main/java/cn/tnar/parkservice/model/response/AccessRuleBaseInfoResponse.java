package cn.tnar.parkservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 11:08:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class AccessRuleBaseInfoResponse {

    private Long accessRuleId; // 准入规则组id

    private String accessRuleName; //规则名称

    private String carType;  //车类类型

    private String regionCodes; //多个区域编号逗号隔开
    private String regionNames; //多个区域名称逗号隔开
}
