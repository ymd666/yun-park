package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.util.List;

/**
 * @Author: xinZhang
 * @Date: 2019/9/4
 * @Description:
 */
@Data
public class TParkOrderRequest {

    @ApiModelProperty(value = "查询记录类型",example = "1.临停车 2.长租车 3.电子支付 4.优惠券 5.异常")
    private Integer recordType;

    /**
     * 进场时间-开始时间
     */
    @ApiModelProperty(value = "进场时间-开始时间",example = "20180102000000")
    private Long inStartDatetime;

    /**
     *进场时间-结束时间
     */
    @ApiModelProperty(value = "进场时间-结束时间",example = "20180102235959")
    private Long inEndDatetime;

    /**
     *出场时间-开始时间
     */
    @ApiModelProperty(value = "出场时间-开始时间",example = "20190102000000")
    private Long outStartDatetime;

    /**
     *出场时间-结束时间
     */
    @ApiModelProperty(value = "出场时间-结束时间",example = "200191015235959")
    private Long outEndDatetime;

    /**
     *车牌号
     */
    @ApiModelProperty(value = "车牌号",example = "京Q63LU9")
    private String carId;

    @ApiModelProperty(value = "异常类型",example = "")
    private Integer outOperate;

    @ApiModelProperty(value = "停车场id")
    private String parkCode;
}
