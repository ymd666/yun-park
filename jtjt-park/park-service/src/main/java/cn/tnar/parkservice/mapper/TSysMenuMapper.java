package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TSysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author qulu
 * @since 2018-09-17
 */
@Repository
@Mapper
public interface TSysMenuMapper extends BaseMapper<TSysMenu> {

    //   查询菜单
    @Select("SELECT m.id,m.audit_flag,m.biz_type,m.func_code,m.icon,m.menu_flag,m.menu_id,m.menu_name,m.menu_name_en,m.modify_oper,m.modify_time,m.mtd_code,m.obj_code,m.parent_menu_id,m.serial_no,m.url,m.depends,m.object_code,m.menu_type,m.platform_type " +
            "FROM t_sys_menu_4 m WHERE m.platform_type = 2 AND  m.menu_id in (\n" +
            "SELECT menu_id FROM t_sys_auth_func_4 WHERE role_id = (\n" +
            "SELECT u.role_id FROM t_sys_user_assign u WHERE u.usr_id = #{custId} ))")
    List<TSysMenu> selectMenu(@Param("custId") String custId);

    @Select("SELECT m.id,m.audit_flag,m.biz_type,m.func_code,m.icon,m.menu_flag,m.menu_id,m.menu_name,m.menu_name_en,m.modify_oper,m.modify_time,m.mtd_code,m.obj_code,m.parent_menu_id,m.serial_no,m.url,m.depends,m.object_code,m.menu_type,m.platform_type " +
            "FROM t_sys_menu_4 m WHERE m.platform_type = 2 and m.parent_menu_id = 1 and m.menu_name_en = 'YGTROOT'")
    TSysMenu selectYgtMenu();

    @Select("SELECT * from  t_sys_menu_4 where platform_type = 2 and parent_menu_id = 1 limit 1")
    TSysMenu selectYunRoot();

    @Select("SELECT * from  t_sys_menu_4 where  parent_menu_id = #{id}")
    List<TSysMenu> selectSubByParentId(@Param("id") Long id);
}
