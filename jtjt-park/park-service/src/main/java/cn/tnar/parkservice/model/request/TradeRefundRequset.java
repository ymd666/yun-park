package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询退款记录 请求类
 * @author Tiyer.Tao
 * @since 2018-09-08
 */
@Data
public class TradeRefundRequset {


    @ApiModelProperty(value = " 停车场编号" ,example = "420111000025")
    private String parkCode;

    @ApiModelProperty(value = " 订单编号" ,example = "201508318010000000334420111000025000240030368")
    private String orderNumber;

    @ApiModelProperty(value = " 车牌号" ,example = "苏J38387")
    private String carNo;

    @ApiModelProperty(value = " 审核状态  1初始2处理中3成功4失败5取消" ,example = "1")
    private int refundState;

    @ApiModelProperty(value = " 起始时间" ,example = "20150830140048")
    private long startDate;

    @ApiModelProperty(value = " 结束时间" ,example = "20150830140049")
    private long endDate;

    @ApiModelProperty(value = " 当前页码" ,example = "1")
    private int currentPage;

    @ApiModelProperty(value = " 单页条数" ,example = "1")
    private int pageSize;

    @ApiModelProperty(value = " 当前用户id" ,example = "4010000120296")
    private long custId;

    private long firstCheckId=0;

    private long secCheckId=0;

    private long thirdCheckId=0;
}
