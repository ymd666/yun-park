package cn.tnar.parkservice.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 查询电子发票返回结果
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryInvoiceDetailResponse extends QueryInvoiceResponse<QueryInvoiceDetailResponse.ApplyInvoiceResponse> {

    public static class ApplyInvoiceResponse {
        private int size;
        private int total;
        private int total_page;
        private String page;
        private List<Map<String,Object>> list;
        private MoneyInfo money_info;

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotal_page() {
            return total_page;
        }

        public void setTotal_page(int total_page) {
            this.total_page = total_page;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public List<Map<String, Object>> getList() {
            return list;
        }

        public void setList(List<Map<String, Object>> list) {
            this.list = list;
        }

        public MoneyInfo getMoney_info() {
            return money_info;
        }

        public void setMoney_info(MoneyInfo money_info) {
            this.money_info = money_info;
        }
    }

    public static class MoneyInfo {
        private BigDecimal amount;
        private BigDecimal fee;
        private String number;

        public MoneyInfo() {
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public BigDecimal getFee() {
            return fee;
        }

        public void setFee(BigDecimal fee) {
            this.fee = fee;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }
}
