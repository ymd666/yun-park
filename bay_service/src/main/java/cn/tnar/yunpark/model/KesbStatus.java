package cn.tnar.yunpark.model;

/**
 * 状态
 * Created by tieh on 2017/6/24.
 */
public interface KesbStatus {
    /**
     * 0 - 车位状态，1 - 传感器状态
     * @return
     */
    String getType();

    /**
     * 转为kesb格式
     * @return
     */
    String toKesbString();
}
