package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.mapper.TCarMemberamountinfoMapper;
import cn.tnar.parkservice.model.entity.TCarMemberamountinfo;
import cn.tnar.parkservice.model.response.RecordResponse;
import cn.tnar.parkservice.service.ITCarMemberamountinfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-08-29
 */
@Service("tCarMemberamountinfoServiceImpl")
public class TCarMemberamountinfoServiceImpl extends ServiceImpl<TCarMemberamountinfoMapper, TCarMemberamountinfo> implements ITCarMemberamountinfoService {

    @Override
    public Page<RecordResponse> getAmountInfo(Integer pageSize,Integer pageNo,Long memberInfoId) {
        Page<RecordResponse> page=new Page<>();
        page.setSize(pageSize);
        page.setCurrent(pageNo);
        page.setRecords(baseMapper.getAmountInfo(page,memberInfoId));
        return page;
    }

    @Override
    public Page<RecordResponse> getStopRecord(Integer pageSize, Integer pageNo, Long memberInfoId) {
        Page<RecordResponse> page=new Page<>();
        page.setSize(pageSize);
        page.setCurrent(pageNo);
        page.setRecords(baseMapper.getStopRecord(page,memberInfoId));
        return page;
    }
}
