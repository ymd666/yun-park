package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.util.List;

/**
 * @Author: xinZhang
 * @Date: 2019/9/11
 * @Description:
 */
@Data
public class VisistRecordResponse {

    private Long id;

    private String visitor;

    private String mobile;

    private String carId;

    private Byte carType;

    private String applyName;

    private String applyMobile;

    private Long visitTimeStart;

    private Long visitTimeEnd;

    private Integer visitState;

    private Long entranceTime;

    private String remarks;

    private Long applyTime;

    private Byte applyStatus;

    private Long accessTime;

    private String reason;

    private Long groupId;

    private Long customerVisitTime;

    private String memberCardName;

    List<String> regionNames;
}
