package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 16:24:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("t_park_offline_coupons")
public class TParkOfflineCoupons extends Model<TParkOfflineCoupons> {

    @TableId("id")
    private Long id;
    @TableField("node_id")
    private Integer nodeId;
    @TableField("park_code")
    private String parkCode;
    @TableField("park_name")
    private String parkName;
    @TableField("free")
    private Integer free;
    @TableField("time_discont")
    private String timeDiscont;
    @TableField("money_discont")
    private String moneyDiscont;
    @TableField("state")
    private Integer state;
    @TableField("time_userdefine")
    private Integer timeUserdefine;
    @TableField("money_userdefine")
    private Integer moneyUserdefine;
    @TableField("create_time")
    private Long createTime;
    @TableField("operno")
    private String operno;

}
