package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 *查询排班详情 返回类
 *
 * @author ouzhijie
 * @since 20181023
 */
@Data
public class ParkShiftResponse {

    private String time;

    private String shiftName;

    private Integer beginTime;

    private Integer endTime;

    private String tollName;

    private Integer workDate;

}
