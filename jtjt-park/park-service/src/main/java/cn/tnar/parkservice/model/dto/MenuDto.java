package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;

public class MenuDto {

    private Long id;
    private Integer audit_flag;
    private Integer biz_type;
    private String func_code;
    private String icon;
    private Integer menu_flag;
    private Long menu_id;
    private String menu_name;
    private String menu_name_en;
    private Long modify_oper;
    private Long modify_time;
    private String mtd_code;
    private String obj_code;
    private Long parent_menu_id;
    private Long serial_no;
    private String url;
    private String depends;
    private String object_code;
    private Integer platformType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAudit_flag() {
        return audit_flag;
    }

    public void setAudit_flag(Integer audit_flag) {
        this.audit_flag = audit_flag;
    }

    public Integer getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(Integer biz_type) {
        this.biz_type = biz_type;
    }

    public String getFunc_code() {
        return func_code;
    }

    public void setFunc_code(String func_code) {
        this.func_code = func_code;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getMenu_flag() {
        return menu_flag;
    }

    public void setMenu_flag(Integer menu_flag) {
        this.menu_flag = menu_flag;
    }

    public Long getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Long menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_name_en() {
        return menu_name_en;
    }

    public void setMenu_name_en(String menu_name_en) {
        this.menu_name_en = menu_name_en;
    }

    public Long getModify_oper() {
        return modify_oper;
    }

    public void setModify_oper(Long modify_oper) {
        this.modify_oper = modify_oper;
    }

    public Long getModify_time() {
        return modify_time;
    }

    public void setModify_time(Long modify_time) {
        this.modify_time = modify_time;
    }

    public String getMtd_code() {
        return mtd_code;
    }

    public void setMtd_code(String mtd_code) {
        this.mtd_code = mtd_code;
    }

    public String getObj_code() {
        return obj_code;
    }

    public void setObj_code(String obj_code) {
        this.obj_code = obj_code;
    }

    public Long getParent_menu_id() {
        return parent_menu_id;
    }

    public void setParent_menu_id(Long parent_menu_id) {
        this.parent_menu_id = parent_menu_id;
    }

    public Long getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(Long serial_no) {
        this.serial_no = serial_no;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDepends() {
        return depends;
    }

    public void setDepends(String depends) {
        this.depends = depends;
    }

    public String getObject_code() {
        return object_code;
    }

    public void setObject_code(String object_code) {
        this.object_code = object_code;
    }

    public Integer getPlatformType() {
        return platformType;
    }

    public void setPlatformType(Integer platformType) {
        this.platformType = platformType;
    }
}
