package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "TParkGroupMemberinfo对象", description = "")
public class TParkGroupMemberinfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Integer nodeId;

    private Long agentId;

    @ApiModelProperty(value = "停车场编号")
    private String parkCode;

    @ApiModelProperty(value = "区域编号")
    private String regionCode;

    @ApiModelProperty(value = "会员姓名")
    private String ownerName;

    @ApiModelProperty(value = "房号")
    private String roomNo;

    @ApiModelProperty(value = "联系方式")
    private String telNo;

    @ApiModelProperty(value = "会员级别 1-注册2-铜牌3-银牌4-金牌5-钻石9-专用")
    private Integer memberLevel;

    @ApiModelProperty(value = "车位数")
    private Integer seatNum;

    @ApiModelProperty(value = "起始日期")
    private Integer startDate;

    @ApiModelProperty(value = "结束日期")
    private Integer endDate;

    @ApiModelProperty(value = "收费总额")
    private BigDecimal feeAmount;

    @ApiModelProperty(value = "删除")
    private Integer delflag;

    @ApiModelProperty(value = "创建日期")
    private Long createTime;

    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    private String remark;

    @ApiModelProperty(value = "数据字典：1 正常/过期、3 暂停")
    private Integer carState;

    @ApiModelProperty(value = "会员状态 1生效 2 失效")
    private Integer member_type;
    @ApiModelProperty(value = "过期时间")
    private Long expireTime;

    @ApiModelProperty(value = "当月累计额度")
    private BigDecimal accumulateAmt;
}
