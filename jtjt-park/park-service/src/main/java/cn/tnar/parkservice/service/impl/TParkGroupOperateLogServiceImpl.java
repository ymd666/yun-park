package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkGroupOperateLogMapper;
import cn.tnar.parkservice.model.entity.TParkGroupOperateLog;
import cn.tnar.parkservice.service.ITParkGroupOperateLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Service("tParkGroupOperateLogServiceImpl")
public class TParkGroupOperateLogServiceImpl extends ServiceImpl<TParkGroupOperateLogMapper, TParkGroupOperateLog> implements ITParkGroupOperateLogService {

}
