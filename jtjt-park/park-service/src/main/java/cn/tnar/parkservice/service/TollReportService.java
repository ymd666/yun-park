package cn.tnar.parkservice.service;

import cn.tnar.parkservice.util.common.ResultJson;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface TollReportService {

    ResultJson tollAutomated(@RequestBody Map<String,Object> map);

}
