package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.dto.TParkVisitDto;
import cn.tnar.parkservice.model.request.TParkVisistApproveRequest;
import cn.tnar.parkservice.model.request.TParkVisitHistoryRequest;
import cn.tnar.parkservice.model.entity.TParkVisitHistory;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;


/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zzb
 * @since 2019-08-28
 */
public interface ITParkVisitHistoryService extends IService<TParkVisitHistory> {

    ResultJson select(String json);

    ResultJson reserve(TParkVisitHistory requset);

    ResultJson queryVisiterReocrd(TParkVisitHistoryRequest request);

    ResultJson applyHistory(TParkVisitDto requset);

    ResultJson visitHistory(TParkVisitDto requset);

    ResultJson getVisitCard(String parkCode);

    ResultJson visiterApprove(TParkVisistApproveRequest request);

    ResultJson checkIn(TParkVisitDto requset);

    ResultJson queryCard(String parkCode);

    ResultJson updateReserve(TParkVisitHistory requset);

    ResultJson cancel(TParkVisitDto requset);

    ResultJson queryBindCard(TParkVisitDto requset);

    /**
     * 修改访客车来访状态
     * @param param
     * @return
     */
    ResultJson updateStatus(Map<String, Object> param);

    ResultJson combineVisit(String parkCode);
}
