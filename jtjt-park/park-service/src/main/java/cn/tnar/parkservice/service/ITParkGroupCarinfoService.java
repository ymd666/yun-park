package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.request.TParkGroupMemberinfoRequest;
import cn.tnar.parkservice.model.entity.TParkGroupCarinfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface ITParkGroupCarinfoService extends IService<TParkGroupCarinfo> {

    List<Map<String, Object>> checkCarId(String carId, Integer carType, String memberinfoIds);

    List<Map<String, Object>> getAllCarIdByMemberIds(String memberinfoIds);

    List<Map<String, Object>> selectBaiList(TParkGroupMemberinfoRequest map);

    List<Map<String, Object>> selectMemberList(TParkGroupMemberinfoRequest map);
}
