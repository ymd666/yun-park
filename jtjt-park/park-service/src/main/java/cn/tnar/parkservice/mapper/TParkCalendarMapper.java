package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkCalendar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月04日 19:47:51
 */
@Repository
@Mapper
public interface TParkCalendarMapper extends BaseMapper<TParkCalendar> {

}
