package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 19:22:28
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(description = "停车价值相关接口请求体")
public class ParkValueReqParam {

    private Long pageIndex;

    private Long pageSize;

    private Long id;

    private Integer type;

    private Integer classify;

    private Long startDate;

    private Long endDate;
}
