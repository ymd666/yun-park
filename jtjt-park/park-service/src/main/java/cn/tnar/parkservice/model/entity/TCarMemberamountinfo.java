package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2019-08-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TCarMemberamountinfo对象", description="")
public class TCarMemberamountinfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer nodeId;

    private Long custId;

    private String parkCode;

    @ApiModelProperty(value = "数据字典：1 交费充值，2 暂停，3启用，4重新启用 ")
    private Integer amountType;

    private Long feesRoleid;

    private String roleName;

    private BigDecimal monthValue;

    private Long upTime;

    private BigDecimal amountNum;

    private Integer startDate;

    private Integer endDate;

    private Long operId;

    private String remark;

    private String regionCode;

    @ApiModelProperty(value = "0: 云岗亭1: App 2: 微信")

    private Integer buyOri;

    private String payId;

    private Long memberId;

    private Long seatId;

    private String carId;


}
