package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkGateMapper;
import cn.tnar.parkservice.model.entity.TParkGate;
import cn.tnar.parkservice.service.ITParkGateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-02-28
 */
@Service("tParkGateServiceImpl")
public class TParkGateServiceImpl extends ServiceImpl<TParkGateMapper, TParkGate> implements ITParkGateService {

}
