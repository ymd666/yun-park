package cn.tnar.parkservice.model.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author kanwen
 * @description 坏账记录导出 DTO
 * @data 2019-03-19 18:27
 */
@Data
public class BadDebtExportDto {

    /**
     * 车牌号
     */
    private String carId;

    /**
     * 收费员
     */
    private String tollName;

    /**
     * 区域
     */
    private String regionName;

    /**
     * 入场时间
     */
    private Long intime;

    /**
     * 出场时间
     */
    private Long outtime;

    /**
     * 停车时长
     */
    private Long parktime;

    /**
     * 欠费金额
     */
   private BigDecimal parkamt;

    /**
     * 坏账时间
     */
    private Long baddebtTime;

    /**
     * 备注
     */
    private String remark;
}
