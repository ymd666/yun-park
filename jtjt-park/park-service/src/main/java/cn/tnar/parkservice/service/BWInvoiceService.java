package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.dto.InvoiceDetailDTO;
import cn.tnar.parkservice.model.entity.Invoice;
import cn.tnar.parkservice.model.request.InvoiceRedRequest;
import cn.tnar.parkservice.model.response.InvoiceDetailResponse;
import cn.tnar.parkservice.model.response.QueryInvoiceDetailResponse;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Description:
 * @author: yl
 * @date 2018/10/16 14:28
 */
@Service
public interface BWInvoiceService extends IService<Invoice> {


    QueryInvoiceDetailResponse getQueryInvoiceV2(Long start_time, Long end_time, Integer state, int invoice_type, int buyer_type, String park_code, int size, int page);

    /**
     * 根据流水号查找单个发票
     * @param serialNo
     * @return
     */
    InvoiceDetailDTO selectInvoiceDetail(String serialNo,String parkCode);

    /**
     * 根据流水号查找单个发票
     * @param serialNo
     * @return
     */
   /* InvoiceDetailDTO selectInvoiceDetail(String serialNo);*/

    List<Map<String,Object>> getRelation(String app_sno, String orderNo);

    InvoiceDetailDTO selectInvoiceDetailBySerialNo(Map<String,String> param);

    InvoiceDetailResponse redInvoiceBW(InvoiceRedRequest lst);

    void updateRedStateV2(HashMap map);
}
