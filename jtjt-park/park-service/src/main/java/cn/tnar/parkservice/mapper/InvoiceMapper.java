package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.Invoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @author: yl
 * @date 2018/10/16 15:16
 */
@Mapper
@Repository
public interface InvoiceMapper extends BaseMapper<Invoice> {
}
