package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TParkGroupSeatinfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface ITParkGroupSeatinfoService extends IService<TParkGroupSeatinfo> {

    List<Map<String, String>> queryCarSeat(Page<Map<String, Object>> page, Long groupId);
}
