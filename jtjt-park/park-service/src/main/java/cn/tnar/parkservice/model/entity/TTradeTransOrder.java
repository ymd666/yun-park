package cn.tnar.parkservice.model.entity;

/**
 * 转账
 */

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.math.BigDecimal;
@TableName("t_trade_trans_order")
public class TTradeTransOrder  extends Model<TTradeTransOrder> {

  private long id;
  private long nodeId;
  private String transCode;
  private String assetsDigestCode;
  private long custId;
  private long secId;
  private String assetsCode;
  private BigDecimal transamt;
  private BigDecimal feeamt;
  private long paymentMethod;
  private long transDirection;
  private String moneyPwd;
  private long targetCustId;
  private long targetSecId;
  private String targetAssetsCode;
  private long transState;
  private long thirdId;
  private long orderDatetime;
  private long matchDatetime;
  private long validDatetime;
  private String resultMsg;
  private String remark;
  private String serialno;
  private String openId;
  private long openidType;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getNodeId() {
    return nodeId;
  }

  public void setNodeId(long nodeId) {
    this.nodeId = nodeId;
  }


  public String getTransCode() {
    return transCode;
  }

  public void setTransCode(String transCode) {
    this.transCode = transCode;
  }


  public String getAssetsDigestCode() {
    return assetsDigestCode;
  }

  public void setAssetsDigestCode(String assetsDigestCode) {
    this.assetsDigestCode = assetsDigestCode;
  }


  public long getCustId() {
    return custId;
  }

  public void setCustId(long custId) {
    this.custId = custId;
  }


  public long getSecId() {
    return secId;
  }

  public void setSecId(long secId) {
    this.secId = secId;
  }


  public String getAssetsCode() {
    return assetsCode;
  }

  public void setAssetsCode(String assetsCode) {
    this.assetsCode = assetsCode;
  }

  public BigDecimal getTransamt() {
    return transamt;
  }

  public void setTransamt(BigDecimal transamt) {
    this.transamt = transamt;
  }

  public BigDecimal getFeeamt() {
    return feeamt;
  }

  public void setFeeamt(BigDecimal feeamt) {
    this.feeamt = feeamt;
  }

  public long getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(long paymentMethod) {
    this.paymentMethod = paymentMethod;
  }


  public long getTransDirection() {
    return transDirection;
  }

  public void setTransDirection(long transDirection) {
    this.transDirection = transDirection;
  }


  public String getMoneyPwd() {
    return moneyPwd;
  }

  public void setMoneyPwd(String moneyPwd) {
    this.moneyPwd = moneyPwd;
  }


  public long getTargetCustId() {
    return targetCustId;
  }

  public void setTargetCustId(long targetCustId) {
    this.targetCustId = targetCustId;
  }


  public long getTargetSecId() {
    return targetSecId;
  }

  public void setTargetSecId(long targetSecId) {
    this.targetSecId = targetSecId;
  }


  public String getTargetAssetsCode() {
    return targetAssetsCode;
  }

  public void setTargetAssetsCode(String targetAssetsCode) {
    this.targetAssetsCode = targetAssetsCode;
  }


  public long getTransState() {
    return transState;
  }

  public void setTransState(long transState) {
    this.transState = transState;
  }


  public long getThirdId() {
    return thirdId;
  }

  public void setThirdId(long thirdId) {
    this.thirdId = thirdId;
  }


  public long getOrderDatetime() {
    return orderDatetime;
  }

  public void setOrderDatetime(long orderDatetime) {
    this.orderDatetime = orderDatetime;
  }


  public long getMatchDatetime() {
    return matchDatetime;
  }

  public void setMatchDatetime(long matchDatetime) {
    this.matchDatetime = matchDatetime;
  }


  public long getValidDatetime() {
    return validDatetime;
  }

  public void setValidDatetime(long validDatetime) {
    this.validDatetime = validDatetime;
  }


  public String getResultMsg() {
    return resultMsg;
  }

  public void setResultMsg(String resultMsg) {
    this.resultMsg = resultMsg;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public String getSerialno() {
    return serialno;
  }

  public void setSerialno(String serialno) {
    this.serialno = serialno;
  }


  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }


  public long getOpenidType() {
    return openidType;
  }

  public void setOpenidType(long openidType) {
    this.openidType = openidType;
  }

}
