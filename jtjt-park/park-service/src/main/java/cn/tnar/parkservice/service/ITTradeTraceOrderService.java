package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TTradeTransOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *  转账服务
 *
 * @author Tiyer.Tao
 * @since 2019-09-26
 */
public interface ITTradeTraceOrderService extends IService<TTradeTransOrder> {

}
