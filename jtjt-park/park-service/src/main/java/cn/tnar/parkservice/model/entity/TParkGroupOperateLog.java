package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TParkGroupOperateLog对象", description="")
public class TParkGroupOperateLog implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Integer nodeId;

    @ApiModelProperty(value = "组id")
    private Long groupId;

    @ApiModelProperty(value = "车位号")
    private Long seatId;

    private Long agentId;

    @ApiModelProperty(value = "停车场编号")
    private String parkCode;

    @ApiModelProperty(value = "区域编号")
    private String regionCode;

    @ApiModelProperty(value = "车牌号")
    private String carId;

    @ApiModelProperty(value = "车辆类型")
    private Integer carType;

    private String remark;

    @ApiModelProperty(value = "操作时间")
    private Long operateTime;

    @ApiModelProperty(value = "操作员编号")
    private String operatorNo;

    @ApiModelProperty(value = "操作类型：1创建，2修改，3新增车位，4删除，5启用，6暂停，7延期，8新增车辆，9删除车辆")
    private Integer operateType;

    @ApiModelProperty(value = "操作增加天数")
    private Integer operateDay;


}
