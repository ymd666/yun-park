package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.model.request.TParkGroupMemberinfoRequest;
import cn.tnar.parkservice.mapper.TParkGroupCarinfoMapper;
import cn.tnar.parkservice.model.entity.TParkGroupCarinfo;
import cn.tnar.parkservice.service.ITParkGroupCarinfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Service("tParkGroupCarinfoServiceImpl")
public class TParkGroupCarinfoServiceImpl extends ServiceImpl<TParkGroupCarinfoMapper, TParkGroupCarinfo> implements ITParkGroupCarinfoService {

    @Override
    public List<Map<String, Object>> checkCarId(String carId,Integer carType, String memberinfoIds) {
        return baseMapper.checkCarId(carId,carType,memberinfoIds);
    }

    @Override
    public List<Map<String, Object>> getAllCarIdByMemberIds(String memberinfoIds) {
        return baseMapper.getAllCarIdByMemberIds(memberinfoIds);
    }

    @Override
    public List<Map<String, Object>> selectBaiList(TParkGroupMemberinfoRequest map) {
        return baseMapper.selectBaiList(map);
    }

    @Override
    public List<Map<String, Object>> selectMemberList( TParkGroupMemberinfoRequest map) {
        return baseMapper.selectMemberList(map);
    }


}
