package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TParkGroupCarinfo对象", description="")
public class TParkGroupCarinfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer nodeId;

    @ApiModelProperty(value = "组id")
    private Long groupId;

    @ApiModelProperty(value = "车牌号")
    private String carId;

    @ApiModelProperty(value = "车辆类型")
    private Integer carType;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "删除标识")
    private Integer delflag;


}
