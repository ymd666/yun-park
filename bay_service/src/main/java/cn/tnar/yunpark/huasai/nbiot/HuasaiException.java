package cn.tnar.yunpark.huasai.nbiot;

/**
 * Created by tieh on 2017/6/21.
 */
public class HuasaiException extends Exception {
    public HuasaiException(String msg) {
        super(msg);
    }
}
