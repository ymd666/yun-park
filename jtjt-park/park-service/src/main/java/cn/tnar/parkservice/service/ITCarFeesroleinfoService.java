package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TCarFeesroleinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface ITCarFeesroleinfoService extends IService<TCarFeesroleinfo> {

}
