package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.model.dto.MemPaymentRecordDto;
import cn.tnar.parkservice.model.response.MemPaymentResponse;
import cn.tnar.parkservice.mapper.MemPaymentRecordMapper;
import cn.tnar.parkservice.model.entity.TCarMemberamountinfo;
import cn.tnar.parkservice.service.MemPaymentRecordService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.parkservice.util.common.StringUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Author: xinZhang
 * @Date: 2019/8/15
 * @Description:
 */
@Service("memPaymentRecordServiceImpl")
public class MemPaymentRecordServiceImpl extends ServiceImpl<MemPaymentRecordMapper, TCarMemberamountinfo> implements MemPaymentRecordService {

    @Autowired
    private MemPaymentRecordMapper memPaymentRecordMapper;

    @Override
    public ResultJson queryRecords(MemPaymentRecordDto memPaymentRecordDto) {
       /* memPaymentRecordDto.setBeginTime(memPaymentRecordDto.getBeginTime()+"000000");
        memPaymentRecordDto.setEndTime(memPaymentRecordDto.getEndTime()+"235959");
        Page<MemPaymentResponse> page = new Page<>(memPaymentRecordDto.getPageNo(),memPaymentRecordDto.getPageSize());
        List<MemPaymentResponse> list = memPaymentRecordMapper.queryRecords(page,memPaymentRecordDto);
        page.setRecords(list);
        Map<String,Object> map = memPaymentRecordMapper.count(memPaymentRecordDto);
        Map<String, Object> response = new HashMap<>();
        response.put("count",map);
        response.put("page",page);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(response);*/
       return null;
    }


    @Override
    public ResultJson baseRecords(MemPaymentRecordDto memPaymentRecordDto) {
        if (StringUtil.isNotBlank(memPaymentRecordDto.getBeginTime()) &&  StringUtil.isNotBlank(memPaymentRecordDto.getEndTime())){
            memPaymentRecordDto.setBeginTime(memPaymentRecordDto.getBeginTime()+"000000");
            memPaymentRecordDto.setEndTime(memPaymentRecordDto.getEndTime()+"235959");
        }
        Page<MemPaymentResponse> page = new Page<>(memPaymentRecordDto.getPageNo(),memPaymentRecordDto.getPageSize());
        List<MemPaymentResponse> list = memPaymentRecordMapper.baseRecords(page, memPaymentRecordDto);
        page.setRecords(list);
        Map<String, Object> count = memPaymentRecordMapper.count(memPaymentRecordDto);
        Map<String, Object> response = new HashMap<>();
        response.put("count",count);
        response.put("page",page);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(response);
    }

    @Override
    public ResultJson detailRecords(Long id) {
        Map<String, Object> custName = memPaymentRecordMapper.queryCustName(id);
        Map<String, Object> operName = memPaymentRecordMapper.queryOperName(id);
        Map<String, Object> parkName = memPaymentRecordMapper.queryParkName(id);
        Map<String, Object> regionName = memPaymentRecordMapper.queryRegionName(id);
        HashMap<String, Object> response = new HashMap<>();
        if (custName != null){
            response.put("custName",custName.get("custName"));
        }else {
            response.put("custName",null);
        }
        if (operName != null){
            response.put("operName",operName.get("operName"));
        }else {
            response.put("operName",null);
        }
        if (parkName != null){
            response.put("parkName",parkName.get("parkName"));
        }else {
            response.put("parkName",null);
        }
        if (regionName != null){
            response.put("regionName",regionName.get("regionName"));
        }else {
            response.put("regionName",null);
        }
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(response);
    }


}
