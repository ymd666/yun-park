package cn.tnar.parkservice.model.dto;

/**
 * @Description:
 * @author: yl
 * @date 2018/10/16 14:40
 */
public class InvoiceDetailDTO {

    /**
     * 发票号码
     */
    private String invoiceCode;

    /*
     * 发票类型
     */
    private String invoiceType;

    /**
     * 发票名称抬头
     */
    private String invoiceName;

    /**
     * 发票金额
     */
    private String invoicePrice;

    /**
     * 申请日期
     */
    private String applyDate;

    /**
     * 开票人账号
     */
    private String buyerAccount;

    /**
     * 开票人银行
     */
    private String buyerBank;
    /**
     * 开票人电话
     */
    private String buyerTel;

    /**
     * 发票状态
     */
    private String invoiceStatus;

    /**
     * 开票时间
     */
    private String invoiceDate;

    /**
     * 买房税号
     */
    private String buyerTaxNo;

    /**
     * 买方地址
     */
    private String buyerAddress;

    /**
     * 买方账号
     */
    private String buyerName;

    /**
     * 开票人手机
     */
    private String buyerPhone;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getInvoicePrice() {
        return invoicePrice;
    }

    public void setInvoicePrice(String invoicePrice) {
        this.invoicePrice = invoicePrice;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getBuyerAccount() {
        return buyerAccount;
    }

    public void setBuyerAccount(String buyerAccount) {
        this.buyerAccount = buyerAccount;
    }

    public String getBuyerBank() {
        return buyerBank;
    }

    public void setBuyerBank(String buyerBank) {
        this.buyerBank = buyerBank;
    }

    public String getBuyerTel() {
        return buyerTel;
    }

    public void setBuyerTel(String buyerTel) {
        this.buyerTel = buyerTel;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getBuyerTaxNo() {
        return buyerTaxNo;
    }

    public void setBuyerTaxNo(String buyerTaxNo) {
        this.buyerTaxNo = buyerTaxNo;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public void setBuyerAddress(String buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }
}
