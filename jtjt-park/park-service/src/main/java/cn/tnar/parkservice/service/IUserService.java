package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.User;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *luqu
 */

public interface IUserService extends IService<User> {

    ResultJson userLogin(User user);

    ResultJson informationByAll(String json);

    ResultJson informationTree();

}
