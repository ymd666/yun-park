package cn.tnar.yunpark;

import cn.tnar.yunpark.model.ParkSpaceStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 车位状态变化事件监听器
 * Created by tieh on 2017/6/15.
 */
@Component
@ConditionalOnProperty(value = "tnar.mq.spaceListener", havingValue = "enable")
public class ParkSpaceStatusListener {
    private static final Logger log = LoggerFactory.getLogger(ParkSpaceStatusListener.class);

    @Bean
    Queue testQueue() {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("x-expires", 300000);
        return new Queue("test.space", true, false, false, args);
    }

    @Bean
    Binding testBinding(Queue queue) {
        TopicExchange exchange = new TopicExchange("amq.topic");
        return BindingBuilder.bind(queue).to(exchange).with("space.#");
    }

    @RabbitListener(queues = "test.space")
    public void onCarInOut(ParkSpaceStatus status) {
        log.info(status.toString());
    }
}
