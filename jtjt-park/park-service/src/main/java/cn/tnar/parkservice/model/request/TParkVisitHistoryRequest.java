package cn.tnar.parkservice.model.request;


import lombok.Data;

import java.util.List;


@Data
public class TParkVisitHistoryRequest {


    private String parkCode;

    private String carId;

    private String visitState;

    private Long startTime;

    private Long endTime;

    private Integer pageNo;

    private Integer pageSize;

    private  Long memberGroupId;

    private String mobile;

    private String applyStatus;

    private List<Integer> statusList;
}
