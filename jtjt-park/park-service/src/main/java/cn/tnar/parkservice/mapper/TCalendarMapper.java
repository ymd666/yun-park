package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.entity.TCalendar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-03-11
 */
@Mapper
@QuerySupport
@Repository
public interface TCalendarMapper extends BaseMapper<TCalendar> {

}
