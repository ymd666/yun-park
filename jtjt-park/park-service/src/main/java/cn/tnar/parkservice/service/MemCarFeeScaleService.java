package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.MemCarFeeScale;
import cn.tnar.parkservice.model.request.MemCarFeeScaleRequest;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface MemCarFeeScaleService extends IService<MemCarFeeScale> {

    ResultJson query(Map<String,Object> map);

    ResultJson save(MemCarFeeScaleRequest request);

    ResultJson update(MemCarFeeScaleRequest request);

}
