package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 11:12:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("t_acct_refund_record")
public class TAcctRefundRecord extends Model<TAcctRefundRecord> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableId("node_id")
    private Integer nodeId;
    @TableId("cust_id")
    private Long custId;
    @TableId("phone")
    private String phone;
    @TableId("refund_amt")
    private BigDecimal refundAmt;
    @TableId("refund_channel")
    private Integer refundChannel;
    @TableId("refund_remark")
    private String refundRemark;
    @TableId("refund_time")
    private Long refundTime;
    @TableId("refund_status")
    private Integer refundStatus;
    @TableId("operational_operator")
    private String operationalOperator;
    @TableId("financial_operator")
    private String financialOperator;
    @TableId("manager_operator")
    private String managerOperator;
    @TableId("operational_checktime")
    private Long operationalChecktime;
    @TableId("financial_checktime")
    private Long financialChecktime;
    @TableId("manager_checktime")
    private Long managerChecktime;
    @TableId("remark")
    private String remark;
    @TableId("refuseRemark")
    private String refuseRemark;
}
