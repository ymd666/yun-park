package cn.tnar.parkservice.model.response;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_sys_menu_4")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TSysMenuDto {

    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField("audit_flag")
    private Integer auditFlag;
    @TableField("biz_type")
    private Integer bizType;
    @TableField("func_code")
    private String funcCode;
    private String icon;
    @TableField("menu_flag")
    private Integer menuFlag;
    @TableField("menu_id")
    private Long menuId;
    @TableField("menu_name")
    private String menuName;
    @TableField("menu_name_en")
    private String menuNameEn;
    @TableField("modify_oper")
    private Long modifyOper;
    @TableField("modify_time")
    private Long modifyTime;
    @TableField("mtd_code")
    private String mtdCode;
    @TableField("obj_code")
    private String objCode;
    @TableField("parent_menu_id")
    private Long parentMenuId;
    @TableField("serial_no")
    private Long serialNo;
    private String url;
    private String depends;
    @TableField("object_code")
    private String objectCode;
    @TableField("menu_type")
    private Integer menuType;
    @TableField("platform_type")
    private Integer platformType;

    @TableField(exist = false)
    private List<TSysMenuDto> childs = new ArrayList<TSysMenuDto>();

    @Override
    public String toString() {
        return "TSysMenu{" +
                ", id=" + id +
                ", auditFlag=" + auditFlag +
                ", bizType=" + bizType +
                ", funcCode=" + funcCode +
                ", icon=" + icon +
                ", menuFlag=" + menuFlag +
                ", menuId=" + menuId +
                ", menuName=" + menuName +
                ", menuNameEn=" + menuNameEn +
                ", modifyOper=" + modifyOper +
                ", modifyTime=" + modifyTime +
                ", mtdCode=" + mtdCode +
                ", objCode=" + objCode +
                ", parentMenuId=" + parentMenuId +
                ", serialNo=" + serialNo +
                ", url=" + url +
                ", depends=" + depends +
                ", objectCode=" + objectCode +
                "}";
    }
}
