package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: xinZhang
 * @Date: 2019/9/26
 * @Description:
 */
@TableName("t_car_feesroleinfo")
@Data
public class MemCarFeeScale {

    private static final long serialVersionUID = 1L;

    @TableId(type= IdType.AUTO)
    private Long id;

    private Integer nodeId;

    private Integer carType;

    @TableField("cust_id")
    private Long custId;

    private String roleName;

    private BigDecimal monthValue;

    private String remark;

    @TableField("delflag")
    private Integer delflag;

    private String regionCode;

    private Integer cardType;

    private Integer giveday;

    private String startTime;

    private String endTime;
}
