package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TCarMemberamountinfo;
import cn.tnar.parkservice.model.response.RecordResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zzb
 * @since 2019-08-29
 */
public interface ITCarMemberamountinfoService extends IService<TCarMemberamountinfo> {


    Page<RecordResponse> getAmountInfo(Integer pageSize, Integer pageNo, Long memberInfoId);
    Page<RecordResponse> getStopRecord(Integer pageSize, Integer pageNo, Long memberInfoId);
}
