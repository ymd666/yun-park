package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:20:35
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "TParkFeeIndex对象", description = "")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("t_park_feeindex")
public class TParkFeeIndex extends Model<TParkFeeIndex> {
    @TableId(value = "id")
    private Long id;

    @TableField(value = "node_id")
    private Integer nodeId;

    @TableField(value = "name")
    private String name;

    @TableField(value = "region_code")
    private String regionCode;

    @TableField(value = "agent_id")
    private Long agentId;

    @TableField(value = "park_code")
    private String parkCode;

    @TableField(value = "cashoff")
    private Integer cashoff;

    @TableField(value = "cardoff")
    private Integer cardoff;

    @TableField(value = "memberoff")
    private Integer memberoff;

    @TableField(value = "gracetime")
    private Integer gracetime;

    @TableField(value = "userid")
    private Long userid;

    @TableField(value = "utime")
    private Long utime;

    @TableField(value = "cycle")
    private Integer cycle;
}
