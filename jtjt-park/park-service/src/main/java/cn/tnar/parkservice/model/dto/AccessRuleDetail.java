package cn.tnar.parkservice.model.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created by fly on 2018/11/28.
 */
@Data
public class AccessRuleDetail {

    private Integer id;

    /**
     * 准入规则名称
     */
    private String ruleName;
    /**
     * 准入停车场名称
     */
    private String parkName;
    /**
     * 准入停车场编号
     */
    private String parkCode;
    /**
     * 准入停车区域
     */
    private String regionCode;
    /**
     * 准入区域名称
     */
    private String regionName;
    /**
     * 准入入场时间
     */
    private String startTime;
    /**
     * 准入出场时间
     */
    private String endTime;
    /**
     * 准入出入口
     */
    private String accessGate;

    /**
     * 会员车位数
     */
    private Integer memberNum;

    /**
     * 临停车位数
     */
    private Integer temporaryNum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 储值车位数
     */
    private Integer storeNum;

    /**
     * 预约车位数
     */
    private Integer bookingNum;

    /**
     * 访客车位数
     */
    private Integer visitorNum;

    /**
     * 其他规则
     */
    private String otherRule;

    /**
     * 月卡车颜色
     */
    private String memberColor;

    /**
     * 临停车颜色
     */
    private String temporaryColor;

    /**
     * 储值车颜色
     */
    private String storeColor;

    /**
     * 预约车颜色
     */
    private String bookingColor;

    /**
     * 访客车颜色
     */
    private String visitorColor;

    /**
     * 单双号限行
     */
    private Integer carstopLimit;

    /**
     * 单双号限行
     */
    private Integer isUsing;




    private List<JSONObject> accessList;

    private Integer actionType;

    private List<Map<String, Object>> gateList;

    /**
     * 准入车辆类型，如1-临时车、2-月卡车、3-储值车，4-内部储值车，5-免费车，6-内部车等
     */
    private String cardType;
    /**
     * 准入车牌颜色，1-蓝，3-黄，6-绿，7-白
     */
    private String carType;

    /**
     * 总车位数限制
     */
    private Integer totalNum;
    /**
     * 无牌车准入数
     */
    private Integer noplateNum;

    /**
     * 准入规则id
     */
    private Long accessRuleId;
}
