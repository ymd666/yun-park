package cn.tnar.yunpark.huasai;

import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * Created by tieh on 2016/9/23.
 */
public class HuasaiMessageHeader {
    byte type;
    long hostSn;
    long prjSn;
    byte prjYear;
    byte prjCode;
    long timestamp;

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public void setHostSn(long hostSn) {
        this.hostSn = hostSn;
    }

    public void setPrjSn(long prjSn) {
        this.prjSn = prjSn;
    }

    public byte getPrjYear() {
        return prjYear;
    }

    public void setPrjYear(byte prjYear) {
        this.prjYear = prjYear;
    }

    public byte getPrjCode() {
        return prjCode;
    }

    public void setPrjCode(byte prjCode) {
        this.prjCode = prjCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestampMs() {
        return timestamp * 1000;
    }

    public static HuasaiMessageHeader parse(ByteBuf buf) {
        ByteBuf b = buf.order(ByteOrder.LITTLE_ENDIAN);
        HuasaiMessageHeader header = new HuasaiMessageHeader();
        header.setType(b.readByte());
        header.setHostSn(b.readUnsignedInt());
        header.setPrjSn(b.readUnsignedInt());
        header.setPrjYear(b.readByte());
        header.setPrjCode(b.readByte());
        header.setTimestamp(b.readLong());
        return header;
    }

    /**
     * 项目号补齐4位，主机号不用补齐
     * @return
     */
    public String getHostCode() {
        return String.format("%X%02X%04d%d", prjCode, prjYear, prjSn, hostSn);
    }
}
