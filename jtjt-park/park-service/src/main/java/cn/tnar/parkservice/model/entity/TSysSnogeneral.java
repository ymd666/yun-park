package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("t_sys_snogeneral")
public class TSysSnogeneral {

  private long lastsno;
  @TableId("opertype")
  private long opertype;
  @TableField("node_id")
  private long nodeId;


  public long getLastsno() {
    return lastsno;
  }

  public void setLastsno(long lastsno) {
    this.lastsno = lastsno;
  }


  public long getOpertype() {
    return opertype;
  }

  public void setOpertype(long opertype) {
    this.opertype = opertype;
  }


  public long getNodeId() {
    return nodeId;
  }

  public void setNodeId(long nodeId) {
    this.nodeId = nodeId;
  }

}
