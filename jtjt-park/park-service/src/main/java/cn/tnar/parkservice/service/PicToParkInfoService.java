package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.dto.AcctBasePhotoDto;

/**
 * @Author: ZhangZhibiao
 * @Date:2018-11-15
 */
public interface PicToParkInfoService {
    int insertPicToParkInfo(String photoKey, String parkCode);

    String getPhotoKeyFrom(String parkCode);

    int selectId(String parkCode);

    int insertPicToBasePhoto(AcctBasePhotoDto dto);

    int delPhotoF(String parkCode);

    int delPhotoX(int id);
}
