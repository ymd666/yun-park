package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * UserMapper
 */
public interface UserMapper extends BaseMapper<User> {
}