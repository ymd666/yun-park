package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2019-03-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TCalendar对象", description="")
public class TCalendar implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("DATUMDATE")
    private String datumdate;

    @TableField("WEEKDAY")
    private Integer weekday;

    @TableField("HOLIDAY")
    private String holiday;

    @TableField("LABEL")
    private String label;

    @TableField("UPDATETIME")
    private String updatetime;


}
