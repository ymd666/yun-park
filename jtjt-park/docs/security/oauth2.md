# Oauth2

```
     +--------+                               +---------------+
     |        |--(A)- Authorization Request ->|   Resource    |
     |        |                               |     Owner     |
     |        |<-(B)-- Authorization Grant ---|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(C)-- Authorization Grant -->| Authorization |
     | Client |                               |     Server    |
     |        |<-(D)----- Access Token -------|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(E)----- Access Token ------>|    Resource   |
     |        |                               |     Server    |
     |        |<-(F)--- Protected Resource ---|               |
     +--------+                               +---------------+
```

## Authorization Grant

a credential representing the resource owner's authorization (to access its protected resources) used by the client to obtain an access token.  

### Grant Type 
authorization code, implicit, resource owner password credentials, and client credentials 
- Authorization Code:  the client directs the resource owner to an authorization server , which in turn directs the resource owner back to the client with the authorization code.
- Implicit: the client is issued an access token directly(for clients implemented in a browser using a language such as JavaScript).
- Resource Owner Password Credentials: can be used directly as an authorization grant to obtain an access token. ( only be used when there is a high degree of trust between the resource owner and the client), and when other authorization grant types are not available. the resource owner credentials are used for a single request and are exchanged for an access token.  
- Client Credentials: limited to the protected resources under the control of the client or to protected resources previously arranged with the authorization server.

## Access Token

credentials used to access protected resources.

usually opaque to the client.

may denote an identifier used to retrieve the authorization information or may self-contain the authorization information in a verifiable manner.

## Refresh Token

credentials used to obtain access tokens. 
used to obtain a new access token when the current access token becomes invalid or expires.

```
  +--------+                                           +---------------+
  |        |--(A)------- Authorization Grant --------->|               |
  |        |                                           |               |
  |        |<-(B)----------- Access Token -------------|               |
  |        |               & Refresh Token             |               |
  |        |                                           |               |
  |        |                            +----------+   |               |
  |        |--(C)---- Access Token ---->|          |   |               |
  |        |                            |          |   |               |
  |        |<-(D)- Protected Resource --| Resource |   | Authorization |
  | Client |                            |  Server  |   |     Server    |
  |        |--(E)---- Access Token ---->|          |   |               |
  |        |                            |          |   |               |
  |        |<-(F)- Invalid Token Error -|          |   |               |
  |        |                            +----------+   |               |
  |        |                                           |               |
  |        |--(G)----------- Refresh Token ----------->|               |
  |        |                                           |               |
  |        |<-(H)----------- Access Token -------------|               |
  +--------+           & Optional Refresh Token        +---------------+
```

## Client Authentication

### Client Password

HTTP Basic authentication

``` 
 Authorization: Basic czZCaGRSa3F0Mzo3RmpmcDBaQnIxS3REUmJuZlZkbUl3
 ```

 or request-body with TLS:

client_id
```
         REQUIRED.  The client identifier issued to the client during
         the registration process described by Section 2.2.
```
client_secret
```
         REQUIRED.  The client secret.  The client MAY omit the
         parameter if the client secret is an empty string.
```

# Protocol Endpoints

- **Authorization endpoint**：  client to obtain authorization from the resource owner via user-agent redirection.
- **Token endpoint**:  client to exchange an authorization grant for an access token, typically with client authentication.
- Redirection endpoint: authorization server to return responses containing authorization credentials to the client via the resource owner user-agent.

---

# Spring Security Oauth


## OAuth 2.0 Provider Implementation
The requests for the tokens are handled by **Spring MVC controller endpoints**, and access to protected resources is handled by standard **Spring Security request filters**. 

- AuthorizationEndpoint:  service requests for authorization (`/oauth/authorize`)
- TokenEndpoint: service requests for access tokens.(`/oauth/token`)

## Authorization Server Configuration

`AuthorizationServerConfigurer` with `@EnableAuthorizationServer`

- ClientDetailsServiceConfigurer: defines the client details service. 
- AuthorizationServerSecurityConfigurer: defines the security constraints(安全约束) on the token endpoint.
- AuthorizationServerEndpointsConfigurer: defines the authorization and token endpoints and the token services.


the way that an authorization code is supplied to an OAuth client.


### Configuring Client Details

ClientDetailsServiceConfigurer: `in-memory`, `JDBC`

- clientId
- secret
- scope
- authorizedGrantTypes
- authorities

### Managing Tokens

the operations that are necessary to manage OAuth 2.0 tokens. 

-  access token is created, the authentication must be stored.
- The access token is used to load the authentication that was used to authorize its creation.


- InMemoryTokenStore: develop, single server
- JdbcTokenStore
- JWT: granted with short expiry and the revocation is handled at the refresh token(can't easily revoke an access token). storing a lot of user credential information in them may quite large.

need `JwtTokenStore`, `JwtAccessTokenConverter` needed because `JwtTokenStore` decode the tokens, both in **Authorization Server** and the **Resource Server**. 

### Grant Types

`AuthorizationEndpoint` configured via the `AuthorizationServerEndpointsConfigurer`