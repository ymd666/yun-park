package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/9/11
 * @Description:
 */
@Data
public class MemberManageRequest {

    private String parkCode;

    private Integer classify;

    private Integer cardId;

    private String startTime;

    private String endTime;

    private String carId;

    private String ownerName;
}
