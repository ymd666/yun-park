package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkMemberGroupRelationMapper;
import cn.tnar.parkservice.model.entity.TParkMemberGroupRelation;
import cn.tnar.parkservice.service.ITParkMemberGroupRelationService;
import cn.tnar.parkservice.service.ITParkMemberGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月02日 15:09:44
 */
@Service("tParkMemberGroupRelationServiceImpl")
public class TParkMemberGroupRelationServiceImpl extends ServiceImpl<TParkMemberGroupRelationMapper, TParkMemberGroupRelation> implements ITParkMemberGroupRelationService {


    @Autowired
    ITParkMemberGroupService itParkMemberGroupService;

    @Override
    public List<String> getMemberIdByChildLook(Long groupId) {
        return baseMapper.getMemberIdByChildLook(groupId);
    }

    @Override
    public List<String> getMemberIdByChild(Long groupId) {
        return baseMapper.getMemberIdByChild(groupId);
    }

    @Override
    public List<String> getMemberIdByParent(Long groupId) {
        List<String> strings = itParkMemberGroupService.selectByParentId(new ArrayList<>(), groupId);
        if (strings.size() == 0) {
            return new ArrayList<>();
        } else {
            String groupIds = String.join(",", strings);
            return baseMapper.getMemberIdByParent(groupIds);
        }

    }

    @Override
    public List<String> getVisitIdByChild(Long groupId) {
        return baseMapper.getVisitIdByChild(String.valueOf(groupId));
    }

    @Override
    public List<String> getVisitIdByParent(Long groupId) {
        List<String> strings = itParkMemberGroupService.selectByParentId(new ArrayList<>(), groupId);
        if (strings.size() == 0) {
            return new ArrayList<>();
        } else {
            String groupIds = String.join(",", strings);
            return baseMapper.getVisitIdByChild(groupIds);
        }

    }

    @Override
    public List<Map<String, String>> checkTel(Long groupId, String telNo) {
        return baseMapper.checkTel(groupId, telNo);
    }

    @Override
    public Long getGroupIdByCarNo(String carNo) {
        return baseMapper.getGroupIdByCarNo(carNo);
    }


}
