package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/9/27
 * @Description:
 */
@Data
public class TDashboardResponse {

    //现金统计
    private BigDecimal cashCount;

    //电子统计
    private BigDecimal ePayCount;

    //异常抬杆统计
    private Long unNormalCar;

    //临停车
    private Long tempCar;

    //访客车
    private Long visiterCar;

    //会员车
    private Long memberCar;

    private Long parkSeat;

    private Long parkBusySeat;

    private List<Map<String,Object>> regionSeat;

    private Long regionBusySeat;
}
