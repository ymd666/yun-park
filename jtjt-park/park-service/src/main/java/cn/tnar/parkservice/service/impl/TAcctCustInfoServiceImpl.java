package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TAcctCustInfoMapper;
import cn.tnar.parkservice.mapper.TAcctCustLoginInfoMapper;
import cn.tnar.parkservice.mapper.TSysRoleMapper;
import cn.tnar.parkservice.model.entity.TAcctCustInfo;
import cn.tnar.parkservice.model.request.TAcctCustInfoRequest;
import cn.tnar.parkservice.model.response.SysRoleResponse;
import cn.tnar.parkservice.model.response.TAcctCustInfoResponse;
import cn.tnar.parkservice.service.ITAcctCustInfoService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Service
public class TAcctCustInfoServiceImpl extends ServiceImpl<TAcctCustInfoMapper, TAcctCustInfo> implements ITAcctCustInfoService {
    private static SimpleDateFormat f_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat f_date = new SimpleDateFormat("yyyyMMdd");

    @Autowired
    private TAcctCustInfoMapper acctCustInfoMapper;

    @Autowired
    private TAcctCustLoginInfoMapper tAcctCustLoginInfoMapper;

    @Autowired
    private TSysRoleMapper tSysRoleMapper;

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

    /**
     * 注册渠道分析
     *
     * @param openDate
     * @return
     */
    @Override
    public List<Map<String, Object>> getStatisNumGroupForDays(String openDate) {
        openDate = openDate.replaceAll("-", "");
        String[] timeArr = openDate.split("~");
        int length = openDate.length();
        int listSize = 12;
        List<Map<String, Object>> mapList = new ArrayList<>();
        List<Map<String, Object>> resList = new ArrayList<>();
        if (length == 4) {
            int beginWith = Integer.valueOf(openDate + "0101");
            int endWith = Integer.valueOf(openDate + "1231");
            mapList = acctCustInfoMapper.getStatisNumGroupForMonth(beginWith, endWith);
        } else if (length == 6) {
            int beginWith = Integer.valueOf(openDate + "01");
            int endWith = Integer.valueOf(openDate + "31");
            mapList = acctCustInfoMapper.getStatisNumGroupForDays(beginWith, endWith);
            listSize = 31;
        }

        for (int i = 1; i <= listSize; i++) {
            String index = ("0" + i);
            index = index.substring(index.length() - 2);
            Map<String, Object> map = new HashMap<>();
            int weixin = 0;
            int ali = 0;
            int app = 0;
            int flypark = 0;
            for (Map<String, Object> m : mapList) {
                String channel = String.valueOf(m.get("open_channel"));
                String time = String.valueOf(m.get("time"));
                int num = Integer.valueOf(String.valueOf(m.get("num")));
                if (time.equals(index)) {
                    if (channel.equals("1")) {
                        weixin = num;
                    } else if (channel.equals("2")) {
                        ali = num;
                    } else if (channel.equals("3")) {
                        app = num;
                    } else if (channel.equals("4")) {
                        flypark = num;
                    }
                }
            }
            map.put("weixin", weixin);
            map.put("ali", ali);
            map.put("app", app);
            map.put("flypark", flypark);
            map.put("index", index);
            resList.add(map);
        }
        return resList;
    }

    /**
     * 用户数活跃度可视化模块
     *
     * @param beginWith
     * @param endWith
     * @return
     */
    @Override
    public Map<String, Object> getStatisNumByTime(int beginWith, int endWith) {

        long now = System.currentTimeMillis();
        long time1 = Long.valueOf(simpleDateFormat.format(new Date(now - 604800000l)));
        long time2 = Long.valueOf(simpleDateFormat.format(new Date(now - 2678400000l)));
        //查询各渠道总数
        Map<String, Object> allMap = new HashMap<>();
        turnList2Map(allMap, acctCustInfoMapper.getNumGroupByChannel(beginWith, endWith));
        //活跃用户
        Map<String, Object> activeMap = new HashMap<>();
        turnList2Map(activeMap, acctCustInfoMapper.getActiveNumByTime(beginWith, endWith, time1));
        //普通用户
        Map<String, Object> commonMap = new HashMap<>();
        turnList2Map(commonMap, acctCustInfoMapper.getCommonNumByTime(beginWith, endWith, time2, time1));
        //僵尸用户
        Map<String, Object> inActiveMap = new HashMap<>();
        turnList2Map(inActiveMap, acctCustInfoMapper.getInactiveNumByTime(beginWith, endWith, time2));

        Map<String, Object> map = new HashMap<>();
        map.put("total", allMap);
        map.put("active", activeMap);
        map.put("common", commonMap);
        map.put("inActive", inActiveMap);

        return map;
    }

    /**
     * 查询系统账号密码
     * @param custId
     * @return
     */
    @Override
    public String queryPassword(Long custId) {
        return tAcctCustLoginInfoMapper.queryPassword(custId);
    }

    /**
     * 查询系统账号列表
     * @param request
     * @return
     */
    @Override
    public ResultJson querySysAccount(TAcctCustInfoRequest request) {
        Integer pageNo =request.getPageNo();
        Integer pageSize =request.getPageSize();
        //根据角色id查询系统账号
        if (request.getRoleId() != null){
            List<Long> custIds = tSysRoleMapper.queryCustIdsByRoleId(request.getRoleId());
            request.setCustIds(custIds);
        }
        Page<TAcctCustInfoResponse> page = new Page<>(pageNo,pageSize);
        List<TAcctCustInfoResponse> list = acctCustInfoMapper.querySysAccount(page,request);
        list.forEach(tAcctCustInfoResponse -> {
            List<SysRoleResponse> sysRoleResponses = tSysRoleMapper.querySysRolesByCustId(tAcctCustInfoResponse.getCustId());
            tAcctCustInfoResponse.setSysRoleResponseList(sysRoleResponses);
        });
        page.setRecords(list);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(page);
    }

    public void turnList2Map(Map<String, Object> map, List<Map<String, Object>> list) {
        int weixin = 0;
        int ali = 0;
        int app = 0;
        int flypark = 0;
        if (null != list && list.size() > 0) {
            for (Map<String, Object> m : list) {
                String channel = String.valueOf(m.get("open_channel"));
                int num = Integer.valueOf(String.valueOf(m.get("num")));
                if (channel.equals("1")) {
                    weixin = num;
                } else if (channel.equals("2")) {
                    ali = num;
                } else if (channel.equals("3")) {
                    app = num;
                } else if (channel.equals("4")) {
                    flypark = num;
                }
            }
        }
        map.put("weixin", weixin);
        map.put("ali", ali);
        map.put("app", app);
        map.put("flypark", flypark);
    }



}
