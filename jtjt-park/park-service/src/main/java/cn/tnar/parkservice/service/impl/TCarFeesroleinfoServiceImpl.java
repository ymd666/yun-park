package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.mapper.TCarFeesroleinfoMapper;
import cn.tnar.parkservice.model.entity.TCarFeesroleinfo;
import cn.tnar.parkservice.service.ITCarFeesroleinfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Service("tCarFeesroleinfoServiceImpl")
public class TCarFeesroleinfoServiceImpl extends ServiceImpl<TCarFeesroleinfoMapper, TCarFeesroleinfo> implements ITCarFeesroleinfoService {


}
