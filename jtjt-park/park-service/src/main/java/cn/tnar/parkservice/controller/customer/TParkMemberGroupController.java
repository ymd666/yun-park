package cn.tnar.parkservice.controller.customer;


import cn.tnar.parkservice.model.entity.MemberGroupReq;
import cn.tnar.parkservice.model.entity.TParkMemberGroupDto;
import cn.tnar.parkservice.model.request.MemberGoupReqParam;
import cn.tnar.parkservice.service.ITParkMemberGroupService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年07月08日 14:53:25
 */
@Api(tags = "会员分组 - Controller")
@RestController
@RequestMapping(value = "/service/memberGroup")
@Slf4j
public class TParkMemberGroupController {

    @Autowired
    @Qualifier("tParkMemberGroupServiceImpl")
    private ITParkMemberGroupService itParkMemberGroupService;

    ResultJson resultJson = new ResultJson();

    /**
     * 新增会员分组信息，
     * 1.当新增目录的时候无需绑定多个停车场，parkCode直接传入当前停车场即可，
     * 2.当新增子卡的时候可以绑定多个停车场，前端需要传入目录及子卡的树形结构，至少包含，名称和类型。
     *
     * @param json
     * @return ResultJson
     */
    @PostMapping("/insert")
    @ApiOperation("新增会员分组信息")
    public ResultJson insertMemberGroup(@RequestBody String json) {
        try {
            MemberGroupReq memberGroupReq = JSON.parseObject(json, MemberGroupReq.class);
            return itParkMemberGroupService.insertMemberGroup(memberGroupReq);
        } catch (Exception e) {
            log.error("新增会员月卡分组信息异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("新增会员月卡分组信息异常")
                    .setData(null);
        }
    }


    /**
     * 禁用会员分组信息
     *
     * @param json
     * @return ResultJson
     */
    @PostMapping("/deleteById")
    @ApiOperation("禁用会员分组信息")
    public ResultJson deleteMemberGroupById(@RequestBody String json) {
        try {
            MemberGoupReqParam req = JSON.parseObject(json, MemberGoupReqParam.class);
            return itParkMemberGroupService.deleteMemberGroupById(req.getId(), req.getDelFlag());
        } catch (Exception e) {
            log.error("禁用会员分组信息异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("禁用会员分组信息异常")
                    .setData(null);
        }
    }


    /**
     * 禁用会员分组信息
     *
     * @param json
     * @return ResultJson
     */
    @PostMapping("/deleteById_NEW")
    @ApiOperation("删除会员分组信息")
    public ResultJson deleteMemberGroupById_NEW(@RequestBody String json) {
        try {
            MemberGoupReqParam req = JSON.parseObject(json, MemberGoupReqParam.class);
            return itParkMemberGroupService.deleteMemberGroupById_NEW(req.getId());
        } catch (Exception e) {
            log.error("删除会员分组信息异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("删除会员分组信息异常")
                    .setData(null);
        }
    }


    /**
     * @param json 运营商已确定的情况下，直接下拉框选择停车场，传入parkCode即可
     * @return ResultJson
     */
    @PostMapping("/selectTree")
    @ApiOperation("会员分组树形结构")
    public ResultJson selectTree(@RequestBody String json) {
        try {
            MemberGoupReqParam memberGoupReqParam = JSON.parseObject(json, MemberGoupReqParam.class);
            return itParkMemberGroupService.selectTree(memberGoupReqParam.getParkCode());
        } catch (Exception e) {
            log.error("查询会员分组树形结构异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("查询会员分组树形结构异常")
                    .setData(null);
        }
    }

    /**
     * @param json 运营商已确定的情况下，直接下拉框选择停车场，传入parkCode即可
     *             {"parkCode":123,"key":"访客"}     模糊查询关键字
     * @return ResultJson
     */
    @PostMapping("/selectTreeByKey")
    @ApiOperation("模糊查询会员分组树形结构")
    public ResultJson selectTreeByKey(@RequestBody String json) {
        try {
            MemberGoupReqParam reqParam = JSON.parseObject(json, MemberGoupReqParam.class);
            String parkCode = reqParam.getParkCode();
            String key = reqParam.getKey();
            ResultJson resultJson = itParkMemberGroupService.selectTree(parkCode);
            if (resultJson != null) {
                Object data = resultJson.getData();
                if (data instanceof TParkMemberGroupDto) {
                    TParkMemberGroupDto resultTree = (TParkMemberGroupDto) data;
                    resultJson = itParkMemberGroupService.selectTreeByKey(parkCode, resultTree, key);
                    return resultJson;
                }
            }
            return resultJson.setCode(ResultCode.SUCCESS)
                    .setMsg(ResultCode.SUCCESS_MSG)
                    .setData(null);
        } catch (Exception e) {
            log.error("查询会员分组树形结构异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("查询会员分组树形结构异常")
                    .setData(null);
        }
    }


    /**
     * 更新会员分组信息
     *
     * @param json
     * @return ResultJson
     */
    @PostMapping("/update")
    @ApiOperation("更新会员分组信息")
    @ApiIgnore
    public ResultJson updateMemberGroup(@RequestBody String json) {
        try {
            MemberGroupReq memberGroupReq = JSON.parseObject(json, MemberGroupReq.class);
            return itParkMemberGroupService.updateMemberGroup(memberGroupReq);
        } catch (Exception e) {
            log.error("修改会员分组信息异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("修改会员分组信息异常")
                    .setData(null);
        }
    }

    /**
     * 更新会员分组信息
     *
     * @param json
     * @return ResultJson
     */
    @PostMapping("/update2")
    @ApiOperation("更新会员分组信息")
    @ApiIgnore
    public ResultJson updateMemberGroup2(@RequestBody String json) {
        try {
            MemberGroupReq memberGroupReq = JSON.parseObject(json, MemberGroupReq.class);
            return itParkMemberGroupService.updateMemberGroup2(memberGroupReq);
        } catch (Exception e) {
            log.error("修改会员分组信息异常", e);
            return resultJson.setCode(ResultCode.FAIL)
                    .setMsg("修改会员分组信息异常")
                    .setData(null);
        }
    }


    @PostMapping("/selectOther")
    @ApiOperation("查询所有自定义子卡")
    public ResultJson selectOther(@RequestBody String json) {
        return itParkMemberGroupService.selectOther(json);

    }

    @PostMapping("/selectAll")
    @ApiOperation("查询停车场下所有卡组(除临时车卡组)")
    public ResultJson selectAll(@RequestBody String json) {
        return itParkMemberGroupService.selectAll(json);
    }

    @PostMapping("/selectByParent")
    @ApiOperation("根据卡组查询子卡(除临时车卡组)")
    public ResultJson selectByParent(@RequestBody String json) {
        return itParkMemberGroupService.selectByParent(json);
    }

    @PostMapping("/configFullFree")
    @ApiOperation("卡组配置满减")
    public ResultJson configFullFree(@RequestBody String json) {
        return itParkMemberGroupService.configFullFree(json);
    }

}
