package cn.tnar.parkservice.model.dto;

public class CloudParkBilling {
    private long id;
    private long index_id;
    private int cartype;
    private int freetime1;
    private float month;
    private float t_limit;
    private int freetime;
    private int d_type;
    private float d_preamt;
    private float d_once;
    private float d_limit;
    private long d_begin;
    private long d_end;
    private int d_unittime1;
    private int d_unittime2;
    private int d_unittime3;
    private int d_unittime4;
    private int d_cycletime1;
    private int d_cycletime2;
    private int d_cycletime3;
    private int d_cycletime4;
    private float d_price1;
    private float d_price2;
    private float d_price3;
    private float d_price4;
    private float n_price1;
    private float n_price2;
    private float n_price3;
    private float n_price4;
    private long n_begin;
    private long n_end;
    private float n_limit;
    private float n_once;
    private float n_preamt;
    private int n_type;
    private int n_unittime1;
    private int n_unittime2;
    private int n_unittime3;
    private int n_unittime4;
    private int n_cycletime1;
    private int n_cycletime2;
    private int n_cycletime3;
    private int n_cycletime4;
    private int worktype;
    private int count;
    private String park_code;
    private String licensekey;
    private int user_type;
    private int carplatecolor;
    private int newCarType;
    private int delflag;

    public int getDelflag() {
        return delflag;
    }

    public void setDelflag(int delflag) {
        this.delflag = delflag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIndex_id() {
        return index_id;
    }

    public void setIndex_id(long index_id) {
        this.index_id = index_id;
    }

    public int getCartype() {
        return cartype;
    }

    public void setCartype(int cartype) {
        this.cartype = cartype;
    }

    public int getFreetime1() {
        return freetime1;
    }

    public void setFreetime1(int freetime1) {
        this.freetime1 = freetime1;
    }

    public float getMonth() {
        return month;
    }

    public void setMonth(float month) {
        this.month = month;
    }

    public float getT_limit() {
        return t_limit;
    }

    public void setT_limit(float t_limit) {
        this.t_limit = t_limit;
    }

    public int getFreetime() {
        return freetime;
    }

    public void setFreetime(int freetime) {
        this.freetime = freetime;
    }

    public int getD_type() {
        return d_type;
    }

    public void setD_type(int d_type) {
        this.d_type = d_type;
    }

    public float getD_preamt() {
        return d_preamt;
    }

    public void setD_preamt(float d_preamt) {
        this.d_preamt = d_preamt;
    }

    public float getD_once() {
        return d_once;
    }

    public void setD_once(float d_once) {
        this.d_once = d_once;
    }

    public float getD_limit() {
        return d_limit;
    }

    public void setD_limit(float d_limit) {
        this.d_limit = d_limit;
    }

    public long getD_begin() {
        return d_begin;
    }

    public void setD_begin(long d_begin) {
        this.d_begin = d_begin;
    }

    public long getD_end() {
        return d_end;
    }

    public void setD_end(long d_end) {
        this.d_end = d_end;
    }

    public int getD_unittime1() {
        return d_unittime1;
    }

    public void setD_unittime1(int d_unittime1) {
        this.d_unittime1 = d_unittime1;
    }

    public int getD_unittime2() {
        return d_unittime2;
    }

    public void setD_unittime2(int d_unittime2) {
        this.d_unittime2 = d_unittime2;
    }

    public int getD_unittime3() {
        return d_unittime3;
    }

    public void setD_unittime3(int d_unittime3) {
        this.d_unittime3 = d_unittime3;
    }

    public int getD_unittime4() {
        return d_unittime4;
    }

    public void setD_unittime4(int d_unittime4) {
        this.d_unittime4 = d_unittime4;
    }

    public int getD_cycletime1() {
        return d_cycletime1;
    }

    public void setD_cycletime1(int d_cycletime1) {
        this.d_cycletime1 = d_cycletime1;
    }

    public int getD_cycletime2() {
        return d_cycletime2;
    }

    public void setD_cycletime2(int d_cycletime2) {
        this.d_cycletime2 = d_cycletime2;
    }

    public int getD_cycletime3() {
        return d_cycletime3;
    }

    public void setD_cycletime3(int d_cycletime3) {
        this.d_cycletime3 = d_cycletime3;
    }

    public int getD_cycletime4() {
        return d_cycletime4;
    }

    public void setD_cycletime4(int d_cycletime4) {
        this.d_cycletime4 = d_cycletime4;
    }

    public float getD_price1() {
        return d_price1;
    }

    public void setD_price1(float d_price1) {
        this.d_price1 = d_price1;
    }

    public float getD_price2() {
        return d_price2;
    }

    public void setD_price2(float d_price2) {
        this.d_price2 = d_price2;
    }

    public float getD_price3() {
        return d_price3;
    }

    public void setD_price3(float d_price3) {
        this.d_price3 = d_price3;
    }

    public float getD_price4() {
        return d_price4;
    }

    public void setD_price4(float d_price4) {
        this.d_price4 = d_price4;
    }

    public float getN_price1() {
        return n_price1;
    }

    public void setN_price1(float n_price1) {
        this.n_price1 = n_price1;
    }

    public float getN_price2() {
        return n_price2;
    }

    public void setN_price2(float n_price2) {
        this.n_price2 = n_price2;
    }

    public float getN_price3() {
        return n_price3;
    }

    public void setN_price3(float n_price3) {
        this.n_price3 = n_price3;
    }

    public float getN_price4() {
        return n_price4;
    }

    public void setN_price4(float n_price4) {
        this.n_price4 = n_price4;
    }

    public long getN_begin() {
        return n_begin;
    }

    public void setN_begin(long n_begin) {
        this.n_begin = n_begin;
    }

    public long getN_end() {
        return n_end;
    }

    public void setN_end(long n_end) {
        this.n_end = n_end;
    }

    public float getN_limit() {
        return n_limit;
    }

    public void setN_limit(float n_limit) {
        this.n_limit = n_limit;
    }

    public float getN_once() {
        return n_once;
    }

    public void setN_once(float n_once) {
        this.n_once = n_once;
    }

    public float getN_preamt() {
        return n_preamt;
    }

    public void setN_preamt(float n_preamt) {
        this.n_preamt = n_preamt;
    }

    public int getN_type() {
        return n_type;
    }

    public void setN_type(int n_type) {
        this.n_type = n_type;
    }

    public int getN_unittime1() {
        return n_unittime1;
    }

    public void setN_unittime1(int n_unittime1) {
        this.n_unittime1 = n_unittime1;
    }

    public int getN_unittime2() {
        return n_unittime2;
    }

    public void setN_unittime2(int n_unittime2) {
        this.n_unittime2 = n_unittime2;
    }

    public int getN_unittime3() {
        return n_unittime3;
    }

    public void setN_unittime3(int n_unittime3) {
        this.n_unittime3 = n_unittime3;
    }

    public int getN_unittime4() {
        return n_unittime4;
    }

    public void setN_unittime4(int n_unittime4) {
        this.n_unittime4 = n_unittime4;
    }

    public int getN_cycletime1() {
        return n_cycletime1;
    }

    public void setN_cycletime1(int n_cycletime1) {
        this.n_cycletime1 = n_cycletime1;
    }

    public int getN_cycletime2() {
        return n_cycletime2;
    }

    public void setN_cycletime2(int n_cycletime2) {
        this.n_cycletime2 = n_cycletime2;
    }

    public int getN_cycletime3() {
        return n_cycletime3;
    }

    public void setN_cycletime3(int n_cycletime3) {
        this.n_cycletime3 = n_cycletime3;
    }

    public int getN_cycletime4() {
        return n_cycletime4;
    }

    public void setN_cycletime4(int n_cycletime4) {
        this.n_cycletime4 = n_cycletime4;
    }

    public int getWorktype() {
        return worktype;
    }

    public void setWorktype(int worktype) {
        this.worktype = worktype;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPark_code() {
        return park_code;
    }

    public void setPark_code(String park_code) {
        this.park_code = park_code;
    }

    public String getLicensekey() {
        return licensekey;
    }

    public void setLicensekey(String licensekey) {
        this.licensekey = licensekey;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public int getCarplatecolor() {
        return carplatecolor;
    }

    public void setCarplatecolor(int carplatecolor) {
        this.carplatecolor = carplatecolor;
    }


    public int getNewCarType() {
        return newCarType;
    }

    public void setNewCarType(int newCarType) {
        this.newCarType = newCarType;
    }

    @Override
    public String toString() {
        return "CloudParkBilling{" +
                "id=" + id +
                ", index_id=" + index_id +
                ", cartype=" + cartype +
                ", freetime1=" + freetime1 +
                ", month=" + month +
                ", t_limit=" + t_limit +
                ", freetime=" + freetime +
                ", d_type=" + d_type +
                ", d_preamt=" + d_preamt +
                ", d_once=" + d_once +
                ", d_limit=" + d_limit +
                ", d_begin=" + d_begin +
                ", d_end=" + d_end +
                ", d_unittime1=" + d_unittime1 +
                ", d_unittime2=" + d_unittime2 +
                ", d_unittime3=" + d_unittime3 +
                ", d_unittime4=" + d_unittime4 +
                ", d_cycletime1=" + d_cycletime1 +
                ", d_cycletime2=" + d_cycletime2 +
                ", d_cycletime3=" + d_cycletime3 +
                ", d_cycletime4=" + d_cycletime4 +
                ", d_price1=" + d_price1 +
                ", d_price2=" + d_price2 +
                ", d_price3=" + d_price3 +
                ", d_price4=" + d_price4 +
                ", n_price1=" + n_price1 +
                ", n_price2=" + n_price2 +
                ", n_price3=" + n_price3 +
                ", n_price4=" + n_price4 +
                ", n_begin=" + n_begin +
                ", n_end=" + n_end +
                ", n_limit=" + n_limit +
                ", n_once=" + n_once +
                ", n_preamt=" + n_preamt +
                ", n_type=" + n_type +
                ", n_unittime1=" + n_unittime1 +
                ", n_unittime2=" + n_unittime2 +
                ", n_unittime3=" + n_unittime3 +
                ", n_unittime4=" + n_unittime4 +
                ", n_cycletime1=" + n_cycletime1 +
                ", n_cycletime2=" + n_cycletime2 +
                ", n_cycletime3=" + n_cycletime3 +
                ", n_cycletime4=" + n_cycletime4 +
                ", worktype=" + worktype +
                ", count=" + count +
                ", park_code='" + park_code + '\'' +
                ", licensekey='" + licensekey + '\'' +
                ", user_type=" + user_type +
                ", carplatecolor=" + carplatecolor +
                ", newCarType=" + newCarType +
                '}';
    }
}
