package cn.tnar.parkservice.model.common;

public class RespResult {
	
	private String MSG_CODE;
	
	private String MSG_TEXT;

	public String getMSG_CODE() {
		return MSG_CODE;
	}

	public void setMSG_CODE(String mSG_CODE) {
		MSG_CODE = mSG_CODE;
	}

	public String getMSG_TEXT() {
		return MSG_TEXT;
	}

	public void setMSG_TEXT(String mSG_TEXT) {
		MSG_TEXT = mSG_TEXT;
	}


}
