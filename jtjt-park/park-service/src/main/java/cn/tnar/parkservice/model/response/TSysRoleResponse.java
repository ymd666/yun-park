package cn.tnar.parkservice.model.response;

import lombok.Data;

@Data
public class TSysRoleResponse {

    private int id;
    private long roleId;
    private String roleCode;
    private String roleName;
    private int roleLevel;
    private String custName;
    private long roleOwner;

}
