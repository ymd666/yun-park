package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class ResultDataDto {

    private int key_version;
    private int output_format;
    private String output_data;
    private String data_fingerprint;
    private String meta_data;
}
