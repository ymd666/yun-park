package cn.tnar.yunpark.puxd;

import java.util.Date;

/**
 * Created by tieh on 2016/11/18.
 */
public class PuxdReqData {
    private String CMCID;
    private String PMDID;
    private String parkingDate;
    /**
     * 车位状态 0 - 无车, 1 - 有车
     */
    private int parkingState = -1;
    private Date reqDate;

    public String getCMCID() {
        return CMCID;
    }

    public void setCMCID(String CMCID) {
        this.CMCID = CMCID;
    }

    public String getPMDID() {
        return PMDID;
    }

    public void setPMDID(String PMDID) {
        this.PMDID = PMDID;
    }

    public String getParkingDate() {
        return parkingDate;
    }

    public void setParkingDate(String parkingDate) {
        parkingDate = parkingDate;
    }

    public int getParkingState() {
        return parkingState;
    }

    public void setParkingState(int parkingState) {
        parkingState = parkingState;
    }

    public Date getReqDate() {
        return reqDate;
    }

    public void setReqDate(Date reqDate) {
        reqDate = reqDate;
    }
}
