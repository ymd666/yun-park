package cn.tnar.yunpark.careyes.tcp.model;

import cn.tnar.yunpark.careyes.CarEyesUtil;
import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * package: cn.tnar.yunpark.careyes.tcp
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/9 17:23
 * desc   : Status
 */

public class Status{

    private int type;
    private int len;
    private int routerId;
    private int dataLen;
    private int province;
    private int city;
    private int area;
    private int street;
    private int parkId;
    private int sensorId;
    private int status;
    private int vbattery;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minutes;
    private int second;
    private int sequence;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public int getRouterId() {
        return routerId;
    }

    public void setRouterId(int routerId) {
        this.routerId = routerId;
    }

    public int getDataLen() {
        return dataLen;
    }

    public void setDataLen(int dataLen) {
        this.dataLen = dataLen;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getProvince() {
        return province;
    }

    public void setProvince(int province) {
        this.province = province;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getStreet() {
        return street;
    }

    public void setStreet(int street) {
        this.street = street;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getVbattery() {
        return vbattery;
    }

    public void setVbattery(int vbattery) {
        this.vbattery = vbattery;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "Status{" +
                "type=" + type +
                ", len=" + len +
                ", routerId=" + routerId +
                ", dataLen=" + dataLen +
                ", province=" + province +
                ", city=" + city +
                ", area=" + area +
                ", street=" + street +
                ", parkId=" + parkId +
                ", sensorId=" + sensorId +
                ", status=" + status +
                ", vbattery=" + vbattery +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minutes=" + minutes +
                ", second=" + second +
                ", sequence=" + sequence +
                '}';
    }

    public static Status parse(ByteBuf byteBuf) {
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        Status st = new Status();
        buf.skipBytes(2);
        st.setType(buf.readByte());
        buf.skipBytes(1);
        buf.skipBytes(1);
        st.setRouterId(buf.readByte());
        buf.skipBytes(2);
        st.setDataLen(buf.readByte());
        buf.skipBytes(1);
        st.setProvince(buf.readByte());
        st.setCity(buf.readByte());
        st.setArea(buf.readByte());
        st.setStreet(buf.readByte());
        st.setParkId(buf.readByte());
        buf.readByte();//routerId
        st.setSensorId(buf.readByte());
        st.setStatus(buf.readByte() & 0xFF);
        st.setVbattery(buf.readByte() & 0xFF);
        st.setYear(CarEyesUtil.bcd2Int(buf.readByte()));
        st.setMonth(CarEyesUtil.bcd2Int(buf.readByte()));
        st.setDay(CarEyesUtil.bcd2Int(buf.readByte()));
        st.setHour(CarEyesUtil.bcd2Int(buf.readByte()));
        st.setMinutes(CarEyesUtil.bcd2Int(buf.readByte()));
        st.setSecond(CarEyesUtil.bcd2Int(buf.readByte()));
        buf.skipBytes(2);
        st.setSequence(buf.readByte());
        return st;
    }
}
