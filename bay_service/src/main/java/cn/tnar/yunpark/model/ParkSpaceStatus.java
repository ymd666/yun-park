package cn.tnar.yunpark.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 车位状态，发送给PDA的
 * Created by tieh on 2017/6/14.
 */
public class ParkSpaceStatus {
    @JsonProperty("park_code")
    String parkCode;

    @JsonProperty("region_code")
    String regionCode;

    @JsonProperty("seat_code")
    String spaceCode;

    @JsonProperty("seat_no")
    String spaceShortCode;

    /**
     * 车位状态("1"-无车，"2"-有车，"99"-异常)
     */
    @JsonProperty("bayflag")
    String status;

    @JsonProperty("time")
    String time;

    public ParkSpaceStatus() {
    }

    public ParkSpaceStatus(ParkSpace space, SpaceStatus status) {
        parkCode = space.getParkCode();
        regionCode = space.getRegionCode();
        spaceCode = space.getSpaceCode();
        spaceShortCode = space.getSpaceShortCode();

        this.status = status.getStatus();
        time = status.getTime();
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getSpaceCode() {
        return spaceCode;
    }

    public void setSpaceCode(String spaceCode) {
        this.spaceCode = spaceCode;
    }

    public String getSpaceShortCode() {
        return spaceShortCode;
    }

    public void setSpaceShortCode(String spaceShortCode) {
        this.spaceShortCode = spaceShortCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SpaceStatus{" +
                "park='" + parkCode + '\'' +
                ", region='" + regionCode + '\'' +
                ", space='" + spaceShortCode + '\'' +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
