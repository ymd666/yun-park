package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkRegionMapper;
import cn.tnar.parkservice.model.entity.TParkRegion;
import cn.tnar.parkservice.model.response.RegionInfo;
import cn.tnar.parkservice.service.ITParkRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zzb
 * @since 2018-12-08
 */
@Service("tParkRegionServiceImpl")
public class TParkRegionServiceImpl extends ServiceImpl<TParkRegionMapper, TParkRegion> implements ITParkRegionService {

    @Autowired
    private TParkRegionMapper regionMapper;
    @Override
    public List<RegionInfo> queryCardAndRegion(List<String> regions) {
        List<RegionInfo> regionInfos = regionMapper.queryCardAndRegion(regions);
        return regionInfos;
    }

    @Override
    public List<String> getName(String regionCodes) {
        return baseMapper.getName(regionCodes);
    }
}
