package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkRecordGroupRelationMapper;
import cn.tnar.parkservice.model.entity.TParkRecordGroupRelation;
import cn.tnar.parkservice.service.ITParkRecordGroupRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月10日 14:13:18
 */
@Service("tParkRecordGroupRelationServiceImpl")
public class TParkRecordGroupRelationServiceImpl extends ServiceImpl<TParkRecordGroupRelationMapper, TParkRecordGroupRelation> implements ITParkRecordGroupRelationService {


    @Autowired
    private TParkRecordGroupRelationMapper tParkRecordGroupRelationMapper;

    @Override
    public List<TParkRecordGroupRelation> queryNeedTransRelations() {
        return tParkRecordGroupRelationMapper.queryNeedTransRelations();
    }
}
