package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkValueRecord;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月09日 19:39:07
 */
public interface ITParkValueRecordService extends IService<TParkValueRecord> {

    ResultJson getRecordByGroupId(String json);
}
