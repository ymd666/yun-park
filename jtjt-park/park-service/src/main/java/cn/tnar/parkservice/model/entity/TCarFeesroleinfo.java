package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_car_feesroleinfo")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class TCarFeesroleinfo extends Model<TCarFeesroleinfo> {

    @TableId("id")
    private Long id;

    @TableField("node_id")
    private Integer nodeId;
    /**
     * 数据字典：1-小型车，2-中型车，3-大型车，4-电动车，5-摩托车
     */
    @TableField("car_type")
    private Integer carType;

    @TableField("cust_id")
    private Long custId;
    /**
     * 如：金证内部月卡收费标准
     */
    @TableField("role_name")
    private String roleName;
    /**
     * 备注：记录月卡车每月交费标准，如 100元/月；200元/月等
     */
    @TableField("month_value")
    private BigDecimal monthValue;

    @TableField("remark")
    private String remark;

    @TableField("delflag")
    private Integer delflag;

    @TableField("region_code")
    private String regionCode;

    @TableField("card_type")
    private Integer cardType;

    private Integer giveday;

    private String startTime;  //HHmmss

    private String endTime;    //HHmmss

}
