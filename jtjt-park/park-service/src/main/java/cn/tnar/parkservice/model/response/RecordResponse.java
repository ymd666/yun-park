package cn.tnar.parkservice.model.response;

import lombok.Data;

@Data
public class RecordResponse {

    private String name;
    private String operName;
    private String operInfo;
    private String amount;
   // private String endDate;
    private String newStartDate;
    private String newEndDate;
    private String operDate;
    private String carId;
    private String carType;


}
