package cn.tnar.parkservice.model.dto;

/**
 * @author Bao
 * @date 2018-10-18
 */
public class BadDebtDetailDto {

    private String carId;

    private String carnocolor;

    private String regionName;

    private Long intime;

    private Long outtime;

    private Long baddebtTime;

    private String tollName;

    private String remark;

    private String parkamt;

    private String sumamt;

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getCarnocolor() {
        return carnocolor;
    }

    public void setCarnocolor(String carnocolor) {
        this.carnocolor = carnocolor;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Long getBaddebtTime() {
        return baddebtTime;
    }

    public void setBaddebtTime(Long baddebtTime) {
        this.baddebtTime = baddebtTime;
    }

    public String getTollName() {
        return tollName;
    }

    public void setTollName(String tollName) {
        this.tollName = tollName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getIntime() {
        return intime;
    }

    public void setIntime(Long intime) {
        this.intime = intime;
    }

    public Long getOuttime() {
        return outtime;
    }

    public void setOuttime(Long outtime) {
        this.outtime = outtime;
    }

    public String getParkamt() {
        return parkamt;
    }

    public void setParkamt(String parkamt) {
        this.parkamt = parkamt;
    }

    public String getSumamt() {
        return sumamt;
    }

    public void setSumamt(String sumamt) {
        this.sumamt = sumamt;
    }

    @Override
    public String toString() {
        return "BadDebtDetailDto{" +
                "carId='" + carId + '\'' +
                ", carnocolor='" + carnocolor + '\'' +
                ", regionName='" + regionName + '\'' +
                ", intime=" + intime +
                ", outtime=" + outtime +
                ", baddebtTime=" + baddebtTime +
                ", tollName='" + tollName + '\'' +
                ", remark='" + remark + '\'' +
                ", parkamt='" + parkamt + '\'' +
                ", sumamt='" + sumamt + '\'' +
                '}';
    }
}
