package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TParkMemberGroupRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月02日 15:08:39
 */
public interface ITParkMemberGroupRelationService extends IService<TParkMemberGroupRelation> {


    /**
     * 子卡下的会员id(t_park_group_memberinfo)包含白名单待审核
     * */

    List<String> getMemberIdByChildLook(Long groupId);

    /**
     * 子卡下的会员id(t_park_group_memberinfo)除待审核
     * */

    List<String> getMemberIdByChild(Long groupId);


    /**
     * 卡组下的会员id(t_park_group_memberinfo)
     * */
    List<String> getMemberIdByParent( Long groupId);
    /**
     * 子卡下的会员id(t_park_visit_history)
     * */

    List<String> getVisitIdByChild(Long groupId);

    /**
     * 卡组下的会员id(t_park_visit_history)
     * */
    List<String> getVisitIdByParent( Long groupId);

    /**
     * 根据卡组id及电话核查会员并查询绑定车位数
     * */

    List<Map<String,String>> checkTel(Long groupId,String telNo);

    /**
     * 根据车牌号获取当前会员最新办理的卡证的id
     *
     * @param carNo
     * @return
     */
    Long getGroupIdByCarNo(String carNo);
}
