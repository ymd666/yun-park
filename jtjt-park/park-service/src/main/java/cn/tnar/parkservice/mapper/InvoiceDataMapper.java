package cn.tnar.parkservice.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by 01 on 2017/4/20.
 */
@Repository
@Mapper
public interface InvoiceDataMapper {

    //获取发票列表
    List<Map<String, Object>> getInvoiceList(@Param("start_date") Long start_date,
                                             @Param("end_date") Long end_date, @Param("invoice_status") Integer invoice_status,
                                             @Param("invoice_type") int invoice_type, @Param("buyer_type") int buyer_type, @Param("park_code") String park_code, @Param("size") int size, @Param("page") int page);

    int getInvoiceCount(@Param("start_date") Long start_date,
                        @Param("end_date") Long end_date, @Param("invoice_status") Integer invoice_status,
                        @Param("invoice_type") int invoice_type, @Param("buyer_type") int buyer_type);

    List<Map<String,Object>> getRelationV2(@Param("apply_sno") String apply_sno,@Param("no") String no);

    List<Map<String,Object>> getParkingInfo(@Param("serialno") String serialno,@Param("order_number") String order_number);

    List<Map<String,Object>> getInvoiceAllInfo(@Param("no") String no);

    //冲红
    void updateRedStateV2(@Param("pdf_red") String pdf_red,@Param("order_id") String order_id);
}
