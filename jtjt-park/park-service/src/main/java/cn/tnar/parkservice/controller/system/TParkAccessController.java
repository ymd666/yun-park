package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/22
 * @Description:
 */
@Api(tags = "停车场出入口(通道)管理（张鑫）")
@RestController
@RequestMapping("service/parkAccess")
public class TParkAccessController {

    private Logger log = LoggerFactory.getLogger(TParkAccessController.class);

    @Autowired
    @Qualifier("kesbAPIService")
    private KesbApiService kesbApiService;

    @ApiOperation("新增停车场出入口")
    @PostMapping("/add")
    public ResultJson addParkAceess(@RequestBody Map<String, Object> param){
        try {
            log.info("新增停车场出入口：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_ACCESS_ADD, param);
            log.info("新增停车场出入口：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("新增停车场出入口:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("删除停车场出入口信息")
    @PostMapping("/delete")
    public ResultJson deleteParkAceess(@RequestBody Map<String, Object> param){
        try {
            log.info("删除停车场出入口信息：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_ACCESS_DELETE, param);
            log.info("删除停车场出入口信息：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("删除停车场出入口信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场出入口信息")
    @PostMapping("/update")
    public ResultJson updateParkAceess(@RequestBody Map<String, Object> param){
        try {
            log.info("修改停车场出入口信息：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_ACCESS_UPDATE, param);
            log.info("修改停车场出入口信息：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("修改停车场出入口信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场出入口信息")
    @PostMapping("/query")
    public ResultJson queryParkAceess(@RequestBody Map<String, Object> param){
        try {
            log.info("查询停车场出入口信息：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_ACCESS_QUERY, param);
            log.info("查询停车场出入口信息：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("查询停车场出入口信息:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }
}
