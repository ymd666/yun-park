package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkVisitHistory;
import cn.tnar.parkservice.model.request.TParkVisistApproveRequest;
import cn.tnar.parkservice.model.request.TParkVisitHistoryRequest;
import cn.tnar.parkservice.model.response.TParkMemberCardResponse;
import cn.tnar.parkservice.model.response.TParkVisitHistoryResponse;
import cn.tnar.parkservice.model.response.VisistRecordResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author clarck
 * @since 2018-10-05
 */
@Repository
@Mapper
public interface TParkVisitHistoryMapper extends BaseMapper<TParkVisitHistory> {

    List<VisistRecordResponse> queryVisiterReocrd(Page<VisistRecordResponse> page, @Param("param") TParkVisitHistoryRequest request);

    List<TParkVisitHistory> visitHistory(@Param("applyStatus")int applyStatus, @Param("custId") String custId);

    List<TParkMemberCardResponse> getVisitCard(@Param("parkCode") String parkCode);

    Integer addRelation(@Param("param") TParkVisistApproveRequest request);

    boolean checkIn(@Param("history")TParkVisitHistory history);

    boolean saveMemRelation(@Param("id")int id,@Param("groupId")String groupId,@Param("time")String time);

    boolean updateMemRelation(@Param("id")int id,@Param("groupId")String groupId,@Param("time")String time);

    boolean cancel(@Param("id")int id, @Param("applyStatus")int applyStatus, @Param("operId")String operId);

    List<Map<String,Object>> getRegionInfo(@Param("regionCodes") String[] regionCodes);

    boolean saveOrUpdateApply(@Param("history")TParkVisitHistory history);

    void queryBindCard();

    List<VisistRecordResponse> querySimpleRecord(Page<VisistRecordResponse> page, @Param("param") Map<String,Object> param);
}
