package cn.tnar.parkservice.model.entity;

import cn.tnar.parkservice.config.ToStringSerializer;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.poi.ss.formula.functions.T;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月04日 19:43:12
 */
@Data
@TableName("t_park_calendar")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class TParkCalendar extends Model<TParkCalendar> {
    @TableId("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @TableField("node_id")
    private Integer nodeId;

    @TableField("holiday")
    private Integer holiday;

    @TableField("region_code")
    private String regionCode;

    @TableField("agent_id")
    private Long agentId;

    @TableField("park_code")
    private String parkCode;

    @TableField(value = "holiay_name", exist = false)
    private String holidayName;

}
