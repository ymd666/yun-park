package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;


/**
 * <p>
 * <p>
 * </p>
 *
 * @author clarck
 * @since 2018-10-05
 */
@Data
@TableName("t_park_visit_history")
public class TParkVisitHistory {

    private int id;

    private String parkName;

    private String parkCode;

    private String visitor;

    private String mobile;

    private String carId;

    private Integer carType;

    private String applyName;

    private String applyMobile;

    private long visitTimeStart;

    private long visitTimeEnd;

    private Integer visitState;

    private String custId;

    private long entranceTime;

    private String remarks;

    private long applyTime;

    private long applyId;

    private int applyStatus;

    private long accessTime;

    private String operId;

    private long customerVisitTime;

    private String reason;

    @TableField(exist = false)
    private String groupId;//卡组ID
}
