package cn.tnar.parkservice.service;

import cn.tnar.parkservice.util.common.ResultJson;

import java.util.List;
import java.util.Map;

public interface TParkDashboardService {

    ResultJson count(Map<String,Object> map);

    /**
     * 当日临停车，访客车，长租车 车辆统计
     */
    List<Map<String,Object>> dayCarCount(Map<String,Object> map);

    /**
     * 首页--最近一周卡类型进场统计
     */
    Map<String,Object> weekCarCount(String startDate,String endDate,String parkCode,boolean isToday);

    /**
     * 当天进出场卡组统计分析
     */
    List<Map<String,Object>> groupCarCount(Map<String,Object> param);

    /**
     * 当天进出场自定义卡组分析
     */
    List<Map<String,Object>> diyMemCarCount(Map<String,Object> param);
}
