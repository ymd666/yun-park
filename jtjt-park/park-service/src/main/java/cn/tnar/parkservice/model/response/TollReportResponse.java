package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: xinZhang
 * @Date: 2019/9/26
 * @Description:
 */
@Data
public class TollReportResponse {

    /**
     * 批次号
     */
    private String batchno;

    /**
     *收费员工号
     */
    private String tollNo;

    /**
     *收费员姓名
     */
    private String tollName;

    /**
     *上班时间
     */
    private Long onDuty;

    /**
     *下班时间
     */
    private Long offDuty;

    /**
     *工作时长
     */
    private String workTime;

    /**
     *收费总额
     */
    private BigDecimal chargeAmount;

    /**
     *优惠券金额
     */
    private BigDecimal couponAmount;

    /**
     *长租车金额
     */
    private BigDecimal memCardAmount;

    /**
     *合计
     */
    private BigDecimal total;
}
