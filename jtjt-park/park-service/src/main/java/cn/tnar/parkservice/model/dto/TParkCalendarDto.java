package cn.tnar.parkservice.model.dto;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月06日 11:14:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class TParkCalendarDto {

    private Integer nodeId; //默认1

    private String holidays; //多个日期逗号隔开

    private Long agentId;

    private String parkCode;

    private String holidayName; //节假日名称
}
