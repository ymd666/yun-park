package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.model.request.MemCarFeeScaleRequest;
import cn.tnar.parkservice.service.MemCarFeeScaleService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/23
 * @Description:
 */
@Api(tags = "套餐收费策略（张鑫）")
@RestController
@RequestMapping("service/feeScale")
public class MemCarFeeScaleController {

    @Autowired
    private MemCarFeeScaleService memCarFeeScaleService;

    @ApiOperation("新增月卡车收费标准记录")
    @PostMapping("/add")
    public ResultJson addFeeScale(@RequestBody MemCarFeeScaleRequest request){
        return memCarFeeScaleService.save(request);
    }

    @ApiOperation("修改月卡车收费标准记录")
    @PostMapping("/update")
    public ResultJson updateFeeScale(@RequestBody MemCarFeeScaleRequest request){
        return memCarFeeScaleService.update(request);
    }

    @ApiOperation("查询月卡车收费标准记录")
    @PostMapping("/query")
    public ResultJson queryFeeScale(@RequestBody Map<String, Object> param){
        return memCarFeeScaleService.query(param);
    }
}
