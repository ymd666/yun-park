package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * 退款信息返回类
 */
@Data
public class TradeRefundResponse {

    private long id;

    private Long custId;

    private String orderNumber;

    private String parkCode;

    private String name;//停车场

    private String carNo;

    private long sendtime;//入场时间

    private long recvtime;//出场时间

    private double tradeAmount;//交易总额

    private double refundAmount;//退款总额

    private String refundreason;//退款原因

    private double fee;//手续费

    private long operId;//发起人id

    private String operName;//发起人

    private String remark;//备注

    private String description;//审核失败原因

    private double prePay;//预付款

    private int parkStatus;//停车状态

    private long firstCheckId;

    private long secCheckId;

    private long thirdCheckId;

    private String firstCheckName;

    private String secCheckName;

    private String thirdCheckName;

    private long firstCheckTime;

    private long secCheckTime;

    private long thirdCheckTime;

}
