package cn.tnar.parkservice.model.entity;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zzb
 * @since 2018-12-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TDictionary对象", description="")
public class TDictionary implements Serializable {

    private static final long serialVersionUID = 1L;

    private String category;

    private String categoryEn;

    private Integer ival;

    private String cval;

    private String item;

    private String itemEn;

    private Integer mdlId;

    private Integer isMeta;

    private Integer auditFlag;

    private Long modifyOper;

    private Long modifyTime;


}
