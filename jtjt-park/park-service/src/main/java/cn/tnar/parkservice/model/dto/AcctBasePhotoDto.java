package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class AcctBasePhotoDto {

    private int id;

    private int fileType;

    private int objectId;

    private  String url;



}
