package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.model.dto.TParkWorkLog;
import cn.tnar.parkservice.model.request.ParkShiftRequset;
import cn.tnar.parkservice.model.request.ParkWorkLogRequset;
import cn.tnar.parkservice.model.response.ParkShiftResponse;
import cn.tnar.parkservice.model.response.TParkWorkLogResponse;
import cn.tnar.parkservice.mapper.TParkWorkLogMapper;
import cn.tnar.parkservice.service.ITParkWorkLogService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@Service("tParkWorkLogServiceImpl")
public class TParkWorkLogServiceImpl extends ServiceImpl<TParkWorkLogMapper, TParkWorkLog> implements ITParkWorkLogService {

    @Autowired
    private TParkWorkLogMapper parkWorkLogMapper;

    @Override
    public Page<TParkWorkLogResponse> query(ParkWorkLogRequset req) {
        Page<TParkWorkLogResponse> page = new Page<>(req.getCurrent(), req.getSize());
        List<TParkWorkLogResponse> list = parkWorkLogMapper.query(page, req);


        page.setRecords(list);
        return page;
    }

    @Override
    public Page<TParkWorkLogResponse> queryNew(ParkWorkLogRequset req) {
        Page<TParkWorkLogResponse> page = new Page<>(req.getCurrent(), req.getSize());
        List<TParkWorkLogResponse> list = parkWorkLogMapper.queryNew(page, req);
        page.setRecords(list);
        return page;
    }

    @Override
    public Page<TParkWorkLogResponse> queryZZ(ParkWorkLogRequset req) {
        Page<TParkWorkLogResponse> page = new Page<>(req.getCurrent(), req.getSize());

        List<TParkWorkLogResponse> list = parkWorkLogMapper.queryZZ(req);

        List<TParkWorkLogResponse> list2 = new ArrayList<>();

        //未上班
        if (req.getIsvalid() == 2) {
            List<TParkWorkLogResponse> list3 = parkWorkLogMapper.queryTollNo(req.getParkCode(), req.getName());
            list2 = parkWorkLogMapper.queryTollNo(req.getParkCode(), req.getName());

            for (int j = list3.size()-1; j >=0; j--) {

                for (int i = 0; i < list.size(); i++) {
                    if (list3.get(j).getTollNo().equals(list.get(i).getTollNo())) {
                        list2.remove(j);
                        break;
                    }
                }

            }

        }


        for (int i = 0; i < list.size(); i++) {
            //允许迟到时间
            String lateTime = list.get(i).getLateTime();
            //上班打卡时间
            String onDuty = list.get(i).getOnDuty();
            String onDutyMS = onDuty.substring(8, 14);
            //格式10700
            String beginTime = list.get(i).getBeginTime().substring(1,7);
            //代班人ID
            String insteadNo = list.get(i).getInsteadNo();

            //正常上班
            if (req.getIsvalid() == 1) {

                if (Integer.parseInt(onDutyMS) <= (Integer.parseInt(beginTime) + Integer.parseInt(lateTime))) {
                    list2.add(list.get(i));

                }


            }
            //迟到
            if (req.getIsvalid() == 3) {
                if (Integer.parseInt(onDutyMS) > (Integer.parseInt(beginTime) + Integer.parseInt(lateTime))) {
                    list2.add(list.get(i));
                }

            }

           /* //代班 （并未迟到）-----北京无需求
            if(req.getIsvalid()==4){
                if(insteadNo!=""){
                    list2.add(list.get(i));
                }
            }*/


        }




        //当前页
        int pageNo = req.getCurrent();
        //每页显示条数
        int count = req.getSize();
        //总条数
        int size = list2.size();

        int pageCount=size/count;
        int fromIndex = count * (pageNo - 1);
        int toIndex = fromIndex + count;
        if (toIndex >= size) {
            toIndex = size;
        }
        if(pageNo>pageCount+1){
            fromIndex=0;
            toIndex=0;
        }
        //设置总条数，默认得到插件的pages
        page.setTotal(size);
        //分页  subList截取
        page.setRecords(list2.subList(fromIndex,toIndex));
        return page;
    }

    @Override
    public Page<ParkShiftResponse> getShiftDetail(ParkShiftRequset req) {
        Page<ParkShiftResponse> page = new Page<>(req.getCurrent(), req.getSize());
        List<ParkShiftResponse> list = parkWorkLogMapper.getShiftDetail(page, req);
        page.setRecords(list);
        return page;
    }

//    @Override
//    public Page<TParkWorkLogResponse> queryParkWorkLog(Page<TParkWorkLogResponse> page, ParkWorkLogRequset parkWorkLogRequset) {
//        if (parkWorkLogRequset.getStarTime() > 0) {
//            parkWorkLogRequset.setStarTime(Long.valueOf(parkWorkLogRequset.getStarTime() + "000000"));
//        }
//        if (parkWorkLogRequset.getEndTime() > 0) {
//            parkWorkLogRequset.setEndTime(Long.valueOf(parkWorkLogRequset.getEndTime() + "235959"));
//        }
//        List<TParkWorkLogResponse> tParkWorkLogResponseList = parkWorkLogMapper.queryParkWorkLog(page, parkWorkLogRequset);
//        page.setRecords(tParkWorkLogResponseList);
//        return page;
//    }




}
