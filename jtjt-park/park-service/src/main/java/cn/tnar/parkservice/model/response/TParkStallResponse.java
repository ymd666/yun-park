package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/9/23
 * @Description:
 */
@Data
public class TParkStallResponse {

    /**
     * id
     */
    private Long id;

    /**
     *车位编号
     */
    private String seatCode;

    /**
     *泊位号
     */
    private String seatNo;

    /**
     *区域名称
     */
    private String regionName;

    /**
     *预约标识
     * 1: 支持预约
     * 2: 不可预约
     */
    private Integer appointFlag;

    /**
     *占闲标识
     * 1: 空闲
     * 2: 占用
     * 21:正常占用(有购买)
     * 22:异常占用(已取证)
     */
    private Integer idle;

    /**
     * 占用车牌号
     */
    private String idleCarId;

    /**
     *最近占用时间
     */
    private Long occuTime;

    /**
     *经度
     */
    private String lng;

    /**
     *纬度
     */
    private String lat;


}
