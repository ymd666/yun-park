package cn.tnar.yunpark.mapper;

import cn.tnar.yunpark.model.ParkSpace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by tieh on 2017/6/15.
 */
@Repository
@Mapper
public interface ParkSpaceMapper {
    ParkSpace getParkSpaceBySensorId(@Param("sensorId") String id);
}
