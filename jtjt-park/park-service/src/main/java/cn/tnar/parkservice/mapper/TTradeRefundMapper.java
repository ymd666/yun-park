package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.dto.TTradeRefundDto;
import cn.tnar.parkservice.model.entity.TTradeRefund;
import cn.tnar.parkservice.model.request.TradeRefundRequset;
import cn.tnar.parkservice.model.response.TradeRefundResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 退款列表 Mapper 接口
 *
 * @author Tiyer.Tao
 * @since 2018-09-20
 */
@Mapper
@Repository
public interface TTradeRefundMapper extends BaseMapper<TTradeRefund> {

    List<TradeRefundResponse> getRefundByPage(Page<TradeRefundResponse> page, @Param("request") TradeRefundRequset request);

    @Update("<script> " +
            "UPDATE t_trade_refund SET refundstate = #{dto.refundstate} ,  description =#{dto.description}  " +
            " <if test='dto.firstCheckId>0'> , first_check_id = #{dto.firstCheckId} </if>" +
            " <if test='dto.secCheckId>0'> , sec_check_id = #{dto.secCheckId} </if>" +
            " <if test='dto.thirdCheckId>0'> , third_check_id = #{dto.thirdCheckId} </if>" +
            " <if test='dto.firstCheckTime>0'> , first_check_time = #{dto.firstCheckTime} </if>" +
            " <if test='dto.secCheckTime>0'> , sec_check_time = #{dto.secCheckTime} </if>" +
            " <if test='dto.thirdCheckId>0'> , third_check_time = #{dto.thirdCheckId} </if>" +
            " WHERE id = #{dto.id}" +
            "</script> ")
    int updateAuditRefund(@Param("dto") TTradeRefundDto dto);

    /**
     * 退款信息查询(导出List)
     * @param parkCode  停车场编码
     * @param carNo 车编号
     * @param orderNumber 订单编号
     * @param refundState 退款状态
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    List<TradeRefundResponse> getRefundList(String parkCode, String carNo, String orderNumber, Integer refundState, Long startTime, Long endTime);
}
