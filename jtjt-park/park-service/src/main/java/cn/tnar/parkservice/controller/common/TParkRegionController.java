package cn.tnar.parkservice.controller.common;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/22
 * @Description:
 */
@Api(tags = "停车场区域管理（张鑫）")
@RestController
@RequestMapping("service/parkRegion")
public class TParkRegionController {

    private Logger log = LoggerFactory.getLogger(TParkRegionController.class);

    @Autowired
    @Qualifier("kesbAPIService")
    private KesbApiService kesbApiService;

    private ResultJson json = new ResultJson();

    @ApiOperation("新增停车场区域")
    @PostMapping("/add")
    public ResultJson addParkRegion(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_REGION_ADD, param);
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG);
        } catch (Exception e) {
            log.error("新增停车场区域失败===>", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("删除停车场区域信息")
    @PostMapping("/delete")
    public ResultJson deleteParkRegion(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_REGION_DELETE, param);
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (Exception e) {
            log.error("删除停车场区域失败===>", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场区域信息")
    @PostMapping("/update")
    public ResultJson updateParkRegion(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_REGION_UPDATE, param);
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (Exception e) {
            log.error("修改停车场区域失败===>", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场区域信息")
    @PostMapping("/query")
    public ResultJson queryParkRegion(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_REGION_QUERY, param);
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (Exception e) {
            log.error("查询停车场区域失败===>", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场区域信息及其收费索引信息")
    @PostMapping("/queryRfo")
    public ResultJson queryParkRegionFeeInfo(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_REGION_FEE_INDEX, param);
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (Exception e) {
            log.error("查询停车场区域信息及其收费索引信息失败===>", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


    @ApiOperation("设置区域类型")
    @PostMapping("/setType")
    public ResultJson setParkRegionType(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_SET_REGION_TYPE, param);
            return json.setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (Exception e) {
            log.error("设置区域类型失败===>", e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }
}
