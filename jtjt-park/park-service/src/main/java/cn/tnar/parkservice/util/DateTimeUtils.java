package cn.tnar.parkservice.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Bao
 * @date 2018-10-22
 */
public class DateTimeUtils {

    private static final DateTimeFormatter LBM_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    public static long toLBM(LocalDateTime dateTime) {
        return Long.valueOf(LBM_FORMATTER.format(dateTime));
    }
}
