package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_trade_refund")
public class TTradeRefund extends Model<TTradeRefund> {

    private static final long serialVersionUID = 1L;

    /**
     * 列表ID
     */
    @ApiParam(value = "列表ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("node_id")
    private int nodeId=0;
//    @TableField("object_id")
//    private Long objectId;
    /**
     * 退款用户ID
     */
    @ApiParam(value = "退款用户ID")
    @TableField("cust_id")
    private Long custId;
    @TableField("order_number")
    private String orderNumber;
    /**
     * 退款金额
     */
    @ApiParam(value = "退款金额")
    private BigDecimal refundamount;
    /**
     * 退款发起时间
     */
    @ApiParam(value = "退款发起时间")
    private Long refundtime;
    /**
     * 退款原因
     */
    @ApiParam(value = "退款原因")
    private String refundreason;
    /**
     * 数据字典：1 初始 2 处理中 3 成功 4 失败 5 取消
     */
    @ApiParam(value = "数据字典：1 初始 2 处理中 3 成功 4 失败 5 取消")
    private Integer refundstate;
    /**
     * 退款完成时间
     */
    @ApiParam(value = "退款完成时间")
    private Long endtime;
    /**
     * 退款操作人
     */
    @ApiParam(value = "退款操作人")
    @TableField("oper_id")
    private Long operId;
    /**
     * 失败原因描述
     */
    @ApiParam(value = "失败原因描述")
    private String description;
    /**
     * 备注
     */
    @ApiParam(value = "备注")
    private String remark;
    /**
     * 审核人1
     */
    @ApiParam(value = "审核人1")
    @TableField("first_check_id")
    private Long firstCheckId;
    /**
     * 审核时间
     */
    @ApiParam(value = "审核时间")
    @TableField("first_check_time")
    private Long firstCheckTime;
    /**
     * 第二次审核人id
     */
    @ApiParam(value = "第二次审核人id")
    @TableField("sec_check_id")
    private Long secCheckId;
    /**
     * 第二次校验时间
     */
    @ApiParam(value = "第二次校验时间")
    @TableField("sec_check_time")
    private Long secCheckTime;
    /**
     * 第三个审核人ID
     */
    @ApiParam(value = "第三个审核人ID")
    @TableField("third_check_id")
    private Long thirdCheckId;
    /**
     * 第三次审核时间
     */
    @ApiParam(value = "第三次审核时间")
    @TableField("third_check_time")
    private Long thirdCheckTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

//    public Long getObjectId() {
//        return objectId;
//    }
//
//    public void setObjectId(Long objectId) {
//        this.objectId = objectId;
//    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getRefundamount() {
        return refundamount;
    }

    public void setRefundamount(BigDecimal refundamount) {
        this.refundamount = refundamount;
    }

    public Long getRefundtime() {
        return refundtime;
    }

    public void setRefundtime(Long refundtime) {
        this.refundtime = refundtime;
    }

    public String getRefundreason() {
        return refundreason;
    }

    public void setRefundreason(String refundreason) {
        this.refundreason = refundreason;
    }

    public Integer getRefundstate() {
        return refundstate;
    }

    public void setRefundstate(Integer refundstate) {
        this.refundstate = refundstate;
    }

    public Long getEndtime() {
        return endtime;
    }

    public void setEndtime(Long endtime) {
        this.endtime = endtime;
    }

    public Long getOperId() {
        return operId;
    }

    public void setOperId(Long operId) {
        this.operId = operId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getFirstCheckId() {
        return firstCheckId;
    }

    public void setFirstCheckId(Long firstCheckId) {
        this.firstCheckId = firstCheckId;
    }

    public Long getFirstCheckTime() {
        return firstCheckTime;
    }

    public void setFirstCheckTime(Long firstCheckTime) {
        this.firstCheckTime = firstCheckTime;
    }

    public Long getSecCheckId() {
        return secCheckId;
    }

    public void setSecCheckId(Long secCheckId) {
        this.secCheckId = secCheckId;
    }

    public Long getSecCheckTime() {
        return secCheckTime;
    }

    public void setSecCheckTime(Long secCheckTime) {
        this.secCheckTime = secCheckTime;
    }

    public Long getThirdCheckId() {
        return thirdCheckId;
    }

    public void setThirdCheckId(Long thirdCheckId) {
        this.thirdCheckId = thirdCheckId;
    }

    public Long getThirdCheckTime() {
        return thirdCheckTime;
    }

    public void setThirdCheckTime(Long thirdCheckTime) {
        this.thirdCheckTime = thirdCheckTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TTradeRefund{" +
        ", id=" + id +
        ", nodeId=" + nodeId +
//        ", objectId=" + objectId +
        ", custId=" + custId +
        ", orderNumber=" + orderNumber +
        ", refundamount=" + refundamount +
        ", refundtime=" + refundtime +
        ", refundreason=" + refundreason +
        ", refundstate=" + refundstate +
        ", endtime=" + endtime +
        ", operId=" + operId +
        ", description=" + description +
        ", remark=" + remark +
        ", firstCheckId=" + firstCheckId +
        ", firstCheckTime=" + firstCheckTime +
        ", secCheckId=" + secCheckId +
        ", secCheckTime=" + secCheckTime +
        ", thirdCheckId=" + thirdCheckId +
        ", thirdCheckTime=" + thirdCheckTime +
        "}";
    }
}
