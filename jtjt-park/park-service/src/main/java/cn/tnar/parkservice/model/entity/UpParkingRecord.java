package cn.tnar.parkservice.model.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zzb
 * @since 2019-03-06
 */
@Data
public class UpParkingRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String parkCode;
    private String serialno;
    private String parkName;
    private String carPlateNo;
    private String carPlateColor;
    private String intime;
    private String outtime;
    private String inphoto;
    private String outphoto;
    private String inGateNo;
    private String outGateNo;

    @Override
    public String toString() {
        return "UpParkingRecord{" +
                "id=" + id +
                ", parkCode='" + parkCode + '\'' +
                ", serialno='" + serialno + '\'' +
                ", parkName='" + parkName + '\'' +
                ", carPlateNo='" + carPlateNo + '\'' +
                ", carPlateColor='" + carPlateColor + '\'' +
                ", intime='" + intime + '\'' +
                ", outtime='" + outtime + '\'' +
                ", inphoto='" + inphoto + '\'' +
                ", outphoto='" + outphoto + '\'' +
                ", inGateNo='" + inGateNo + '\'' +
                ", outGateNo='" + outGateNo + '\'' +
                '}';
    }
}
