package cn.tnar.yunpark.puxd;

/**
 * Created by tieh on 2016/11/18.
 */
public class PuxdException extends Throwable {
    public PuxdException(String msg) {
        super(msg);
    }
}
