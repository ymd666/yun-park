package cn.tnar.parkservice.controller.customer;

import cn.tnar.parkservice.model.dto.MemPaymentRecordDto;
import cn.tnar.parkservice.service.MemPaymentRecordService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;


/**
 * @Author: xinZhang
 * @Date: 2019/8/27
 * @Description:
 */
@Api(tags = "客户管理-交费查询")
@RestController
@RequestMapping("service/payment")
public class TParkPaymentController {

    @Autowired
    @Qualifier("memPaymentRecordServiceImpl")
    private MemPaymentRecordService memPaymentRecordService;

    @ApiOperation("交费记录,统计查询")
    @PostMapping("/query")
    public ResultJson queryPayment(@RequestBody MemPaymentRecordDto memPaymentRecordDto){
        return memPaymentRecordService.baseRecords(memPaymentRecordDto);
    }

    @ApiOperation("缴费记录详情查询")
    @GetMapping("/detail")
    public ResultJson detail(@RequestParam Long id){
        return memPaymentRecordService.detailRecords(id);
    }
}
