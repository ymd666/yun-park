package cn.tnar.parkservice.util;

import org.springframework.beans.BeanUtils;


public class DtoUtil {
    public static <T> T convertObject(Object sourceObj, Class<T> targetClz) {
        if (sourceObj == null) {
            return null;
        }

        if (targetClz == null) {
            throw new IllegalArgumentException("parameter clz shoud not be null");
        }
        try {
            Object targetObj = targetClz.newInstance();
            BeanUtils.copyProperties(sourceObj, targetObj);
            return (T) targetObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
