package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkGroupMemberinfo;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
public interface ITParkGroupMemberinfoService extends IService<TParkGroupMemberinfo> {

    ResultJson insert(String json);

    ResultJson check(String json);

    ResultJson select(String json);

    ResultJson selectOne(String json);

    ResultJson stop(String json);

    ResultJson defer(String json);

    ResultJson delete(String json);

    ResultJson start(String json);

    ResultJson update(String json);

    ResultJson queryAll(String json);

    ResultJson queryOtherAll(String json);

    ResultJson insertMember(String json);

    ResultJson updateMember(String json);

    ResultJson deleteMemberSeat(String json);

    ResultJson queryMemberCarId(String json);

    ResultJson checkMemberCarId(String json);

    ResultJson deleteMemberCarId(String json);

    ResultJson insertMemberCarId(String json);

    ResultJson updateMemberCarId(String json);

    ResultJson queryMemberSeat(String json);

    ResultJson insertMemberSeat(String json);

    ResultJson stopMemberSeat(String json);

    ResultJson startMemberSeat(String json);

   // ResultJson deferMemberSeat(String json);

    ResultJson deferConfirmMemberSeat(String json);

    ResultJson changeMemberGroup(String json);

    ResultJson weChatSelect(String json);

    ResultJson weChatSelectInfo(String json);

    ResultJson queryCardInfos(String json);

    ResultJson deferMember(String json);

    ResultJson getMoney(String json);

    ResultJson callbackMember(String json);

    ResultJson memberPrepareOrder(String json);

    ResultJson getMemberGroup(String json);

    ResultJson queryMemberRecord(String json);

    ResultJson syncMemberInfo(String json);

    ResultJson selectBankInfo(String json);

    ResultJson updateMemAccAmt(Map<String, String> param);
}
