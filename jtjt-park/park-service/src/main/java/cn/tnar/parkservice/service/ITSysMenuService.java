package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.dto.MenuDto;
import cn.tnar.parkservice.model.entity.TSysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface ITSysMenuService extends IService<TSysMenu> {

    List<MenuDto> selectMenu(String CustId);

    TSysMenu selectYunRoot();

    List<TSysMenu> selectSubByParentId(Long id);


}
