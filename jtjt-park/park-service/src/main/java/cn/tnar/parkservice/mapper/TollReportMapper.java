package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.response.TollReportResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TollReportMapper {

    List<TollReportResponse> getTollReport(Page page, @Param("param") Map<String,Object> param);
}
