package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkDashboardMapper;
import cn.tnar.parkservice.mapper.TParkMemberGroupMapper;
import cn.tnar.parkservice.model.entity.TParkMemberGroup;
import cn.tnar.parkservice.service.TParkDashboardService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/23
 * @Description:
 */
@Service
public class TParkDashboardServiceImpl implements TParkDashboardService {

    @Autowired
    private TParkDashboardMapper tParkDashboardMapper;

    @Autowired
    private TParkMemberGroupMapper tParkMemberGroupMapper;
    /**
     * 首页统计。包含现金，电子收入；区域在场，占用车位数，总车位数
     * 异常抬杆；临停，月租，访客车；
     */
    @Override
    public ResultJson count(Map<String,Object> map) {
        //现金统计
        Map<String, Object> cashCount = tParkDashboardMapper.cashCount(map);
        //电子支付
        Map<String, Object> ePayCount = tParkDashboardMapper.ePayCount(map);
        //停车场车位统计
        Map<String, Object> parkSeatCount = tParkDashboardMapper.parkSeatCount(map);
        //区域车位统计
        List<Map<String, Object>> regionSeatCount = tParkDashboardMapper.regionSeatCount(map);
        //区域占用车位统计
        List<Map<String, Object>> regionBusySeatCount = tParkDashboardMapper.regionBusySeatCount(map);
        //异常抬杆统计
        Map<String, Object> unNormalCar = tParkDashboardMapper.unNormalCar(map);
//        List<Map<String,Object>> carCount = tParkDashboardMapper.carCount(map);
        //临停车，访客车，长租从统计
        List<Map<String, Object>> carCount = this.dayCarCount(map);
        HashMap<String, Object> response = new HashMap<>();
        //将区域占用车位数统计 合并到区域车位数统计中
        if (CollectionUtils.isNotEmpty(regionSeatCount)){
            for (Map<String, Object> responseMap : regionSeatCount) {
                for (Map<String, Object> stringObjectMap : regionBusySeatCount) {
                    if (responseMap.get("region_code").equals(stringObjectMap.get("region_code"))){
                        responseMap.put("busy_count",stringObjectMap.get("busy_count"));
                    }else {
                        responseMap.put("busy_count","0");
                    }
                }
            }
        }
        //通过区域占用车位数计算总占用车位数
        Long busy_count = null;
        if (CollectionUtils.isNotEmpty(regionBusySeatCount)){
            busy_count  = regionBusySeatCount.stream().mapToLong(p -> Long.valueOf(p.get("busy_count").toString())).sum();
        }
        //现金统计
        response.put("cashCount",cashCount);
        //电子支付统计
        response.put("ePayCount",ePayCount);
        //区域车位数统计
        response.put("regionSeatCount",regionSeatCount);
        //停车场总车位数统计 占用车位总数统计
        parkSeatCount.put("busy_count",busy_count);
        response.put("parkSeatCount",parkSeatCount);
        //异常抬杆统计
        response.put("unNormalCar",unNormalCar);
        //临停车，月租车，访客车统计
        response.put("carCount",carCount);

        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(response);
    }

    /**
     * 当日临停车，访客车，长租车 车辆统计
     */
    @Override
    public List<Map<String,Object>> dayCarCount(Map<String,Object> map) {
        String startDate = map.get("startTime").toString();
        String endDate = map.get("endTime").toString();
        String parkCode = map.get("parkCode").toString();
        //临停车统计
        Map<String, Object> tempCar = tParkDashboardMapper.dayTempCar(startDate,endDate,parkCode);
        tempCar.put("carType",1);
        //访客车统计
        Map<String, Object> visiterCar = tParkDashboardMapper.dayVisiterCar(startDate,endDate,parkCode);
        visiterCar.put("carType",2);
        //自定义卡组统计
        Map<String, Object> memCar = tParkDashboardMapper.dayMemCar(startDate,endDate,parkCode);
        memCar.put("carType",3);

        List<Map<String,Object>> list = new ArrayList<>();
        list.add(tempCar);
        list.add(visiterCar);
        list.add(memCar);
        return list;
    }


    /**
     * 首页-- 临停，访客，月租车统计-最近一周统计
     */
    @Override
    public Map<String,Object> weekCarCount(String startDate,String endDate,String parkCode,boolean isTody) {
        Map<String, Object> tempCar;
        Map<String, Object> visiterCar;
        Map<String, Object> memCar;
        if (isTody){
            //当日统计
            tempCar = tParkDashboardMapper.dayTempCar(startDate, endDate, parkCode);
            visiterCar = tParkDashboardMapper.dayVisiterCar(startDate, endDate, parkCode);
            memCar = tParkDashboardMapper.dayMemCar(startDate, endDate, parkCode);
        }else {
            //历史统计
            tempCar = tParkDashboardMapper.hisTempCar(startDate, endDate, parkCode);
            visiterCar = tParkDashboardMapper.hisVisiterCar(startDate, endDate, parkCode);
            memCar = tParkDashboardMapper.hisMemCar(startDate, endDate, parkCode);
        }
        tempCar.put("carType",1);
        visiterCar.put("carType",2);
        memCar.put("carType",3);

        List<Map<String,Object>> list = new ArrayList<>();
        list.add(tempCar);
        list.add(visiterCar);
        list.add(memCar);
        Map<String,Object> resp = new HashMap<>();
        resp.put("count",list);
        return resp;
    }

    /**
     * 当天进出场卡组分类统计分析
     */
    @Override
    public List<Map<String, Object>> groupCarCount(Map<String, Object> param) {
        String startDate = param.get("startTime").toString();
        String endDate = param.get("endTime").toString();
        String parkCode = param.get("parkCode").toString();
        //访客车统计
        Map<String, Object> tempCar = tParkDashboardMapper.dayTempCar(startDate,endDate,parkCode);
        tempCar.put("classify",1);
        //访客，黑白名称，自定义卡组统计
        List<Map<String, Object>> maps = tParkDashboardMapper.groupCarCount(param);
        List<Map<String, Object>> list = new ArrayList<>();
        list.add(tempCar);
        Map<String,Object> map = null;
        //当一条数据也没有时
        if (CollectionUtils.isEmpty(maps)){
            for (int i = 2; i < 6; i++) {
                map = new HashMap<>();
                map.put("classify",i);
                map.put("amount",0);
                list.add(map);
            }
            return list;
        }else {
            //有数据，但是可能某个卡组车没有数据
            //integers1:统计数据中classify的类型 interger2：classify的总共类型
            ArrayList<Integer> integers1 = new ArrayList<>();
            maps.forEach(p->integers1.add(Integer.parseInt(p.get("classify").toString())));
            ArrayList<Integer> integers2 = new ArrayList<>();
            integers2.add(2);
            integers2.add(3);
            integers2.add(4);
            integers2.add(5);
            integers2.removeAll(integers1);

            for (Integer integer : integers2) {
                map = new HashMap<>();
                map.put("classify",integer);
                map.put("amount",0);
                maps.add(map);
            }
            maps.add(tempCar);
            return maps;
        }
    }

    /**
     * 当天进出场自定义卡组分析
     */
    @Override
    public List<Map<String, Object>> diyMemCarCount(Map<String, Object> param) {
        String parkCode = param.get("parkCode").toString();
        String startTime = param.get("startTime").toString();
        String endTime = param.get("endTime").toString();
        //1.查询所有自定义卡组
        QueryWrapper<TParkMemberGroup> qw = new QueryWrapper<>();
        qw.eq("classify", 5);
        qw.eq("type", 0);
        qw.eq("del_flag", 0);
        qw.eq("park_code", parkCode);
        List<TParkMemberGroup> list = tParkMemberGroupMapper.selectList(qw);

        //2.查询各卡组的车辆统计
        List<Map<String, Object>> resp = new ArrayList<>();
        for (TParkMemberGroup tParkMemberGroup : list) {
            Long id = tParkMemberGroup.getId();
            Map<String, Object> map = tParkDashboardMapper.diyMemCarCount(parkCode, id,startTime,endTime);
            map.put("groupName", tParkMemberGroup.getName());
            map.put("groupId", id);
            resp.add(map);
        }
        return resp;
    }

}
