package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.dto.TParkWorkLog;
import cn.tnar.parkservice.model.request.ParkShiftRequset;
import cn.tnar.parkservice.model.request.ParkWorkLogRequset;
import cn.tnar.parkservice.model.response.ParkShiftResponse;
import cn.tnar.parkservice.model.response.TParkWorkLogResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface ITParkWorkLogService extends IService<TParkWorkLog> {

    Page<TParkWorkLogResponse> query(ParkWorkLogRequset req);

    Page<TParkWorkLogResponse> queryNew(ParkWorkLogRequset req);

    Page<TParkWorkLogResponse> queryZZ(ParkWorkLogRequset req);

    Page<ParkShiftResponse> getShiftDetail(ParkShiftRequset req);

//    Page<TParkWorkLogResponse> queryParkWorkLog(Page<TParkWorkLogResponse> page, ParkWorkLogRequset parkWorkLogRequset);
}
