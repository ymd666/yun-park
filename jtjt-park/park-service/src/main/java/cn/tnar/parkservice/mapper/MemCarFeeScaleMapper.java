package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.MemCarFeeScale;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MemCarFeeScaleMapper extends BaseMapper<MemCarFeeScale> {
}
