package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkFee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:53:33
 */
@Mapper
@Repository
public interface TParkFeeMapper extends BaseMapper<TParkFee> {

}
