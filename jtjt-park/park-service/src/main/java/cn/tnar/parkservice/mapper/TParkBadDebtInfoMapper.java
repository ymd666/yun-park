package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.BadDebtDetailDto;
import cn.tnar.parkservice.model.entity.TParkBadDebtInfo;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

/**
 * @author Bao
 * @date 2018-10-18
 */
@QuerySupport
public interface TParkBadDebtInfoMapper extends BaseMapper<TParkBadDebtInfo> {

    @QuerySupport
    @Select("SELECT bd.car_id as carId, bd.carnocolor, r.`name` AS regionName, bd.intime, bd.outtime, bd.baddebt_time AS baddebtTime, bd.parkamt, t.`name` AS tollName, bd.remark\n" +
            "        from t_park_bad_debt_info bd\n" +
            "                 LEFT JOIN t_park_region r ON bd.region_code=r.region_code\n" +
            "                 LEFT JOIN t_park_toll t ON bd.chargemanno=t.no")
    Page<BadDebtDetailDto> pageBadDebtInfoDetail(Page<BadDebtDetailDto> page, @Param(Constants.WRAPPER) Wrapper<BadDebtDetailDto> wrapper);

    @QuerySupport
    @Select("SELECT SUM(parkamt) FROM t_park_bad_debt_info bd")
    BigDecimal sumBadDebtInfoAmount(@Param(Constants.WRAPPER) Wrapper<BigDecimal> wrapper);
}
