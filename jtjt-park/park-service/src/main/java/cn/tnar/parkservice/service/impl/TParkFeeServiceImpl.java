package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.mapper.TParkFeeMapper;
import cn.tnar.parkservice.model.entity.TParkFee;
import cn.tnar.parkservice.service.ITParkFeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:39:07
 */
@Service("tParkFeeServiceImpl")
public class TParkFeeServiceImpl extends ServiceImpl<TParkFeeMapper, TParkFee> implements ITParkFeeService {

}
