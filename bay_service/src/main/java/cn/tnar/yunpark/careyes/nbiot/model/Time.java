package cn.tnar.yunpark.careyes.nbiot.model;

import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * package: cn.tnar.yunpark.careyes.nbiot.model
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/12 17:03
 * desc   : Time
 */
public class Time extends Message{

    private int deviceType;
    private int len;
    private String sensorId;
    private byte[] sensorIdBytes = new byte[6];
    private int city;
    private int area;
    private String parkId;
    private byte[] parkIdBytes = new byte[4];
    private int sequence;

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public byte[] getSensorIdBytes() {
        return sensorIdBytes;
    }

    public void setSensorIdBytes(byte[] sensorIdBytes) {
        this.sensorIdBytes = sensorIdBytes;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }

    public byte[] getParkIdBytes() {
        return parkIdBytes;
    }

    public void setParkIdBytes(byte[] parkIdBytes) {
        this.parkIdBytes = parkIdBytes;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        return "Time{" +
                "deviceType=" + deviceType +
                ", len=" + len +
                ", sensorId='" + sensorId + '\'' +
                ", city=" + city +
                ", area=" + area +
                ", parkId='" + parkId + '\'' +
                ", sequence=" + sequence +
                '}';
    }

    public static Time parse(ByteBuf byteBuf){
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        Time t = new Time();
        buf.skipBytes(2);
        buf.skipBytes(1);
        buf.skipBytes(1);
        t.setDeviceType(buf.readByte());
        byte[] sidBytes = new byte[6];
        buf.readBytes(sidBytes);
        t.setSensorIdBytes(sidBytes);
        buf.skipBytes(1);
        t.setCity(buf.readByte());
        t.setArea(buf.readByte());
        byte[] pidBytes = new byte[4];
        buf.readBytes(pidBytes);
        t.setParkIdBytes(pidBytes);
        buf.skipBytes(1);
        buf.skipBytes(1);
        t.setSequence(buf.readByte());
        return t;
    }
}
