package cn.tnar.parkservice.model.dto;

public class TParkEscapeDto {

    private String name;
    private String carId;
    private String parkname;
    private String regionname;
    private String intime;
    private String outtime;
    private String parktime;
    private String parkamt;
    private String chargeamt;
    private String paystate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getParkname() {
        return parkname;
    }

    public void setParkname(String parkname) {
        this.parkname = parkname;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }

    public String getParktime() {
        return parktime;
    }

    public void setParktime(String parktime) {
        this.parktime = parktime;
    }

    public String getParkamt() {
        return parkamt;
    }

    public void setParkamt(String parkamt) {
        this.parkamt = parkamt;
    }

    public String getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(String chargeamt) {
        this.chargeamt = chargeamt;
    }

    public String getPaystate() {
        return paystate;
    }

    public void setPaystate(String paystate) {
        this.paystate = paystate;
    }

    public String getRegionname() {
        return regionname;
    }

    public void setRegionname(String regionname) {
        this.regionname = regionname;
    }
}
