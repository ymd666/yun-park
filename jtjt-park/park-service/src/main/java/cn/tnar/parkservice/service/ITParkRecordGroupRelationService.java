package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkRecordGroupRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月10日 14:12:39
 */
public interface ITParkRecordGroupRelationService extends IService<TParkRecordGroupRelation> {
    List<TParkRecordGroupRelation> queryNeedTransRelations();
}
