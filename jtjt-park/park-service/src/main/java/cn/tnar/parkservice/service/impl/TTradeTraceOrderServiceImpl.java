package cn.tnar.parkservice.service.impl;


import cn.tnar.parkservice.mapper.TTradeTransOrderMapper;
import cn.tnar.parkservice.model.entity.TTradeTransOrder;
import cn.tnar.parkservice.service.ITTradeTraceOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gl
 * @since 2018-11-28
 */
@Service
public class TTradeTraceOrderServiceImpl extends ServiceImpl<TTradeTransOrderMapper, TTradeTransOrder> implements ITTradeTraceOrderService {

}
