package cn.tnar.parkservice.model.dto;

public class CloudParkFeeIndex {
    /*
     * 停车场收费索引id
     * */
    private long id;

    /*
     * 计费标准索引名称
     * */
    private String name;

    /*
     * 运营商id
     * */
    private String agent_id;

    /*
     * 现金折扣
     * */
    private int cashoff;

    /*
     * 刷卡折扣
     * */
    private int cardoff;

    /*
     * 会员折扣
     * */
    private int memberoff;

    /*
     * 出场宽限时间（分钟）
     * */
    private int gracetime;

    /*
     * 更新时间
     * */
    private long utime;

    /*
     * 更新人
     * */
    private String userid;

    /*
     * 数据状态
     * */
    private int delflag;

    /*
     * 24小时计算方式
     * */
    private int cycle;

    /*
     * 停车场区域编号
     */
    private String regionCodes;

    public String getRegionCodes() {
        return regionCodes;
    }

    public void setRegionCodes(String regionCodes) {
        this.regionCodes = regionCodes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(String agent_id) {
        this.agent_id = agent_id;
    }

    public int getCashoff() {
        return cashoff;
    }

    public void setCashoff(int cashoff) {
        this.cashoff = cashoff;
    }

    public int getCardoff() {
        return cardoff;
    }

    public void setCardoff(int cardoff) {
        this.cardoff = cardoff;
    }

    public int getMemberoff() {
        return memberoff;
    }

    public void setMemberoff(int memberoff) {
        this.memberoff = memberoff;
    }

    public int getGracetime() {
        return gracetime;
    }

    public void setGracetime(int gracetime) {
        this.gracetime = gracetime;
    }

    public long getUtime() {
        return utime;
    }

    public void setUtime(long utime) {
        this.utime = utime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getDelflag() {
        return delflag;
    }

    public void setDelflag(int delflag) {
        this.delflag = delflag;
    }

    public int getCycle() {
        return cycle;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }

    @Override
    public String toString() {
        return "CloudParkFeeIndex{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", agent_id='" + agent_id + '\'' +
                ", cashoff=" + cashoff +
                ", cardoff=" + cardoff +
                ", memberoff=" + memberoff +
                ", gracetime=" + gracetime +
                ", utime=" + utime +
                ", userid='" + userid + '\'' +
                ", delflag=" + delflag +
                ", cycle=" + cycle +
                '}';
    }
}
