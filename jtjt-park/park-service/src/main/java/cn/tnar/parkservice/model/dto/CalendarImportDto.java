package cn.tnar.parkservice.model.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月17日 17:04:19
 */
@Data
@ToString
public class CalendarImportDto {

    private int year;
}
