package cn.tnar.parkservice.controller.customer;
import cn.tnar.parkservice.service.ITParkGroupMemberinfoService;
import cn.tnar.parkservice.util.common.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zzb
 * @since 2019-09-20
 */
@Api(tags = "公众号-月卡(张志彪)")
@RestController
@RequestMapping("monthCard/weChatMemberinfo")
public class WeChatMemberinfoController {
    @Autowired
    private ITParkGroupMemberinfoService itParkGroupMemberinfoService;

    @ApiOperation(value = "根据手机号查询月卡缴费界面")
    @PostMapping(value = "select")
    public ResultJson weChatSelect(@RequestBody String json) {
        return itParkGroupMemberinfoService.weChatSelect(json);
    }
    @ApiOperation(value = "月卡缴费-详情")
    @PostMapping(value = "weChatSelectInfo")
    public ResultJson weChatSelectInfo(@RequestBody String json) {
        return itParkGroupMemberinfoService.weChatSelectInfo(json);
    }
    @ApiOperation(value = "月卡缴费-批量详情")
    @PostMapping(value = "queryCardInfos")
    public ResultJson queryCardInfos(@RequestBody String json) {
        return itParkGroupMemberinfoService.queryCardInfos(json);
    }
    @ApiOperation(value = "月卡缴费-延期")
    @PostMapping(value = "deferMember")
    public ResultJson deferMember(@RequestBody String json) {
        return itParkGroupMemberinfoService.deferMember(json);
    }
    @ApiOperation(value = "月卡缴费-显示金额")
    @PostMapping(value = "getMoney")
    public ResultJson getMoney(@RequestBody String json) {
        return itParkGroupMemberinfoService.getMoney(json);
    }

    @ApiOperation(value = "月卡缴费-预下单（待完成）")
    @PostMapping(value = "memberPrepareOrder")
    public ResultJson memberPrepareOrder(@RequestBody String json) {
        return itParkGroupMemberinfoService.memberPrepareOrder(json);
    }
    @ApiOperation(value = "月卡缴费-支付回调（待完成）")
    @PostMapping(value = "callbackMember")
    public ResultJson callbackMember(@RequestBody String json) {
        return itParkGroupMemberinfoService.callbackMember(json);
    }
    @ApiOperation(value = "查询柜台信息")
    @PostMapping(value = "selectBankInfo")
    public ResultJson selectBankInfo(@RequestBody String json) {
        return itParkGroupMemberinfoService.selectBankInfo(json);
    }
}
