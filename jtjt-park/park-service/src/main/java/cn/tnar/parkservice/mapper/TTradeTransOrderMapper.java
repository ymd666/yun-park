package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TTradeTransOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 转账记录 Mapper 接口
 *
 * @author Tiyer.Tao
 * @since 2019-09-26
 */
@Mapper
@Repository
public interface TTradeTransOrderMapper extends BaseMapper<TTradeTransOrder> {

}
