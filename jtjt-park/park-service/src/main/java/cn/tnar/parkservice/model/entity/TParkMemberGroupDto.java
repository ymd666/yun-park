package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年07月08日 14:39:58
 */
@TableName("t_park_member_group")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class TParkMemberGroupDto extends Model<TParkMemberGroupDto> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("no")
    private Long no;   //卡组编号
    @TableField("name")
    private String name;  //卡组名称
    @TableField("parent_id")
    private Long parentId; //父id
    @TableField("park_code")
    private String parkCode; //停车场编码
    @TableField("region_code")
    private String regionCode;//区域编码
    @TableField("fee_index_id")
    private Long feeIndexId;  //计费索引id
    @TableField("feesroleinfo_id")
    private Long feesroleinfoId;   //套餐策略id
    @TableField("access_rule_id")
    private Long accessRuleId;  //准入规则id
    @TableField("del_flag")
    private Integer delFlag;   //删除标识
    @TableField("create_time")
    private Long createTime;  //创建时间
    @TableField("update_time")
    private Long updateTime;  //更新时间
    @TableField("type")
    private Integer type; //分组类型 0 目录 1卡证
    @TableField("classify")
    private Integer classify; //类别
    @Transient
    private List<TParkMemberGroupDto> childs = new ArrayList<TParkMemberGroupDto>();
    @TableField("month_value")
    private BigDecimal monthValue; //套餐标准
    @TableField("role_name")
    private String roleName; //套餐名称
    @TableField("card_type")
    private Integer cardType; //套餐类型

    @TableField("is_fullFree")
    private Integer isFullFree; //1 开启 0 关闭
    @TableField("fullFree_amt")
    private BigDecimal fullFreeAmt;
    @TableField("total_fullFree_amt")
    private BigDecimal totalFullFreeAmt;

}
