package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_sys_menu")
public class TSysMenu {

    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField("audit_flag")
    private Integer auditFlag;
    @TableField("biz_type")
    private Integer bizType;
    @TableField("func_code")
    private String funcCode;
    private String icon;
    @TableField("menu_flag")
    private Integer menuFlag;
    @TableField("menu_id")
    private Long menuId;
    @TableField("menu_name")
    private String menuName;
    @TableField("menu_name_en")
    private String menuNameEn;
    @TableField("modify_oper")
    private Long modifyOper;
    @TableField("modify_time")
    private Long modifyTime;
    @TableField("mtd_code")
    private String mtdCode;
    @TableField("obj_code")
    private String objCode;
    @TableField("parent_menu_id")
    private Long parentMenuId;
    @TableField("serial_no")
    private Long serialNo;
    private String url;
    private String depends;
    @TableField("object_code")
    private String objectCode;
    @TableField("platform_type")
    private Integer platformType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAuditFlag() {
        return auditFlag;
    }

    public void setAuditFlag(Integer auditFlag) {
        this.auditFlag = auditFlag;
    }

    public Integer getBizType() {
        return bizType;
    }

    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getMenuFlag() {
        return menuFlag;
    }

    public void setMenuFlag(Integer menuFlag) {
        this.menuFlag = menuFlag;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuNameEn() {
        return menuNameEn;
    }

    public void setMenuNameEn(String menuNameEn) {
        this.menuNameEn = menuNameEn;
    }

    public Long getModifyOper() {
        return modifyOper;
    }

    public void setModifyOper(Long modifyOper) {
        this.modifyOper = modifyOper;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getMtdCode() {
        return mtdCode;
    }

    public void setMtdCode(String mtdCode) {
        this.mtdCode = mtdCode;
    }

    public String getObjCode() {
        return objCode;
    }

    public void setObjCode(String objCode) {
        this.objCode = objCode;
    }

    public Long getParentMenuId() {
        return parentMenuId;
    }

    public void setParentMenuId(Long parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    public Long getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Long serialNo) {
        this.serialNo = serialNo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDepends() {
        return depends;
    }

    public void setDepends(String depends) {
        this.depends = depends;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public Integer getPlatformType() {
        return platformType;
    }

    public void setPlatformType(Integer platformType) {
        this.platformType = platformType;
    }

    @Override
    public String toString() {
        return "TSysMenu{" +
        ", id=" + id +
        ", auditFlag=" + auditFlag +
        ", bizType=" + bizType +
        ", funcCode=" + funcCode +
        ", icon=" + icon +
        ", menuFlag=" + menuFlag +
        ", menuId=" + menuId +
        ", menuName=" + menuName +
        ", menuNameEn=" + menuNameEn +
        ", modifyOper=" + modifyOper +
        ", modifyTime=" + modifyTime +
        ", mtdCode=" + mtdCode +
        ", objCode=" + objCode +
        ", parentMenuId=" + parentMenuId +
        ", serialNo=" + serialNo +
        ", url=" + url +
        ", depends=" + depends +
        ", objectCode=" + objectCode +
        "}";
    }
}
