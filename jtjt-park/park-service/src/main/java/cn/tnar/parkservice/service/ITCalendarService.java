package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TCalendar;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zzb
 * @since 2019-03-11
 */
public interface ITCalendarService extends IService<TCalendar> {
    ResultJson query(MultipartFile file);

    void downCal(HttpServletResponse response, int year);

    ResultJson excelInCal( MultipartFile file);

    ResultJson queryCal( String json);
}
