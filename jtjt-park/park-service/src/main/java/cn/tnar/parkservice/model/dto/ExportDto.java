package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class ExportDto {
    private String json;

    private String service;

    //文件名 ".xls"
    private String filename;
    private int type;

}
