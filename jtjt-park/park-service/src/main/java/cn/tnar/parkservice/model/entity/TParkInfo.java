package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zzb
 * @since 2018-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "TParkInfo对象", description = "")
public class TParkInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Integer nodeId;

    private Long custId;

    private String parkCode;

    private String name;

    private String regionCode;

    private String businessCode;

    @ApiModelProperty(value = "每层多少个，每个片区多少个车位")
    private String capacityDesc;

    private Integer capacityNum;

    private Integer freeNum;

    private Integer memberNum;

    private Integer tempNum;

    private Integer appointNum;

    private Integer chargeNum;

    private Long lng;

    private Long lat;

    private Long feeindex;

    private BigDecimal feeamt;

    private String feedesc;

    private Integer freetime;

    @ApiModelProperty(value = "数据字典：收费评分 1-便宜 2-适中 3-偏贵")
    private Integer feelevel;

    private Long userId;

    @ApiModelProperty(value = " yyyyMMddHHmmss")
    private Long utime;

    @ApiModelProperty(value = "数据字典：1为上班，2为下班")
    private Integer logon;

    @ApiModelProperty(value = "数据字典：1路内 2路外")
    private Integer ptype;

    @ApiModelProperty(value = "数据字典：路内：(20以内)  01收费02党政机关03单位个人04免费 05分时免费 路外：(20以上)  01商圈02景区03宾馆04酒店05政府06小区07园区 08写字楼")
    private Integer subtype;

    @ApiModelProperty(value = " 数据字典：1已接入 2未接入")
    private Integer status;

    private Long addtime;

    private Long jointime;

    private String address;

    private String remark;

    @ApiModelProperty(value = "对服务或环境类评分")
    private Integer score;

    @ApiModelProperty(value = " 数据字典：1通过 2待审核")
    private Integer audit;

    private Long collector;

    private Long collectTime;

    @ApiModelProperty(value = "数据字典：1正常 2已删除")
    private Integer delflag;

    @ApiModelProperty(value = "如：9,9表示最大可以有81个车位，横竖都有9个车位")
    private String seatarea;

    @ApiModelProperty(value = "数据字典: 1支持预约  2不可预约")
    private Integer appointflag;

    private Integer islockfix;

    private Integer isonline;

    private Long heartime;

    private String photokey;

    private Long maxvermembertable;

    private Integer membercarflag;

    private Integer maxMemberNum;

    private String contactPerson;

    private Long isMemberSync;

    private Long isCustMemberSync;

    private String thirdpartyParkcode;

    private String thirdpartyCompanies;

    @TableField("appKey")
    private String appKey;

    private String rc4key;

    private Integer firstbuyflag;

    private Long uploadtime;

    private String roundDesc;

    private Integer membertimeCalctype;

    @TableField("supportOffline")
    private Integer supportOffline;

    @ApiModelProperty(value = "停车场交易费率")
    @TableField("feeRate")
    private BigDecimal feeRate;

    @ApiModelProperty(value = " 数据字典：1已接入 2未接入")
    private Integer dataAccess;

    private Long mtime;

    @ApiModelProperty(value = "居住区认证:0-不认证,1-接入认证")
    private Integer authenticateFlag;

    @ApiModelProperty(value = "园中园计费模式:0:区域合并单独计费，1:区域合并统一计费")
    private Integer yzyFeeType;

    @ApiModelProperty(value = "居住区名字")
    private String houseName;


}
