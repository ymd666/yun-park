package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.model.dto.TTradeCommodityOrder;
import cn.tnar.parkservice.model.dto.TTradeRefundDto;
import cn.tnar.parkservice.model.request.TradeRefundRequset;
import cn.tnar.parkservice.model.response.TradeRefundResponse;
import cn.tnar.parkservice.mapper.TTradeCommodityOrderMapper;
import cn.tnar.parkservice.mapper.TTradeRefundMapper;
import cn.tnar.parkservice.model.entity.TTradeRefund;
import cn.tnar.parkservice.service.ITTradeRefundService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * 退款信息 服务实现类
 *
 * @author Tiyer.Tao
 * @since 2018-09-17
 */
@Service("tTradeRefundServiceImpl")
public class TTradeRefundServiceImpl extends ServiceImpl<TTradeRefundMapper, TTradeRefund> implements ITTradeRefundService {



    @Autowired
    private TTradeRefundMapper tTradeRefundMapper;

    @Autowired
    private TTradeCommodityOrderMapper tradeCommodityOrderMapper;

    private SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");


    @Override
    public boolean updateTradeCommodityState(String orderNumber, BigDecimal refundamount) {
        TTradeCommodityOrder tradeCommodityOrder = new TTradeCommodityOrder();
        tradeCommodityOrder.setOrderNumber(orderNumber);
        tradeCommodityOrder.setRefundamout(refundamount);
        QueryWrapper<TTradeCommodityOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("order_number",orderNumber);
        int reflectRow = tradeCommodityOrderMapper.update(tradeCommodityOrder,wrapper);
        if(reflectRow>0){
            return true;
        }
        return false;
    }

    /**
     * 退款信息查询
     *
     * @param tradeRefundRequset
     * @return
     */
    @Override
    public Page<TradeRefundResponse> getRefundByPage(TradeRefundRequset tradeRefundRequset) {

        Page<TradeRefundResponse> page = new Page<TradeRefundResponse>(tradeRefundRequset.getCurrentPage(), tradeRefundRequset.getPageSize());
        return page.setRecords(tTradeRefundMapper.getRefundByPage(page, tradeRefundRequset));
    }

    /**
     * 退款审核
     *
     * @return
     */
    @Override
    public boolean updateAuditRefund(TTradeRefund tradeRefund) {
        long first_check_id = 0;
        long sec_check_id = 0;
        long third_check_id = 0;
        long first_check_time = 0;
        long sec_check_time = 0;
        long third_check_time = 0;
        long now = Long.valueOf(sf.format(new Date()));
        Long cust_id = tradeRefund.getCustId();
        int refundState = tradeRefund.getRefundstate();
        Long id = tradeRefund.getId();
        String description = tradeRefund.getDescription();
        String orderNumber = tradeRefund.getOrderNumber();
        if (cust_id == 4010000120636l) {
            //财务
            sec_check_id = cust_id;
            sec_check_time = now;
        } else if (cust_id == 4010000120637l) {
            //总经办
            if (refundState == 2) {
                refundState = 3;
            }
            third_check_time = now;
            third_check_id = cust_id;
        } else {
            first_check_time = now;
            first_check_id = cust_id;
        }

        TTradeRefundDto refundDto = new TTradeRefundDto();
        refundDto.setId(id);
        refundDto.setRefundstate(refundState);
        refundDto.setDescription(description);
        refundDto.setFirstCheckId(first_check_id);
        refundDto.setFirstCheckTime(first_check_time);
        refundDto.setSecCheckId(sec_check_id);
        refundDto.setSecCheckTime(sec_check_time);
        refundDto.setThirdCheckId(third_check_id);
        refundDto.setThirdCheckTime(third_check_time);
        int a = tTradeRefundMapper.updateAuditRefund(refundDto);
        if (a > 0) {
//            if(refundState == 3){
//                int state= 7;//7:退款成功
//                updateTradeCommodityState(orderNumber,state, tradeRefund.getRefundamount());
//            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<TradeRefundResponse> getRefundList(String parkCode, String carNo, String orderNumber, Integer refundState, Long startTime, Long endTime){
        return tTradeRefundMapper.getRefundList(parkCode, carNo, orderNumber, refundState, startTime, endTime);
    }


}
