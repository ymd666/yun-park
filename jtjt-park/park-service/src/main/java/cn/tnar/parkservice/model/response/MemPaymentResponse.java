package cn.tnar.parkservice.model.response;


import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: xinZhang
 * @Date: 2019/8/15
 * @Description:
 */
@Data
public class MemPaymentResponse {


    private Long id;
    /**
     * 车牌号
     */
    private String carId;

    /**
     *车主
     */
    private String ownerName	;

    /**
     *停车场名称
     */
    private String parkName;

    /**
     *停车场区域名称
     */
    private String regionName;

    /**
     *收费标准名称
     */
    private String roleName;

    /**
     * 变更类型
     */
    private Short amountType;

    /**
     *交费金额
     */
    private BigDecimal amountNum;

    /**
     *发生时间
     */
    private String upTime;

    //初始开始时间

    /**
     *初始到期日期
     */
    private String startDate;

    //变更后开始时间

    /**
     *变更后到期日期
     */
    private String endDate;

    /**
     * 房号
     */
    private String roomNo;

    /**
     *操作员名称
     */
    private String operName;

    /**
     *运营商名称
     */
    private String custName;

    /**
     *备注
     */
    private String remark;
    
}
