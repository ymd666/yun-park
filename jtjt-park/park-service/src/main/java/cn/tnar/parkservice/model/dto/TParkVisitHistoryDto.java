package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author clarck
 * @since 2018-10-05
 */
@Data
@TableName("t_park_visit_history")
public class TParkVisitHistoryDto {

    private Integer id;

    private String parkName;

    private String parkCode;

    private String visitor;

    private String mobile;

    private String carId;

    private Integer carType;

    private String applyName;

    private String applyMobile;

    private Long visitTimeStart;

    private Long visitTimeEnd;

    private Integer visitState;

    private String custId;

    private Long entranceTime;

    private String remarks;

    private Long applyTime;

    private Long applyId;

    private Integer applyStatus;

    private Long accessTime;

    private String operId;

    private Long customerVisitTime;
//    private String memGroupId;

    private Integer flyosStatus; //0新增或者1删除
}

