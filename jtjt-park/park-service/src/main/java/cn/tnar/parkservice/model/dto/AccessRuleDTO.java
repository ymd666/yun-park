package cn.tnar.parkservice.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Description:准入规则传输对象
 * @author: yl
 * @date 2018/9/13 11:51
 */
@Data
public class AccessRuleDTO {

    @ApiModelProperty(value = "ID可不填", example = "1")
    private Integer id=0;

    /**
     * 准入规则名称
     */
    @ApiModelProperty(value = "准入规则名称", example = "襄阳面馆停车场准入规则")
    private String ruleName;

    /**
     * 准入停车场名称
     */
    @ApiModelProperty(value = "停车场名称", example = "襄阳面馆停车场")
    private String parkName;
    /**
     * 准入停车场编号
     */
    @ApiModelProperty(value = "停车场编号,唯一", example = "123344556")
    private String parkCode;
    /**
     * 准入停车区域
     */
    @ApiModelProperty(value = "停车场区域编码", example = "1234566775")
    private String regionCode;
    /**
     * 准入停车区域名称
     */
    @ApiModelProperty(value = "停车场区域名称", example = "东地下区域")
    private String regionName;
    /**
     * 准出入口
     */
    @ApiModelProperty(value = "准出入口", example = "A,B,C")
    private String accessGate;

    /**
     * 月卡车位数
     */
    @ApiModelProperty(value = "可停靠会员车数 不传", example = "50")
    private Integer memberNum=0;

    /**
     * 临停车位数
     */
    @ApiModelProperty(value = "可停靠的临时车数 不传", example = "100")
    private Integer temporaryNum=0;

    /**
     * 储值车位数
     */
    @ApiModelProperty(value = "可停靠的储值车数 不传", example = "20")
    private Integer storeNum=0;

    /**
     * 预约车位数
     */
    @ApiModelProperty(value = "可停靠的预约车数 不传", example = "10")
    private Integer bookingNum=0;

    /**
     * 访客车位数
     */
    @ApiModelProperty(value = "可停靠的访客车数 不传", example = "5")
    private Integer visitorNum=0;

    /**
     * 月卡车颜色
     */
    @ApiModelProperty(value = "可停靠月卡车颜色 不传", example = "1")
    private String memberColor="";

    /**
     * 临停车颜色
     */
    @ApiModelProperty(value = "可停靠的临时车颜色 不传", example = "0")
    private String temporaryColor="";

    /**
     * 储值车颜色
     */
    @ApiModelProperty(value = "可停靠的储值车颜色 不传", example = "3")
    private String storeColor="";

    /**
     * 预约车颜色
     */
    @ApiModelProperty(value = "可停靠的预约车数 不传", example = "2")
    private String bookingColor="";

    /**
     * 访客车颜色
     */
    @ApiModelProperty(value = "可停靠的访客车颜色 不传", example = "5")
    private String visitorColor="";

    /**
     * 单双号限停
     */
    @ApiModelProperty(value = "单双号限停 0-不勾选 1-勾选", example = "0")
    private Integer carstopLimit=0;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注,可不传", example = "八百标兵奔北坡")
    private String remark="";

    /**
     * 启用状态
     */
    @ApiModelProperty(value = "启用状态 0-禁用 1-启用", example = "0")
    private Integer isUsing;

//    @ApiModelProperty(value = "准入规则，必传", example = "")
//    private List<Map<String, String>> accessList;

    @ApiModelProperty(value = "入口List，必传", example = "")
    private List<Map<String, Object>> gateList;

    /**
     * 准入车辆类型，如1-临时车、2-月卡车、3-储值车，4-内部储值车，5-免费车，6-内部车等
     */
    private String cardType;
    /**
     * 准入车牌颜色，1-蓝，3-黄，6-绿，7-白
     */
    private String carType;
    /**
     * 准入开始时间
     */
    private Long startTime;
    /**
     * 准入结束时间
     */
    private Long endTime;
    /**
     * 总车位数限制
     */
    private Integer totalNum;
    /**
     * 无牌车准入数
     */
    private Integer noplateNum;
    /**
     * 其他规则设置
     */
    private String otherRule;
    /**
     * 准入规则id
     */
    private Long accessRuleId;

    /**
     * 卡组id
     */
    private Long groupId;

    private Integer pageNo;

    private Integer pageSize;

}

