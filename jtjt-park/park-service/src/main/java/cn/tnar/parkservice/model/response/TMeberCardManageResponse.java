package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/9/12
 * @Description:
 */
@Data
public class TMeberCardManageResponse {

    /**
     * 记录id
     */
    private Long id;

    /**
     *被访者名称
     */
    private String owner_name;

    /**
     *电话
     */
    private String tel_no;

    /**
     *卡证名称
     */
    private String role_name;

    /**
     *车牌号
     */
    private String car_id;

    /**
     *开始日期
     */
    private Long start_date;

    /**
     *到期日期
     */
    private Long end_date;

    /**
     *申请状态
     */
    private Integer state;
}
