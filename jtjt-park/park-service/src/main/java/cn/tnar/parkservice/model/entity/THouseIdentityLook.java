package cn.tnar.parkservice.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zzb
 * @since 2018-12-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "THouseIdentityLook对象", description = "")
public class THouseIdentityLook implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Integer Id;

    @ApiModelProperty(value = "对应申请表主键")
    private Integer applyId;

    @ApiModelProperty(value = "审核人ID")
    private Long lookId;

    @ApiModelProperty(value = "审核人级别")
    private Integer lookLevel;

    @ApiModelProperty(value = "审核人姓名")
    private String lookName;

    @ApiModelProperty(value = "审核时间")
    private Long lookTime;

    @ApiModelProperty(value = "审核状态 1:审核通过 2:审核不通过 3:打回修改 4:待审核 5:审核中")
    private Integer lookState;

    @ApiModelProperty(value = "审核说明")
    private String lookRemark;

    @ApiModelProperty(value = "角色Id")
    private Long roleId;

    @ApiModelProperty(value = "几证合一")
    private Integer combine;

}
