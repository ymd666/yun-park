package cn.tnar.parkservice.controller.common;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.model.entity.TParkInfo;
import cn.tnar.parkservice.service.ITParkInfoService;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/22
 * @Description:
 */
@Api(tags = "停车场信息（张鑫）")
@RestController
@RequestMapping("service/parkinfo")
public class TParkInfoController {

    private Logger log = LoggerFactory.getLogger(TParkInfoController.class);

    @Autowired
    private KesbApiService kesbApiService;

    @Autowired
    private ITParkInfoService parkInfoService;

    @ApiOperation("新增停车场基础信息")
    @PostMapping("/add")
    public ResultJson addParkInfo(@RequestBody Map<String, Object> param){
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_INFO_ADD, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场基础信息")
    @PostMapping("/update")
    public ResultJson updateParkInfo(@RequestBody Map<String, Object> param){
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_INFO_UPDATE, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("删除停车场基础信息")
    @PostMapping("/delete")
    public ResultJson deleteParkInfo(@RequestBody Map<String, Object> param){
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_INFO_DELETE, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场基础信息")
    @PostMapping("/query")
    public ResultJson queryParkInfo(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_INFO_QUERY, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询停车场扩展信息")
    @PostMapping("/queryExtend")
    public ResultJson queryParkExtendInfo(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_PARK_EXTEND_INFO_QERUY, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改停车场扩展信息")
    @PostMapping("/updateExtend")
    public ResultJson updateParkExtendInfo(@RequestBody Map<String, Object> param){
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_EXTEND_INFO_UPDATE, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("新增停车场扩展信息")
    @PostMapping("/addExtend")
    public ResultJson addParkExtendInfo(@RequestBody Map<String, Object> param){
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_PARK_EXTEND_INFO_ADD, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation(value = "访客管理（公众号） - 查询配置了计费规则的停车场信息 ")
    @PostMapping("/queryAllParks")
    public ResultJson queryAllParks(@RequestBody Map<String, Object> param){
        try {
            List<Map<String, String>> result = parkInfoService.queryAllParks(param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        }  catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation(value = " 实时总览 停车场车位信息查询- t_park_info")
    @GetMapping(value = "queryByCode")
    public ResultJson queryByCode(@RequestParam("parkCode") String parkCode) {
        QueryWrapper<TParkInfo> ew = new QueryWrapper();
        ew.eq("park_code", parkCode);
        TParkInfo parkInfo = parkInfoService.getOne(ew);
        return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(parkInfo);
    }

}
