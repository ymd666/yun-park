package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TSysRoleOrgMapper;
import cn.tnar.parkservice.model.entity.TSysRoleOrg;
import cn.tnar.parkservice.service.TSysRoleOrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Author: xinZhang
 * @Date: 2019/10/10
 * @Description:
 */
@Service
public class TSysRoleOrgServiceImpl extends ServiceImpl<TSysRoleOrgMapper, TSysRoleOrg> implements TSysRoleOrgService {
}
