package cn.tnar.parkservice.service;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年11月22日 16:32:52
 */
public interface MqService {

    /**
     * 发送mq消息
     *
     * @param exchangeName
     * @param queueName
     * @param object
     */
    void convertAndSend(String exchangeName, String queueName, Object object);

    boolean mqSend(String jsonString, String topic);
}
