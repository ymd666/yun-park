package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.dto.OrderAmountDto;
import cn.tnar.parkservice.model.entity.TBerthCount;
import cn.tnar.parkservice.model.entity.TParkInfo;
import cn.tnar.parkservice.model.request.CurrentViewRequset;
import cn.tnar.parkservice.model.response.ParkInfoInResponse;
import cn.tnar.parkservice.model.response.TparkInfoBaseResponse;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zzb
 * @since 2018-12-17
 */
public interface ITParkInfoService extends IService<TParkInfo> {

    TParkInfo selectByParkCode(String parkCode);

    List<TParkInfo> selectParkNo(long custId);

    List<TParkInfo> selectPtype(String parkCode, int ptype);

    String getPosId(String custId);

    List<ParkInfoInResponse> getParkInfoIn(String code);

    //根据运营商custId获取parkCode 仅平台下不准 多出已删除的二级运营商下的停车场
    List<String> getParkCodeByCustId(String custId);

    //根据运营商custId和停车场名字获取parkCode
    String getParkCodeByCustIdAndParkName(String custId, String parkName);

    Map<String, Integer> queryParkInfoByParkCode(String parkCode);

    Map<String, Object> statisGroupByParkType(CurrentViewRequset currentViewRequset);

    List<Map<String, Object>> statisGroupByCarType(CurrentViewRequset currentViewRequset);

    Map<String, Object> statisGroupByPayChannel(CurrentViewRequset currentViewRequset);

    Integer selectTransCount(String beginTime, String endTime, String parkCode, Integer ptype);

    Integer queryBerthInOrOut(Integer ptype, String parkCode);

    Integer countTotal(String code);

    Integer queryRecordIn(String beginTime, String nowStr, String parkCode);

    Integer queryRecordOut(String beginTime, String nowStr, String parkCode);

    TBerthCount selectBerthCount(TBerthCount entity);

    Integer queryOccupyBerthNum(TBerthCount entity);

    /**
     * 查询今日收益统计
     *
     * @param currentViewRequset
     * @return
     */
    List<OrderAmountDto> statisticToday(CurrentViewRequset currentViewRequset);


    /**
     * 查询路内路外停车场信息
     *
     * @param ptype
     * @return
     */
    public List<Map<String, Object>> queryParkByPtype(int ptype);

    /**
     * 查询路内路外运营商下停车场信息
     *
     * @param ptype custId
     * @return
     */
    List<Map<String, Object>> queryParkByPtypeCustId(int ptype, String custId);

    /**
     * 根据停车场code查询停车场信息
     *
     * @param parkCode
     * @return
     */
    List<TparkInfoBaseResponse> selectByPackCode(int ptype, String parkCode);

    /**
     * 查询路内路外停车场基本信息
     *
     * @return
     */
    List<TparkInfoBaseResponse> selectBase();


    Map<String, Object> getTodayProject(String parkCode, String beginTime, String endTime, int type, int ptype);

    Integer countByOpenChannel(int open_channel);

    List<Map<String, String>> queryAllParks(Map<String, Object> param);

    String getParkNameByParkCode(String parkCode);
}
