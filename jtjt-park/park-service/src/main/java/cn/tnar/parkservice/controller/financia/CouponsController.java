package cn.tnar.parkservice.controller.financia;


import cn.tnar.parkservice.exception.CouponsException;
import cn.tnar.parkservice.model.request.ParkingInfo;
import cn.tnar.parkservice.model.request.ParkingRequest;
import cn.tnar.parkservice.model.request.RegRequest;
import cn.tnar.parkservice.service.CouponsService;
import cn.tnar.parkservice.util.ResponseUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xzhang
 * @date 2019/9/6
 */
@RestController
@RequestMapping("/coupons")
@Slf4j
@Api(tags = "线上优惠券管理")
public class CouponsController {

    @Autowired
    CouponsService couponsService;

    @PostMapping("/getMobileCode")
    public Object getMobileCode(@RequestBody ParkingRequest parkingRequest) {
        try {

            if (StringUtils.isBlank(parkingRequest.getMobile())) {
                return ResponseUtils.fail(402, "手机号码不能为空");
            }

            return ResponseUtils.ok(couponsService.getMobileCode(parkingRequest.getMobile()));
        } catch (CouponsException e) {
            return ResponseUtils.fail(Integer.parseInt(e.getCode()), e.getMessage());
        }
    }

    @PostMapping("/registerTnarMember")
    public Object registerTnarMember(@RequestBody RegRequest regRequest) {
        try {
            return ResponseUtils.ok(couponsService.registerTnarMember(regRequest));
        } catch (CouponsException e) {
            return ResponseUtils.fail(Integer.parseInt(e.getCode()), e.getMessage());
        }
    }

    @PostMapping("/bindCarPlateNo")
    public Object bindCarPlateNo(@RequestBody ParkingRequest parkingRequest) {
        try {
            couponsService.bindCarPlateNo(parkingRequest.getCustId(), parkingRequest.getCarId(), parkingRequest.getCarType(), parkingRequest.getCustSession());
            return ResponseUtils.ok();
        } catch (CouponsException e) {
            return ResponseUtils.fail(Integer.parseInt(e.getCode()), e.getMessage());
        }
    }

    @PostMapping("/queryParkingRecord")
    public Object queryParkingRecord(@RequestBody ParkingRequest parkingRequest) {
        try {
            final List<ParkingInfo> data = couponsService.queryParkingRecord(parkingRequest.getCustId(), parkingRequest.getCarId(), parkingRequest.getCarType());
            if (data == null || data.size() == 0) {
                return ResponseUtils.fail(1000, "找不到停车记录");
            }
            return ResponseUtils.ok(data);
        } catch (CouponsException e) {
            return ResponseUtils.fail(Integer.parseInt(e.getCode()), e.getMessage());
        }
    }

    @PostMapping("/pushCouponsInfo")
    public Object pushCouponsInfo(@RequestBody ParkingRequest parkingRequest) {
        try {
            couponsService.pushCouponsInfo(parkingRequest.getCarId(), parkingRequest.getParkCode(), parkingRequest.getCarType());
        } catch (CouponsException e) {
            return ResponseUtils.fail(Integer.parseInt(e.getCode()), e.getMessage());
        }
        return ResponseUtils.ok();
    }


    /**
     * 计算停车费用
     *{
     *   "car_id": "鄂Ajku77",
     *   "cartype": "1",
     *   "intime": "20191112050000",
     *   "park_code": "110101200034",
     *   "region_code": "11010120003402"
     * }
     * @param parkingInfo
     * @return
     */
    @PostMapping("/calcFee")
    public Object calcFee(@RequestBody ParkingInfo parkingInfo) {
        try {
            ParkingInfo data = couponsService.calcFee(parkingInfo);
            return ResponseUtils.ok(data);
        } catch (CouponsException e) {
            return ResponseUtils.fail(Integer.parseInt(e.getCode()), e.getMessage());
        }
    }

}
