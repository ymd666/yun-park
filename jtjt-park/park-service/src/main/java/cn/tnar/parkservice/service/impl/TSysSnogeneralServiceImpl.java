package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TSysSnogeneralMapper;
import cn.tnar.parkservice.model.entity.TSysSnogeneral;
import cn.tnar.parkservice.service.ITSysSnogeneralService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-03-05
 */
@Service("tSysSnogeneralServiceImpl")
public class TSysSnogeneralServiceImpl extends ServiceImpl<TSysSnogeneralMapper, TSysSnogeneral> implements ITSysSnogeneralService {
    @Autowired
    private TSysSnogeneralMapper tSysSnogeneralMapper;

    @Override
    @Transactional
    public Long GetNextSno(int operType) throws Exception {
        int nodeId = 1;

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("step", 1);
        paramMap.put("opertype", operType);
        paramMap.put("nodeId", nodeId);
        int updateResult = tSysSnogeneralMapper.updateByTypeAndNodeId(paramMap);
        if (updateResult < 1) {
            TSysSnogeneral sysSnogeneral = new TSysSnogeneral();
            sysSnogeneral.setLastsno(10000L);
            sysSnogeneral.setOpertype(operType);
            sysSnogeneral.setNodeId(nodeId);
            tSysSnogeneralMapper.insert(sysSnogeneral);
        }

        QueryWrapper qw = new QueryWrapper();
        qw.eq("opertype", operType);
        qw.eq("node_id", nodeId);
        TSysSnogeneral result = tSysSnogeneralMapper.selectOne(qw);
        if (result == null) {
            throw new Exception("select lastsno failed.");
        }
        return result.getLastsno();
    }


    @Override
    @Transactional
    public String GetNextPrefix(int operType, String prefix) throws Exception {
        int nodeId = 1;

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("step", 1);
        paramMap.put("opertype", operType);
        paramMap.put("nodeId", nodeId);
        paramMap.put("prefix", prefix);

        int updateResult = tSysSnogeneralMapper.updatePrefixByType(paramMap);
        if (updateResult < 1) {
            tSysSnogeneralMapper.savePrefix(paramMap);
        }

        String result = tSysSnogeneralMapper.selectPrefix(paramMap);
        if (result == null) {
            throw new Exception("select lastsno failed.");
        }
        return result;
    }
}
