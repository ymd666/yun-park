package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.dto.BadDebtExportDto;
import cn.tnar.parkservice.model.dto.OverduePaymentExportDto;
import cn.tnar.parkservice.model.dto.TParkEscapeDto;
import cn.tnar.parkservice.model.entity.TParkEscape;
import com.baomidou.mybatisplus.extension.service.IService;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @author gl
 * @date 2018-10-22
 */
public interface ITParkEscapeService extends IService<TParkEscape> {

    //List<TParkEscapeDto> getTParkEscapeList( Wrapper<TParkEscapeDto> entityWrapper);
    List<TParkEscapeDto> getTParkEscapeList(Long startTime, Long endTime, String carId, String chargemanno, String parkCode);

    void exportTParkEscape(Long startTime, Long endTime, List<TParkEscapeDto> list, HttpServletResponse response) throws IOException, ParseException;

    List<String> selectCustIds(String custId);

    /**
     * 获取欠费补交记录
     * @param carId 车牌号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param chargemanno 收费员编码
     * @param parkCode 停车场编号
     * @return
     */
    List<OverduePaymentExportDto> getOverduePaymentList(String carId, Long startTime, Long endTime, String chargemanno, String parkCode);

    /**
     * 获取坏账记录
     * @param carId 车牌号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param chargemanno 收费员编码
     * @param parkCode 停车场编号
     * @return
     */
    List<BadDebtExportDto> getBadDebtList(String carId, Long startTime, Long endTime, String chargemanno, String parkCode);
}
