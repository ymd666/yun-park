package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TSysRoleOrg;
import com.baomidou.mybatisplus.extension.service.IService;

public interface TSysRoleOrgService extends IService<TSysRoleOrg> {
}
