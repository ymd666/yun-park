package cn.tnar.yunpark.service.impl;

import cn.tnar.yunpark.model.KesbStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by tieh on 2017/6/24.
 */
public class StatusToKesb implements Function<KesbStatus, Map<String, String>> {

    public static final StatusToKesb INSTANCE = new StatusToKesb();

    @Override
    public Map<String, String> apply(KesbStatus status) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("baynum", "1");
        map.put("type", status.getType());
        map.put("datas", status.toKesbString());
        return map;
    }

}
