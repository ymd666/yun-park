package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkFeeIndex;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:38:31
 */
public interface ITParkFeeIndexService extends IService<TParkFeeIndex> {

    ResultJson updateFeeIndex(Long id,String parkCode);

    String getRegionNameFromSysReg(String regionCode);

    ResultJson syncFeeIndexAndCalc(Map<String, String> map);

}
