package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.entity.TApplyTicketAcctInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2019-03-27
 */
@Mapper
@QuerySupport
@Repository
public interface TApplyTicketAcctInfoMapper extends BaseMapper<TApplyTicketAcctInfo> {

    @Select("SELECT id,cust_id,park_code,park_name,url,appId,appSecret,appSalt," +
            "version,userName,passWord,enterpriseCode,taxpayerId,fpqqlsh,dsptbm,nsrsbh,nsrmc," +
            "xhf_dz,xhf_dh,xhf_yhzh,kpy,sky,fhy,spbm,sl,delflag,create_time," +
            "vatSpecialManagement,goodsName,priceTaxMark" +
            " FROM t_apply_ticket_acct_info WHERE delflag = 1 AND park_code IN (${parkCodes}) order by id desc ")
    List<TApplyTicketAcctInfo> query(Page<TApplyTicketAcctInfo> page, @Param("parkCodes") String parkCodes);

    @Select("SELECT taxpayerId AS bwTaxNo FROM t_apply_ticket_acct_info WHERE park_code = #{parkCode} AND delflag=1;")
    String getParkTaxNo(@Param("parkCode") String parkCode);
}
