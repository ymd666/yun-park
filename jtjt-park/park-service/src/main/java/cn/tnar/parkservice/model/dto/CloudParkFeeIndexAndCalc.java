package cn.tnar.parkservice.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年11月18日 14:21:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class CloudParkFeeIndexAndCalc {

    @JsonProperty(value = "FeeIndex")
    private CloudParkFeeIndex FeeIndex;

    @JsonProperty(value = "Billing")
    private CloudParkBilling Billing;
}
