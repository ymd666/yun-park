import cn.tnar.yunpark.puxd.SignatureChecker;
import cn.tnar.yunpark.util.RC4Util;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tieh on 2016/11/18.
 */
public class TestPuxdSign {
    private static  final Logger log = LoggerFactory.getLogger(TestPuxdSign.class);

    @Test
    public void testDecrypt() {
        String key = "6F3CA4FF-C99D-4EBA-999C-CD79F70EA6D3";
        String encoded = "c2be3f55c39bc2af22c28d42c39b532ac2aec2b25815c39445c386c389c2aac2a022c380c29ec2b2c291c3ac20c396c2997c3d736633c39bc2adc2a5c28865367a2f50c28c0d48c393c39c66711ac38fc29cc3b05a7cc38a01c3b72f781f2b0660c2b4135dc2bbc2b37ac292c28fc39ac398c39cc392c3a2c295";

        String expectDecoded = "{\"CMCID\":\"09071021\",\"PMDID\":\"09020259\",\"commandCode\":\"19500101\",\"heartbeat\":\"0\"}";
        String decoded = RC4Util.DecryRC4(encoded, key);
        Assert.assertEquals(expectDecoded, decoded);
    }

    @Test
    public void testSign() {
        String key = "93C24753-CDBB-4402-BB10-1EB23ED85006";
        String json = "{\"CMCID\":\"09071021\",\"PMDID\":\"09020259\",\"commandCode\":\"19500101\",\"heartbeat\":\"0\"}";
        String expectSign = "c91e10d11adde67d707f626389d08a30";
        String sign = SignatureChecker.sign(json, key);
        Assert.assertEquals(expectSign, sign.toLowerCase());
    }
    @Test
    public void testSign2() {
        String key = "93C24753-CDBB-4402-BB10-1EB23ED85006";
        String json = "{\"CMCID\":\"08041000\",\"PMDID\":\"0612012F\",\"parkingDate\":\"20161118145315\",\"parkingState\":\"1\",\"reqDate\":\"20161118145315\"}";
        String expectSign = "d06fa4d71e8565b73bed9b90ca2bc03a";
        String sign = SignatureChecker.sign(json, key);
        Assert.assertEquals(expectSign, sign.toLowerCase());
    }
}
