package cn.tnar.yunpark.util;

import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by tieh on 2017/6/21.
 */
public class DateTimeUtil {
    public static LocalDateTime ofSeconds(long seconds) {
        Clock clock = Clock.systemDefaultZone();
        final Instant now = clock.instant();
        ZoneOffset offset = clock.getZone().getRules().getOffset(now);
        return LocalDateTime.ofEpochSecond(seconds, 0, offset);
    }

    public static DateTimeFormatter DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static DateTimeFormatter BASIC_DATE_TIME = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    public static SimpleDateFormat SIMPLE_BASIC_DATE_TIME = new SimpleDateFormat("yyyyMMddHHmmss");

    public static String format(long seconds) {
        return SIMPLE_BASIC_DATE_TIME.format(Date.from(Instant.ofEpochSecond(seconds)));
    }

    public static String format(LocalDateTime time) {
        return DATE_TIME.format(time);
    }

    public static String formatBasic(LocalDateTime time) {
        return BASIC_DATE_TIME.format(time);
    }

    public static String now() {
        return format(LocalDateTime.now());
    }

    public static String nowBasic() {
        return formatBasic(LocalDateTime.now());
    }
}
