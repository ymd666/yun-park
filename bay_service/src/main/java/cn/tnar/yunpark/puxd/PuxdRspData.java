package cn.tnar.yunpark.puxd;

/**
 * Created by tieh on 2016/11/3.
 */
public class PuxdRspData {
    private int resultCode;
    private String resultMSG;

    public PuxdRspData() {
    }

    public PuxdRspData(int resultCode, String resultMSG) {
        this.resultCode = resultCode;
        this.resultMSG = resultMSG;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMSG() {
        return resultMSG;
    }

    public void setResultMSG(String resultMSG) {
        this.resultMSG = resultMSG;
    }
}
