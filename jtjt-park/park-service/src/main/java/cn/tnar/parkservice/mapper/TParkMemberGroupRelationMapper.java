package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.MemberinfoMqDto;
import cn.tnar.parkservice.model.dto.TParkMemberGroupRelationDto;
import cn.tnar.parkservice.model.entity.TParkMemberGroupRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月02日 15:07:52
 */
@Mapper
@QuerySupport
@Repository
public interface TParkMemberGroupRelationMapper extends BaseMapper<TParkMemberGroupRelation> {


    @Select("SELECT r.member_id from t_park_member_group_relation r left join t_park_group_memberinfo m  on m.id = r.member_id WHERE m.member_type=1 and r.group_id = #{groupId}")
    List<String> getMemberIdByChild(@Param("groupId") Long groupId);

    @Select("SELECT r.member_id from t_park_member_group_relation r left join t_park_group_memberinfo m  on m.id = r.member_id WHERE r.group_id = #{groupId}")
    List<String> getMemberIdByChildLook(@Param("groupId") Long groupId);
    @Select("SELECT r.member_id from t_park_member_group_relation r left join t_park_group_memberinfo m  on m.id = r.member_id WHERE m.member_type=1 and r.group_id in( ${groupIds})")
    List<String> getMemberIdByParent(@Param("groupIds") String groupIds);

    @Select("SELECT r.member_id from t_park_member_group_relation r  WHERE  r.group_id in (#{groupId})")
    List<String> getVisitIdByChild(@Param("groupId") String groupId);

    /**
    * 审核时判断卡组下是否存在相同车牌
     * */
    @Select("SELECT h.car_id from t_park_member_group_relation r  left join   t_park_visit_history h on r.member_id =h.id  WHERE  r.group_id = #{groupId} and r.member_id <> #{memberId} and h.car_id=#{carId} and h.visit_state<>2 and h.apply_status=2 and visit_time_end >= DATE_FORMAT(now(),'%Y%m%d%H%i%s')")
    List<String> getCarByChild(@Param("groupId") String groupId,@Param("memberId") Long memberId,@Param("carId") String carId);

    List<Map<String,String>> checkTel(@Param("groupId") Long groupId,@Param("telNo") String telNo);

    @Select("SELECT b.group_id from t_park_group_carinfo a  INNER JOIN t_park_member_group_relation b\n" +
            "on a.group_id = b.member_id where a.car_id = #{carNo}  order by b.create_time desc limit 1")
    Long getGroupIdByCarNo(@Param("carNo") String carNo);

    @Select("select id,group_id,member_id,create_time,update_time,1 as type from t_park_member_group_relation where group_id=#{groupId}")
    List<TParkMemberGroupRelationDto> getRelation(@Param("groupId") Long groupId);

    @Select("SELECT car_id ,car_type,visit_time_end as end_date,id as group_id, id,3 as member_type , 0 as oper_type, visitor as owner_name,park_code,remarks as remark, 0 as seat_num, DATE_FORMAT(now(),'%Y%m%d%H%i%s') as send_time,visit_time_start as start_date  FROM t_park_visit_history where id =#{id} and apply_status=2 and visit_state <>2 ")
    Map<String,Object> getVisit(@Param("id") Long id);
}
