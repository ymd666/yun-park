package cn.tnar.parkservice.model.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * @Description:
 * @author: yl
 * @date 2018/10/9 16:08
 */
@TableName("t_park_invoice_baiwang")
public class Invoice extends Model<Invoice> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 发票类型
     */
    private String invoice_type;

    /**
     * 发票名称抬头
     */
    private String buyer_name;

    /**
     * 发票金额
     */
    private String invoice_price;

    /**
     * 申请日期
     */
    private String invoice_date;

    /**
     * 开票人账号
     */
    private String buyer_bank_account;

    private String invoice_status;
    private String serial_no;

    private String invoice_code;
    private String invoice_no;
    /**
     * 开票人电话
     */
    private String buyer_address_tel;
    private String seller_address_tel;
    private String buyer_tax_no;
    private String seller_tax_no;
    private String buyer_phone;
    private String goods_name;

    @Override
    protected Serializable pkVal() {
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoice_type() {
        return invoice_type;
    }

    public void setInvoice_type(String invoice_type) {
        this.invoice_type = invoice_type;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public String getInvoice_price() {
        return invoice_price;
    }

    public void setInvoice_price(String invoice_price) {
        this.invoice_price = invoice_price;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getBuyer_bank_account() {
        return buyer_bank_account;
    }

    public void setBuyer_bank_account(String buyer_bank_account) {
        this.buyer_bank_account = buyer_bank_account;
    }

    public String getInvoice_status() {
        return invoice_status;
    }

    public void setInvoice_status(String invoice_status) {
        this.invoice_status = invoice_status;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getInvoice_code() {
        return invoice_code;
    }

    public void setInvoice_code(String invoice_code) {
        this.invoice_code = invoice_code;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getBuyer_address_tel() {
        return buyer_address_tel;
    }

    public void setBuyer_address_tel(String buyer_address_tel) {
        this.buyer_address_tel = buyer_address_tel;
    }

    public String getSeller_address_tel() {
        return seller_address_tel;
    }

    public void setSeller_address_tel(String seller_address_tel) {
        this.seller_address_tel = seller_address_tel;
    }

    public String getBuyer_tax_no() {
        return buyer_tax_no;
    }

    public void setBuyer_tax_no(String buyer_tax_no) {
        this.buyer_tax_no = buyer_tax_no;
    }

    public String getSeller_tax_no() {
        return seller_tax_no;
    }

    public void setSeller_tax_no(String seller_tax_no) {
        this.seller_tax_no = seller_tax_no;
    }

    public String getBuyer_phone() {
        return buyer_phone;
    }

    public void setBuyer_phone(String buyer_phone) {
        this.buyer_phone = buyer_phone;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }
}
