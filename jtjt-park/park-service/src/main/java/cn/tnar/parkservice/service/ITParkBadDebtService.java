package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.dto.BadDebtDetailDto;
import cn.tnar.parkservice.model.entity.TParkBadDebtInfo;
import cn.tnar.parkservice.model.entity.TParkEscape;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * @author Bao
 * @date 2018-10-18
 */
public interface ITParkBadDebtService extends IService<TParkBadDebtInfo> {

    Page<BadDebtDetailDto> pageBadDebtInfoDetail(Page<BadDebtDetailDto> page, Wrapper<BadDebtDetailDto> wrapper);

    BigDecimal sumBadDebtInfoAmount(Wrapper<BigDecimal> wrapper);

    /**
     * 逃逸流水 -> 坏账
     * 存坏账，删逃逸流水
     *
     * @param escape 逃逸流水 id
     * @param remark 坏账备注
     */
    void escapeToBadDebt(TParkEscape escape, String remark);
}
