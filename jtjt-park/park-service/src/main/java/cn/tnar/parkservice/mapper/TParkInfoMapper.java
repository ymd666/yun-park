package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.dto.OrderAmountDto;
import cn.tnar.parkservice.model.entity.TBerthCount;
import cn.tnar.parkservice.model.entity.TParkInfo;
import cn.tnar.parkservice.model.request.CurrentViewRequset;
import cn.tnar.parkservice.model.response.ParkInfoInResponse;
import cn.tnar.parkservice.model.response.TparkInfoBaseResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2018-12-17
 */
@Repository
@Mapper
public interface TParkInfoMapper extends BaseMapper<TParkInfo> {

    @Select("select name,house_name from t_park_info where park_code = #{parkCode} and authenticate_flag = 1")
    TParkInfo selectByParkCode(@Param("parkCode") String parkCode);

    List<TParkInfo> selectParkNo(@Param("parkCode") String parkCode);

    @Select("SELECT sum(space_count) FROM `t_park_region` WHERE park_code =#{code}")
    Integer countSeatTotal(@Param("code") String code);

    @Select("SELECT count(1) FROM `t_park_day_parking_record` WHERE result=90 and park_code=#{code}")
    Integer countSeatUse(@Param("code") String code);

    List<TParkInfo> selectPtype(@Param("parkCode") String parkCode, @Param("ptype") int ptype);

    @Select("select pos_id from t_acct_enterprise_info where cust_id=#{custId} ")
    String getPosId(@Param("custId") String custId);

    @Select("select park_code ,name from t_park_info where park_code in (${code}) ")
    List<ParkInfoInResponse> getParkInfoIn(@Param("code") String code);

    @Select("select * from t_park_info where park_code in (${code}) ")
    List<TParkInfo> getParkInfoTask(@Param("code") String code);

    List<String> getParkCodeByCustId(@Param("custId") String custId);

    String getParkCodeByCustIdAndParkName(@Param("custId") String custId, @Param("parkName") String parkName);

    Map<String, Integer> queryParkInfoByParkCode(@Param("parkCode") String parkCode);


    @Select("select count(1) from t_park_day_parking_record where (intime >=#{beginTime} and intime  <= #{endTime} ) and park_code in ( select park_code from t_park_info where ptype=#{ptype} and park_code in (${code}))  and outoperate <>6")
    Integer selectTransCount(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("ptype") Integer ptype, @Param("code") String code);

    @Select("SELECT  IFNULL(sum(space_count),0)  FROM `t_park_region`  where park_code in ( select park_code from t_park_info where ptype=#{ptype} and park_code in (${code})) ")
    Integer queryBerthInOrOut(@Param("ptype") Integer ptype, @Param("code") String code);

    @Select("SELECT  IFNULL(sum(space_count),0)  FROM `t_park_region`  where park_code in (${code}) ")
    Integer countTotal(@Param("code") String code);


    @Select("select day+his from " +
            "(select count(1) as day from t_park_day_parking_record where (intime >=#{beginTime} and intime < #{nowStr} ) and park_code in (${parkCode}) and outoperate <>6) as a," +
            "(select count(1) as his from t_park_his_parking_record where (intime >=#{beginTime} and intime < #{nowStr} ) and park_code in (${parkCode}) and outoperate <>6) as b")
    Integer queryRecordIn(@Param("beginTime") String beginTime, @Param("nowStr") String nowStr, @Param("parkCode") String parkCode);

    @Select("select day+his from" +
            "(select count(1) as day from t_park_day_parking_record where  (outtime >=#{beginTime} and outtime < #{nowStr} )  and park_code in (${parkCode}) and outoperate <>6) as a," +
            "(select count(1) as his from t_park_his_parking_record where  (outtime >=#{beginTime} and outtime < #{nowStr} )  and park_code in (${parkCode}) and outoperate <>6) as b")
    Integer queryRecordOut(@Param("beginTime") String beginTime, @Param("nowStr") String nowStr, @Param("parkCode") String parkCode);

    TBerthCount selectTBerthCount(@Param("ptype") int ptype, @Param("startTime") Long startTime, @Param("endTime") Long endTime, @Param("parkCode") String parkCode);


    Integer queryOccupyBerthNum(TBerthCount entity);

    List<OrderAmountDto> statisticToday(@Param("req") CurrentViewRequset req);

    List<TparkInfoBaseResponse> selectByPackCode(@Param("ptype") int ptype, @Param("parkCode") String parkCode);

    List<TparkInfoBaseResponse> selectBase();

    Map<String, Object> getTodayProject(@Param("parkCode") String parkCode, @Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("type") int type, @Param("ptype") int ptype);

    @Select("SELECT count( * ) FROM t_acct_cust_info info WHERE info.cust_opendate =  DATE_FORMAT( now( ), '%Y%m%d' ) and info.open_channel =#{open_channel}")
    Integer countByOpenChannel(@Param("open_channel") int open_channel);

    @Select("SELECT park_code as parkCode ,name as parkName FROM t_park_info WHERE delflag = 1 and park_code in " +
            "(SELECT DISTINCT(park_code) FROM t_park_member_group  " +
            "WHERE classify=2 and feesroleinfo_id >0 )")
    List<Map<String, String>> queryAllParks(@Param("parkName") String parkName);
}
