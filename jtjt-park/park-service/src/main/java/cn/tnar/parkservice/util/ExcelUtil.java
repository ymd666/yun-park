package cn.tnar.parkservice.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class ExcelUtil {

    private static final Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * 导出Excel
     * @param sheetName sheet名称
     * @param title 标题
     * @param values 内容
     * @param wb HSSFWorkbook对象
     * @return
     */
    public static XSSFWorkbook getXSSFWorkbook(String sheetName, String []title, String [][]values, XSSFWorkbook wb){

        // 第一步，创建一个XSSFWorkbook  xlsx，对应一个Excel文件
        if(wb == null){
            wb = new XSSFWorkbook();
        }

        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = wb.createSheet(sheetName);
        //列宽15
        sheet.setDefaultColumnWidth(15);
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
        XSSFRow row = sheet.createRow(0);

        // 第四步，创建单元格，并设置值表头 设置表头居中
        XSSFCellStyle style = wb.createCellStyle();

        style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

        //声明列对象
        XSSFCell cell = null;

        //创建标题
        for(int i=0;i<title.length;i++){
            cell = row.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(style);


        }

        //创建内容
        for(int i=0;i<values.length;i++){
            row = sheet.createRow(i + 1);
            for(int j=0;j<values[i].length;j++){
                cell= row.createCell(j);
                //将内容按顺序赋给对应的列对象
                cell.setCellValue(values[i][j]);
                //设置每行单元格水平居中
                cell.setCellStyle(style);
            }
        }
        return wb;
    }

    /**
     * @param fileName 导出文件名称
     * @param sheetName 工作薄名称
     * @param title	标题
     * @param contents 内容
     * @param response
     * @throws IOException
     * @功能描述: 导出Excel
     * @Author:阚文
     * @date:2018年10月6日  下午6:16:06
     * @Version:1.1.0
     */
    public static void export(String fileName, String sheetName, String[] title,String[][] contents, HttpServletResponse response) throws IOException {
        /*
         * 第一步、创建一个webbook，对应一个Excel文件
         */
        HSSFWorkbook wb = new HSSFWorkbook();
        /*
         * 第二步、在webbook中添加一个sheet,对应Excel文件中的sheet
         */
        Sheet sheet = wb.createSheet(sheetName);
        /*
         * 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
         */
        Row row0 = sheet.createRow((int) 0);
        /*
         * 第四步、设置Excel默认样式
         */
        //1、设置标题字体
        Font font = wb.createFont();
        font.setFontName("宋体");
       // font.setColor(HSSFColor.RED.index);//字体颜色
        font.setFontHeightInPoints((short) 10); // 字体大小
        font.setBoldweight(Font.BOLDWEIGHT_BOLD); // 加粗
        //2、设置标题cell样式
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER); // 左右居中
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        //	cellStyle.setLocked(true);
        //	cellStyle.setWrapText(true); // 自动换行
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);
        //3、设置数据字体
        Font font2 = wb.createFont();
        font2.setFontName("宋体");
        font2.setFontHeightInPoints((short) 9);
        //4、设置条目cell样式
        DataFormat format = wb.createDataFormat();
        //5、数字style
        CellStyle cellStyleNum = wb.createCellStyle();
        cellStyleNum.setFont(font2);
        cellStyleNum.setAlignment(CellStyle.ALIGN_CENTER);//左右居左
        cellStyleNum.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        // cellStyleNum.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));//
        //6、设置单元类型
        cellStyleNum.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderTop(CellStyle.BORDER_THIN);
        //7、文字style
        CellStyle cellStyleText = wb.createCellStyle();
        cellStyleText.setFont(font2);
        cellStyleText.setAlignment(CellStyle.ALIGN_RIGHT);
        cellStyleText.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        cellStyleText.setDataFormat(format.getFormat("0.00"));// 设置单元类型
        cellStyleText.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleText.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleText.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleText.setBorderTop(CellStyle.BORDER_THIN);
        /*
         * 第五步、对应插入数据
         */
        //1、输出标题
        for (int i = 0; i < title.length; i++) {
            Cell titleCell = row0.createCell((short) i);
            titleCell.setCellType(Cell.CELL_TYPE_STRING);
            titleCell.setCellStyle(cellStyle);
            titleCell.setCellValue(title[i]);
        }
        //2、输出内容
        if(contents!=null) {
            Cell[] contentCell = new Cell[title.length];
            for (int i = 0; i < contents.length; i++) {
                Row contentRow = sheet.createRow(i + 1);
                for (int j = 0; j < contents[i].length; j++) {
                    contentCell[j] = contentRow.createCell(j);
                    contentCell[j].setCellType(Cell.CELL_TYPE_STRING);
                    contentCell[j].setCellStyle(cellStyleNum);
                    contentCell[j].setCellValue(contents[i][j]);
                }
            }
        }
        /*
         * 第六步、调整自适应宽度
         */
        //输出标题
        for (int i = 0; i < title.length; i++) {//后面放置在这里是为了解决设置这个自适应宽度特别慢的问题
            // 调整列宽度
            sheet.autoSizeColumn((short) i);
        }
        /*
         * 第七步，导出Excel
         */
        ServletOutputStream out = null;
        try {
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment;filename="+ new String(fileName.getBytes(),"ISO8859-1"));
            out = response.getOutputStream();
            wb.write(out);
        } catch (Exception e) {
            logger.info("导出失败================>>>" + e.getStackTrace());
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    /**
     * @param fileName 导出文件名称
     * @param sheetName 工作薄名称
     * @param map 表格顶部汇总数据
     * @param title	标题
     * @param contents 内容
     * @param response
     * @throws IOException
     * @功能描述: 导出Excel 列表上部有汇总数据
     * @Author:阚文
     * @date:2018年10月6日  下午6:16:06
     * @Version:1.1.0
     */
    public static void export(String fileName, String sheetName, Map<String, Object> map, String[] title, String[][] contents, HttpServletResponse response) throws IOException {
        /*
         * 第一步、创建一个webbook，对应一个Excel文件
         */
        HSSFWorkbook wb = new HSSFWorkbook();
        /*
         * 第二步、在webbook中添加一个sheet,对应Excel文件中的sheet
         */
        Sheet sheet = wb.createSheet(sheetName);

        /*
         * 第三步、设置Excel默认样式
         */
        //1、设置标题字体
        Font font = wb.createFont();
        font.setFontName("宋体");
        font.setColor(HSSFColor.RED.index);//字体颜色
        font.setFontHeightInPoints((short) 10); // 字体大小
        font.setBoldweight(Font.BOLDWEIGHT_BOLD); // 加粗
        //2、设置标题cell样式
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER); // 左右居中
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        //	cellStyle.setLocked(true);
        //	cellStyle.setWrapText(true); // 自动换行
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);
        //3、设置数据字体
        Font font2 = wb.createFont();
        font2.setFontName("宋体");
        font2.setFontHeightInPoints((short) 9);
        //4、设置条目cell样式
        DataFormat format = wb.createDataFormat();
        //5、数字style
        CellStyle cellStyleNum = wb.createCellStyle();
        cellStyleNum.setFont(font2);
        cellStyleNum.setAlignment(CellStyle.ALIGN_LEFT);//左右居左
        cellStyleNum.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        // cellStyleNum.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));//
        //6、设置单元类型
        cellStyleNum.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderTop(CellStyle.BORDER_THIN);
        //7、文字style
        CellStyle cellStyleText = wb.createCellStyle();
        cellStyleText.setFont(font2);
        cellStyleText.setAlignment(CellStyle.ALIGN_RIGHT);
        cellStyleText.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        cellStyleText.setDataFormat(format.getFormat("0.00"));// 设置单元类型
        cellStyleText.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleText.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleText.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleText.setBorderTop(CellStyle.BORDER_THIN);

        //表格顶部汇总数据，设置
        int rowNumber = 0;
        for ( String key: map.keySet()) {
            Row row = sheet.createRow(rowNumber);
            Cell titleCell = row.createCell(0);
            titleCell.setCellType(Cell.CELL_TYPE_STRING);
            titleCell.setCellStyle(cellStyleNum);
            titleCell.setCellValue(map.get(key).toString());
            rowNumber++;
        }

        /*
         * 第五步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
         */
        Row row0 = sheet.createRow(rowNumber);

        /*
         * 第六步、对应插入数据
         */
        //1、输出标题
        for (int i = 0; i < title.length; i++) {
            Cell titleCell = row0.createCell((short) i);
            titleCell.setCellType(Cell.CELL_TYPE_STRING);
            titleCell.setCellStyle(cellStyle);
            titleCell.setCellValue(title[i]);
        }
        //2、输出内容
        if(contents.length>0) {
            Cell[] contentCell = new Cell[title.length];
            for (int i = 0; i < contents.length; i++) {
                Row contentRow = sheet.createRow(i + rowNumber);
                for (int j = 0; j < contents[i].length; j++) {
                    contentCell[j] = contentRow.createCell(j);
                    contentCell[j].setCellType(Cell.CELL_TYPE_STRING);
                    contentCell[j].setCellStyle(cellStyleNum);
                    contentCell[j].setCellValue(contents[i][j]);
                }
            }
        }
        /*
         * 第七步、调整自适应宽度
         */
        //输出标题
        for (int i = 0; i < title.length; i++) {//后面放置在这里是为了解决设置这个自适应宽度特别慢的问题
            // 调整列宽度
            sheet.autoSizeColumn((short) i);
        }
        /*
         * 第八步，导出Excel
         */
        ServletOutputStream out = null;
        try {
                response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename="+ new String(fileName.getBytes(),"iso-8859-1"));
            out = response.getOutputStream();
            wb.write(out);
        } catch (Exception e) {
            logger.info("导出失败================>>>" + e.getStackTrace());
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    /**
     *  导出Excel 列表头分两部分
     * @param fileName 导出文件名称
     * @param sheetName 工作薄名称
     * @param title_1 表头_1
     * @param title_head_1 表头_1 合并单元格
     * @param title_2 表头_2
     * @param title_head_2 表头_2 合并单元格
     * @param contents 内容
     * @param response
     * @throws IOException
     */
    public static void export(String fileName, String sheetName,  String[] title_1, String[] title_head_1, String[] title_2, String[] title_head_2, String[][] contents, HttpServletResponse response) throws IOException {
        /*
         * 第一步、创建一个webbook，对应一个Excel文件
         */
        HSSFWorkbook wb = new HSSFWorkbook();
        /*
         * 第二步、在webbook中添加一个sheet,对应Excel文件中的sheet
         */
        Sheet sheet = wb.createSheet(sheetName);
        /*
         * 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
         */
        Row row_0 = sheet.createRow((int) 0);

        Row row_1 = sheet.createRow((int) 1);
        /*
         * 第四步、设置Excel默认样式
         */
        //1、设置标题字体
        Font font = wb.createFont();
        font.setFontName("宋体");
        font.setColor(HSSFColor.RED.index);//字体颜色
        font.setFontHeightInPoints((short) 10); // 字体大小
        font.setBoldweight(Font.BOLDWEIGHT_BOLD); // 加粗
        //2、设置标题cell样式
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER); // 左右居中
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        //	cellStyle.setLocked(true);
        //	cellStyle.setWrapText(true); // 自动换行
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);
        //3、设置数据字体
        Font font2 = wb.createFont();
        font2.setFontName("宋体");
        font2.setFontHeightInPoints((short) 9);
        //4、设置条目cell样式
        DataFormat format = wb.createDataFormat();
        //5、数字style
        CellStyle cellStyleNum = wb.createCellStyle();
        cellStyleNum.setFont(font2);
        cellStyleNum.setAlignment(CellStyle.ALIGN_LEFT);//左右居左
        cellStyleNum.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        // cellStyleNum.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));//
        //6、设置单元类型
        cellStyleNum.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleNum.setBorderTop(CellStyle.BORDER_THIN);
        //7、文字style
        CellStyle cellStyleText = wb.createCellStyle();
        cellStyleText.setFont(font2);
        cellStyleText.setAlignment(CellStyle.ALIGN_RIGHT);
        cellStyleText.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 上下居中
        cellStyleText.setDataFormat(format.getFormat("0.00"));// 设置单元类型
        cellStyleText.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleText.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleText.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleText.setBorderTop(CellStyle.BORDER_THIN);
        /*
         * 第五步、对应插入数据
         */


        //1、输出标题
        for (int i = 0; i < title_1.length; i++) {
            Cell titleCell = row_0.createCell((short) i);
            titleCell.setCellType(Cell.CELL_TYPE_STRING);
            titleCell.setCellStyle(cellStyle);
            titleCell.setCellValue(title_1[i]);
        }

        for (int i = 0; i < title_head_1.length; i++) {
            String[] temp = title_head_1[i].split(",");
            Integer startrow = Integer.parseInt(temp[0]);
            Integer overrow = Integer.parseInt(temp[1]);
            Integer startcol = Integer.parseInt(temp[2]);
            Integer overcol = Integer.parseInt(temp[3]);
            sheet.addMergedRegion(new CellRangeAddress(startrow, overrow,
                    startcol, overcol));
        }

        //1、输出标题
        for (int i = 0; i < title_1.length - 2 ; i++) {
            if(i < title_2.length){
                Cell titleCell = row_1.createCell((short) i + 2);
                titleCell.setCellType(Cell.CELL_TYPE_STRING);
                titleCell.setCellStyle(cellStyle);
                titleCell.setCellValue(title_2[i]);
            }else{
                Cell titleCell = row_1.createCell((short) i + 2);
                titleCell.setCellType(Cell.CELL_TYPE_STRING);
                titleCell.setCellStyle(cellStyle);
            }
       }

        //2、输出内容
        if(contents.length>0) {
            Cell[] contentCell = new Cell[title_1.length];
            for (int i = 0; i < contents.length; i++) {
                Row contentRow = sheet.createRow(i + 2);
                for (int j = 0; j < contents[i].length; j++) {
                    contentCell[j] = contentRow.createCell(j);
                    contentCell[j].setCellType(Cell.CELL_TYPE_STRING);
                    contentCell[j].setCellStyle(cellStyleNum);
                    contentCell[j].setCellValue(contents[i][j]);
                }
            }
        }
        /*
         * 第六步、调整自适应宽度
         */
        //输出标题
        for (int i = 0; i < title_1.length; i++) {//后面放置在这里是为了解决设置这个自适应宽度特别慢的问题
            // 调整列宽度
            sheet.autoSizeColumn((short) i);
        }
        /*
         * 第七步，导出Excel
         */
        ServletOutputStream out = null;
        try {
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename="+ new String(fileName.getBytes(),"iso-8859-1"));
            out = response.getOutputStream();
            wb.write(out);
        } catch (Exception e) {
            logger.info("导出失败================>>>" + e.getStackTrace());
            e.printStackTrace();
        } finally {
            out.close();
        }
    }
    public static String isNull(Object str) {
        if (StringUtils.isEmpty(str)) {
            return "--";
        }

        return str.toString();

    }





}
