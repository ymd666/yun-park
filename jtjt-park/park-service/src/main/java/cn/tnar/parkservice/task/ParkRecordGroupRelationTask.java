package cn.tnar.parkservice.task;

import cn.tnar.parkservice.model.entity.TParkRecordGroupRelation;
import cn.tnar.parkservice.service.ITParkRecordGroupRelationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年11月26日 17:08:48
 */
@Component
@Slf4j
public class ParkRecordGroupRelationTask {


    @Autowired
    private ITParkRecordGroupRelationService tParkRecordGroupRelationService;

    //    @Scheduled(cron = "0 30 4 * * ?")
    @Transactional(rollbackFor = Exception.class)
    public void transParkRecordGroupRelation() {
        int count = 0;
        try {
            //查询not in 流水表中的 serialNo
            List<TParkRecordGroupRelation> tParkRecordGroupRelations = tParkRecordGroupRelationService.queryNeedTransRelations();
            if (!CollectionUtils.isEmpty(tParkRecordGroupRelations)) {
                //转移
                tParkRecordGroupRelationService.saveBatch(tParkRecordGroupRelations);
                List<Long> collect = tParkRecordGroupRelations.parallelStream()
                        .filter(x -> x.getId() != null)
                        .map(TParkRecordGroupRelation::getId)
                        .collect(Collectors.toList());
                //删除
                tParkRecordGroupRelationService.removeByIds(collect);
            }
        } catch (Exception e) {
            log.error("error", e);
            do {
                transParkRecordGroupRelation();
                count++;
            } while (count > 3);
        }
    }


}
