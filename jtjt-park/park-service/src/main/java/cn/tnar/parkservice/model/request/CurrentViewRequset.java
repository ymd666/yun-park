package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 实时总览 请求类
 *
 * @author Tiyer.Tao
 * @since 2018-10-01
 */
@Data
public class CurrentViewRequset {

    @ApiModelProperty(value = "停车场编码", example = "420602200001,350802200007")
    private String parkCode;

    @ApiModelProperty(value = "开始时间", example = "20180928000000")
    private long startTime;

    @ApiModelProperty(value = "结束时间", example = "20180928190833")
    private long endTime;

    @ApiModelProperty(value = "运营商", example = "420602")
    private String agentId;

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
}
