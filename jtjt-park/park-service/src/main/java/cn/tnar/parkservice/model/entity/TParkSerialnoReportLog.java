package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 停车流水记录信息
 *
 * @author Tiyer.Tao
 * @since 2018-09-13
 */
@TableName("t_park_serialno_report_log")
@Data
public class TParkSerialnoReportLog {

    private long id;
    private int nodeId;
    private long agentId;
    private String parkCode;
    private String tollno;
    private int occurDate;
    private double tmpTotalShouldAccount;//现金应收
    private double tmp_total_actual_account;//现金实收
    private int balance_total_count;//储值卡次数
    private double balance_should_account;//储值卡应收
    private double balance_actual_account;//储值卡实收
    private int self_total_count;//自助交费次数
    private int citycard_total_count;//城市通次数
    private double self_weixin_account;//微信实收
    private double self_zhifubao_account;//支付宝实收
    private double self_citycard_account;//城市通实收
    private double self_third_weixin_account;//第三方微信实收
    private double self_third_zhifubao_account;//第三方支付宝实收
    private double self_third_citycard_account;//第三方城市通实收
    private double citycard_actual_account;//
    private double citycard_third_actual_account;//
    private int out_car_count;//出场车次数
    private int in_car_count;//进场车次数
    private int countCouponMoney;//金额优惠次数
    private int countCouponTime;//时长优惠次数
    private int countCouponFree;//全免优惠次数
    private double sumCouponMoney;//金额优惠金额
    private int sumCouponTime;//时长优惠总时长
    private double sumCouponFree;//全免优惠金额
    private double sumCouponMOney_time;//时长优惠金额
    private double exception_should_account;//异常应收
    private double exception_actual_account;//异常实收
    private int exception_car_count;//异常条数
    private int total_car_count;//总次数
    private double total_should_account;//应收
    private double total_actual_account;//实收
    private int settle_count;//
    private int settle_status;//
    private String tollname;//收费员姓名
    private String batchno;
    private double owner_pay_time_relate_account;//自助缴费15分钟抵扣金额
    private double auto_pay_time_relate_account;//电子缴费15分钟抵金额
    private double bank_card_actual_account;
    private double bank_card_should_account;//刷卡应收
    private double pay_time_relate_account;//15分钟时长抵扣金额
    private double monthcard_amt;//月卡抵用金额
    private int monthcard_count;//套餐卡抵用次数
    private int charge_count;//涉及现金缴费的笔数
    private int elec_coupon_count;//电子优惠券抵用笔数
    private double elec_coupon_amt;//电子优惠券抵用金额
    private int inner_card_count;//
    private double inner_card_amt;//
    private int self_weixin_count;//
    private int self_zfb_count;//
    private int self_citycard_count;//
    private int escape_repay_count;//
    private double escape_repay_amt;//
    private int yu_epay_count;//停哪儿余额支付次数
    private double yu_epay_amt;//停哪儿余额支付金额
    private double noclear_ec_amt;//线上券不结算金额
    private int noclear_ec_count;//线上券不结算数量
    private double freepay_guest_amt;//代客缴费免金额支付金额
    private int freepay_guest_count;//代客缴费免金额支付次数
    private long cash_to_elec_cnt;//现金转电子次数
    private double cash_to_elec_amt;//现金转电子总额


}
