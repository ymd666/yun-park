package cn.tnar.parkservice.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.OBJ_ADAPTER;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TParkDashboardMapper {

    /**
     * 现金收入统计
     */
    Map<String,Object> cashCount(@Param("param") Map<String,Object> map);

    /**
     *电子收入统计
     */
    Map<String,Object> ePayCount(@Param("param") Map<String,Object> map);

    /**
     *停车场车位数统计
     */
    Map<String, Object> parkSeatCount(@Param("param") Map<String,Object> map);

    /**
     *区域车位统计
     */
    List<Map<String, Object>> regionSeatCount(@Param("param") Map<String,Object> map);

    /**
     *区域占用车位统计
     */
    List<Map<String,Object>> regionBusySeatCount(@Param("param") Map<String,Object> map);

    /**
     *异常抬杆统计
     */
    Map<String,Object> unNormalCar(@Param("param") Map<String,Object> map);

    /**
     * 当日临停车统计查询
     */
    Map<String,Object> dayTempCar(@Param("startDate")String startDate,@Param("endDate")String endDate,@Param("parkCode")String parkCode);

    /**
     * 当日访客车统计查询
     */
    Map<String,Object> dayVisiterCar(@Param("startDate")String startDate,@Param("endDate")String endDate,@Param("parkCode")String parkCode);

    /**
     * 当日自定义卡组统计查询
     */
    Map<String,Object> dayMemCar(@Param("startDate")String startDate,@Param("endDate")String endDate,@Param("parkCode")String parkCode);

    /**
     * 最近一周临停车统计查询
     */
    Map<String,Object> hisTempCar(@Param("startDate")String startDate,@Param("endDate")String endDate,@Param("parkCode")String parkCode);

    /**
     * 最近一周访客车统计查询
     */
    Map<String,Object> hisVisiterCar(@Param("startDate")String startDate,@Param("endDate")String endDate,@Param("parkCode")String parkCode);

    /**
     * 最近一周自定义卡组统计查询
     */
    Map<String,Object> hisMemCar(@Param("startDate")String startDate,@Param("endDate")String endDate,@Param("parkCode")String parkCode);

    /**
     * 当天进出场卡组统计分析
     */
    List<Map<String,Object>> groupCarCount(@Param("param")Map<String,Object> param);

    /**
     * 当天进出场自定义卡组分析
     */
    Map<String,Object> diyMemCarCount(@Param("parkCode")String parkCode,@Param("parentId")Long parentId,
                                      @Param("startTime")String startTime,@Param("endTime") String endTime);

    /**
     * 查询所有的自定义卡组分类
     */
    List<Map<String, Object>> getDiyMemGroup(String parkCode);
}
