package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class MemberinfoMqDto {
    private String car_id;
    private String car_state = "1";
    private String car_type;
    private String end_date;
    private String group_id;
    private String id;
    private String member_level = "1";
    // 1 白名单
    // 3 新版会员
    // 7 黑名单
    // 8访客车
    private String member_type;

    //    新增会员车位信息 1
    //    删除会员车位 2
    //    启用车位 3
    //    暂停车位 4
    //    车位延期 5
    //    修改会员信息 6
    private String oper_type;
    private String owner_name;
    private String park_code;
    private String region_code="";
    private String remark;
    private String seat_num;
    private String send_time;
    private String start_date;

}
