package cn.tnar.parkservice.config;


/**
 * @author xzhang
 * @date 2019-09-07
 */
public class ParkConstant {

    /**
     * 停车场信息查询接口号
     */
    public static final String API_PARK_INFO = "85101004";


    public static final String API_GET_MOBILE_KEY = "82101060";

    public static final String API_GET_MOBILE_CODE = "82101015";

    /**
     * 查找用户绑定车牌信息
     */
    public static final String API_QUERY_BIND_CAR_INFO = "82206040";


    /**
     * 绑定车牌
     */
    public static final String API_BIND_CAR_PLATE_NO = "82202016";


    /**
     * 用户注册接口
     */
    public static final String API_USER_REGISTRY = "82101030";


    /**
     * 查询停车记录并计费
     */
    public static final String API_QUERY_PARKING_CALC_FEE = "87202100";


    /**
     * 查询停车记录
     */
    public static final String API_QUERY_PARKING = "87202011";

    /**
     * 计算费用
     */
    public static final String API_CALC_FEE = "87202006";


    /**
     * 查询用户最新领取的一张优惠券
     */
    public static final String API_QUERY_LATEST_COUPON = "";

    /**
     * 领取优惠券
     */
    public static final String API_GET_COUPON = "82202016";

}
