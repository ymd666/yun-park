package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Bao
 * @date 2018-10-18
 */
@ApiModel("查询逃逸列表请求")
@Data
public class BadDebtRequest {

    @ApiModelProperty("车牌号")
    private String carId;

    @ApiModelProperty("开始时间")
    private Long startTime;

    @ApiModelProperty("结束时间")
    private Long endTime;

    private int pageNo;

    private int pageSize;

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "BadDebtRequest{" +
                "carId='" + carId + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                '}';
    }
}
