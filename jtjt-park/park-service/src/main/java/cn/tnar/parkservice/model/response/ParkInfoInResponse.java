package cn.tnar.parkservice.model.response;

import lombok.Data;

@Data
public class ParkInfoInResponse {
    private String parkCode;

    private String name;
}
