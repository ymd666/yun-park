package cn.tnar.parkservice.config;

/**
 * @author kanwen
 * @description 导出Excel的Title常量
 * @data 2019-03-18 21:43:05
 */
public class ExportConstant {

    /**
     * 欠款补交记录
     */
    public static final String[] OVERDUE_PAYMENT_TITLE = {"序号","车牌号","收费员","区域","入场时间","出场时间","停车时长","应收金额","实收金额","补交时间","是否缴费"};
    /**
     * 坏账记录
     */
    public static final String[] BAD_DEBT_TITLE = {"序号","车牌号","收费员","区域","入场时间","出场时间","停车时长","欠费金额","坏账时间","备注"};

    /**
     * 订单明细 - 交易
     */
    public static final String[] ORDER_DETAIL_TRADE = {"序号","订单号","商户号","交易时间","交易金额","手续费"};
    /**
     * 订单明细 - 长款
     */
    public static final String[] ORDER_DETAIL_LONG = {"序号","订单号","商户号","交易时间","交易金额","差异类型"};
    /**
     * 订单明细 - 短款
     */
    public static final String[] ORDER_DETAIL_SHORT = {"序号","订单号","商户号","交易时间","交易金额", "差异类型"};
    /**
     * 订单明细 - 合并支付
     */
    public static final String[] ORDER_DETAIL_MERGE = {"序号","订单号","商户号","交易时间","交易金额","手续费","合并笔数"};

    /**
     * 收费员排班报表 - 表头_1
     */
    public static final String[] TOLL_AUTOMATED_1 = {"序号", "批次号", "收费员", "收费员", "上下班时间", "上下班时间", "上下班时间", "收费总额", "优惠劵金额", "长租车金额", "合计"};

    /**
     * 收费员排班报表 - 表头_1_合并单元格
     */
    public static final String[] TOLL_AUTOMATED_1_HEAD = {"0,1,0,0", "0,1,1,1", "0,0,2,3", "0,0,4,6", "0,1,7,7", "0,1,8,8", "0,1,9,9", "0,1,10,10"};

    /**
     * 收费员排班报表 - 表头_2
     */
    public static final String[] TOLL_AUTOMATED_2 = {"收费员工号", "收费员姓名", "上班时间", "下班时间", "工作时长"};

    /**
     * 收费员排班报表 - 表头_2_合并单元格
     */
    public static final String[] TOLL_AUTOMATED_2_HEAD = {"1,1,2,2", "1,1,3,3", "1,1,4,4", "1,1,5,5", "1,1,6,6"};

    /**
     * 收费员日统计报表
     */
    public static final String[] TOLL_DAILY_REPORT = {"序号", "收费员姓名", "收费总额", "优惠劵", "长租车", "合计"};
    //退款记录
    public static final String[] Refund_title = {"序号","订单号","停车场名称","车牌号","入场时间","出场时间","退款金额","发起人"};
    public static final String[] CALENDAR_TITLE = {"日期(YYYY-MM-DD)", "星期几", "是否为节假日(Y/N)"};

    /**
     * 卡组管理-访客车导出
     */
    public static final String[] visit_title = {"序号","车牌号","姓名","手机号","缴费标准","预约来访时间","状态"};

    /**
     * 卡组管理-白名单
     */
    public static final String[] white_title = {"序号","姓名","联系方式","绑定车牌","开始日期","结束日期","状态"};
    /**
     * 卡组管理-黑名单
     */
    public static final String[] black_title = {"序号","车牌号","姓名","联系方式","通行区域限制","开始限制日期","结束限制日期","备注"};
    /**
     * 卡组管理-其他卡
     */
    public static final String[] other_title = {"序号", "停车场", "会员姓名", "联系方式", "车牌号","交费标准", "开始日期", "到期日期", "车位状态"};

    /**
     * 卡组管理-其他卡2 开启满免的卡组
     */
    public static final String[] other_title2 = {"序号", "停车场", "会员姓名", "联系方式", "车牌号","交费标准", "开始日期", "到期日期", "车位状态","累积满免金额"};


    /**
     * 卡组管理-其他卡-预约车
     */
    public static final String[] other_title_four = {"序号", "停车场", "会员姓名", "联系方式", "车牌号","交费标准","过期时间"};


    /**
     * 访客管理-访客审批
     */
    public static final String[] visit_apply_title = {"序号", "访客", "车牌号", "手机号", "车辆类型", "申请时间", "来访时间", "被访者", "状态"};

    /**
     * 访客管理-访客权限
     */
    public static final String[] visit_jurisdiction_title = {"序号", "被访人员", "手机号", "申请时间", "状态"};

    /**
     * 客户管理-交费查询
     */
    public static final String[] payment_query_title = {"序号", "车牌号", "车主", "收费标准", "交费金额", "发生时间", "初始到期时间", "变更到期时间"};

    /**
     * 人员管理-收费统计-收费员排班报表
     */
    public static final String[] toll_shift_title = {"序号", "收费员工号", "收费员姓名", "上班时间", "下班时间", "工作时长", "收费总额", "优惠券金额", "长租车金额", "合计"};


    /**
     * 人员管理-收费统计-日统计报表
     */
    public static final String[] day_statistics_title = {"序号", "收费员姓名", "收费总额", "优惠券", "长租车", "合计"};


    /**
     * 人员管理-考勤管理-出勤统计
     */
    public static final String[] attendance_statistics_title = {"序号", "停车场", "姓名", "账号", "角色名称", "出勤日期", "上班时间", "下班时间", "签到时间", "签出时间", "上班状态", "代班人"};

    /**
     * 人员管理-考勤管理-收费员账号
     */
    public static final String[] toll_account_title = {"序号", "工号", "姓名"};

    /**
     * 订单管理-订单查询
     */
    public static final String[] order_query_title = {"序号", "车牌号", "交易时间", "缴费类型", "支付渠道","交易金额","手续费"};
    /**
     * 订单管理-欠费记录-欠费记录
     */
    public static final String[] order_arrearage_title = {"序号", "车牌号", "收费员", "区域", "入场时间","出场时间","停车时长","应收金额","实收金额"};

    /**
     * 订单管理-欠费记录-欠费补交记录
     */
    public static final String[] order_arrearage_pay_title = {"序号", "车牌号", "收费员", "区域", "入场时间","出场时间","停车时长","应收金额","实收金额","补交时间","是否缴费"};

    /**
     * 订单管理-欠费记录-坏账记录
     */
    public static final String[] order_badDebt_title = {"序号", "车牌号", "收费员", "区域", "入场时间","出场时间","停车时长","欠费金额","坏账时间","备注"};
    /**
     * 订单管理-异常订单
     */
    public static final String[] order_abNormal_title = {"序号", "车牌号", "入场时间","出场时间","应收金额","实收金额","异常类型","处理状态"};
     /**
     * 订单管理-停车流水(tab 1 全部收入)
     */
    public static final String[] order_record_title_one = {"序号", "车牌号", "入场时间","出场时间","停车时长(分钟)","交易金额","支付方式","异常类型"};
    /**
     * 订单管理-停车流水(tab 2 过场车 3电子支付  5优惠券)
     */
    public static final String[] order_record_title_two = {"序号", "车牌号", "入场时间","出场时间","停车时长(分钟)","交易金额","优惠金额","支付方式"};
    /**
     * 订单管理-停车流水(tab 4长租车)
     */
    public static final String[] order_record_title_four = {"序号", "车牌号", "入场时间","出场时间","停车时长(分钟)","交易金额","卡证优免金额","支付方式","异常类型"};

    /**
     * 订单管理-停车流水(tab 6异常)
     */
    public static final String[] order_record_title_six = {"序号", "车牌号", "入场时间","出场时间","停车时长(分钟)","支付方式","异常类型"};
    /**
     * 订单管理-停车流水(tab 7停车中)
     */
    public static final String[] order_record_title_seven = {"序号", "车牌号", "入场时间","出场时间","停车时长(分钟)"};
    /**
     * 财务管理-电子发票统计
     */
    public static final String[] financia_statistics_title = {"序号", "发票类型", "发票抬头", "发票金额", "申请时间","开票人账号","联系方式","开票状态","开票时间"};
    /**
     * 财务管理-财务报表-日汇总
     */
    public static final String[] financia_title_day = {"停车场",	"日期",	"临停总额",	"临停笔数",	"电子支付总额",	"电子支付笔数",	"套餐收入总额",	"套餐收入笔数",	"支出总额",	"支出笔数",	"期初可提现余额",	"期末可提现余额",	"支付宝总额",	"支付宝笔数",	"微信总额",	"微信笔数",	"ETC总额",	"ETC笔数",	"电子券总额",	"电子券笔数",	"结算总额",	"结算笔数",	"不结算总额",	"不结算笔数",	"异常流水实收总额",	"异常流水实收笔数",	"冲正总额",	"冲正笔数",	"提现总额",	"提现笔数",
            "微信/支付宝手续费总额","微信/支付宝手续费笔数"};
    public static final String[] financia_title_month = {"停车场",	"日期",	"临停总额",	"临停笔数",	"电子支付总额",	"电子支付笔数",	"套餐收入总额",	"套餐收入笔数",	"支出总额",	"支出笔数",	"期初可提现余额",	"期末可提现余额","电子券总额",	"电子券笔数",	"结算总额",	"结算笔数",	"不结算总额",	"不结算笔数",	"异常实收总额",	"异常流水实收笔数",	"冲正总额",	"冲正笔数",	"提现总额",	"提现笔数",
            "微信/支付宝手续费总额","微信/支付宝手续费笔数"};
}
