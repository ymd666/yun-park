package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TParkFeeIndex;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:36:07
 */

@Mapper
@Repository
public interface TParkFeeIndexMapper extends BaseMapper<TParkFeeIndex> {

    @Select("SELECT regionname from  t_sys_region where region_code = #{regionCode} limit 1")
    String getRegionNameFromSysReg(@Param("regionCode") String regionCode);
}
