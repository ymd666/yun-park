package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.request.ParkDayHighparkingRecordRequest;
import cn.tnar.parkservice.model.entity.TApplyTicketAcctInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zzb
 * @since 2019-03-27
 */
public interface ITApplyTicketAcctInfoService extends IService<TApplyTicketAcctInfo> {

    Page<TApplyTicketAcctInfo> query(ParkDayHighparkingRecordRequest info);
}
