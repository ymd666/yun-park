package cn.tnar.yunpark.xunlang;

/**
 * author : CaineZhu
 * date   : 2017/12/26 ${time}
 * desc   : class desc
 */
public class XunLangUtil {

    public static String toLbmStatus(int status) {
        switch (status) {
            case 0:
                return "1";
            case 1:
                return "2";
            default:
                return null;    // 未知状态不报
        }
    }
}
