package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询出勤统计 请求类
 *
 * @author xinzhang
 * @since 2019-08-13
 */
@Data
public class ParkWorkLogRequset {

    @ApiModelProperty(value = "停车场编码", example = "")
    private String parkCode;

    @ApiModelProperty(value = "姓名", example = "")
    private String name;

    @ApiModelProperty(value = "开始日期", example = "20171018")
    private long starTime;

    @ApiModelProperty(value = "结束日期", example = "20181018")
    private long endTime;

    @ApiModelProperty(value = "上班状态", example = "0")
    private int isvalid;

    @ApiModelProperty(value = "当前页", example = "1")
    private int current;

    @ApiModelProperty(value = "每页大小", example = "10")
    private int size;
}
