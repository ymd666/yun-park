package cn.tnar.yunpark.service;

import java.util.Map;

public interface BayService {

	/**
	 * 批量同步地磁状态
	 * @param params   权限 key privilegekey  , 数量 baynum  ,  数据串	datas --格式：地磁编号,状态,更新日期|地磁编号,状态,更新日期|地磁编号,状态,更新日期
     */
	public void BatchSyncBayStatus(Map<String, String> params);


}
