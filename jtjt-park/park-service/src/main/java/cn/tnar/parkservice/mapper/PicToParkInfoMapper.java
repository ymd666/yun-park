package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.dto.AcctBasePhotoDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**
 * @Author: ZhangZhibiao
 * @Date:2018-11-15
 *
 * */
@Repository
@Mapper
public interface PicToParkInfoMapper {
    @Update("update t_park_info set photokey=#{photoKey} where park_code=#{parkCode}")
    int insertPicToParkInfo(@Param("photoKey") String photoKey, @Param("parkCode") String parkCode);

    @Select("select photokey from t_park_info where park_code=#{parkCode}")
    String getPhotoKeyFrom(@Param("parkCode") String parkCode);

    @Select("select id from t_park_info where park_code=#{parkCode}")
    int selectId(@Param("parkCode") String parkCode);

    int insertPicToBasePhoto(@Param("dto") AcctBasePhotoDto dto);

    @Update("update t_park_info set photokey='' where park_code=#{parkCode} ")
    int delPhotoF(@Param("parkCode") String parkCode);

    @Delete("DELETE FROM t_acct_base_photo WHERE id=#{id}")
    int delPhotoX(@Param("id") int id);
}
