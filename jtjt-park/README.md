# Spring Cloud Alibaba Demo 文档

- [security解读](docs/security/oauth2.md)
- [依赖调查](docs/dependency/dependency.md)
- [源码解读](docs/source-code/index.md)

## 环境

- JDK: [JDK 11](https://adoptopenjdk.net/) || [JDK 8](https://github.com/alibaba/dragonwell8)
- IDE: [idea 2019.1 +](https://www.jetbrains.com/idea/) || [eclipse](https://www.eclipse.org/downloads/packages/)
- DB: MySQL 5.7+
- Build Tools: [Gradle-5.4.1+](https://gradle.org/) 项目中直接使用了 `Gradle Wrapper` 不需要本地下载。

## [Nacos](https://github.com/alibaba/nacos)

### 下载
下载 `https://github.com/alibaba/nacos/releases` 最新稳定版(当前 1.1.0)。

### 数据库初始化

目前只支持 mysql

```shell
wget https://github.com/alibaba/nacos/releases/download/1.1.0/nacos-address-server-1.1.0.zip
unzip nacos-server-1.0.0.zip
```

<!-- ```shell
mysql -upark -p
``` -->

``` sql
# 创建 nacos 配置数据库
CREATE DATABASE `nacos_config` CHARACTER SET utf8 COLLATE utf8_general_ci;

use nacos_config;
# 执行 nacos 目录下 conf 下 nacos-mysql.sql
source /path_to_nacos_conf/nacos-mysql.sql;
```

### 配置链接数据库

```properties
spring.datasource.platform=mysql

db.num=1
db.url.0=jdbc:mysql://192.168.110.197:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=
db.password=
```

![nacos_config 数据表](./img/nacos-config-db.png)

### 单机启动

```shell
./startup.sh -m standalone    
```

### 集群的建立

创建 `nacos/conf/cluster.conf`，填入集群中的 `ip:port`。

### 启动

没有参数就是以集群模式启动，分别在各个节点启动 **nacos**
```shell
sh startup.sh
```

![nacos_cluster.jpg](./img/nacos_cluster_mode.jpg)

上述已经完成了集群各节点启动，按照上图还差一个统一入口。这里可以使用 *nginx* 实现。


http://192.168.110.197:8888/nacos/v1/ns/catalog/instances?serviceName=service-provider&clusterName=DEFAULT&pageSize=10&pageNo=1&namespaceId=

### 基本概念

#### 命名空间（Namespace）

常用场景之一是不同环境的配置的区分隔离，例如开发测试环境和生产环境的资源（如配置、服务）隔离等。

#### 配置集 ID（Data ID）

通常用于组织划分系统的配置集。一个系统或者应用可以包含多个配置集，每个配置集都可以被一个有意义的名称标识。Data ID 通常采用类 Java 包（如 com.taobao.tc.refund.log.level）的命名规则保证全局唯一性。此命名规则非强制。

#### 配置分组（Group）

通过一个有意义的字符串（如 Buy 或 Trade ）对配置集进行分组，从而区分 Data ID 相同的配置集。配置分组的常见场景：不同的应用或组件使用了相同的配置类型，如 database_url 配置和 MQ_topic 配置。

#### 服务名（Service Name）

服务提供的标识，通过该标识可以唯一确定其指代的服务。

![nacos 数据模型](img/nacos_data_model.jpg)

## 服务注册与发现

使用  `@EnableDiscoveryClient` 开启服务注册与发现。

在 `properties` 或 `y(a)ml` 文件中加入:

```properties
spring.cloud.nacos.discovery.server-addr=ip1:port1,ip2:por2,....
spring.application.name=servcie-name
```

服务启动后:

![服务列表](./img/service_list.png)


![服务详情](./img/service_detail.png)

## 配置中心

配置中心使用的也是 `nacos`。注册中心的配置相对比较简单, 根据 `spring cloud` 在 `bootstrap.properties` 中配置：

```properties
spring.application.name=service-name
spring.cloud.nacos.config.server-addr=ip1:port, ip2:port, ...
spring.cloud.nacos.discovery.server-addr=ip1:port, ip2:port, ...
```

<!-- 在 `application.yml` 中加入:

```yaml
management:
  endpoints:
    web:
      exposure:
        include: "*"
``` -->

在服务启动过程中会从 `nacos` 取所需的配置，如果无法取出配置又没有配置默认值则无法启动服务。

![nacos 配置列表](img/nacos_config_list.png)


![nacos 配置详细](img/nacos_config_detail.png)

**编辑配置明细会自动刷新到服务中。**

``` log
2019-05-30 17:21:12.361  INFO 7131 --- [           main] o.s.c.a.n.c.NacosPropertySourceBuilder   : Loading nacos data, dataId: 'msf-common.properties', group: 'DEFAULT_GROUP'
2019-05-30 17:21:12.367  INFO 7131 --- [           main] b.c.PropertySourceBootstrapConfiguration : Located property source: CompositePropertySource {name='NACOS', propertySources=[NacosPropertySource {name='consumer-demo.properties'}, NacosPropertySource {name='msf-common.properties'}]}
2019-05-30 17:21:12.373  INFO 7131 --- [           main] c.t.m.d.c.ConsumerDemoApplication        : No active profile set, falling back to default profiles: default
```

### 配置加载方式：

所有配置都在 `spring.cloud.nacos.config.` 前缀后。
| C2
- A: shared-dataids: **共享配置**的 `data-id`
  - refreshable-dataids: 共享配置的 Data Id 在配置变化时，应用中是否可动态刷新， 感知到最新的配置值
- B: ext-config[n].data-id: 扩展 `data-id`
- C: 内部相关规则(应用名、应用名+ Profile )自动生成相关的 Data Id 配置
  ```
  ${spring.application.name}-${profile}.${file-extension:properties}
  ```

加载优先： A < B < C
  
  

## 服务调用

服务调用使用 `feign`。需要加入 `@EnableFeignClients` 注解。

``` java
@FeignClient("service-provider")
public interface EchoService {

    @GetMapping("/echo/{str}")
    String echo(@PathVariable String str);
}
```

`@FeignClient("xxx")` 用来创建一个 **Ribbon load balancer**。创建的 **Ribbon client** 发现 **xxx** 服务的物理地址。

核心是 `named client`。 
每个 `feign client` 都是一组组件的一部分，这些组件按需联系远程服务器来协同工作。每个组件都通过 `@FeignClien` 制定一个名字。
每个 `feign` 组件包括 `feign.Decoder`, `feign.Encoder`, `feign.Contract`。


``` java
@FeignClient(name = "xxx", configuration = XxxConfiguration.class)
public interface XxxClient {
    // ...
}
```
上例可以通过自定义 `XxxConfiguration`(`FeignClientsConfiguration`) 对`feign` 进行完全控制。


`Ribbon` 客户端负载均衡器（`HTTP`, `TCP`）。


`RibbonClientConfiguration`，
每个 `Ribbon` 包含 `ILoadBalancer`, `RestClient` 和 `ServerListFilter`。


- Rule: 决定从服务器列表返回哪个服务器的逻辑组件(`NFLoadBalancerRuleClassName`)
  - RoundRobineRule: 循环、轮流规则
  - AvailabilityFilteringRule: 跳过被认为是 `circuit tipped`（默认触发条件是最后 3 次连接失败，触发后该状态维持 30s。之后继续连接失败时） 或者大并发的连接。
  - WeightedResponseTimeRule: 每个服务器会根据平均返回时间给出一个权重值。
- Ping： 在后台保证服务的活性的组件
- ServerList: 服务器列表可静态也可动态（`DynamicServerListLoadBalancer`，后台线程定时刷新）(NIWSServerListClassName)
  - 种类：
    - 静态略过
    - DiscoveryEnabledNIWSServerList: 从注册中心获取
  - ServerListFilter: 被用于 `DynamicServerListLoadBalancer` 动态返回服务器列表中过滤服务器 
    - ZoneAffinityServerListFilter: 如果客户端的 `client zone` 中有可用的 `server`则过滤不在同一个 `zone` 中的 `server`
    - ServerListSubsetFilter: 只能见到 `server list` 固定的子集。


## [Sentinel](https://github.com/alibaba/Sentinel)

由于 `Hystrix` 进入**维护状态**， 使用 `Sentinel`。

由于使用 `feign` 进行服务间调用，需要配置 `feign` 对 `Sentinel` 的支持。

``` properties
feign.sentinel.enabled=true
```
### 基本概念

- 资源： 可以是 Java 应用程序中的任何内容。可以是**服务**（调用方，被调用方），**一段代码**等等。
- 规则： 围绕资源的实时状态设定的规则
    - 流量控制规则：用于调整网络包的发送数据。
    - 熔断降级规则：对不稳定资源的调用进行限制，并让请求快速失败，避免影响到其它的资源。
        - 并发线程数控制：不同于 `Hystrix` 的**隔离线程池**。
        - 响应时间进行降级：相应时间过长，所有对该资源的访问直接被拒绝，等到制定的时间窗口后才重新恢复。
    - 系统保护规则： 防止雪崩。让系统的入口流量和系统的负载达到一个平衡。

### 使用方式

#### 主流框架的适配
#### 抛出异常式 (`BlockException`)
```java
// 资源名可使用任意有业务语义的字符串，比如方法名、接口名或其它可唯一标识的字符串。
try (Entry entry = SphU.entry("resourceName")) {
    // 被保护的业务逻辑
    // do something here...
} catch (BlockException ex) {
    // 资源访问阻止，被限流或被降级
    // 在此处进行相应的处理操作
}
```

#### 返回 true/false
```java
// 资源名可使用任意有业务语义的字符串
if (SphO.entry("自定义资源名")) {
    // 务必保证finally会被执行
    try {
        /**
         * 被保护的业务逻辑
         */
    } finally {
        SphO.exit();
    }
} else {
    // 资源访问阻止，被限流或被降级
    // 进行相应的处理操作
}
```
#### 注解
```java
// 原本的业务方法.
@SentinelResource(blockHandler = "blockHandlerForGetUser")
public User getUserById(String id) {
    throw new RuntimeException("getUserById command failed");
}

// blockHandler 函数，原方法调用被限流/降级/系统保护的时候调用
public User blockHandlerForGetUser(String id, BlockException ex) {
    return new User("admin");
}
```

#### 异步(TODO)

### 规则

![rule-uml](img/sentinel-rule-uml.png)


Field | 说明 |
---------|----------
 Aresource | 资源名，资源名是限流规则的作用对象
 limitApp | 流控针对的调用来源

#### 流量控制规则 (FlowRule)

**监控应用流量的 QPS 或并发线程数等指标，当达到指定的阈值时对流量进行控制，以避免被瞬时的流量高峰冲垮，从而保障应用的高可用性。**

[官方文档](https://github.com/alibaba/Sentinel/wiki/%E6%B5%81%E9%87%8F%E6%8E%A7%E5%88%B6)

```java
// grade 取值
// com.alibaba.csp.sentinel.slots.block.RuleConstant
// 线程数（并发数量）
public static final int FLOW_GRADE_THREAD = 0;
// QPS（默认）
public static final int FLOW_GRADE_QPS = 1;

// strategy 取值
// 资源自身
public static final int STRATEGY_DIRECT = 0;
// 其它关联资源 
public static final int STRATEGY_RELATE = 1;
// 链路入口
public static final int STRATEGY_CHAIN = 2;

// controlBehavior 取值
// 直接拒绝
public static final int CONTROL_BEHAVIOR_DEFAULT = 0;
// 预热/冷启动方式（缓慢增加，给系统预热时间）
public static final int CONTROL_BEHAVIOR_WARM_UP = 1;
// 匀速排队（漏桶算法）
public static final int CONTROL_BEHAVIOR_RATE_LIMITER = 2;
// 匀速慢热？
public static final int CONTROL_BEHAVIOR_WARM_UP_RATE_LIMITER = 3;

```

#### 熔断降级规则 (DegradeRule)

**对调用链路中不稳定的资源进行熔断降级也是保障高可用的重要措施之一**

**在调用链路中某个资源出现不稳定状态时（例如调用超时或异常比例升高），对这个资源的调用进行限制，让请求快速失败，避免影响到其它的资源而导致级联错误。**

当资源被降级后，在接下来的**降级时间窗口**之内，对该资源的调用都**自动熔断**（默认行为是抛出 `DegradeException`）。

[官方文档](https://github.com/alibaba/Sentinel/wiki/%E7%86%94%E6%96%AD%E9%99%8D%E7%BA%A7)

```java
// grade 取值
// com.alibaba.csp.sentinel.slots.block.RuleConstant
// 平均响应时间(ms)
public static final int DEGRADE_GRADE_RT = 0;
// 异常比例 
public static final int DEGRADE_GRADE_EXCEPTION_RATIO = 1;
// 异常数（1 分钟）
public static final int DEGRADE_GRADE_EXCEPTION_COUNT = 2;
```

#### 系统保护规则 (SystemRule)

[官方文档](https://github.com/alibaba/Sentinel/wiki/%E7%B3%BB%E7%BB%9F%E8%87%AA%E9%80%82%E5%BA%94%E9%99%90%E6%B5%81)

让系统的**入口流量**和**系统的负载**达到一个**平衡**，让系统尽可能跑在最大吞吐量的同时保证系统整体的稳定性。

**在系统不被拖垮的情况下，提高系统的吞吐率，而不是 load 一定要到低于某个阈值。**

用 load1 作为启动控制流量的值，而允许通过的流量由处理请求的能力，即请求的响应时间以及当前系统正在处理的请求速率来决定。

#### 访问控制规则 (AuthorityRule)

根据调用方来限制资源是否通过，类似黑白名单。请求来源（origin）限制资源是否通过，若配置白名单则只有请求来源位于白名单内时才可通过；若配置黑名单则请求来源位于黑名单时不通过，其余的请求通过。

```java
// strategy 取值
// com.alibaba.csp.sentinel.slots.block.RuleConstant
public static final int AUTHORITY_WHITE = 0;
public static final int AUTHORITY_BLACK = 1;
```

#### 热点规则 (ParamFlowRule)

### 流程

![sentinel-workflow](img/sentinel-product-flow.png)

现经过对于 `Sentinel Dashboard` 的改造已经实现了上图流控流程。

## 全链路跟踪

<!-- ### Jaeger

![Jaeger Architecture](img/jaeger-constructure.png)

- jaeger-client：客户端直接集成在目标 Application 中，其作用是记录和发送 Span 到 Jaeger Agent。
- jaeger-agent：暂存 Jaeger Client 发来的 Span，并批量向 Jaeger Collector 发送 Span。
- jaeger-collector：接受 Jaeger Agent 发来的数据，并将其写入存储后端。
- jaeger-query & jaeger-ui：读取存储后端中的数据，以直观的形式呈现。


![jaeger-release](img/jaeger-release.png)

在 demo 中直接执行 `jaeger-all-in-one`, 在生产环境中需要进行集群化部署。

在整合进 `Srping` 中的过程中，有官方的轮子。

```xml
<dependency>
    <groupId>io.opentracing.contrib</groupId>
    <artifactId>opentracing-spring-jaeger-cloud-starter</artifactId>
</dependency>
```

同时在配置文件中配置：

```properties
opentracing.jaeger.http-sender.url=http://...
```

**_TODO: 对于每一个请求，都会有一个贯穿整个请求流程的 Request ID。在 Trace 的起始处，将 Trace ID 设置为 Request ID。与日志系统整合。_**


-->
### [SkyWalking](https://github.com/apache/skywalking)

分布式系统的应用程序性能监视工具，专为微服务、云原生架构和基于容器（Docker、K8s、Mesos）架构而设计。

![skywalking-bin](img/skywalking-bin.png)

- 数据采集和发送: 探针、SDK。（`SkyWalking` 使用**自动探针为主**, `Zipkin` 使用的 **Java 手动探针**）。
- 数据收集、分析、汇总、告警和存储: Collector 模块。
- 展现： UI

![skywalking-architecture](img/skywalking-architecture.jpeg)


使用无侵入式设计，不需要改代码，只需要在启动中加入 `javaagent` 即可:

```
-javaagent:/path/to/skywalking-package/agent/skywalking-agent.jar
```


#### 样例

官方示例，比较简单的 `spring cloud` 服务调用。

- 使用 `eureka` 进行服务注册 & 发现。
- `projectA` 调用 `projectB` 和 `projectC`。
- `projectB` 会往 `H2` 数据库中存入一条记录。
- `projectC` 会使用 `Apache HttpClient` 请求 `baidu.com`。

其中 `projectB` 对于 `H2` 的操作加入了依赖： 

```
<dependency>
    <groupId>org.apache.skywalking</groupId>
    <artifactId>apm-toolkit-trace</artifactId>
</dependency>
```

代码如下：

```java
@Trace
public void saveUser(String name) {
    ActiveSpan.tag("user.name", name);
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    try {
        connection = dataSource.getConnection();
        preparedStatement = connection.prepareStatement("INSERT INTO user(name) VALUES(?)");
        preparedStatement.setString(1, name == null ? "" : name);
        preparedStatement.execute();
    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
        }
    }
}

@Trace(operationName = "selectUser")
public void selectUser(String name) {
    ActiveSpan.tag("user.name", name);
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    try {
        connection = dataSource.getConnection();
        preparedStatement = connection.prepareStatement("SELECT * FROM user WHERE name =?");
        preparedStatement.setString(1, name == null ? "" : name);
        preparedStatement.execute();
    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
        }
    }
}
```


![skywalking 追踪](img/skywalking-trace.png)
![skywalking 拓扑](img/skywalking-topology.png)


### OpenTracing

```
一个tracer过程中，各span的关系


        [Span A]  ←←←(the root span)
            |
     +------+------+
     |             |
 [Span B]      [Span C] ←←←(Span C 是 Span A 的孩子节点, ChildOf)
     |             |
 [Span D]      +---+-------+
               |           |
           [Span E]    [Span F] >>> [Span G] >>> [Span H]
                                       ↑
                                       ↑
                                       ↑
                         (Span G 在 Span F 后被调用, FollowsFrom)


––|–––––––|–––––––|–––––––|–––––––|–––––––|–––––––|–––––––|–> time

 [Span A···················································]
   [Span B··············································]
      [Span D··········································]
    [Span C········································]
         [Span E·······]        [Span F··] [Span G··] [Span H··]
```


- Trace: 一个 `trace`代表了一个事务或者流程在（分布式）系统中的执行过程。在 `OpenTracing` 标准中，`trace` 是多个`span` 组成的一个**有向无环图（DAG）**，每一个 `span` 代表 `trace` 中被命名并计时的连续性的执行片段。
一个 `trace`代表了一个事务或者流程在（分布式）系统中的执行过程。在 `OpenTracing` 标准中，`trace` 是多个`span` 组成的一个**有向无环图（DAG）**，每一个 `span` 代表 `trace` 中被命名并计时的连续性的执行片段。
- Span: 一个 `span` 代表系统中具有**开始时间**和**执行时长**的逻辑运行单元。`span` 之间通过**嵌套**或者**顺序排列**建立逻辑因果关系。
- Operation Names: 操作名称（简单、高可读）。应该是一个抽象、通用的标识，能够明确的、具有统计意义的名称；更具体的子类型的描述，请使用Tags。
- Span References： `span` 之间的关系。
    - ChildOf
    - FollowsFrom
- Log：需要一个**带时间戳的时间名称**，以及可选的**任意大小的存储结构**。
- Tag： (key: value) 形式，简单的对span进行注解和补充。
- SpanContext: 代表跨越进程边界，传递到**下级span**的状态,用于封装 `Baggage`。在跨越进程边界，和在追踪图中创建边界的时候会使用。
- Baggage: 存储在`SpanContext` 中的一个键值对(SpanContext)集合。在一条追踪链路上的所有span内全局传输，包含这些span对应的SpanContexts。
- Inject and Extract：增加或获取跨进程通讯数据（HTTP头）。`SpanContexts` 可以跨越进程边界，并提供足够的信息来建立跨进程的 `span` 间关系。


## [日志(log4j2)](https://logging.apache.org/log4j/2.x/)

使用 `log4j2` 记录日志。日志模板待定。

### 全异步日志

![log4j2-performance](img/log4j2-async-throughput-comparison.png)


配置文件 `pom.xml`:

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <exclusions>
        <!-- 排除原有的 logging -->
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-logging</artifactId>
        </exclusion>
    </exclusions>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-log4j2</artifactId>
</dependency>

<!-- 全异步需要增加 disruptor -->
<dependency>
    <groupId>com.lmax</groupId>
    <artifactId>disruptor</artifactId>
    <version>${disruptor.version}</version>
</dependency>
```


启动时增加命令行增加

```bash
java -jar xxx.jar  -DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector
```

或者 `mvn` 增加

```bash
mvn spring-boot:run -DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector
```

未开启异步：

![log4j2-sync-write](img/log4j2-sync.png)


开启异步：

![log4j2-async-write](img/log4j2-async.png)