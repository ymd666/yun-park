package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkDayParkingRecordMapper;
import cn.tnar.parkservice.model.entity.TParkDayParkingRecord;
import cn.tnar.parkservice.service.TParkDayParkingRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月10日 11:48:01
 */
@Service("tParkDayParkingRecordServiceImpl")
public class TParkDayParkingRecordServiceImpl extends ServiceImpl<TParkDayParkingRecordMapper, TParkDayParkingRecord> implements TParkDayParkingRecordService {
}
