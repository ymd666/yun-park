package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TParkOfflineCoupons;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年10月28日 16:31:04
 */

public interface ITParkOfflineCouponsService extends IService<TParkOfflineCoupons> {
}
