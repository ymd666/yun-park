package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * Created by zhangkun on 2018/11/12.
 */
@Data
public class TparkInfoBaseResponse {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String parkCode;
    private String name;
}
