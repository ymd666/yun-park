package cn.tnar.yunpark.careyes.nbiot;

import cn.tnar.yunpark.service.SensorEventDispatcher;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * author : CaineZhu
 * date   : 2017/12/22 ${time}
 * desc   : class desc
 */

@Component
public class CarEyesNBUDPServer {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    Bootstrap boss = null;
    EventLoopGroup group = null;
    @Value("${tnar.careyes.udp.port}")
    private int port = 4702;
    @Autowired
    private SensorEventDispatcher dispatcher;


    public void server() {
        boss = new Bootstrap();
        group = new NioEventLoopGroup();
        try {
            boss.group(group)
                    .option(ChannelOption.SO_BROADCAST, true)//支持广播
                    .option(ChannelOption.SO_RCVBUF, 1024 * 1024)// 设置UDP读缓冲区为1M
                    .option(ChannelOption.SO_SNDBUF, 1024 * 1024)// 设置UDP写缓冲区为1M
                    .channel(NioDatagramChannel.class)
                    .handler(new ChannelInitializer<NioDatagramChannel>() {
                        @Override
                        protected void initChannel(NioDatagramChannel nioDatagramChannel) throws Exception {
                            ChannelPipeline channel = nioDatagramChannel.pipeline();
                            channel//.addLast("framer", new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer(new byte[]{(byte) 0xFF})))
                                    .addLast("decoder", new CarEyesNBMessageDecoder())
                                    //.addLast("decoder", new CarEyesTCPMessageDecoder())
                                    .addLast("handler", new CarEyesNBMessageHandler(dispatcher));
                        }
                    });
            System.out.println("==========careyes udp server started==========");
            boss.bind(port).sync().channel().closeFuture().await();
        } catch (Exception e) {
            logger.error("netty init Exception:" + e.getLocalizedMessage());
        }
    }

    //关闭监听
    public void stopThread() {
        if (null != group && !group.isShutdown())
            group.shutdownGracefully();

        if (null != boss)
            boss = null;

    }
}
