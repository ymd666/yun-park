package cn.tnar.parkservice.model.dto;

import lombok.Data;

@Data
public class CalendarDto {
    Integer year;
}
