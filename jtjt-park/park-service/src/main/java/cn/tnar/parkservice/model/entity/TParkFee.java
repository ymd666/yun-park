package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:20:35
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "TParkFee对象", description = "")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("t_park_fee")
public class TParkFee extends Model<TParkFee> {


    @TableId("id")
    private Long id;
    @TableField("node_id")
    private Integer nodeId;
    @TableField("index_id")
    private Long indexId;
    /**
     * 数据字典： 1-小型车 2-中型车 3-大型车 4-其它
     */
    @TableField("cartype")
    private Integer cartype;
    @TableField("user_type")
    private Integer userType;
    @TableField("worktype")
    private Integer worktype;
    /**
     * 单位（分钟）
     */
    @TableField("freetime")
    private Integer freetime;
    /**
     * 单位（分钟）
     */
    @TableField("freetime1")
    private Integer freetime1;
    /**
     * 单位（分钟）
     */
    @TableField("t_limit")
    private BigDecimal tLimit;
    /**
     * 单位（分/月）
     */
    @TableField("month")
    private BigDecimal month;
    /**
     * HHMMSS
     */
    @TableField("d_begin")
    private Integer dBegin;
    @TableField("d_end")
    private Integer dEnd;
    /**
     * 数据字典：1-按次数计费 2-按计时 3-按时阶梯计费 4-包月（自然月）保留 8：免费
     */
    @TableField("d_type")
    private Integer dType;
    /**
     * 单位（分）
     */
    @TableField("d_limit")
    private BigDecimal dLimit;
    /**
     * 单位（分/次）
     */
    @TableField("d_once")
    private BigDecimal dOnce;
    /**
     * 单位（分钟）
     */
    @TableField("d_unittime1")
    private Integer dUnittime1;
    /**
     * 单位（分钟）
     */
    @TableField("d_cycletime1")
    private Integer dCycletime1;
    /**
     * 单位（分）
     */
    @TableField("d_price1")
    private BigDecimal dPrice1;
    /**
     * 单位（分钟）
     */
    @TableField("d_unittime2")
    private Integer dUnittime2;
    /**
     * 单位（分钟）
     */
    @TableField("d_cycletime2")
    private Integer dCycletime2;
    /**
     * 单位（分）
     */
    @TableField("d_price2")
    private BigDecimal dPrice2;
    /**
     * 单位（分钟）
     */
    @TableField("d_unittime3")
    private Integer dUnittime3;
    /**
     * 单位（分钟）
     */
    @TableField("d_cycletime3")
    private Integer dCycletime3;
    /**
     * 单位（分）
     */
    @TableField("d_price3")
    private BigDecimal dPrice3;
    /**
     * 单位（分钟）
     */
    @TableField("d_unittime4")
    private Integer dUnittime4;
    /**
     * 单位（分钟）
     */
    @TableField("d_cycletime4")
    private Integer dCycletime4;
    /**
     * 单位（分）
     */
    @TableField("d_price4")
    private BigDecimal dPrice4;
    @TableField("d_preamt")
    private BigDecimal dPreamt;
    /**
     * HHMMSS
     */
    @TableField("n_begin")
    private Integer nBegin;
    @TableField("n_end")
    private Integer nEnd;
    /**
     * 数据字典：1-按次数计费 2-按计时 3-按时阶梯计费 4-包月（自然月）保留 8：免费
     */
    @TableField("n_type")
    private Integer nType;
    /**
     * 单位（分）
     */
    @TableField("n_limit")
    private BigDecimal nLimit;
    /**
     * 单位（分/次）
     */
    @TableField("n_once")
    private BigDecimal nOnce;
    /**
     * 单位（分钟）
     */
    @TableField("n_unittime1")
    private Integer nUnittime1;
    /**
     * 单位（分钟）
     */
    @TableField("n_cycletime1")
    private Integer nCycletime1;
    /**
     * 单位（分）
     */
    @TableField("n_price1")
    private BigDecimal nPrice1;
    /**
     * 单位（分钟）
     */
    @TableField("n_unittime2")
    private Integer nUnittime2;
    /**
     * 单位（分钟）
     */
    @TableField("n_cycletime2")
    private Integer nCycletime2;
    /**
     * 单位（分）
     */
    @TableField("n_price2")
    private BigDecimal nPrice2;
    /**
     * 单位（分钟）
     */
    @TableField("n_unittime3")
    private Integer nUnittime3;
    /**
     * 单位（分钟）
     */
    @TableField("n_cycletime3")
    private Integer nCycletime3;
    /**
     * 单位（分）
     */
    @TableField("n_price3")
    private BigDecimal nPrice3;
    /**
     * 单位（分钟）
     */
    @TableField("n_unittime4")
    private Integer nUnittime4;
    /**
     * 单位（分钟）
     */
    @TableField("n_cycletime4")
    private Integer nCycletime4;
    /**
     * 单位（分）
     */
    @TableField("n_price4")
    private BigDecimal nPrice4;
    @TableField("n_preamt")
    private BigDecimal nPreamt;
    @TableField("out_freetime")
    private Integer outFreetime;
}
