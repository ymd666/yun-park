package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkCalendarMapper;
import cn.tnar.parkservice.model.entity.TParkCalendar;
import cn.tnar.parkservice.service.ITParkCalendarService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年09月04日 19:51:46
 */
@Service("tParkCalendarServiceImpl")
public class TParkCalendarServiceImpl extends ServiceImpl<TParkCalendarMapper, TParkCalendar> implements ITParkCalendarService {


}
