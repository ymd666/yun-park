package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: xinZhang
 * @Date: 2019/10/16
 * @Description:
 */
@Data
public class TOrderResponse {

    private Long id;

    private String usingobj;

    private String ordertime;

    private Integer payType;

    private Integer tradetype;

    private BigDecimal orderamout;

    private BigDecimal tradeamount;

    private BigDecimal feeamt;

    private String inphoto;
}
