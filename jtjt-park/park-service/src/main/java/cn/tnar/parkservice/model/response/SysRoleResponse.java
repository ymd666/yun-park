package cn.tnar.parkservice.model.response;

import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/9/29
 * @Description:
 */
@Data
public class SysRoleResponse {

    private Long id;

    /**
     * 角色代码
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     *角色类型 1系统管理角色，2业务岗位角色，3个人角色
     */
    private Integer roleType;

    /**
     *角色允许分配最大人员数
     */
    private Integer maxAssignMembers;

    /**
     *备注
     */
    private String remarks;

    /**
     * 父节点id
     */
    private Long parentRoleId;
}
