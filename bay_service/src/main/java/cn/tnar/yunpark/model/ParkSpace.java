package cn.tnar.yunpark.model;

/**
 * 车位信息
 * Created by tieh on 2017/6/14.
 */
public class ParkSpace {
    /**
     * 停车场编号
     */
    private String parkCode;

    /**
     * 区域编号(路段)
     */
    private String regionCode;

    /**
     * 车位编号(完整)
     */
    private String spaceCode;

    /**
     * 车位短号(喷地上的)
     */
    private String spaceShortCode;

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getSpaceCode() {
        return spaceCode;
    }

    public void setSpaceCode(String spaceCode) {
        this.spaceCode = spaceCode;
    }

    public String getSpaceShortCode() {
        return spaceShortCode;
    }

    public void setSpaceShortCode(String spaceShortCode) {
        this.spaceShortCode = spaceShortCode;
    }
}
