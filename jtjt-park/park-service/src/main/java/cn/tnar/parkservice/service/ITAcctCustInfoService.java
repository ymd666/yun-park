package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TAcctCustInfo;
import cn.tnar.parkservice.model.request.TAcctCustInfoRequest;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface ITAcctCustInfoService extends IService<TAcctCustInfo> {

    /**
     * 注册渠道分析
     * @param openDate
     * @return
     */
    public List<Map<String,Object>> getStatisNumGroupForDays(String openDate);

    /**
     * 用户数活跃度可视化模块
     * @param beginWith
     * @param endWith
     * @return
     */
    public Map<String,Object> getStatisNumByTime(int beginWith, int endWith);

    /**
     * 查询密码
     * @return
     */
    String queryPassword(Long custId);

    /**
     * 查询系统账号列表
     */
    ResultJson querySysAccount(TAcctCustInfoRequest request);
}
