#!/usr/bin/env bash
name=bay-service

# link to jar without version
latestName=`ls -1 $name-*.jar | tail -n1`
echo $latestName
chmod +x $latestName
ln -sf $latestName $name.jar

# install as system service
# ln -sf `pwd`/$name.jar /etc/init.d/$name