package cn.tnar.parkservice.util;


import cn.tnar.parkservice.model.dto.OssAliyunField;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.PutObjectRequest;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


/**
 * @Author: 陈亮
 * @Date: 2018/9/26 14:49
 */
@Component
public class OssAliyunUtil {

    @Resource(name = "defaultOssAliyunField")
    private OssAliyunField defaultOssAliyunField;

    /**
     * 上传文件（选择默认的OSS配置）
     *
     * @param file
     * @return
     */
    public String upload(MultipartFile file) {
        String url = null;
        try {
            url = upload(defaultOssAliyunField, getKey(defaultOssAliyunField.getPrefix(), FileUtils.getSuffix(file)),
                    file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * 上传文件
     *
     * @param ossAliyunField 配置类，不同的配置类上传的配置就不一样
     * @param file
     * @return
     */
    public String upload(OssAliyunField ossAliyunField, MultipartFile file) {
        String url = null;
        InputStream inputStream =null;

        try {
            inputStream = file.getInputStream();
            BufferedImage bufImg = Thumbnails.of(inputStream).scale(1f).asBufferedImage();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            String frontFileName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
            ImageIO.write(bufImg, frontFileName, os); //图片写入到 ImageOutputStream
            inputStream = new ByteArrayInputStream(os.toByteArray());
            url = upload(ossAliyunField, getKey(ossAliyunField.getPrefix(), FileUtils.getSuffix(file)),
                    inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * 上传文件<>基础方法</>
     *
     * @param accessKeyId     授权 ID
     * @param accessKeySecret 授权密钥
     * @param bucketName      桶名
     * @param endpoint        节点名
     * @param styleName       样式名
     * @param key             文件名
     * @param inputStream     文件流
     * @return 访问路径 ，结果为null时说明上传失败
     */
    public String upload(String accessKeyId, String accessKeySecret, String bucketName, String endpoint, String
            styleName, String key, InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        String url = null;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try {
            // 带进度条的上传

           ossClient.putObject(new PutObjectRequest(bucketName, key, inputStream));
        } catch (OSSException oe) {
            oe.printStackTrace();
            key = null;
        } catch (ClientException ce) {
            ce.printStackTrace();
            key = null;
        } catch (Exception e) {
            e.printStackTrace();
            key = null;
        } finally {
            ossClient.shutdown();
        }
        if (key != null) {
            // 拼接文件访问路径。由于拼接的字符串大多为String对象，而不是""的形式，所以直接用+拼接的方式没有优势
            StringBuffer sb = new StringBuffer();
            sb.append("http://").append(bucketName).append(".").append(endpoint.replaceAll("-internal", "")).append("/").append(key);
            if (StringUtils.isNotBlank(styleName)) {
                sb.append("/").append(styleName);
            }
            url = sb.toString();
        }
        return url;
    }

    /**
     * 上传文件
     *
     * @param field       配置对象 此值从OssAliyunConfig当中选择的
     * @param key         文件名
     * @param inputStream 文件流
     * @return
     */
    public String upload(OssAliyunField field, String key, InputStream inputStream) {
        return upload(field.getAccessKeyId(), field.getAccessKeySecret(), field.getBucketName(), field
                .getEndPoint(), field.getStyleName(), key, inputStream);
    }

    /**
     * 删除单个文件<>基础方法</>
     *
     * @param accessKeyId     授权 ID
     * @param accessKeySecret 授权密钥
     * @param bucketName      桶名
     * @param endpoint        节点名
     * @param key             文件名
     */
    public void delete(final String accessKeyId, final String accessKeySecret, final String bucketName,
                       final String endpoint, final String key) {
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // 删除Object
        ossClient.deleteObject(bucketName, key);
        // 关闭client
        ossClient.shutdown();
    }

    /**
     * 删除单个文件
     *
     * @param field OSS相关配置
     * @param key   文件名
     */
    public void delete(OssAliyunField field, String key) {
        delete(field.getAccessKeyId(), field.getAccessKeySecret(), field.getBucketName(), field
                .getEndPoint(), key);
    }





    /**
     * 获取文件名（bucket里的唯一key）
     * 上传和删除时除了需要bucketName外还需要此值
     *
     * @param prefix 前缀（非必传），可以用于区分是哪个模块或子项目上传的文件
     * @param suffix 后缀（非必传）, 可以是 png jpg 等
     * @return
     */
    public static String getKey(final String prefix, final String suffix) {
        //生成uuid,替换 - 的目的是因为后期可能会用 - 将key进行split，然后进行分类统计
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        //文件路径
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String path = sdf.format(new Date()) + "-" + uuid;

        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + "-" + path;
        }
        if (suffix != null) {
            if (suffix.startsWith(".")) {
                path = path + suffix;
            } else {
                path = path + "." + suffix;
            }
        }
        return path;
    }


    public String getBaseKey(final String prefix, final String suffix,String base) {
        //生成uuid,替换 - 的目的是因为后期可能会用 - 将key进行split，然后进行分类统计
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        //文件路径
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String path = sdf.format(new Date()) + "-" + base;

        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + "-" + path;
        }
        if (suffix != null) {
            if (suffix.startsWith(".")) {
                path = path + suffix;
            } else {
                path = path + "." + suffix;
            }
        }
        return path;
    }




    public String uploadTXT(String accessId,String accessKey,String bucket,String endpoint,String prefix, String txt)  {

        InputStream is=new ByteArrayInputStream(txt.getBytes());
        String url = upload(accessId, accessKey, bucket, endpoint, "",getKey(prefix,"txt"), is);
        return url;

    }


}
