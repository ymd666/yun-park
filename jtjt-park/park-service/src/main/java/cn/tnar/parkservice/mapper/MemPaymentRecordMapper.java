package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.dto.MemPaymentRecordDto;
import cn.tnar.parkservice.model.response.MemPaymentResponse;
import cn.tnar.parkservice.model.entity.TCarMemberamountinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface MemPaymentRecordMapper extends BaseMapper<TCarMemberamountinfo> {


    Map<String, Object> count(@Param("param") MemPaymentRecordDto memPaymentRecordDto);

    Map<String,Object> queryParkName(@Param("id") Long id);

    Map<String,Object> queryCustName(@Param("id") Long id);

    Map<String,Object> queryOperName(@Param("id") Long id);

    Map<String,Object> queryRegionName(@Param("id") Long id);

    //基本信息 白名单 自定义卡组
    List<MemPaymentResponse> baseRecords(Page<MemPaymentResponse> page,@Param("param") MemPaymentRecordDto memPaymentRecordDto);

}
