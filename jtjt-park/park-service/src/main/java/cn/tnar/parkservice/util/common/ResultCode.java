package cn.tnar.parkservice.util.common;


public interface ResultCode {
    /**
     * 成功
     */
    Integer SUCCESS = 200;
    String SUCCESS_MSG = "success";
    /**
     * 失败
     */
    Integer FAIL = 900;
    String FAIL_MSG = "fail";
    /**
     * 缺少参数
     */
    Integer MISS_PARAM = 901;
    String MISS_PARAM_MSG = "缺少参数";
    /**
     * 新增失败
     */
    Integer INSERT_ERROR = 902;
    String INSERT_ERROR_MSG = "新增失败";
    /**
     * 修改失败
     */
    Integer UPDATE_ERROR = 903;
    String UPDATE_ERROR_MSG = "修改失败";
    /**
     * 删除失败
     */
    Integer DELETE_ERROR = 903;
    String DELETE_ERROR_MSG = "删除失败";
    /**
     * SQL语法错误
     */
    Integer SQL_GRAMMAR_ERROR = 904;
    String SQL_GRAMMAR_ERROR_MSG = "SQL语法错误";
    /**
     * 未找到结果
     */
    Integer NOSUCHRESULT_ERROR = 905;
    String NOSUCHRESULT_ERROR_MSG = "找不到对应的记录";
    /**
     * 重复提交
     */
    Integer REPEATCOMMIT_ERROR = 906;
    String REPEATCOMMIT_ERROR_MSG = "当前操作已完成，无法再次提交";
    /**
     * 缺少计费
     */
    Integer MISS_FEESROLEINFO_ERROR = 907;
    String MISS_FEESROLEINFO_MSG = "缺少参数";
    /**
     * 金额不一致
     */
    Integer PAY_MONEY_ERROR = 908;
    String PAY_MONEY_ERROR_MSG = "支付金额不匹配";


}
