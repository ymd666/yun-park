package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.TDictionary;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public interface ITDictionaryService extends IService<TDictionary> {

    ResultJson batchQuery(List<String> categoryEns);



}
