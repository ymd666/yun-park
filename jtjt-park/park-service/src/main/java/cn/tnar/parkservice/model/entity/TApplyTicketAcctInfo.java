package cn.tnar.parkservice.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zzb
 * @since 2019-03-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "TApplyTicketAcctInfo对象", description = "")
public class TApplyTicketAcctInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long custId;

    private String parkCode;

    private String parkName;

    @ApiModelProperty(value = "开票地址")
    private String url;

    @ApiModelProperty(value = "应用标识")
    @TableField("appId")
    private String appId;

    @ApiModelProperty(value = "应用秘钥")
    @TableField("appSecret")
    private String appSecret;

    @ApiModelProperty(value = "盐值")
    @TableField("appSalt")
    private String appSalt;

    @ApiModelProperty(value = "接口版本")
    private String version;

    @ApiModelProperty(value = "用户名")
    @TableField("userName")
    private String userName;

    @ApiModelProperty(value = "密码")
    @TableField("passWord")
    private String passWord;

    @ApiModelProperty(value = "服务商编码,大象提供")
    @TableField("enterpriseCode")
    private String enterpriseCode;

    @ApiModelProperty(value = "纳税人识别码")
    @TableField("taxpayerId")
    private String taxpayerId;

    private String fpqqlsh;

    @ApiModelProperty(value = "企业平台编码")
    private String dsptbm;

    @ApiModelProperty(value = "开票方识别码")
    private String nsrsbh;

    @ApiModelProperty(value = "开票方名称")
    private String nsrmc;

    @ApiModelProperty(value = "地址")
    private String xhfDz;

    @ApiModelProperty(value = "电话")
    private String xhfDh;

    @ApiModelProperty(value = "银行账号")
    private String xhfYhzh;

    @ApiModelProperty(value = "开票员")
    private String kpy;

    @ApiModelProperty(value = "收款员")
    private String sky;

    @ApiModelProperty(value = "复核员")
    private String fhy;

    @ApiModelProperty(value = "商品编码")
    private String spbm;

    @ApiModelProperty(value = "税率")
    private String sl;

    private Integer delflag;

    private Long createTime;

    @ApiModelProperty(value = "优惠政策")
    @TableField("vatSpecialManagement")
    private String vatSpecialManagement;

    @ApiModelProperty(value = "开票内容，商品内容")
    @TableField("goodsName")
    private String goodsName;

    @ApiModelProperty(value = "含税标志0：不含税 1：含税")
    @TableField("priceTaxMark")
    private int priceTaxMark=0;


}
