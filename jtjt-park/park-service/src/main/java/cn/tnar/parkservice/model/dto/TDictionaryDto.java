package cn.tnar.parkservice.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
public class TDictionaryDto {

    /**
     * 自增长ID
     */
    @ApiModelProperty(value = "自增长ID")
    private Long id;
    private String category;
    @TableField("category_en")
    private String categoryEn;
    private Integer ival;
    private String cval;
    private String item;
    @TableField("item_en")
    private String itemEn;
    @TableField("mdl_id")
    private Integer mdlId;
    @TableField("is_meta")
    private Integer isMeta;
    @TableField("audit_flag")
    private Integer auditFlag;
    @TableField("modify_oper")
    private Long modifyOper;
    @TableField("modify_time")
    private Long modifyTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryEn() {
        return categoryEn;
    }

    public void setCategoryEn(String categoryEn) {
        this.categoryEn = categoryEn;
    }

    public Integer getIval() {
        return ival;
    }

    public void setIval(Integer ival) {
        this.ival = ival;
    }

    public String getCval() {
        return cval;
    }

    public void setCval(String cval) {
        this.cval = cval;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemEn() {
        return itemEn;
    }

    public void setItemEn(String itemEn) {
        this.itemEn = itemEn;
    }

    public Integer getMdlId() {
        return mdlId;
    }

    public void setMdlId(Integer mdlId) {
        this.mdlId = mdlId;
    }

    public Integer getIsMeta() {
        return isMeta;
    }

    public void setIsMeta(Integer isMeta) {
        this.isMeta = isMeta;
    }

    public Integer getAuditFlag() {
        return auditFlag;
    }

    public void setAuditFlag(Integer auditFlag) {
        this.auditFlag = auditFlag;
    }

    public Long getModifyOper() {
        return modifyOper;
    }

    public void setModifyOper(Long modifyOper) {
        this.modifyOper = modifyOper;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

}
