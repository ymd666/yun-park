package cn.tnar.parkservice.model.dto;

/**
 * @Description:
 * @author: yl
 * @date 2018/10/16 14:19
 */
public class InvoiceDTO {
    /**
     * 发票类型
     */
    private String invoiceType;

    /**
     * 发票金额
     */
    private String invoicePrice;

    /**
     * 申请日期
     */
    private String applyDate;

    /**
     * 开票人账号
     */
    private String buyerBankAccount;

    /**
     * 开票人电话
     */
    private String buyerPhone;

    /**
     * 发票状态
     */
    private String invoiceStatus;

    /**
     * 开票时间
     */
    private String invoiceDate;

    /**
     * 流水号
     */
    private String serialNo;

    /**
     * 发票抬头(买家称呼)
     */
    private String buyerName;

    private String invoiceCode;

    private String invoiceNo;

    /**
     * 开票人地址电话
     */
    private String buyerAddressTel;

    private String sellerAddressTel;

    /**
     * 开票人税号
     */
    private String buyerTaxNo;

    private String sellerTaxNo;

    /**
     * 商品名称,停车费或充值月卡
     */
    private String goodsName;

    private String parkCode;

    private String orderNo;

    private String parkName;

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoicePrice() {
        return invoicePrice;
    }

    public void setInvoicePrice(String invoicePrice) {
        this.invoicePrice = invoicePrice;
    }

    public String getBuyerBankAccount() {
        return buyerBankAccount;
    }

    public void setBuyerBankAccount(String buyerBankAccount) {
        this.buyerBankAccount = buyerBankAccount;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getBuyerAddressTel() {
        return buyerAddressTel;
    }

    public void setBuyerAddressTel(String buyerAddressTel) {
        this.buyerAddressTel = buyerAddressTel;
    }

    public String getSellerAddressTel() {
        return sellerAddressTel;
    }

    public void setSellerAddressTel(String sellerAddressTel) {
        this.sellerAddressTel = sellerAddressTel;
    }

    public String getBuyerTaxNo() {
        return buyerTaxNo;
    }

    public void setBuyerTaxNo(String buyerTaxNo) {
        this.buyerTaxNo = buyerTaxNo;
    }

    public String getSellerTaxNo() {
        return sellerTaxNo;
    }

    public void setSellerTaxNo(String sellerTaxNo) {
        this.sellerTaxNo = sellerTaxNo;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
}
