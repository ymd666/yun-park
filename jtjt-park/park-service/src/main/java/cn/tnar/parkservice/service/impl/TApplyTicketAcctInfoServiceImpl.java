package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.model.request.ParkDayHighparkingRecordRequest;
import cn.tnar.parkservice.mapper.TApplyTicketAcctInfoMapper;
import cn.tnar.parkservice.model.entity.TApplyTicketAcctInfo;
import cn.tnar.parkservice.service.ITApplyTicketAcctInfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-03-27
 */
@Service("tApplyTicketAcctInfoServiceImpl")
public class TApplyTicketAcctInfoServiceImpl extends ServiceImpl<TApplyTicketAcctInfoMapper, TApplyTicketAcctInfo> implements ITApplyTicketAcctInfoService {

    @Autowired
    private TApplyTicketAcctInfoMapper mapper;

    @Override
    public Page<TApplyTicketAcctInfo> query(ParkDayHighparkingRecordRequest info) {
        Page<TApplyTicketAcctInfo> page=new Page<>(info.getPageNo(),info.getPageSize());
        return page.setRecords( mapper.query(page,info.getParkCode()));
    }
}
