package cn.tnar.parkservice.controller.employee;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.service.KesbApiService;
import cn.tnar.parkservice.service.TollReportService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/8/21
 * @Description:
 */
@Api(tags = "收费统计（张鑫）")
@RestController
@RequestMapping("service/tollStatistics")
public class TollReportController {

    private Logger log = LoggerFactory.getLogger(TollReportController.class);

    @Autowired
    private KesbApiService kesbApiService;

    @Autowired
    private TollReportService tollReportService;

    @ApiOperation("收费员排班报表")
    @PostMapping("/tollAutomated")
    public ResultJson tollAutomated(@RequestBody Map<String,Object> param){
        param.put("startDate",param.get("startDate").toString().concat("000000"));
        param.put("endDate",param.get("endDate").toString().concat("235959"));
        return tollReportService.tollAutomated(param);
    }

    @ApiOperation("日统计报表")
    @PostMapping("/tollDailyReport")
    public ResultJson tollDailyReport(@RequestBody Map<String,Object> param){
        try {
            log.info("日统计报表：C接口请求参数，param：======>"+param);
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_Daily_STATISTICS, param);
            log.info("日统计报表：C接口返回参数，response：======>"+list);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(list);
        } catch (KesbException e) {
            log.error("日统计报表C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


}
