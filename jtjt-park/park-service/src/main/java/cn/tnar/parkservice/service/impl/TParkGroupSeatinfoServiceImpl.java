package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.mapper.TParkGroupSeatinfoMapper;
import cn.tnar.parkservice.model.entity.TParkGroupSeatinfo;
import cn.tnar.parkservice.service.ITParkGroupSeatinfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zzb
 * @since 2019-08-20
 */
@Service("tParkGroupSeatinfoServiceImpl")
public class TParkGroupSeatinfoServiceImpl extends ServiceImpl<TParkGroupSeatinfoMapper, TParkGroupSeatinfo> implements ITParkGroupSeatinfoService {

    @Autowired
    private TParkGroupSeatinfoMapper tParkGroupSeatinfoMapper;

    @Override
    public List<Map<String, String>> queryCarSeat(Page page, Long groupId) {
        List<Map<String, String>> maps = tParkGroupSeatinfoMapper.queryCarSeat(page, groupId);
        return maps;
    }


}
