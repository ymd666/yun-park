package cn.tnar.parkservice.service;


import cn.tnar.parkservice.model.entity.MCardLevelRelations;
import cn.tnar.parkservice.model.entity.MemberGroupReq;
import cn.tnar.parkservice.model.entity.TParkMemberGroup;
import cn.tnar.parkservice.model.entity.TParkMemberGroupDto;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年07月08日 14:50:44
 */
public interface ITParkMemberGroupService extends IService<TParkMemberGroup> {


    /**
     * 新增会员分组信息
     *
     * @param memberGroupreq
     * @return
     */
    ResultJson insertMemberGroup(MemberGroupReq memberGroupreq);

    /**
     * 新增会员分组信息（暂不用）
     *
     * @param memberGroupreq
     * @return
     */
    ResultJson insertMemberGroup_old(MemberGroupReq memberGroupreq);

    /**
     * 禁用会员分组信息
     *
     * @param id
     * @return
     */
    ResultJson deleteMemberGroupById(Long id, Integer delFlag);


    /**
     * 查询会员分组树形结构
     *
     * @param parkCode
     * @return
     */
    ResultJson selectTree(String parkCode);

    /**
     * 根据停车场编号和关键字模糊查询树形结构
     *
     * @param parkCode
     * @param key
     * @return
     */
    ResultJson selectTreeByKey(String parkCode, TParkMemberGroupDto groupDto, String key);



    /**
     * 更新会员分组树形结构
     */
    ResultJson updateMemberGroup(MemberGroupReq memberGroupreq);

    ResultJson updateMemberGroup2(MemberGroupReq memberGroupreq);


    /**
     * 获取下一个卡组编号
     *
     * @return
     */
    Long getNextNo();

    /**
     * 根据卡组编号和类型查询月卡分组
     *
     * @param no
     * @param type
     * @return
     */
    List<TParkMemberGroup> selectMemberGroupByNoAndType(Long no, Integer type);

    /**
     * 根据map删除月卡分组
     *
     * @param paramMap
     * @return
     */
    int deleteByParamMap(Map<String, Object> paramMap);

    /**
     * 查询当前停车场下当前节点在同一级目录下是否存在
     *
     * @param memberGroupName
     * @param parkCode
     * @param parentId
     * @return
     */
    TParkMemberGroup isExistInParentNode(String memberGroupName, String parkCode, Long parentId);

    /**
     * 新增会员分组，并返回会员分组的id
     *
     * @param tParkMemberGroup
     * @return
     */
    Long insertMemberGroup(TParkMemberGroup tParkMemberGroup);


    /**
     * 判断多个子卡下是否还有未到期的用户
     *
     * @return boolean
     */
    boolean isExistMemberOnGroup(String ids);


    /**
     * 向上循环获取卡组树形结构
     *
     * @param relations
     * @return
     */
    MCardLevelRelations UpwardGetTree(MCardLevelRelations relations);

    /**
     * 查询卡组下所有的子卡id
     *
     * @return boolean
     */
    List<String> selectByParentId(List<String> stringList,Long id);


      /**
     * 查询所有的自定义子卡id
     *
     * @return ResultJson
     */
    ResultJson selectOther(String json);

    /**
     * 查询停车场下所有的卡组
     *
     * @return ResultJson
     */
    ResultJson selectAll(String json);
    /**
     * 根据卡组查询子卡
     *
     * @return ResultJson
     */
    ResultJson selectByParent(String json);


    /**
     * 查询所有白名单子卡id
     *
     * @return
     */
    List<String> getAllWhiteId();

    /**
     * 根据父卡组id查询所有的子卡证集合
     */
    List<Long> queryGroupIdsByParentId(List<Long> list ,Long parentId,String parkCode);


    /**
     * 根据计费索引判断当前计费规则是否绑卡，并获取所有卡组的区域编号
     *
     * @param feeIndexId
     * @return String
     */
    String getRegionCodeByFeeIndexId(Long feeIndexId);


    /**
     * 同步当前停车场的卡组信息
     *
     * @param json
     * @return
     */
    ResultJson syncToFlyos(String json);

    /**
     * 删除卡组
     *
     * @param id
     * @return
     */
    ResultJson deleteMemberGroupById_NEW(Long id);

    /**
     * 更新卡组当月累计满免总金额
     *
     * @param param
     * @return
     */
    ResultJson updateCardGroupTotalAmt(Map<String, String> param);


    ResultJson configFullFree(String json);
}
