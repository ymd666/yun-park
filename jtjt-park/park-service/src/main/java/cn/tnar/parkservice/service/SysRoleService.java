package cn.tnar.parkservice.service;

import cn.tnar.parkservice.model.entity.TSysRole;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface SysRoleService extends IService<TSysRole> {

    ResultJson querySysRoles(Map<String, Object> param);

    ResultJson querySysRolesTree(Map<String, Object> param);

}
