package cn.tnar.parkservice.model.dto;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author clarck
 * @since 2018-09-17
 */
@TableName("t_park_work_log")
public class TParkWorkLog extends Model<TParkWorkLog> {

    private static final long serialVersionUID = 1L;

    @TableId(type=IdType.AUTO)
    private Long id;
    @TableField("node_id")
    private Integer nodeId;
    /**
     * 所属运营商
     */
    @ApiParam(value = "所属运营商")
    @TableField("agent_id")
    private Long agentId;
    @TableField("park_code")
    private String parkCode;
    /**
     * 区域代码
     */
    @ApiParam(value = "区域代码")
    @TableField("region_code")
    private String regionCode;
    /**
     * 设备编号
     */
    @ApiParam(value = "设备编号")
    @TableField("device_code")
    private String deviceCode;
    @TableField("toll_no")
    private String tollNo;
    /**
     * 停车场+区域+班次
     */
    @ApiParam(value = "停车场+区域+班次")
    private String batchno;
    /**
     * 上班时间
     */
    @ApiParam(value = "上班时间")
    @TableField("on_duty")
    private Long onDuty;
    /**
     * 下班时间
     */
    @ApiParam(value = "下班时间")
    @TableField("off_duty")
    private Long offDuty;
    /**
     * 状态
     */
    @ApiParam(value = "状态")
    private Integer isvalid;
    /**
     * 代班收费员工号
     */
    @ApiParam(value = "代班收费员工号")
    @TableField("instead_no")
    private String insteadNo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getTollNo() {
        return tollNo;
    }

    public void setTollNo(String tollNo) {
        this.tollNo = tollNo;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public Long getOnDuty() {
        return onDuty;
    }

    public void setOnDuty(Long onDuty) {
        this.onDuty = onDuty;
    }

    public Long getOffDuty() {
        return offDuty;
    }

    public void setOffDuty(Long offDuty) {
        this.offDuty = offDuty;
    }

    public Integer getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Integer isvalid) {
        this.isvalid = isvalid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TParkWorkLog{" +
        ", id=" + id +
        ", nodeId=" + nodeId +
        ", agentId=" + agentId +
        ", parkCode=" + parkCode +
        ", regionCode=" + regionCode +
        ", deviceCode=" + deviceCode +
        ", tollNo=" + tollNo +
        ", batchno=" + batchno +
        ", onDuty=" + onDuty +
        ", offDuty=" + offDuty +
        ", isvalid=" + isvalid +
        ", insteadNo=" + insteadNo +
        "}";
    }
}
