package cn.tnar.yunpark.service.impl;

import cn.tnar.yunpark.mapper.ParkSpaceMapper;
import cn.tnar.yunpark.model.ParkSpace;
import cn.tnar.yunpark.service.ParkSpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 停车位服务实现类
 * Created by tieh on 2017/6/14.
 */
@Service
public class TnarParkSpaceService implements ParkSpaceService {

    @Autowired
    ParkSpaceMapper mapper;

    @Override
    public ParkSpace getSpaceInfoBySensorId(String sensorId) {
        return mapper.getParkSpaceBySensorId(sensorId);
    }
}
