package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.util.List;

/**
 * @Author: xinZhang
 * @Date: 2019/10/12
 * @Description:
 */
@Data
public class TAcctCustInfoResponse {

    private Long id;

    private Long custId;

    /**
     * 系统账号
     */
    private String account;

    /**
     *姓名
     */
    private String name;

    /**
     *性别
     */
    private Integer gender;

    /**
     *手机号
     */
    private String mobile;

    /**
     *职业
     */
    private Integer property;

    /**
     *数据字典 客户状态:	 1-未激活 2-正常 3-冻结 4-注销
     */
    private Integer custStatus;

    /**
     *角色集合
     */
    private List<SysRoleResponse> sysRoleResponseList;
}
