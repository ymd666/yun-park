package cn.tnar.parkservice.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询订单信息
 * @author Tiyer.Tao
 * @since 2018-09-08
 */
@Data
public class OrderRequest {

//    @ApiModelProperty(value = " 订单ID",example = "988160")
//    private long id;

    @ApiModelProperty(value = " 订单编号",example = "201809016000000351800000000000000000000014073")
    private String orderNumber;

    @ApiModelProperty(value = " 订单状态",example = "988160")
    private int orderstate;

//    @ApiModelProperty(value = " 批次编号",example = "35080220000110336")
//    private String batchNumber;
//
//    @ApiModelProperty(value = " 收货人",example = "")
//    private String recvname;
//
//    @ApiModelProperty(value = " 联系电话",example = "")
//    private String recvmobile;
//
//    @ApiModelProperty(value = " 资产类型",example = "1")
//    private int assetsType;
//
//    @ApiModelProperty(value = " 商品代码",example = "8010000000001010000001")
//    private String commCode;

    @ApiModelProperty(value = " 开始时间",example = "20180901000900")
    private long startTime;

    @ApiModelProperty(value = " 结束时间",example = "20180901000959")
    private long endTime;

    @ApiModelProperty(value = " 停车时(车牌号)",example = "鄂FLN629")
    private String usingobj;

    @ApiModelProperty(value = " 每页条数",example = "10")
    private int pageSize=10;

    @ApiModelProperty(value = " 当前页",example = "1")
    private int current=1;


}
