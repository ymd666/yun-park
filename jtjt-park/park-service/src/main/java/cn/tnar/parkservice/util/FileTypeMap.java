package cn.tnar.parkservice.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: 陈亮
 * @Date: 2018/9/26 14:52
 */
public class FileTypeMap {

    public static final Map<String, String> map = new HashMap<String, String>(9);

    static {
        map.put("jpeg", "FFD8FF");
        map.put("jpg", "FFD8FFE0");
        map.put("png", "89504E47");
        map.put("wav", "57415645");
        map.put("avi", "41564920");
        map.put("mp4", "00000020667479706D70");
        map.put("mp3", "49443303000000002176");
    }

}
