package cn.tnar.yunpark;


import cn.tnar.yunpark.careyes.nbiot.CarEyesNBUDPServer;
import cn.tnar.yunpark.careyes.tcp.CarEyesTCPServer;
import cn.tnar.yunpark.huasai.nbiot.HuasaiNbiotProcessor;
import cn.tnar.yunpark.util.RequestDumpFilter;
import cn.tnar.yunpark.xunlang.XunLangUDPServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.Filter;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan
@EnableScheduling
public class Application implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(Application.class);

    @Bean
    public Filter requestDumpFilter() {
        return new RequestDumpFilter();
    }

    @Bean
    public MessageConverter amqMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }


    /**
     * Start
     */
    public static void main(String[] args) {
        final ApplicationContext applicationContext = SpringApplication.run(Application.class, args);
        logger.info("SpringBoot Start Success");

        new Thread(new Runnable() {
            @Override
            public void run() {
                SocketServer socketServer = applicationContext.getBean(SocketServer.class);
                socketServer.server();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                XunLangUDPServer socketServer = applicationContext.getBean(XunLangUDPServer.class);
                socketServer.server();
            }
        }).start();

        //careyes tcp
        new Thread(new Runnable(){
            @Override
            public void run() {
               CarEyesTCPServer socketServer = applicationContext.getBean(CarEyesTCPServer.class);
               socketServer.server();
            }
        }).start();

        //careyes UDP NB-IOT
        new Thread(new Runnable(){
            @Override
            public void run() {
                CarEyesNBUDPServer socketServer = applicationContext.getBean(CarEyesNBUDPServer.class);
                socketServer.server();
            }
        }).start();
    }

    @Autowired
    HuasaiNbiotProcessor receiver;

    @Override
    public void run(String... strings) throws Exception {
        receiver.start();
    }
}
