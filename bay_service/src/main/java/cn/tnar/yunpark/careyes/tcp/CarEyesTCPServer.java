package cn.tnar.yunpark.careyes.tcp;

import cn.tnar.yunpark.service.SensorEventDispatcher;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * author : CaineZhu
 * date   : 2017/12/22 ${time}
 * desc   : class desc
 */

@Component
public class CarEyesTCPServer {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private EventLoopGroup bossGroup = null;
    private EventLoopGroup workerGroup = null;
    private ChannelFuture channelFuture = null;
    private ServerBootstrap serverBootstrap = null;

    @Autowired
    private SensorEventDispatcher dispatcher;

    @Value("${tnar.netty.read-timeout}")
    int readTimeout = 10 * 60;    // 10 minutes

    @Value("${tnar.netty.so-timeout}")
    int timeout = 60000;

    @Value("${tnar.careyes.tcp.port}")
    private int port = 4703;

    @Value("${tnar.netty.num-boss-threads}")
    int nBossThreads = 2;

    @Value("${tnar.netty.num-worker-threads}")
    int nWorkerThreads = 4;


    public void server() {
        if (null != bossGroup || null != workerGroup) {
            return;
        }
        logger.info("Socket Server init");

        bossGroup = new NioEventLoopGroup(nBossThreads);
        workerGroup = new NioEventLoopGroup(nWorkerThreads);
        try {
            serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup);
            serverBootstrap.channel(NioServerSocketChannel.class);
            serverBootstrap.option(ChannelOption.SO_BACKLOG, 128);
            serverBootstrap.option(ChannelOption.SO_TIMEOUT, timeout);
            serverBootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    ChannelPipeline p = socketChannel.pipeline();
                    p.addLast(new ReadTimeoutHandler(readTimeout))
                            .addLast(new CarEyesTCPMessageDecoder())
                            .addLast(new CarEyesTCPMessageHandler(dispatcher));
                }
            });
            channelFuture = serverBootstrap.bind(port).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            logger.error("Socket Server init error.", e);
        } finally {
            if (null != workerGroup) {
                workerGroup.shutdownGracefully();
            }
            if (null != bossGroup) {
                bossGroup.shutdownGracefully();
            }
        }
    }

    //关闭监听
    public void stopThread() {
        if (null != workerGroup && !workerGroup.isShutdown())
            workerGroup.shutdownGracefully();

        if (null != bossGroup && !bossGroup.isShutdown())
            bossGroup.shutdownGracefully();
    }
}
