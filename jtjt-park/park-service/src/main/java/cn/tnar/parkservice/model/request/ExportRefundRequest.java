package cn.tnar.parkservice.model.request;

import lombok.Data;

/**
 * @Author: xinZhang
 * @Date: 2019/9/2
 * @Description:
 */
@Data
public class ExportRefundRequest {

    private String parkCode;

    private String carNo;

    private String orderNumber;

    private Integer refundState;

    private Long startTime;

    private Long endTime;
}
