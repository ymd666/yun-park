package cn.tnar.parkservice.service;



import cn.tnar.parkservice.model.dto.TParkVisitDto;
import cn.tnar.parkservice.model.request.TParkVisitHistoryRequest;
import cn.tnar.parkservice.model.entity.TParkVisitApply;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @Description: 访客管理-被访者Service
 * @author: Tiyer.Tao
 * @date 2019/8/24 11:14
 */
public interface ITParkVisitApplyService extends IService<TParkVisitApply> {

    ResultJson queryInfo(String requset);

    ResultJson apply(TParkVisitApply requset);

    ResultJson audit(Map<String, Object> requset);

    ResultJson visiterApplyRecord(TParkVisitHistoryRequest request);

    ResultJson rights(TParkVisitDto requset);
}
