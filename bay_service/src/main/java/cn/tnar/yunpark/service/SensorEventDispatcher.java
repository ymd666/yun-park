package cn.tnar.yunpark.service;

import cn.tnar.yunpark.model.KesbStatus;

/**
 * 车位地磁传感器事件分发器
 * Created by tieh on 2017/6/14.
 */
public interface SensorEventDispatcher {
    void dispatch(KesbStatus event);
}
