package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TSysSnogeneral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
@Mapper
public interface TSysSnogeneralMapper extends BaseMapper<TSysSnogeneral> {

    TSysSnogeneral selectLastNo();

    /**
     * 根据 操作类型 和 nodeId （默认1），更新 sys_snogeneral
     *
     * @param paramMap
     * @return
     */
    int updateByTypeAndNodeId(@Param("paramMap") Map<String, Object> paramMap);


    /**
     * 根据 操作类型 和 nodeId （默认1），更新 sys_snogeneral
     *
     * @param paramMap
     * @return
     */

    int updatePrefixByType(@Param("paramMap") Map<String, Object> paramMap);

    int savePrefix(@Param("paramMap") Map<String, Object> paramMap);

    @Select("select  CONCAT(prefix, LPAD(lastsno,7, 0))  from t_sys_prefix_sno where node_id=1 and opertype=#{paramMap.opertype} and prefix=#{paramMap.prefix}")
    String  selectPrefix(@Param("paramMap") Map<String, Object> paramMap);
}
