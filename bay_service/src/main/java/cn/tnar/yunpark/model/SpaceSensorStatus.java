package cn.tnar.yunpark.model;

/**
 * 车位传感器状态
 * Created by tieh on 2017/6/24.
 */
public class SpaceSensorStatus implements KesbStatus {
    private String sensorId;
    private int power;
    private String powerShow;
    private int rssi;
    private String rssiShow;

    public SpaceSensorStatus(String sensorId, int power, String powerShow, int rssi, String rssiShow) {
        this.sensorId = sensorId;
        this.power = power;
        this.powerShow = powerShow;
        this.rssi = rssi;
        this.rssiShow = rssiShow;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getPowerShow() {
        return powerShow;
    }

    public void setPowerShow(String powerShow) {
        this.powerShow = powerShow;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public String getRssiShow() {
        return rssiShow;
    }

    public void setRssiShow(String rssiShow) {
        this.rssiShow = rssiShow;
    }

    @Override
    public String getType() {
        return "1";
    }

    @Override
    public String toKesbString() {
        return getSensorId()
                + "," + getPower() + "," + getPowerShow()
                + "," + getRssi() + "," + getRssiShow();
    }

    @Override
    public String toString() {
        return "SpaceSensorStatus{" +
                "id='" + sensorId + '\'' +
                ", power=" + power +
                ", rssi=" + rssi +
                '}';
    }
}
