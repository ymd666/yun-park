package cn.tnar.parkservice.util;

import java.util.HashMap;
import java.util.Map;

public class ResponseUtils {
    public static final Object ok() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", 200);
        obj.put("msg", "成功");
        return obj;
    }

    public static final Object ok(Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", 200);
        obj.put("data", data);
        obj.put("msg", "成功");
        return obj;
    }

    public static final Object ok(String msg, Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", 200);
        obj.put("data", data);
        obj.put("msg", msg);
        return obj;
    }

    public static final Object fail() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", -1);
        obj.put("msg", "错误");
        return obj;
    }

    public static final Object nullError(String msg) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", 502);
        obj.put("msg", msg + "不能为空");
        return obj;
    }

    public static final Object fail(int code, String msg) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", code);
        obj.put("msg", msg);
        return obj;
    }

    public static final Object fail401() {
        return fail(401, "请登录");
    }

    public static final Object unlogin() {
        return fail401();
    }

    public static final Object fail402() {
        return fail(402, "参数不对");
    }

    public static final Object badArgument() {
        return fail402();
    }

    public static final Object fail403() {
        return fail(403, "参数值不对");
    }

    public static final Object badArgumentValue() {
        return fail403();
    }

    public static final Object fail501() {
        return fail(501, "业务不支持");
    }

    public static final Object unsupport() {
        return fail501();
    }

    public static final Object fail502() {
        return fail(502, "系统内部错误");
    }

    public static final Object serious() {
        return fail502();
    }


}

