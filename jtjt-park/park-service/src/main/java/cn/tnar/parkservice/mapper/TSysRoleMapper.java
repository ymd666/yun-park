package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TSysRole;
import cn.tnar.parkservice.model.response.SysRoleResponse;
import cn.tnar.parkservice.model.response.SysRoleResponseDto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Author: xinZhang
 * @Date: 2019/9/29
 * @Description:
 */
@Mapper
@Repository
public interface TSysRoleMapper extends BaseMapper<TSysRole> {

    List<SysRoleResponse> querySysRoles(Page page, @Param("param") Map<String,Object> map);

    SysRoleResponseDto queryRootSysRoles(@Param("param") Map<String, Object> map);

    @Select("select * from  t_sys_role where parent_role_id = #{parentRoleId}")
    List<SysRoleResponseDto> querySubListByParentId(@Param("parentRoleId") Long parentRoleId);

    List<SysRoleResponse> querySysRolesByCustId(@Param("custId") Long custId);

    @Select("SELECT usr_id FROM t_sys_user_assign WHERE role_id = #{roleId}")
    List<Long> queryCustIdsByRoleId(@Param("roleId") Long roleId);
}
