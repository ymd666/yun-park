package cn.tnar.parkservice.util;

import cn.tnar.parkservice.util.common.ResultJson;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.stream.Collectors;

/**
 * @author Bao
 * @date 2018-10-22
 */
public class ErrorUtils {

    public static ResultJson error(BindingResult result) {
        String msg = result.getAllErrors()
                .stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining("，"));
        return ResultJson.clientError().msg(msg).build();
    }
}
