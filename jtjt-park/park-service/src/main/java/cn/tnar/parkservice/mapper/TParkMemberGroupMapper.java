package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.model.entity.TParkMemberGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年07月08日 14:49:48
 */
@Mapper
@Repository
public interface TParkMemberGroupMapper extends BaseMapper<TParkMemberGroup> {

    @Select("select max(no) from t_park_member_group limit 1")
    Long selectMaxNo();

    @Select("select * from t_park_member_group where no=#{no} and type=#{type} ")
    List<TParkMemberGroup> selectMemberGroupByNoAndType(@Param("no") Long no, @Param("type") Integer type);

    @Delete("delete from t_park_member_group where no={paramMap.no} and type={paramMap.type}" +
            " and park_code in (${paramMap.parkCodes})")
    int deleteByParamMap(@Param("paramMap") Map<String, Object> paramMap);

    @Select("select * from  t_park_member_group where name=#{memberGroupName}" +
            " and  park_code = #{parkCode} and parent_id = #{parentId} limit 1")
    TParkMemberGroup isExistInParentNoder(@Param("memberGroupName") String memberGroupName,
                                          @Param("parkCode") String parkCode,
                                          @Param("parentId") Long parentId);

    @Insert("  INSERT INTO  t_park_member_group(no,name,parent_id,park_code,\n" +
            "        del_flag,create_time,type) values (#{no},#{name},#{parentId},\n" +
            "        #{parkCode},#{delFlag},#{createTime},#{type})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Long insertMemberGroup(TParkMemberGroup tParkMemberGroup);

    /**
     * 判断访客卡下是否还存在未到期的用户 ，根据t_park_visit_history的visit_time_end字段
     *
     * @param id
     * @param date
     * @return
     */
    @Select("SELECT COUNT(*) from t_park_member_group a  INNER JOIN t_park_member_group_relation b \n" +
            "on a.id = b.group_id INNER JOIN t_park_visit_history c on b.member_id = c.id\n" +
            "where a.id in (#{id}) and c.visit_time_end div 1 >= #{date}")
    int isExistVisitorMemberOnGroup(Long id, Integer date);

    /**
     * 判断白名单子卡下是否还存在未到期的用户，根据t_park_group_memberinfo的end_date字段
     *
     * @param id
     * @param date
     * @return
     */
    @Select("SELECT COUNT(*) from  t_park_member_group a  INNER JOIN t_park_member_group_relation b\n" +
            "on  a.id = b.group_id  INNER JOIN t_park_group_memberinfo c ON b.member_id = c.id\n" +
            "where  a.id in (#{id}) and  c.end_date DIV 1 >= #{date} ")
    int isExistWhiteMemberOnGroup(Long id, Integer date);

    /**
     * 判断黑名单子卡下是否还存在未到期的用户，根据t_park_group_memberinfo的end_date字段
     *
     * @param id
     * @param date
     * @return
     */
    @Select("SELECT COUNT(*) from  t_park_member_group a  INNER JOIN t_park_member_group_relation b\n" +
            "on  a.id = b.group_id  INNER JOIN t_park_group_memberinfo c ON b.member_id = c.id\n" +
            "where  a.id in (#{id}) and  c.end_date DIV 1 >= #{date} ")
    int isExistBlackMemberOnGroup(Long id, Integer date);

    /**
     * 判断自定义子卡下是否还存在未到期的用户,根据t_park_group_seatinfo的end_date字段
     *
     * @param id
     * @param date
     * @return
     */
    @Select("SELECT count(*) from  t_park_member_group a  INNER JOIN t_park_member_group_relation b\n" +
            "on  a.id = b.group_id  INNER JOIN t_park_group_memberinfo c ON b.member_id = c.id\n" +
            "INNER JOIN t_park_group_seatinfo d on  c.id = d.group_id\n" +
            "where  a.id in (#{id}) and  d.end_date div 1 > #{date} ")
    int isExistCustomMemberOnGroup(Long id, Integer date);

    @Select("select id from t_park_member_group where del_flag=0 and classify=3 and type=1")
    List<String> getAllWhiteId();


    List<Map<String,Object>> selectChildInfo(@Param("groupMembers")String groupMembers);

    @Select("select DISTINCT region_code from t_park_member_group where fee_index_id = #{feeIndexId} and region_code is not null and region_code != '' ")
    List<String> getRegionCodesByFeeIndexId(@Param("feeIndexId") Long feeIndexId);
 }
