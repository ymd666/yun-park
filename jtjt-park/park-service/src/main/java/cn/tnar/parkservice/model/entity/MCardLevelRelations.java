package cn.tnar.parkservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月13日 15:27:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class MCardLevelRelations {

    private Long id;

    private Long parentId;

    private Integer type;

    private String name;

    private MCardLevelRelations child;

}
