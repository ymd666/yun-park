package cn.tnar.parkservice.model.entity;


import java.math.BigDecimal;

public class TParkEscape {

  private long id;
  private long nodeId;
  private long agentId;
  private String parkCode;
  private String regionCode;
  private String parkname;
  private String inbatch;
  private String serialno;
  private String inoperno;
  private String outbatch;
  private String outoperno;
  private String traceIndex;
  private long tracetype;
  private long clienttype;
  private long seatnum;
  private String seatCode1;
  private String seatno1;
  private String seatCode2;
  private String seatno2;
  private long ownerCustId;
  private long cartype;
  private String carId;
  private String carnocolor;
  private long intime;
  private String ingatenoId;
  private String inphoto;
  private String insmallphoto;
  private long inconfidence;
  private long outtime;
  private String outgatenoId;
  private String outphoto;
  private String outsmallphoto;
  private long outconfidence;
  private long parktime;
  private long paydatetime;
  private BigDecimal preamt;
  private BigDecimal parkamt;
  private BigDecimal chargeamt;
  private long paystate;
  private long outtype;
  private String chargemanno;
  private long whitetype;
  private long result;
  private String deviceCode;
  private String posCode;
  private String reserve;
  private long settledate;
  private long inoperate;
  private long outoperate;
  private long uploadtime;
  private long payflag;
  private long tradeorderid;
  private long hisflag;
  private long memberdiscount;
  private String feedesc;
  private long settle;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getNodeId() {
    return nodeId;
  }

  public void setNodeId(long nodeId) {
    this.nodeId = nodeId;
  }


  public long getAgentId() {
    return agentId;
  }

  public void setAgentId(long agentId) {
    this.agentId = agentId;
  }


  public String getParkCode() {
    return parkCode;
  }

  public void setParkCode(String parkCode) {
    this.parkCode = parkCode;
  }


  public String getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(String regionCode) {
    this.regionCode = regionCode;
  }


  public String getParkname() {
    return parkname;
  }

  public void setParkname(String parkname) {
    this.parkname = parkname;
  }


  public String getInbatch() {
    return inbatch;
  }

  public void setInbatch(String inbatch) {
    this.inbatch = inbatch;
  }


  public String getSerialno() {
    return serialno;
  }

  public void setSerialno(String serialno) {
    this.serialno = serialno;
  }


  public String getInoperno() {
    return inoperno;
  }

  public void setInoperno(String inoperno) {
    this.inoperno = inoperno;
  }


  public String getOutbatch() {
    return outbatch;
  }

  public void setOutbatch(String outbatch) {
    this.outbatch = outbatch;
  }


  public String getOutoperno() {
    return outoperno;
  }

  public void setOutoperno(String outoperno) {
    this.outoperno = outoperno;
  }


  public String getTraceIndex() {
    return traceIndex;
  }

  public void setTraceIndex(String traceIndex) {
    this.traceIndex = traceIndex;
  }


  public long getTracetype() {
    return tracetype;
  }

  public void setTracetype(long tracetype) {
    this.tracetype = tracetype;
  }


  public long getClienttype() {
    return clienttype;
  }

  public void setClienttype(long clienttype) {
    this.clienttype = clienttype;
  }


  public long getSeatnum() {
    return seatnum;
  }

  public void setSeatnum(long seatnum) {
    this.seatnum = seatnum;
  }


  public String getSeatCode1() {
    return seatCode1;
  }

  public void setSeatCode1(String seatCode1) {
    this.seatCode1 = seatCode1;
  }


  public String getSeatno1() {
    return seatno1;
  }

  public void setSeatno1(String seatno1) {
    this.seatno1 = seatno1;
  }


  public String getSeatCode2() {
    return seatCode2;
  }

  public void setSeatCode2(String seatCode2) {
    this.seatCode2 = seatCode2;
  }


  public String getSeatno2() {
    return seatno2;
  }

  public void setSeatno2(String seatno2) {
    this.seatno2 = seatno2;
  }


  public long getOwnerCustId() {
    return ownerCustId;
  }

  public void setOwnerCustId(long ownerCustId) {
    this.ownerCustId = ownerCustId;
  }


  public long getCartype() {
    return cartype;
  }

  public void setCartype(long cartype) {
    this.cartype = cartype;
  }


  public String getCarId() {
    return carId;
  }

  public void setCarId(String carId) {
    this.carId = carId;
  }


  public String getCarnocolor() {
    return carnocolor;
  }

  public void setCarnocolor(String carnocolor) {
    this.carnocolor = carnocolor;
  }


  public long getIntime() {
    return intime;
  }

  public void setIntime(long intime) {
    this.intime = intime;
  }


  public String getIngatenoId() {
    return ingatenoId;
  }

  public void setIngatenoId(String ingatenoId) {
    this.ingatenoId = ingatenoId;
  }


  public String getInphoto() {
    return inphoto;
  }

  public void setInphoto(String inphoto) {
    this.inphoto = inphoto;
  }


  public String getInsmallphoto() {
    return insmallphoto;
  }

  public void setInsmallphoto(String insmallphoto) {
    this.insmallphoto = insmallphoto;
  }


  public long getInconfidence() {
    return inconfidence;
  }

  public void setInconfidence(long inconfidence) {
    this.inconfidence = inconfidence;
  }


  public long getOuttime() {
    return outtime;
  }

  public void setOuttime(long outtime) {
    this.outtime = outtime;
  }


  public String getOutgatenoId() {
    return outgatenoId;
  }

  public void setOutgatenoId(String outgatenoId) {
    this.outgatenoId = outgatenoId;
  }


  public String getOutphoto() {
    return outphoto;
  }

  public void setOutphoto(String outphoto) {
    this.outphoto = outphoto;
  }


  public String getOutsmallphoto() {
    return outsmallphoto;
  }

  public void setOutsmallphoto(String outsmallphoto) {
    this.outsmallphoto = outsmallphoto;
  }


  public long getOutconfidence() {
    return outconfidence;
  }

  public void setOutconfidence(long outconfidence) {
    this.outconfidence = outconfidence;
  }


  public long getParktime() {
    return parktime;
  }

  public void setParktime(long parktime) {
    this.parktime = parktime;
  }


  public long getPaydatetime() {
    return paydatetime;
  }

  public void setPaydatetime(long paydatetime) {
    this.paydatetime = paydatetime;
  }


  public BigDecimal getPreamt() {
    return preamt;
  }

  public void setPreamt(BigDecimal preamt) {
    this.preamt = preamt;
  }

  public BigDecimal getParkamt() {
    return parkamt;
  }

  public void setParkamt(BigDecimal parkamt) {
    this.parkamt = parkamt;
  }

  public BigDecimal getChargeamt() {
    return chargeamt;
  }

  public void setChargeamt(BigDecimal chargeamt) {
    this.chargeamt = chargeamt;
  }

  public long getPaystate() {
    return paystate;
  }

  public void setPaystate(long paystate) {
    this.paystate = paystate;
  }


  public long getOuttype() {
    return outtype;
  }

  public void setOuttype(long outtype) {
    this.outtype = outtype;
  }


  public String getChargemanno() {
    return chargemanno;
  }

  public void setChargemanno(String chargemanno) {
    this.chargemanno = chargemanno;
  }


  public long getWhitetype() {
    return whitetype;
  }

  public void setWhitetype(long whitetype) {
    this.whitetype = whitetype;
  }


  public long getResult() {
    return result;
  }

  public void setResult(long result) {
    this.result = result;
  }


  public String getDeviceCode() {
    return deviceCode;
  }

  public void setDeviceCode(String deviceCode) {
    this.deviceCode = deviceCode;
  }


  public String getPosCode() {
    return posCode;
  }

  public void setPosCode(String posCode) {
    this.posCode = posCode;
  }


  public String getReserve() {
    return reserve;
  }

  public void setReserve(String reserve) {
    this.reserve = reserve;
  }


  public long getSettledate() {
    return settledate;
  }

  public void setSettledate(long settledate) {
    this.settledate = settledate;
  }


  public long getInoperate() {
    return inoperate;
  }

  public void setInoperate(long inoperate) {
    this.inoperate = inoperate;
  }


  public long getOutoperate() {
    return outoperate;
  }

  public void setOutoperate(long outoperate) {
    this.outoperate = outoperate;
  }


  public long getUploadtime() {
    return uploadtime;
  }

  public void setUploadtime(long uploadtime) {
    this.uploadtime = uploadtime;
  }


  public long getPayflag() {
    return payflag;
  }

  public void setPayflag(long payflag) {
    this.payflag = payflag;
  }


  public long getTradeorderid() {
    return tradeorderid;
  }

  public void setTradeorderid(long tradeorderid) {
    this.tradeorderid = tradeorderid;
  }


  public long getHisflag() {
    return hisflag;
  }

  public void setHisflag(long hisflag) {
    this.hisflag = hisflag;
  }


  public long getMemberdiscount() {
    return memberdiscount;
  }

  public void setMemberdiscount(long memberdiscount) {
    this.memberdiscount = memberdiscount;
  }


  public String getFeedesc() {
    return feedesc;
  }

  public void setFeedesc(String feedesc) {
    this.feedesc = feedesc;
  }


  public long getSettle() {
    return settle;
  }

  public void setSettle(long settle) {
    this.settle = settle;
  }

}
