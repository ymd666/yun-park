package cn.tnar.parkservice.service.impl;

import cn.tnar.parkservice.config.ExceptionConst;
import cn.tnar.parkservice.mapper.TParkFeeIndexMapper;
import cn.tnar.parkservice.model.dto.CloudParkBilling;
import cn.tnar.parkservice.model.dto.CloudParkFeeIndex;
import cn.tnar.parkservice.model.dto.CloudParkFeeIndexAndCalc;
import cn.tnar.parkservice.model.entity.TParkFee;
import cn.tnar.parkservice.model.entity.TParkFeeIndex;
import cn.tnar.parkservice.service.ITParkFeeIndexService;
import cn.tnar.parkservice.service.ITParkFeeService;
import cn.tnar.parkservice.service.ITParkMemberGroupService;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dzx
 * @ClassName:
 * @Description:
 * @date 2019年08月22日 10:39:07
 */
@Service("tParkFeeIndexServiceImpl")
@Slf4j
public class TParkFeeIndexServiceImpl extends ServiceImpl<TParkFeeIndexMapper, TParkFeeIndex> implements ITParkFeeIndexService {

    @Autowired
    private TParkFeeIndexMapper tParkFeeIndexMapper;

    @Autowired
    private ITParkMemberGroupService itParkMemberGroupService;

    @Autowired
    private ITParkFeeService itParkFeeService;

    private ResultJson RESULT_JSON = new ResultJson();


    @Override
    public ResultJson updateFeeIndex(Long id, String parkCode) {
        TParkFeeIndex tParkFeeIndex = tParkFeeIndexMapper.selectById(id);
        if (tParkFeeIndex != null) {
            tParkFeeIndex.setParkCode(parkCode);
            tParkFeeIndexMapper.updateById(tParkFeeIndex);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(true);
        } else {
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(ExceptionConst.ERROR_FEEINDEX_NOT_FOUNT).setData(false);
        }
    }

    @Override
    public String getRegionNameFromSysReg(String regionCode) {
        return tParkFeeIndexMapper.getRegionNameFromSysReg(regionCode);
    }

    @Override
    public ResultJson syncFeeIndexAndCalc(Map<String, String> map) {
        try {
            String indexIdIn = map.get("indexIdIn");
            QueryWrapper<TParkFeeIndex> queryWrapper = new QueryWrapper<>();
            queryWrapper.inSql("id", indexIdIn);
            List<TParkFeeIndex> list = this.list(queryWrapper);
            if (!CollectionUtils.isEmpty(list)) {
                List<CloudParkFeeIndexAndCalc> cloudParkFeeIndexAndCalcs = new ArrayList<CloudParkFeeIndexAndCalc>();
                for (TParkFeeIndex feeIndex : list) {
                    Long indexId = feeIndex.getId();
                    String parkCode = feeIndex.getParkCode();
                    //获取当前索引关联的卡组的区域
                    String regionCodes = itParkMemberGroupService.getRegionCodeByFeeIndexId(indexId);
                    if (StringUtils.isNotBlank(regionCodes)) {
                        QueryWrapper<TParkFee> wrapper = new QueryWrapper<>();
                        wrapper.eq("index_id", indexId);
                        List<TParkFee> feeList = itParkFeeService.list(wrapper);
                        if (!CollectionUtils.isEmpty(feeList)) {
                            for (TParkFee tParkFee : feeList) {
                                CloudParkFeeIndex cloudParkFeeIndex = genCloudParkFeeIndex(feeIndex, regionCodes);
                                CloudParkBilling cloudParkBilling = genCloudParkBilling(parkCode, feeList, tParkFee);
                                CloudParkFeeIndexAndCalc cloudParkFeeIndexAndCalc = new CloudParkFeeIndexAndCalc(cloudParkFeeIndex, cloudParkBilling);

                                cloudParkFeeIndexAndCalcs.add(cloudParkFeeIndexAndCalc);
                            }
                        }
                    }
                }
                return getSuccessResultJson(cloudParkFeeIndexAndCalcs);
            }
            return getSuccessResultJson(null);
        } catch (Exception e) {
            log.error("error", e);
            return getFailResultJson(null);
        }
    }

    /**
     * 生成车道对应的计费规则实体类
     *
     * @param parkCode
     * @param feeList
     * @param tParkFee
     * @return
     */
    private CloudParkBilling genCloudParkBilling(String parkCode, List<TParkFee> feeList, TParkFee tParkFee) {
        CloudParkBilling cloudParkBilling = new CloudParkBilling();
        cloudParkBilling.setId(tParkFee.getId());
        cloudParkBilling.setIndex_id(tParkFee.getIndexId());
        cloudParkBilling.setCartype(tParkFee.getCartype());
        cloudParkBilling.setFreetime1(tParkFee.getFreetime1());
        cloudParkBilling.setMonth(tParkFee.getMonth().floatValue());
        cloudParkBilling.setT_limit(tParkFee.getTLimit().floatValue());
        cloudParkBilling.setFreetime(tParkFee.getFreetime());
        cloudParkBilling.setD_type(tParkFee.getDType());
        cloudParkBilling.setD_preamt(tParkFee.getDPreamt().floatValue());
        cloudParkBilling.setD_once(tParkFee.getDOnce().floatValue());
        cloudParkBilling.setD_limit(tParkFee.getDLimit().floatValue());
        cloudParkBilling.setD_begin(tParkFee.getDBegin().longValue());
        cloudParkBilling.setD_end(tParkFee.getDEnd());
        cloudParkBilling.setD_unittime1(tParkFee.getDUnittime1());
        cloudParkBilling.setD_unittime2(tParkFee.getDUnittime2());
        cloudParkBilling.setD_unittime3(tParkFee.getDUnittime3());
        cloudParkBilling.setD_unittime4(tParkFee.getDUnittime4());
        cloudParkBilling.setD_cycletime1(tParkFee.getDCycletime1());
        cloudParkBilling.setD_cycletime2(tParkFee.getDCycletime2());
        cloudParkBilling.setD_cycletime3(tParkFee.getDCycletime3());
        cloudParkBilling.setD_cycletime4(tParkFee.getDCycletime4());
        cloudParkBilling.setD_price1(tParkFee.getDPrice1().floatValue());
        cloudParkBilling.setD_price2(tParkFee.getDPrice2().floatValue());
        cloudParkBilling.setD_price3(tParkFee.getDPrice3().floatValue());
        cloudParkBilling.setD_price4(tParkFee.getDPrice4().floatValue());
        cloudParkBilling.setN_price1(tParkFee.getNPrice1().floatValue());
        cloudParkBilling.setN_price2(tParkFee.getNPrice2().floatValue());
        cloudParkBilling.setN_price3(tParkFee.getNPrice3().floatValue());
        cloudParkBilling.setN_price4(tParkFee.getNPrice4().floatValue());
        cloudParkBilling.setN_begin(tParkFee.getNBegin());
        cloudParkBilling.setN_end(tParkFee.getNEnd());
        cloudParkBilling.setN_limit(tParkFee.getNLimit().floatValue());
        cloudParkBilling.setN_once(tParkFee.getNOnce().floatValue());
        cloudParkBilling.setN_preamt(tParkFee.getNPreamt().floatValue());
        cloudParkBilling.setN_type(tParkFee.getNType());
        cloudParkBilling.setN_unittime1(tParkFee.getNUnittime1());
        cloudParkBilling.setN_unittime2(tParkFee.getNUnittime2());
        cloudParkBilling.setN_unittime3(tParkFee.getNUnittime3());
        cloudParkBilling.setN_unittime4(tParkFee.getNUnittime4());
        cloudParkBilling.setN_cycletime1(tParkFee.getNCycletime1());
        cloudParkBilling.setN_cycletime2(tParkFee.getNCycletime2());
        cloudParkBilling.setN_cycletime3(tParkFee.getNCycletime3());
        cloudParkBilling.setN_cycletime4(tParkFee.getNCycletime4());
        cloudParkBilling.setWorktype(tParkFee.getWorktype());
        cloudParkBilling.setCount(feeList.size());
        cloudParkBilling.setPark_code(parkCode);
        //cloudParkBilling.setLicensekey("");
        cloudParkBilling.setUser_type(tParkFee.getUserType());
        cloudParkBilling.setDelflag(1);//新增
        //cloudParkBilling.setCarplatecolor(1);
        cloudParkBilling.setNewCarType(tParkFee.getCartype());
        return cloudParkBilling;
    }

    /**
     * 生成车道计费对应的计费索引实体类
     *
     * @param feeIndex
     * @param regionCodes
     * @return
     */
    private CloudParkFeeIndex genCloudParkFeeIndex(TParkFeeIndex feeIndex, String regionCodes) {
        CloudParkFeeIndex cloudParkFeeIndex = new CloudParkFeeIndex();
        cloudParkFeeIndex.setId(feeIndex.getId());
        cloudParkFeeIndex.setName(feeIndex.getName());
        cloudParkFeeIndex.setAgent_id(String.valueOf(feeIndex.getAgentId()));
        cloudParkFeeIndex.setCashoff(feeIndex.getCashoff());
        cloudParkFeeIndex.setCardoff(feeIndex.getCardoff());
        cloudParkFeeIndex.setMemberoff(feeIndex.getMemberoff());
        cloudParkFeeIndex.setGracetime(feeIndex.getGracetime());
        cloudParkFeeIndex.setUtime(feeIndex.getUtime());
        cloudParkFeeIndex.setUserid(String.valueOf(feeIndex.getUserid()));
        cloudParkFeeIndex.setDelflag(1);//新增
        cloudParkFeeIndex.setCycle(feeIndex.getCycle());
        cloudParkFeeIndex.setRegionCodes(regionCodes);
        return cloudParkFeeIndex;
    }

    /**
     * 获取失败返回消息体
     *
     * @param data
     * @return
     */
    private ResultJson getFailResultJson(Object data) {
        return RESULT_JSON.setCode(ResultCode.FAIL)
                .setMsg(String.valueOf(data)).setData(null);
    }

    /**
     * 获取成功返回消息体
     *
     * @param data
     * @return
     */
    private ResultJson getSuccessResultJson(Object data) {
        return RESULT_JSON.setCode(ResultCode.SUCCESS)
                .setMsg(ResultCode.SUCCESS_MSG).setData(data);
    }
}
