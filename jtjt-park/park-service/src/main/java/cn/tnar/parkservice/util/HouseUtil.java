package cn.tnar.parkservice.util;

import cn.tnar.parkservice.util.common.ResultJson;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

public class HouseUtil {

//*
//     * 功能：签到获取tooken
//     *
//     * @author zzb
//     * @date 2019年03月13日


    public static String signIn(String url, String application_id, String api_key) {
        long ts = new Date().getTime() / 1000;
        JSONObject object = new JSONObject();
        object.put("application_id", application_id);
        object.put("api_key", api_key);
        object.put("ts", ts);
        RestTemplate template = new RestTemplate();

        JSONObject retJson = template.postForObject(url + "/auth/signIn", object, JSONObject.class);
        String data = retJson.getString("data");
        String token = data.split(",")[0].split("token=")[1];
        return token;

    }

    public static ResultJson cryptoData(String token, int operation_type, String user_id, String input_data, String url) {
        long ts = new Date().getTime() / 1000;
        JSONObject object = new JSONObject();
        object.put("token", token);
        object.put("key_index", 1);
        object.put("key_version", 1);
        object.put("operation_type",operation_type);
        object.put("data_type", 1);
        object.put("user_id", user_id);
        object.put("input_format", 0);
        object.put("input_data", input_data);
        object.put("output_format", 0);
        object.put("ts", ts);
        RestTemplate template = new RestTemplate();
        JSONObject jsonObject = template.postForObject(url + "/crypto/data", object, JSONObject.class);
        ResultJson resultJson = JSONObject.toJavaObject(jsonObject, ResultJson.class);

        return resultJson;

    }

    public static ResultJson cryptoFile(String token,int operation_type,String meta_data,String input_data, String url) {
        long ts = new Date().getTime() / 1000;
        JSONObject object = new JSONObject();
        object.put("token", token);
        object.put("key_index", 1);
        object.put("key_version", 1);
        object.put("operation_type", operation_type);
        object.put("meta_data", meta_data);
        object.put("input_data",input_data );
        object.put("ts", ts);
        RestTemplate template = new RestTemplate();

        JSONObject jsonObject = template.postForObject(url + "/crypto/file", object, JSONObject.class);
        ResultJson resultJson = JSONObject.toJavaObject(jsonObject, ResultJson.class);
        return resultJson;

    }


}
