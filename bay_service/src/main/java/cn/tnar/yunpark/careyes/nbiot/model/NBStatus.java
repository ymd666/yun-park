package cn.tnar.yunpark.careyes.nbiot.model;

import cn.tnar.yunpark.careyes.CarEyesUtil;
import cn.tnar.yunpark.util.ByteTool;
import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;

/**
 * package: cn.tnar.yunpark.careyes.nbiot
 * author : CaineZhu
 * mail   : CaineZhu@me.com
 * date   : 2018/1/8 15:54
 * desc   : NBStatus
 */
public class NBStatus extends Message{

    private int type;
    /*
    * deviceType
    * 00 地磁
    * 01 保留
    * 02 保留
    * 03 地锁
    * 04 车载标签
    * 05 地磁配置工具
    * */
    private int deviceType;
    private String sensorId;
    private byte[] sensorIdBytes = new byte[6];
    private int city;
    private int area;
    private String parkId;
    private byte[] parkIdBytes = new byte[4];
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minutes;
    private int second;
    private int status;
    private int vbattery;
    private int sequence;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public byte[] getSensorIdBytes() {
        return sensorIdBytes;
    }

    public void setSensorIdBytes(byte[] sensorIdBytes) {
        this.sensorIdBytes = sensorIdBytes;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }

    public byte[] getParkIdBytes() {
        return parkIdBytes;
    }

    public void setParkIdBytes(byte[] parkIdBytes) {
        this.parkIdBytes = parkIdBytes;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getVbattery() {
        return vbattery;
    }

    public void setVbattery(int vbattery) {
        this.vbattery = vbattery;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        return "NBStatus{" +
                "type=" + type +
                ", deviceType=" + deviceType +
                ", sensorId='" + sensorId + '\'' +
                ", parkId='" + parkId + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minutes=" + minutes +
                ", second=" + second +
                ", Status=" + status +
                ", vbattery=" + vbattery +
                '}';
    }

    public static NBStatus parse(ByteBuf byteBuf) {
        ByteBuf buf = byteBuf.order(ByteOrder.LITTLE_ENDIAN);
        NBStatus nbStatus = new NBStatus();
        buf.skipBytes(2);
        nbStatus.setType(buf.readByte() & 0xFF);
        buf.skipBytes(1);
        nbStatus.setDeviceType(buf.readByte() & 0xFF);
        //nbStatus.setType(buf.readByte() & 0xFF);
        byte[] sensorIdBytes = new byte[6];
        buf.readBytes(sensorIdBytes);
        nbStatus.setSensorId(ByteTool.bytes2StringWithOutBlank(sensorIdBytes));
        nbStatus.setSensorIdBytes(sensorIdBytes);
        buf.skipBytes(1);
        nbStatus.setCity(buf.readByte());
        nbStatus.setArea(buf.readByte());
        byte[] parkIdBytes = new byte[4];
        buf.readBytes(parkIdBytes);
        nbStatus.setParkId(ByteTool.bytes2StringWithOutBlank(parkIdBytes));
        nbStatus.setParkIdBytes(parkIdBytes);
        nbStatus.setYear(CarEyesUtil.bcd2Int(buf.readByte()));
        nbStatus.setMonth(CarEyesUtil.bcd2Int(buf.readByte()));
        nbStatus.setDay(CarEyesUtil.bcd2Int(buf.readByte()));
        nbStatus.setHour(CarEyesUtil.bcd2Int(buf.readByte()));
        nbStatus.setMinutes(CarEyesUtil.bcd2Int(buf.readByte()));
        nbStatus.setSecond(CarEyesUtil.bcd2Int(buf.readByte()));
        buf.skipBytes(6);
        buf.skipBytes(1);
        nbStatus.setStatus(buf.readByte() & 0xFF);
        nbStatus.setVbattery(buf.readByte() & 0xFF);
        buf.skipBytes(1);
        buf.skipBytes(2);
        nbStatus.setSequence(buf.readByte());
        return nbStatus;
    }
}
