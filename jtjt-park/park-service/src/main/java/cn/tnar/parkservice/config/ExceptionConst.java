package cn.tnar.parkservice.config;

/**
 * 异常常量
 */
public class ExceptionConst {


    /**
     *  KESB调用失败
     */
    public static final String KESB_ERROR = "KESB调用失败";

    public static final String KESB_INSERT_ERROR = "新增记录失败";
    public static final String KESB_DELETE_ERROR = "删除记录失败";
    public static final String KESB_UPDATE_ERROR = "修改记录失败";
    public static final String KESB_QUERT_ERROR =  "查询记录失败";

    /**
     * 其他异常定义
     */
    public static final String API_VISIT_QUERY_ERROR = "查询用户访客权限异常";
    public static final String API_VISIT_APPLY_ERROR = "申请审核权限异常";
    public static final String API_VISIT_AUDIT_ERROR = "访客申请审核异常";
    public static final String API_VISIT_RESERVE_ERROR = "预约停车服务异常";
    public static final String API_VISIT_HIS_ERROR = "查询来访记录异常";
    public static final String API_VISIT_CHECK_ERROR = "审核访客申请异常";
    public static final String API_APPLY_HIS_ERROR = "查询预约记录异常";
    public static final String API_QUERY_CARD_ERROR = "查询访客卡异常";
    public static final String API_VISIT_APPROVE_ERROR = "访客审批异常";

    /**
     * 订单相关异常
     */
    public static final String API_ORDER_NOMAL_QUERY_ERROR = "订单查询失败";
    public static final String API_ORDER_ARREARAGE_QUERY_ERROR = "欠费订单查询失败";
    public static final String API_ORDER_ARREARAGE_PAY_QUERY_ERROR = "欠费补交订单查询失败";
    public static final String API_ORDER_ABNOMAL_QUERY_ERROR = "异常订单查询失败";
    public static final String API_ORDER_BADEBT_QUERY_ERROR = "坏账流水查询失败";
    public static final String API_ORDER_PARK_DALIY_QUERY_ERROR = "当日停车流水查询失败";
    public static final String API_ORDER_PARK_HISTORY_QUERY_ERROR = "历史停车流水查询失败";

    public static final String PARAM_PARK_CODE_NOT_NULL = "park_code不能为空";
    public static final String ERROR_FEEINDEX_NOT_FOUNT ="计费索引不存在";
    public static final String ERROR_RECORD_NOT_FOUND = "没有查询到此记录";
    public static final String DON_NOT_OPERA_REPEAT = "请勿重复操作";
    public static final String AMQP_ERROR = "下发车道失败";
    public static final String ERROR_APPROVE_DEFAULT = "审核失败";
    public static final String ERROR_PASSWORD = "密码错误";
}
