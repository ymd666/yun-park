package cn.tnar.parkservice.mapper;


import cn.tnar.parkservice.config.query.QuerySupport;
import cn.tnar.parkservice.model.dto.CarInfoDto;
import cn.tnar.parkservice.model.entity.THouseIdentityApply;
import cn.tnar.parkservice.model.entity.THouseIdentityApplyHis;
import cn.tnar.parkservice.model.entity.THouseIdentityLook;
import cn.tnar.parkservice.model.response.BallotNameResponse;
import cn.tnar.parkservice.model.response.HouseParkInfoResponse;
import cn.tnar.parkservice.model.response.THouseIdentityApplyResponse;
import cn.tnar.parkservice.model.response.TSysRoleResponse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zzb
 * @since 2018-12-05
 */
@Mapper
@QuerySupport
@Repository
public interface THouseIdentityApplyMapper extends BaseMapper<THouseIdentityApply> {

    THouseIdentityApply selectByPrimaryKey(@Param("id") Integer id);

    int insertSelective(@Param("tHouseIdentityApply") THouseIdentityApply tHouseIdentityApply);

    THouseIdentityApplyResponse selectByCustId(@Param("custId") long custId);

    int updateByPrimaryKeySelective(@Param("tHouseIdentityApply") THouseIdentityApply tHouseIdentityApply);

    @Select("select cust_id, car_id,car_type from t_assets_carinfo where cust_id=#{custId} and car_type in (1,6)")
    List<CarInfoDto> selectCarIdByCustId(@Param("custId") long custId);

    @Select("select  cust_id, car_id,car_type from t_assets_carinfo where car_id=#{carId}")
    List<CarInfoDto> selectCarColor(@Param("carId") String carId);


    List<THouseIdentityApply> selectAll(Page<THouseIdentityApply> page, @Param("houseId") long houseId, @Param("applyResult") int applyResult);


    int countNum(@Param("houseId") long houseId, @Param("applyResult") int applyResult);

    @Select("SELECT park_code,house_name FROM `t_park_info` WHERE authenticate_flag =1 and delflag=1 ")
    List<HouseParkInfoResponse> queryHouseList();

    @Select("SELECT park_code,house_name FROM `t_park_info` WHERE authenticate_flag =1 and delflag=1 and park_code in (${parkCode})")
    List<HouseParkInfoResponse> queryHouseListByPower(@Param("parkCode") String parkCode);


    @Select("select r.id,  r.role_id,r.role_code,r.role_name,r.role_level ,i.name as custName,r.role_owner from t_sys_role r left join t_acct_cust_info i on r.role_owner =i.cust_id where r.stat=1 and r.role_level=#{level}")
    TSysRoleResponse selectSysRole(@Param("level") int level);

    @Insert("insert into t_house_identity_look (apply_id,look_id,look_level,look_name,look_state,look_remark,role_id,combine) values (#{look.applyId}," +
            "#{look.lookId},#{look.lookLevel},#{look.lookName},#{look.lookState},#{look.lookRemark},#{look.roleId},0)")
    int insertLook(@Param("look") THouseIdentityLook look);


    int insertApplyHis(@Param("tHouseIdentityApply") THouseIdentityApply tHouseIdentityApply);

    @Update("update t_house_identity_look set look_state=4,look_time=null,look_remark='' WHERE apply_id=#{applyId}")
    int updateLookAgain(@Param("applyId") int applyId);

    @Select("select id,apply_id, cust_id, house_id, house_name,house_num, cust_name, cust_phone, car_id, photo_key, apply_result," +
            "apply_reason, apply_time, result_time, del_time, delflag,modify_time,look_id,region_code,cheap_id,pay_mode from t_house_identity_apply_his where cust_id=#{custId}")
    List<THouseIdentityApplyHis> queryApplyHis(@Param("custId") long custId);


    BallotNameResponse queryBallotName(long custId, long houseId);

    @Select("select name from t_park_region where region_code=#{regionCode}")
    String selectRegionName(@Param("regionCode") String regionCode);


    @Select("select activity_time from t_house_cheap_info where id=#{cheapId} and delflag=0 ")
    String getActivtyTime(@Param("cheapId") int cheapId);
}
