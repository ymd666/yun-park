package cn.tnar.parkservice.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TAcctCustLoginInfoMapper {

    @Select("select trade_pwd from t_acct_cust_login_info where cust_id = #{custId}")
    String queryPassword (@Param("custId") Long custId);
}
