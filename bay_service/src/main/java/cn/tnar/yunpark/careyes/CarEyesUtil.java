package cn.tnar.yunpark.careyes;

/**
 * author : CaineZhu
 * date   : 2017/12/26 ${time}
 * desc   : class desc
 */
public class CarEyesUtil {

    public static final byte[] HEADER = new byte[]{(byte)0xFF,(byte)0XFE};
    //TCP
    public static final byte STATUS_TCP = 0x07;
    public static final byte HEARTBEAT_ROUTER_TCP = 0x09;
    public static final byte HEARTBEAT_DEVICE_TCP = 0x30;
    public static final byte TIME_CALIBRATION_TCP = 0x08;
    //UDP
    public static final byte STATUS = 0x72;
    public static final byte HEART_BEAT = 0x73;
    public static final byte TIME = 0x74;
    public static final byte STATUS_REPLY = (byte)0x92;
    public static final int STATUS_LEN = 0x28;


    public static String toLbmStatus(int status) {
        switch (status) {
            case 0:
                return "1";
            case 1:
                return "2";
            default:
                return null; // 未知状态不报
        }
    }

    public static int bcd2Int(byte tmp){
        int val;
        val = tmp / 0X10 * 0X0A;
        val = val + tmp % 0X10;
        return val;
    }

    public static int int2Bcd(int tmp){
        int val;
        val = tmp / 0X0A * 0X10 ;
        val = val + tmp % 0X0A;
        return val;
    }
}
