package cn.tnar.yunpark.huasai;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tieh on 2016/9/23.
 */
public class HuasaiAck {
    public static final int LEN = 24;

    private static final int ACK_MAGIC = 0x77;

    private static final int ACK_OK = 0x70;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    private int stopNo;

    private Date date;

    public HuasaiAck(int stopNo) {
        this.stopNo = stopNo;
        this.date = new Date();
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ByteBuf encode() {
        ByteBuf buf = Unpooled.buffer(LEN);
        buf.writeInt(ACK_MAGIC);
        buf.writeShort(ACK_OK);
        buf.writeShort(stopNo);
        buf.writeBytes(DATE_FORMAT.format(date).getBytes());

        buf.writeShort(CRC16.calc(buf));
        return buf;
    }
}
