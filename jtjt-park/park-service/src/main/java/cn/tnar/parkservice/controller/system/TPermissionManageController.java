package cn.tnar.parkservice.controller.system;

import cn.tnar.parkservice.config.Constant;
import cn.tnar.parkservice.model.dto.MenuDto;
import cn.tnar.parkservice.model.entity.TSysRole;
import cn.tnar.parkservice.model.entity.TSysRoleOrg;
import cn.tnar.parkservice.service.*;
import cn.tnar.parkservice.util.common.ResultCode;
import cn.tnar.parkservice.util.common.ResultJson;
import cn.tnar.pms.kesb.KesbException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: xinZhang
 * @Date: 2019/8/22
 * @Description:
 */
@Api(tags = "权限管理")
@RestController
@RequestMapping("service/permisson")
public class TPermissionManageController {

    private Logger log = LoggerFactory.getLogger(TPermissionManageController.class);

    @Autowired
    private KesbApiService kesbApiService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private TSysRoleOrgService tSysRoleOrgService;

    @Autowired
    private IUserService iUserService;


    @Autowired
    private ITSysMenuService itSysMenuService;


    @ApiOperation("新增角色")
    @PostMapping("/addRole")
    @Transactional
    public ResultJson addRole(@RequestBody Map<String, Object> param) {
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_ROLE_ADD, param);
            //修改t_sys_role该条记录role_type为岗亭角色
            Long id = Long.valueOf(result.get("id"));
            TSysRole tSysRole = new TSysRole();
            tSysRole.setRoleType(4);
            tSysRole.setId(id);
            sysRoleService.updateById(tSysRole);
            //查询机构角色
            HashMap<String, Object> map = new HashMap<>();
            map.put("g_custsession",param.get("g_custsession").toString());
            map.put("g_custid",param.get("g_custid"));
            map.put("org_id",param.get("org_id"));
            List<Map<String, String>> list = kesbApiService.queryKesbApiList(Constant.C_QUERY_ORG_ROLE, map);
            List<String> roleIdList = list.stream().map(p -> p.get("role_id")).collect(Collectors.toList());
            roleIdList.add(id.toString());
            //调用C接口 分配该角色到岗亭上
            map.put("role_id_list",roleIdList.toString().substring(1,roleIdList.toString().length()-1));
            kesbApiService.queryKesbApiList(Constant.C_DISTRIBUTE_ROLE,map);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("修改角色")
    @PostMapping("/updateRole")
    public ResultJson updateRole(@RequestBody Map<String, Object> param) {
        try {
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_ROLE_UPDATE, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("/删除角色")
    @PostMapping("/deleteRole")
    public ResultJson deleteRole(@RequestBody Map<String, Object> param) {
        try {
            //从t_sys_role_org 中删除已分配的角色关联
            QueryWrapper<TSysRoleOrg> qw = new QueryWrapper<>();
            qw.eq("role_id",Long.valueOf(param.get("id").toString()));
            tSysRoleOrgService.remove(qw);
            //C接口删除角色
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_ROLE_DELETE, param);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询所有角色信息")
    @PostMapping("/queryRole")
    public ResultJson queryRole(@RequestBody Map<String, Object> param) {
        return sysRoleService.querySysRoles(param);
    }

    @ApiOperation("查询所有菜单信息")
    @PostMapping("/queryPermission")
    public ResultJson queryPermission(@RequestBody Map<String, Object> param) {
        try {
            ResultJson resultJson = iUserService.informationTree();
            return resultJson;
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("查询已选中权限信息")
    @PostMapping("/queryChecked")
    public ResultJson queryChecked(@RequestBody Map<String, Object> param) {
        try {
            List<MenuDto> menu = itSysMenuService.selectMenu(param.get("cust_id").toString());
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(menu);
        } catch (KesbException e) {
            log.error(e.getMessage(), e);
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }

    @ApiOperation("角色添加权限")
    @PostMapping("/addPermission")
    public ResultJson addPermission(@RequestBody Map<String, Object> param) {
        try {
            log.info("查询收费员账号：C接口请求参数，param：======>"+param);
            Map<String, String> result = kesbApiService.queryKesbApi(Constant.C_ROLE_ADDPERMISSION, param);
            log.info("查询收费员账号：C接口返回参数，response：======>"+result);
            return new ResultJson().setCode(ResultCode.SUCCESS).setMsg(ResultCode.SUCCESS_MSG).setData(result);
        } catch (KesbException e) {
            log.error("查询收费员账号:C接口调用异常："+e.getMessage());
            return new ResultJson().setCode(ResultCode.FAIL).setMsg(e.getMessage()).setData(null);
        }
    }


    @ApiOperation("查询所有角色信息树形图")
    @PostMapping("/queryRoleTree")
    public ResultJson queryRoleTree(@RequestBody Map<String, Object> param) {
        return sysRoleService.querySysRolesTree(param);
    }

}
