package cn.tnar.parkservice.mapper;

import cn.tnar.parkservice.model.entity.TAcctBankMerchantInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @Author: 瞿露
 * @Date:2018-11-15
 *
 * */
@Repository
@Mapper
public interface TAcctBankMerchantInfoMapper {

    @Select("SELECT * FROM t_acct_bank_merchant_info WHERE agent_id = #{custId} limit 1 ")
    TAcctBankMerchantInfo selectBankInfo(@Param("custId") Long custId);
}
