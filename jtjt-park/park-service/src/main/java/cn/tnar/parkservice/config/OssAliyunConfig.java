package cn.tnar.parkservice.config;

import cn.tnar.parkservice.model.dto.OssAliyunField;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: 陈亮
 * @Date: 2018/9/26 15:02
 */
@Configuration
public class OssAliyunConfig {

    @Bean(value = "defaultOssAliyunField")
    @ConfigurationProperties("oss.aliyun")
    public OssAliyunField defaultOssAliyunField() {
        return new OssAliyunField();
    }


}
