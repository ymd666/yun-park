package cn.tnar.parkservice.model.response;

import lombok.Data;

import java.util.List;

@Data
public class VisitCardResponse {

    private long id;//卡组主键

    private long no;//卡组编号

    private String name;//访客卡名称

    private List<RegionInfo> region;

}

